(function() {
	var ua = navigator.userAgent,
		media = '',
		alternate = $('link[rel="alternate"][media="only screen and (max-width: 640px)"]').attr('href');

	// ユーザーエージェントがスマフォかPCかを判定
	if( ua.indexOf("iPhone") > 0 || ua.indexOf("iPod") > 0 || ua.indexOf("Android") > 0){
		media = 'sp';
	} else {
		media = 'pc';
	}

	// ユーザーエージェントがスマフォ、且つ、スマフォページ移動の確認で「キャンセル」を一度も選択していない場合に、リダイレクト
	if( media == 'sp' && document.cookie.indexOf('passed_ua_check_sp') < 0 ){
		if(window.confirm('スマートフォン用のページに移動しますか？')){
			location.href = alternate.replace('http://www.mitsubishi-motors.co.jp','');
		} else {
			document.cookie ='passed_ua_check_sp=true; path=/;';
		}
	}
	// ボタンを表示
	if( media == 'sp'){
		$('document').ready(function(){
			$('body').prepend('<div id="SmartphoneButton"><a href="' + alternate + '">スマートフォン用のページに移動する</a></div>');
		});
	}
})();