$(document).ready(function() {
	var wh = $(window).height()*0.7;
	
	$('.toolbox li').click(function() {
		
		if( $('.tool_content').is(':not(:animated)') ){
			if( $('.tool_content_inner > .cr').attr('id') == $(this).attr('class') ) {
				$('.tool_content').stop().fadeOut(300,function() {
					$('.tool_content_inner > .cr').hide();
					$('.tool_content_inner > .cr').removeClass('cr');
					
				});
			} else {
				$('.tool_content_bottom div').removeClass();
				$('.tool_content_bottom div').addClass($(this).attr('class'));
				if( $('.tool_content').css('display') == 'block' ) {
					$('.tool_content_inner > .cr').hide();
					$('#'+$(this).attr('class')).fadeIn(300);
					
				} else {
					$('#'+$(this).attr('class')).show();
					$('.tool_content').stop().fadeIn(300);
					
				}
				$('.tool_content_inner > div').removeClass('cr');
				$('#'+$(this).attr('class')).addClass('cr');
				
				
				var mh = $('#tool_menu').height();
				var sh = $('#tool_sitemap').height();
				if ( wh < mh) {
					$('#tool_menu').css({'height': wh+'px'});
					
				}
				if ( wh < sh) {
					$('#tool_sitemap').css({'height': wh+'px'});
				}
			}
		}
		
	});
	/* close*/
	$('.tool_close').click(function() {
		if( $('.tool_content').is(':not(:animated)') ){
			$('.tool_content').stop().fadeOut(300,function() {
				$('.tool_content_inner > .cr').hide();
				$('.tool_content_inner > .cr').removeClass('cr');
				
			});
		}
	});
	
	
	var fpause = $('.flex-pause');
	var fplay = $('.flex-play');
	
	$('.wrap_toolbox').skOuterClick(function() {
    	if( $('.tool_content').is(':not(:animated)') ){
			$('.tool_content').stop().fadeOut(300,function() {
				$('.tool_content_inner > .cr').hide();
				$('.tool_content_inner > .cr').removeClass('cr');
				
			});
		}
  	},fpause, fplay);
	
	
});