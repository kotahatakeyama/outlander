$(function(){

$('.flexslider').flexslider({
	animation: "slide",
	slideshow: true,
	controlNav: "thumbnails",
	slideshowSpeed: 5000,
	pauseOnAction: true,
	pauseOnHover: true,
	directionNav: false,
	start: function(slider){
		$('body').removeClass('loading');
	}
});

$('.flexslider_in').flexslider({
	selector: ".slides_in > li",
	animation: "slide",
	pauseOnHover: true,
	slideshow: false,
	slideshowSpeed: 4000,
	pauseOnAction: true,
	pauseOnHover: true,
	directionNav: true,
	controlNav: false,
	pausePlay: true,
	prevText: "",
	nextText: "",
	start: function(slider){
		$('body').removeClass('loading');		
	}
});

});

var atscroll = new ATScroll({
	noScroll : 'noSmoothScroll',
	setHash : false,
	duration : 800,
	interval : 10,
	animation : 'quinticOut'
});
atscroll.load();