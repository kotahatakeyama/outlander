var html = '<div class="box_sitemap clearfix">';
html += '<div class="box">';
html += '<h3><a href="/jp/investors/">株主・投資家の皆様へ</a></h3>';
html += '</div>';

html += '<div class="box">';
html += '<h3><a href="/jp/investors/corpmanage/">企業・経営情報</a></h3>';
html += '<ul>';
html += '<li><a href="/jp/investors/corpmanage/topmessage.html">トッップメッセージ</a></li>';
html += '<li><a href="/jp/investors/individual/mp.html">中期経営計画</a></li>';
html += '<li><a href="/jp/investors/corpmanage/guideline.html">重要情報の適時開示に関する指針</a></li>';
html += '<li><a href="/jp/investors/corpmanage/risk.html">事業等のリスク</a></li>';
html += '<li><a href="/jp/corporate/philosophy/">三綱領・企業理念</a></li>';
html += '<li><a href="/jp/corporate/aboutus/profile/">企業プロファイル</a></li>';
html += '</ul>';
html += '</div>';

html += '<div class="box">';
html += '<h3><a href="/jp/investors/governance/">コーポレート・ガバナンス</a></h3>';
html += '<ul>';
html += '<li><a href="/jp/investors/governance/policy.html">基本的な考え方と体制</a></li>';
html += '<li><a href="/jp/investors/governance/system.html">内部統制システム</a></li>';
html += '<li><a href="/jp/investors/governance/risk.html">リスク管理</a></li>';
html += '<li><a href="/jp/investors/governance/director.html">役員一覧</a></li>';
html += '<li><a href="/jp/investors/governance/compensation.html">役員報酬</a></li>';
html += '<li><a href="/jp/investors/governance/antisocial.html">反社会勢力への対応</a></li>';
html += '<li><a href="/jp/social/csr/pdf/governance_2015.pdf" target="_blank">コーポレート・ガバナンス報告書</a><img src="/jp/investors/images/icon_pdf.png" width="12" height="12" alt="PDFを別ウィンドウで表示します" /></li>';
html += '</ul>';
html += '</div>';

html += '<div class="box">';
html += '<h3><a href="/jp/investors/finance_result/">業績・財務情報</a></h3>';
html += '<ul>';
html += '<li><a href="/jp/investors/finance_result/highlight.html">当期の業績・実績報告</a></li>';
html += '<li><a href="/jp/investors/finance_result/indices.html">業績・財務データ</a></li>';
html += '<li><a href="/jp/investors/finance_result/data.html">財務諸表</a></li>';
html += '<li><a href="/jp/investors/finance_result/segment.html">地域別データ</a></li>';
html += '<li><a href="/jp/investors/finance_result/result.html">生産・販売・輸出実績</a></li>';
html += '</ul>';
html += '</div>';
html += '</div>';

html += '<div class="box_sitemap clearfix">';
html += '<div class="box">';
html += '<h3><a href="/jp/investors/library/">IRライブラリー</a></h3>';
html += '<ul>';
html += '<li><a href="/jp/investors/library/earning.html">決算短信・関連資料</a></li>';
html += '<li><a href="/jp/investors/library/anual.html">アニュアルレポート</a></li>';
html += '<li><a href="/jp/investors/library/fact.html">ファクトブック</a></li>';
html += '<li><a href="/jp/investors/library/yuka.html">有価証券報告書</a></li>';
html += '<li><a href="/jp/investors/library/tsushin.html">株主通信</a></li>';
html += '<li><a href="/jp/social/report/">CSRレポート</a></li>';
html += '</ul>';
html += '</div>';

html += '<div class="box">';
html += '<h3><a href="/jp/investors/event/">IRイベント</a></h3>';
html += '<ul>';
html += '<li><a href="/jp/investors/event/calendar.html">IRカレンダー</a></li>';
html += '<li><a href="/jp/investors/library/earning.html">決算説明会</a></li>';
html += '<li><a href="/jp/investors/event/plan.html">中期経営計画説明会</a></li>';
html += '<li><a href="/jp/investors/event/others.html">個別施策説明会</a></li>';
html += '<li><a href="/jp/investors/event/meeting.html">株主総会</a></li>';
html += '</ul>';
html += '</div>';

html += '<div class="box">';
html += '<h3><a href="/jp/investors/stockinfo/">株式・社債情報</a></h3>';
html += '<ul>';
html += '<li><a href="/jp/investors/stockinfo/stock_status.html">株式の状況</a></li>';
html += '<li><a href="/jp/investors/stockinfo/overview.html">株主構成</a></li>';
html += '<li><a href="/jp/investors/stockinfo/rating.html">格付け・社債情報</a></li>';
html += '<li><a href="/jp/investors/stockinfo/analyst.html">アナリストカバレッジ</a></li>';
html += '<li><a href="/jp/investors/stockinfo/procedure.html">株式手続きのご案内</a></li>';
html += '<li><a href="/jp/corporate/ir/stockinfo/koukoku.html">電子公告</a></li>';
html += '<li><a href="/jp/investors/stockinfo/return.html">株主還元</a></li>';
html += '<li><a href="/jp/investors/stockinfo/pdf/teikan.pdf" target="_blank">定款</a><img src="/jp/investors/images/icon_pdf.png" width="12" height="12" alt="PDFを別ウィンドウで表示します" /></li>';

html += '</ul>';
html += '</div>';

html += '<div class="box">';
html += '<h3><a href="/jp/investors/individual/">個人投資家の皆様へ</a></h3>';
html += '<ul>';
html += '<li><a href="/jp/investors/individual/mp.html">中期経営計画</a></li>';
html += '<li><a href="/jp/investors/individual/about.html">三菱自動車を知る</a></li>';
html += '<li><a href="/jp/investors/stockinfo/return.html">株主還元</a></li>';
html += '<li><a href="/jp/investors/individual/ad.html">広告・動画集</a></li>';
html += '<li><a href="/jp/investors/individual/relation.html">関連コンテンツ</a></li>';
html += '<li><a href="/jp/investors/individual/event.html">個人投資家向け説明会</a></li>';
html += '</ul>';
html += '</div>';
html += '</div>';

html += '<div class="box_sitemap clearfix">';
html += '<div class="box">';
html += '<h3><a href="/jp/investors/irnews/">IRニュース</a></h3>';
html += '</div>';

html += '<div class="box">';
html += '<h3><a href="/jp/investors/inquiry.html">IRに関するお問い合わせ</a></h3>';
html += '</div>';

html += '<div class="box">';
html += '<h3><a href="/jp/investors/faq.html">よくあるご質問</a></h3>';
html += '</div>';

html += '<div class="box">';
html += '<h3><a href="/jp/investors/exemption.html">IRサイト免責事項</a></h3>';
html += '</div>';
html += '</div>';

html += '<div class="box_sitemap clearfix">';
html += '<div class="box mt5">';
html += '<h3><a href="/jp/investors/rule.html">IRサイトの使い方</a></h3>';
html += '</div>';

html += '<div class="box mt5">';
html += '<h3><a href="/jp/investors/mail/">IRニュースメール</a></h3>';
html += '</div>';

html += '<div class="box mt5">';
html += '<h3><a href="//www.bloomberg.co.jp/apps/quote?T=jp09/quote.wm&ticker=7211:JP" target="_blank">株価情報<img src="/share/images/blank_icon_01.gif" alt="別ウィンドウで表示します" width="18" height="12" /></a></h3>';
html += '</div>';

html += '<div class="box mt5">';
html += '<h3><a href="/en/investors/">English IR</a></h3>';
html += '</div>';
html += '</div>';

document.write(html);