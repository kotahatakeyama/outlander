$(function() {
	var secTopArr = new Array();
	var current = -1;

	// 各sectionの位置を入れる
	$('.wrap_pager').each(function (i) {
		secTopArr[i] = $(this).offset().top;
	});

	// スクロールイベント
	$(window).scroll(function () {

		scrollHeight = $(document).height();
		scrollPosition = $(window).height() + $(window).scrollTop();

		for (var i = secTopArr.length-1; i>=0; i--) {
			if ($(window).scrollTop() > secTopArr[i] - 50) {
				chengeSection(i);
				break;
			} else if ((scrollHeight - scrollPosition) / scrollHeight <= 0.01){
				chengeSection(3);
				break;
			}
		}
	});

	function chengeSection(secNum) {
		if (secNum != current) {
			current = secNum;
			// 現在地による制御 //////////////////////////////////////////////////////
			if (current == 0) {
				$("#btn_more").fadeIn();
				$("#btn_more a").attr("href","#idx_ir");
				$("#btn_pager li a").removeClass("active");
				$("#btn_pager .item01 a").addClass("active");
			} else if (current == 1) {
				$("#btn_more").fadeIn();
				$("#btn_more a").attr("href","#idx_category");
				$("#btn_pager li a").removeClass("active");
				$("#btn_pager .item02 a").addClass("active");
			} else if (current == 2) {
				$("#btn_more").fadeIn();
				$("#btn_more a").attr("href","#idx_support");
				$("#btn_pager li a").removeClass("active");
				$("#btn_pager .item03 a").addClass("active");
			} else if (current == 3) {
				$("#btn_more").fadeOut();
				$("#btn_pager li a").removeClass("active");
				$("#btn_pager .item04 a").addClass("active");
			}
		}
	}
});

$(function() {
	$('#btn_pager a').each(function(){
		$(this).css("margin-left", "0")
	})
	$('#btn_pager a').hover(
		function(){
			$(this).stop().animate({
				'marginLeft':'-97px'
			},'fast');
		},
		function () {
			$(this).stop().animate({
				'marginLeft':'0'
			},'fast');
		}
	);
});