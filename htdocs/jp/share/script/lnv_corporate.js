
var _localload = (function(){
						   
var navi;
var langageCode = 'jp'
var categoryCode = 'corporate';

var path = langageCode +'/'+ categoryCode;

var navi = ['<div id="localNavigation"><div class="inner"><ul>'];
navi[navi.length] = '<li><a href="/'+path+'/index.html"><span>企業情報・投資家情報トップ</span></a></li>';
navi[navi.length] = '<li id="philosophy"><a href="/'+path+'/philosophy/index.html"><span>経営の考え方</span></a>';

	navi[navi.length] = '<ul>';
	navi[navi.length] = '<li id="philosophy_principle"><a href="/'+path+'/philosophy/principle.html"><span>三菱グループ三綱領</span></a></li>';
	navi[navi.length] = '<li id="philosophy_philosophy"><a href="/'+path+'/philosophy/philosophy.html"><span>企業理念</span></a></li>';
	navi[navi.length] = '<li><a href="/'+langageCode+'/spirit/communication_word/index.html"><span>Drive＠earth</span></a></li>';
	navi[navi.length] = '</ul></li>';

navi[navi.length] = '<li id="aboutus"><a href="/'+path+'/aboutus/index.html"><span>三菱自動車の概要</span></a>';

	navi[navi.length] = '<ul>';
	navi[navi.length] = '<li id="aboutus_profile"><a href="/'+path+'/aboutus/profile/index.html"><span>企業プロファイル</span></a>';
	
		navi[navi.length] = '<ul>';
		navi[navi.length] = '<li id="aboutus_profile_baselist"><a href="/'+path+'/aboutus/profile/world.html"><span>拠点一覧</span></a></li>';
		navi[navi.length] = '</ul></li>';
	
	navi[navi.length] = '<li id="aboutus_director"><a href="/'+langageCode+'/investors/governance/director.html"><span>役員一覧</span></a></li>';
	navi[navi.length] = '<li id="aboutus_facilities"><a href="/'+path+'/aboutus/facilities/index.html"><span>事業所・関連施設</span></a>';
	
		navi[navi.length] = '<ul>';
		navi[navi.length] = '<li id="aboutus_facilities_honsya"><a href="/'+path+'/aboutus/facilities/guide_map/honsya.html"><span>本社</span></a></li>';
		navi[navi.length] = '<li id="aboutus_facilities_tokyodesign"><a href="/'+path+'/aboutus/facilities/guide_map/tokyodesign.html"><span>東京デザインスタジオ</span></a></li>';
		navi[navi.length] = '<li id="aboutus_facilities_okazaki"><a href="/'+path+'/aboutus/facilities/guide_map/okazaki.html"><span>技術センター</span></a></li>';
		navi[navi.length] = '<li id="aboutus_facilities_okazaki_2"><a href="/'+path+'/aboutus/facilities/guide_map/okazaki_2.html"><span>EV技術センター</span></a></li>';
		navi[navi.length] = '<li id="aboutus_facilities_kyoto"><a href="/'+path+'/aboutus/facilities/guide_map/kyoto.html"><span>京都研究所</span></a></li>';
		navi[navi.length] = '<li id="aboutus_facilities_tokachi"><a href="/'+path+'/aboutus/facilities/guide_map/tokachi.html"><span>十勝研究所</span></a></li>';
		navi[navi.length] = '<li id="aboutus_facilities_nagoya"><a href="/'+path+'/aboutus/facilities/guide_map/nagoya.html"><span>名古屋製作所</span></a></li>';
		navi[navi.length] = '<li id="aboutus_facilities_mizushima"><a href="/'+path+'/aboutus/facilities/guide_map/mizushima.html"><span>水島製作所</span></a></li>';
		navi[navi.length] = '<li id="aboutus_facilities_gifu"><a href="/'+path+'/aboutus/facilities/guide_map/gifu.html"><span>パジェロ製造（株）</span></a></li>';
		navi[navi.length] = '<li id="aboutus_facilities_kyoto_factory"><a href="/'+path+'/aboutus/facilities/guide_map/kyoto_factory.html"><span>パワートレイン製作所 京都工場</span></a></li>';
		navi[navi.length] = '<li id="aboutus_facilities_shiga"><a href="/'+path+'/aboutus/facilities/guide_map/shiga.html"><span>パワートレイン製作所 滋賀工場</span></a></li>';
		navi[navi.length] = '</ul></li>';
		
	navi[navi.length] = '<li id="aboutus_subsidiary"><a href="/'+path+'/aboutus/subsidiary.html"><span>主要関連会社</span></a></li>';
	navi[navi.length] = '<li id="aboutus_history"><a href="/'+path+'/aboutus/history/1870/index.html"><span>沿革</span></a></li>';
	
	navi[navi.length] = '</ul></li>';


navi[navi.length] = '<li id="ir"><a href="/jp/investors/index.html"><span>株主・投資家の皆様へ</span></a>';

	navi[navi.length] = '<ul>';

	
	navi[navi.length] = '<li id="ir_corpmanage"><a href="/jp/investors/corpmanage/index.html"><span>企業・経営情報</span></a>';
	
		navi[navi.length] = '<ul>';
		navi[navi.length] = '<li id="ir_corpmanage_topmessage"><a href="/jp/investors/corpmanage/topmessage.html"><span>トップメッセージ</span></a></li>';
		navi[navi.length] = '<li id="ir_corpmanage_mp"><a href="/jp/investors/individual/mp.html"><span>中期経営計画 </span></a></li>';
		navi[navi.length] = '<li id="ir_corpmanage_guideline"><a href="/jp/investors/corpmanage/guideline.html"><span>重要情報の適時開示に関する指針</span></a></li>';
		navi[navi.length] = '<li id="ir_corpmanage_risk"><a href="/jp/investors/corpmanage/risk.html"><span>事業等のリスク</span></a></li>';
		navi[navi.length] = '<li><a href="/'+path+'/philosophy/index.html"><span>三綱領・企業理念</span></a></li>';
		navi[navi.length] = '<li><a href="/'+path+'/aboutus/profile/index.html"><span>企業プロファイル</span></a></li>';
		navi[navi.length] = '</ul></li>';
		

	navi[navi.length] = '<li id="ir_governance"><a href="/jp/investors/governance/index.html"><span>コーポレート・ガバナンス</span></a>';
	
		navi[navi.length] = '<ul>';
		navi[navi.length] = '<li id="ir_governance_policy"><a href="/jp/investors/governance/policy.html"><span>基本的な考え方と体制</span></a></li>';
		navi[navi.length] = '<li id="ir_governance_system"><a href="/jp/investors/governance/system.html"><span>内部統制システム</span></a></li>';
		navi[navi.length] = '<li id="ir_governance_risk"><a href="/jp/investors/governance/risk.html"><span>リスク管理</span></a></li>';
		navi[navi.length] = '<li id="ir_governance_director"><a href="/jp/investors/governance/director.html"><span>役員一覧</span></a></li>';
		navi[navi.length] = '<li id="ir_governance_compensation"><a href="/jp/investors/governance/compensation.html"><span>役員報酬</span></a></li>';
		navi[navi.length] = '<li id="ir_governance_antisocial"><a href="/jp/investors/governance/antisocial.html"><span>反社会勢力への対応</span></a></li>';
		navi[navi.length] = '<li><a href="/jp/social/csr/pdf/governance_2015.pdf" target="_blank"><span>コーポレート・ガバナンス報告書</span></a></li>';
		navi[navi.length] = '</ul></li>';

	
	navi[navi.length] = '<li id="ir_financial"><a href="/jp/investors/finance_result/index.html"><span>業績・財務情報</span></a>';
	
		navi[navi.length] = '<ul>';
		navi[navi.length] = '<li id="ir_financial_highlight"><a href="/jp/investors/finance_result/highlight.html"><span>当期の業績・実績報告</span></a></li>';
		navi[navi.length] = '<li id="ir_financial_indices"><a href="/jp/investors/finance_result/indices.html"><span>業績・財務データ</span></a></li>';
		navi[navi.length] = '<li id="ir_financial_data"><a href="/jp/investors/finance_result/data.html"><span>財務諸表</span></a></li>';
		navi[navi.length] = '<li id="ir_financial_segment"><a href="/jp/investors/finance_result/segment.html"><span>地域別データ</span></a></li>';
		navi[navi.length] = '<li id="ir_financial_result"><a href="/jp/investors/finance_result/result.html"><span>生産・販売・輸出実績</span></a></li>';
		navi[navi.length] = '</ul></li>';

	
	navi[navi.length] = '<li id="ir_library"><a href="/jp/investors/library/index.html"><span>IRライブラリー</span></a>';
	
		navi[navi.length] = '<ul>';
		navi[navi.length] = '<li id="ir_library_earning"><a href="/jp/investors/library/earning.html"><span>決算短信・関連資料</span></a></li>';
		navi[navi.length] = '<li id="ir_library_anual"><a href="/jp/investors/library/anual.html"><span>アニュアルレポート</span></a></li>';
		navi[navi.length] = '<li id="ir_library_fact"><a href="/jp/investors/library/fact.html"><span>ファクトブック</span></a></li>';
		navi[navi.length] = '<li id="ir_library_yuka"><a href="/jp/investors/library/yuka.html"><span>有価証券報告書</span></a></li>';
		navi[navi.length] = '<li id="ir_library_tsushin"><a href="/jp/investors/library/tsushin.html"><span>株主通信</span></a></li>';
		navi[navi.length] = '<li><a href="/jp/social/report/index.html"><span>CSRレポート</span></a></li>';
		navi[navi.length] = '</ul></li>';
		
		
	navi[navi.length] = '<li id="ir_event"><a href="/jp/investors/event/index.html"><span>IRイベント</span></a>';
	
		navi[navi.length] = '<ul>';
		navi[navi.length] = '<li id="ir_event_calendar"><a href="/jp/investors/event/calendar.html"><span>IRカレンダー</span></a></li>';
		navi[navi.length] = '<li id="ir_event_presentation"><a href="/jp/investors/library/earning.html"><span>決算説明会</span></a></li>';
		navi[navi.length] = '<li id="ir_event_plan"><a href="/jp/investors/event/plan.html"><span>中期経営計画説明会</span></a></li>';
		navi[navi.length] = '<li id="ir_event_others"><a href="/jp/investors/event/others.html"><span>個別施策説明会</span></a></li>';
		navi[navi.length] = '<li id="ir_event_meeting"><a href="/jp/investors/event/meeting.html"><span>株主総会</span></a></li>';
		navi[navi.length] = '</ul></li>';


	navi[navi.length] = '<li id="ir_stockinfo"><a href="/jp/investors/stockinfo/index.html"><span>株式・社債情報</span></a>';
	
		navi[navi.length] = '<ul>';
		navi[navi.length] = '<li id="ir_stockinfo_stock_status"><a href="/jp/investors/stockinfo/stock_status.html"><span>株式の状況</span></a></li>';
		navi[navi.length] = '<li id="ir_stockinfo_overview"><a href="/jp/investors/stockinfo/overview.html"><span>株主構成</span></a></li>';
		navi[navi.length] = '<li id="ir_stockinfo_rating"><a href="/jp/investors/stockinfo/rating.html"><span>格付け・社債情報</span></a></li>';
		navi[navi.length] = '<li id="ir_stockinfo_analyst"><a href="/jp/investors/stockinfo/analyst.html"><span>アナリストカバレッジ</span></a></li>';
		navi[navi.length] = '<li id="ir_stockinfo_procedure"><a href="/jp/investors/stockinfo/procedure.html"><span>株式手続きのご案内</span></a></li>';
		navi[navi.length] = '<li id="ir_stockinfo_koukoku"><a href="/jp/corporate/ir/stockinfo/koukoku.html"><span>電子公告</span></a></li>';
		navi[navi.length] = '<li id="ir_stockinfo_return"><a href="/jp/investors/stockinfo/return.html"><span>株主還元</span></a></li>';
		navi[navi.length] = '<li id="ir_stockinfo_teikan"><a href="/jp/investors/stockinfo/pdf/teikan.pdf" target="_blank"><span>定款</span></a></li>';
		navi[navi.length] = '</ul></li>';
		
		
	navi[navi.length] = '<li id="ir_individual"><a href="/jp/investors/individual/index.html"><span>個人投資家の皆様へ</span></a>';
	
		navi[navi.length] = '<ul>';
		navi[navi.length] = '<li id="ir_individual_mp"><a href="/jp/investors/individual/mp.html"><span>中期経営計画</span></a></li>';
		navi[navi.length] = '<li id="ir_individual_about"><a href="/jp/investors/individual/about.html"><span>三菱自動車を知る</span></a></li>';
		navi[navi.length] = '<li id="ir_individual_return"><a href="/jp/investors/stockinfo/return.html"><span>株主還元</span></a></li>';
		navi[navi.length] = '<li id="ir_individual_ad"><a href="/jp/investors/individual/ad.html"><span>広告・動画集</span></a></li>';
		navi[navi.length] = '<li id="ir_individual_relation"><a href="/jp/investors/individual/relation.html"><span>関連コンテンツ</span></a></li>';
		navi[navi.length] = '<li id="ir_individual_event"><a href="/jp/investors/individual/event.html"><span>個人投資家向け説明会</span></a></li>';
		navi[navi.length] = '</ul></li>';
	

	navi[navi.length] = '</ul></li>';

navi[navi.length] = '<li id="recruit"><a href="/'+path+'/recruit/index.html"><span>採用情報</span></a></li>';
navi[navi.length] = '<li id="pressrelease"><a href="/publish/pressrelease_jp/index.html"><span>プレスリリース</span></a></li>';


navi[navi.length] = '</ul>';
navi[navi.length] = '</div></div>';		

return navi.join('');
})();


function localNavi_write(){

document.write(_localload);

//DOMready.tgFunc(localnaviStay);
}


