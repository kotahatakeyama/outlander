
var _localload = (function(){
						   
var navi;
var langageCode = 'jp'
var categoryCode = 'social';

var path = langageCode +'/'+ categoryCode;

var navi = ['<div id="localNavigation"><div class="inner"><ul>'];
navi[navi.length] = '<li><a href="/'+path+'/index.html"><span>CSR・環境・社会貢献トップ</span></a></li>';
navi[navi.length] = '<li id="social_message"><a href="/'+path+'/top_message/"><span>トップメッセージ</span></a></li>';

	navi[navi.length] = '<li id="csr"><a href="/'+path+'/csr/index.html"><span>CSRへの取り組み</span></a>';
	
		navi[navi.length] = '<ul>';
		navi[navi.length] = '<li id="csr_csr_promotion"><a href="/'+path+'/csr/csr_promotion.html"><span>CSRの考え方</span></a></li>';
		navi[navi.length] = '<li id="csr_governance"><a href="/'+langageCode+'/investors/governance/index.html"><span>コーポレート・ガバナンス</span></a></li>';
		navi[navi.length] = '<li id="csr_system"><a href="/jp/investors/governance/system.html"><span>内部統制システム</span></a></li>';
		navi[navi.length] = '<li id="csr_risk"><a href="/jp/investors/governance/risk.html"><span>リスク管理</span></a></li>';
		navi[navi.length] = '<li id="csr_compliance"><a href="/'+path+'/csr/compliance.html"><span>コンプライアンス</span></a></li>';
		navi[navi.length] = '</ul></li>';
	

navi[navi.length] = '<li id="environment"><a href="/'+path+'/environment/index.html"><span>環境への取り組み</span></a>';

navi[navi.length] = '<ul>';

navi[navi.length] = '<li id="environment_policy"><a href="/'+path+'/environment/policy/index.html"><span>取り組み方針</span></a>';

	navi[navi.length] = '<ul>';
	navi[navi.length] = '<li id="environment_policy_index"><a href="/'+path+'/environment/policy/index.html"><span>環境担当役員メッセージ</span></a></li>';
	navi[navi.length] = '<li id="environment_policy_basicpolicy"><a href="/'+path+'/environment/policy/basicpolicy.html"><span>三菱自動車環境指針</span></a></li>';
	navi[navi.length] = '<li id="environment_policy_vision2020"><a href="/'+path+'/environment/policy/vision2020.html"><span>環境ビジョン2020</span></a></li>';
	navi[navi.length] = '<li id="environment_policy_plan2015"><a href="/'+path+'/environment/policy/plan2015.html"><span>環境行動計画2015</span></a></li>';
	navi[navi.length] = '</ul></li>';

navi[navi.length] = '<li id="environment_topics"><a href="/'+path+'/environment/topics/index.html"><span>環境トピックス</span></a>';

	navi[navi.length] = '<ul>';
	navi[navi.length] = '<li id="environment_topics_index"><a href="/'+path+'/environment/topics/index.html"><span>環境トピックス一覧</span></a>';
	navi[navi.length] = '</li>';
	navi[navi.length] = '</ul></li>';

navi[navi.length] = '<li id="environment_management"><a href="/'+path+'/environment/management/index.html"><span>環境マネジメント</span></a>';

	navi[navi.length] = '<ul>';
	navi[navi.length] = '<li id="environment_management_index"><a href="/'+path+'/environment/management/index.html"><span>環境マネジメントの体制</span></a></li>';
	navi[navi.length] = '<li id="environment_management_lca"><a href="/'+path+'/environment/management/lca.html"><span>LCA(ライフサイクルアセスメント)の取り組み</span></a></li>';
	navi[navi.length] = '<li id="environment_management_education"><a href="/'+path+'/environment/management/education.html"><span>環境教育</span></a></li>';
	navi[navi.length] = '</ul></li>';

navi[navi.length] = '<li id="environment_product"><a href="/'+path+'/environment/product/index.html"><span>商品・技術</span></a>';

	navi[navi.length] = '<ul>';
	navi[navi.length] = '<li id="environment_product_index"><a href="/'+path+'/environment/product/index.html"><span>走行時のCO<sub>2</sub>排出量低減</span></a></li>';
	navi[navi.length] = '<li id="environment_product_electric"><a href="/'+path+'/environment/product/electric.html"><span>電動化技術の開発</span></a>';
		navi[navi.length] = '<ul>';
		navi[navi.length] = '<li id="environment_product_electric_electric_ev"><a href="/'+path+'/environment/product/electric_ev.html"><span>電気自動車</span></a>';
		navi[navi.length] = '<li id="environment_product_electric_electric_phev"><a href="/'+path+'/environment/product/electric_phev.html"><span>プラグインハイブリッド車</span></a>';
		navi[navi.length] = '</ul></li>';
	navi[navi.length] = '<li id="environment_product_mileage"><a href="/'+path+'/environment/product/mileage.html"><span>燃費向上技術の開発</span></a>';
		navi[navi.length] = '<ul>';
		navi[navi.length] = '<li id="environment_product_mileage_mileage_engine"><a href="/'+path+'/environment/product/mileage_engine.html"><span>エンジンでの取り組み</span></a>';
		navi[navi.length] = '<li id="environment_product_mileage_mileage_frame"><a href="/'+path+'/environment/product/mileage_frame.html"><span>車体での取り組み</span></a>';
		navi[navi.length] = '</ul></li>';
	navi[navi.length] = '<li id="environment_product_emission"><a href="/'+path+'/environment/product/emission.html"><span>走行時の排出ガスのクリーン化</span></a></li>';
	navi[navi.length] = '<li id="environment_product_voc"><a href="/'+path+'/environment/product/voc.html"><span>車室内VOC削減</span></a></li>';
	navi[navi.length] = '<li id="environment_product_recycle"><a href="/'+path+'/environment/product/recycle.html"><span>リサイクルの取り組み</span></a>';
		navi[navi.length] = '<ul>';
		navi[navi.length] = '<li id="environment_product_recycle_recycle_development"><a href="/'+path+'/environment/product/recycle_development.html"><span>リサイクルに配慮した設計・開発</span></a>';
		navi[navi.length] = '</ul></li>';
	navi[navi.length] = '<li id="environment_product_environmental_burden"><a href="/'+path+'/environment/product/environmental_burden.html"><span>環境負荷物質の低減</span></a></li>';
	navi[navi.length] = '</ul></li>';

navi[navi.length] = '<li id="environment_activity"><a href="/'+path+'/environment/activity/index.html"><span>事業活動</span></a>';

	navi[navi.length] = '<ul>';
	navi[navi.length] = '<li id="environment_activity_index"><a href="/'+path+'/environment/activity/index.html"><span>生産での取り組み</span></a>';
		navi[navi.length] = '<ul>';
		navi[navi.length] = '<li id="environment_activity_index_factory_co2"><a href="/'+path+'/environment/activity/factory_co2.html"><span>CO<sub>2</sub>排出量低減</span></a>';
		navi[navi.length] = '<li id="environment_activity_index_factory_airpollution"><a href="/'+path+'/environment/activity/factory_airpollution.html"><span>大気汚染防止</span></a>';
		navi[navi.length] = '<li id="environment_activity_index_factory_prtr"><a href="/'+path+'/environment/activity/factory_prtr.html"><span>化学物質管理</span></a>';
		navi[navi.length] = '<li id="environment_activity_index_factory_soilpollution"><a href="/'+path+'/environment/activity/factory_soilpollution.html"><span>土壌汚染・水質汚濁の防止</span></a>';
		navi[navi.length] = '<li id="environment_activity_index_factory_resource"><a href="/'+path+'/environment/activity/factory_resource.html"><span>資源有効利用の促進</span></a>';
		navi[navi.length] = '</ul></li>';
	navi[navi.length] = '<li id="environment_activity_distribution"><a href="/'+path+'/environment/activity/distribution.html"><span>物流での取り組み</span></a></li>';
	navi[navi.length] = '<li id="environment_activity_supplier"><a href="/'+path+'/environment/activity/supplier.html"><span>購買お取引先との取り組み</span></a></li>';
	navi[navi.length] = '<li id="environment_activity_office"><a href="/'+path+'/environment/activity/office.html"><span>オフィス・販売店での取り組み</span></a></li>';
	navi[navi.length] = '</ul></li>';

navi[navi.length] = '<li id="environment_society"><a href="/'+path+'/environment/society/index.html"><span>社会との協働</span></a>';

	navi[navi.length] = '<ul>';
	navi[navi.length] = '<li id="environment_society_index"><a href="/'+path+'/environment/society/index.html"><span>生物多様性保全の取り組み</span></a></li>';
	navi[navi.length] = '<li id="environment_society_communication"><a href="/'+path+'/environment/society/communication.html"><span>環境コミュニケーション・地域との連携</span></a></li>';
	navi[navi.length] = '</ul></li>';

navi[navi.length] = '<li id="environment_report"><a href="/'+path+'/environment/report/index.html"><span>環境報告書</span></a>';

	navi[navi.length] = '<ul>';
	navi[navi.length] = '<li id="environment_report_index"><a href="/'+path+'/environment/report/index.html"><span>環境報告書</span></a></li>';
	navi[navi.length] = '</ul></li>';

navi[navi.length] = '<li id="environment_data"><a href="/'+path+'/environment/data/index.html"><span>環境データ集</span></a>';

	navi[navi.length] = '<ul>';
	navi[navi.length] = '<li id="environment_data_index"><a href="/'+path+'/environment/data/index.html"><span>製作所環境データ</span></a>';
		navi[navi.length] = '<ul>';
		navi[navi.length] = '<li id="environment_data_index_factory_archive"><a href="/'+path+'/environment/data/factory_archive.html"><span>過去の実績</span></a>';
		navi[navi.length] = '</ul></li>';
	navi[navi.length] = '<li id="environment_data_greenpurchasing"><a href="/'+path+'/environment/data/greenpurchasing.html"><span>グリーン購入法について</span></a></li>';
	navi[navi.length] = '</ul></li>';

navi[navi.length] = '<li id="environment_recyclelow"><a href="/'+path+'/environment/recyclelow/index.html"><span>自動車リサイクル</span></a>';

	navi[navi.length] = '<ul>';
	navi[navi.length] = '<li id="environment_recyclelow_index"><a href="/'+path+'/environment/recyclelow/index.html"><span>自動車リサイクル法への対応</span></a>';
		navi[navi.length] = '<ul>';
		navi[navi.length] = '<li id="environment_recyclelow_index_customer"><a href="/'+path+'/environment/recyclelow/customer.html"><span>お客様向けリサイクル料金</span></a>';
		navi[navi.length] = '<li id="environment_recyclelow_index_trader"><a href="/'+path+'/environment/recyclelow/trader.html"><span>関係事業者向けリサイクル料金</span></a>';
		navi[navi.length] = '<li id="environment_recyclelow_index_trader_place"><a href="/'+path+'/environment/recyclelow/trader_place.html"><span>関係事業者向け引取基準・指定引取場所</span></a>';
		navi[navi.length] = '<li id="environment_recyclelow_index_result"><a href="/'+path+'/environment/recyclelow/result.html"><span>自動車リサイクル法再資源化等の実績</span></a>';
		navi[navi.length] = '<li id="environment_recyclelow_index_battery"><a href="/'+path+'/environment/recyclelow/battery.html"><span>駆動用・アシストバッテリーのリサイクル</span></a>';
		navi[navi.length] = '</ul></li>';
	navi[navi.length] = '</ul></li>';
	
navi[navi.length] = '</ul></li>';



	navi[navi.length] = '<li id="approach"><a href="/'+path+'/approach/index.html"><span>社会への取り組み</span></a>';
	
		navi[navi.length] = '<ul>';
		navi[navi.length] = '<li id="approach_customer"><a href="/'+path+'/pdf/with_customers.pdf" target="_blank"><span>お客様満足への取り組み</span></a></li>';
		navi[navi.length] = '<li id="approach_partner"><a href="/'+path+'/pdf/with_suppliers.pdf" target="_blank"><span>ビジネスパートナーへの取り組み</span></a></li>';
		navi[navi.length] = '<li id="approach_stock"><a href="/'+path+'/pdf/with_investors.pdf" target="_blank"><span>株主・投資家への取り組み</span></a></li>';
		navi[navi.length] = '<li id="approach_region"><a href="/'+path+'/pdf/with_employee.pdf" target="_blank"><span>従業員への取り組み</span></a></li>';
		navi[navi.length] = '<li id="approach_staff"><a href="/'+path+'/pdf/with_local_community.pdf" target="_blank"><span>地域社会への取り組み</span></a></li>';
		navi[navi.length] = '</ul></li>';
	
	navi[navi.length] = '<li id="ethics_com"><a href="/'+path+'/ethics_com/index.html"><span>企業倫理委員会</span></a></li>';
	navi[navi.length] = '<li id="contribution"><a href="/'+path+'/contribution/index.html"><span>社会貢献活動</span></a>';
	
		navi[navi.length] = '<ul>';
		navi[navi.length] = '<li id="contribution_consult"><a href="/'+path+'/contribution/consult.html"><span>小学生自動車相談室</span></a></li>';
		navi[navi.length] = '<li id="contribution_program"><a href="/'+path+'/contribution/program.html"><span>三菱自動車体験授業プログラム</span></a></li>';
		navi[navi.length] = '<li id="contribution_visit"><a href="/'+path+'/contribution/visit.html"><span>企業訪問学習</span></a></li>';
		navi[navi.length] = '<li id="contribution_kidzania"><a href="/'+path+'/contribution/kidzania.html"><span>キッザニア</span></a></li>';
		navi[navi.length] = '<li id="contribution_kids"><a href="/jp/social/contribution/kids/index.html"><span>こどもクルマミュージアム</span></a></li>';
		navi[navi.length] = '<li id="contribution_kurumano_gakko"><a href="/'+path+'/contribution/kurumano_gakko.html"><span>クルマの学校</span></a></li>';
		navi[navi.length] = '<li id="contribution_pajero_forest"><a href="/'+path+'/contribution/pajero_forest.html"><span>パジェロの森</span></a></li>';
		navi[navi.length] = '<li id="contribution_factory"><a href="/'+path+'/contribution/factory.html"><span>工場見学</span></a></li>';
		navi[navi.length] = '<li id="contribution_others"><a href="/'+path+'/contribution/others.html"><span>その他の取り組み</span></a></li>';
		navi[navi.length] = '</ul></li>';
		
	navi[navi.length] = '<li id="report"><a href="/'+path+'/report/index.html"><span>CSRレポート</span></a></li>';
	
	navi[navi.length] = '</ul></li>';


navi[navi.length] = '</ul>';
navi[navi.length] = '</div></div>';		

return navi.join('');
})();


function localNavi_write(){

document.write(_localload);

//DOMready.tgFunc(localnaviStay);
}


