//var nv_root_path="http://www.mitsubishi-motors.coms";


var globalnvList = new Array();
var lang = '/jp';

/* 商品情報 */
globalnvList[0] = [["<a href='"+lang+"/products/index.html'><span>商品情報一覧</span></a>"]];

/* 三菱自動車のクルマづくり */
globalnvList[1] = [
				["<a href='"+lang+"/spirit/communication_word/index.html'><span>企業コミュニケーションワード / Drive@earth</span></a>"],
				["<a href='"+lang+"/spirit/earth_technology/index.html'><span>クルマづくりへの想い / @earth TECHNOLOGY</span></a>"],
				["<a href='"+lang+"/products/'><span>商品情報一覧</span></a>"],
				["<a href='"+lang+"/spirit/technology/library/index.html'><span>クルマの技術</span></a>"],
				["<a href='"+lang+"/spirit/design/index.html' target='_blank'><span>デザイン</span></a>"],
				["<a href='"+lang+"/spirit/history/index.html'><span>三菱車の歴史</span></a>"]
				];

/* CSR・環境・社会 */
globalnvList[2] = [
				["<a href='"+lang+"/social/top_message/'><span>トップメッセージ</span></a>"],
				["<a href='"+lang+"/social/csr/index.html'><span>CSRへの取り組み</span></a>"],
				["<a href='"+lang+"/social/environment/index.html'><span>環境への取り組み</span></a>"],
				["<a href='"+lang+"/social/approach/index.html'><span>社会への取り組み</span></a>"],
				["<a href='"+lang+"/social/ethics_com/index.html'><span>企業倫理委員会</span></a>"],
				["<a href='"+lang+"/social/contribution/index.html'><span>社会貢献活動</span></a>"]
				];


/* 企業情報・投資家情報 */
globalnvList[3] = [
				["<a href='"+lang+"/corporate/philosophy/index.html'><span>三綱領・企業理念</span></a>"],
				["<a href='"+lang+"/corporate/aboutus/index.html'><span>三菱自動車の概要</span></a>"],
				["<a href='"+lang+"/investors/'><span>株主・投資家の皆様へ</span></a>"],
				["<a href='"+lang+"/corporate/recruit/index.html'><span>採用情報</span></a>"],
				["<a href='"+lang+"/corporate/ir/stockinfo/koukoku.html'><span>電子公告</span></a>"]
				];

/* 知る・楽しむ */
globalnvList[4] = [
				["<a href='/publish/pressrelease_jp/index.html'><span>プレスリリース</span></a>"],
				["<a href='"+lang+"/events/motorshow/index.html'><span>モーターショー</span></a>"],
				["<a href='http://www.mitsubishi-motors.co.jp/motorsports/index.html' target='_blank'><span>モータースポーツ</span></a>"],
				["<a href='"+lang+"/events/baja/2015/index.html' target='_blank'><span>バハ・ポルタレグレ500</span></a>"],
				["<a href='"+lang+"/events/ppihc/2014/index.html' target='_blank'><span>パイクスピークチャレンジ</span></a>"],
				["<a href='"+lang+"/events/star/index.html' target='_blank'><span>見上げるプロジェクト</span></a>"],
				["<a href='"+lang+"/events/designchallenge/index.html'><span>社内デザインコンテスト</span></a>"]
];

var firston = true;
var timerb;
var time_sec = 1;
var timestop = false;
var previd = '';
var gnvpa = '';

function floatmenu(){
	var doc = document;
	var basediv = doc.createElement('div');
	basediv.id = "flbase";
	
	var innerdiv = doc.createElement('div');
	innerdiv.id = "flinnerdiv";
	
	var nvimg = module.$('mainnv').getElementsByTagName('img');
	
	for(var i=0;i<nvimg.length;i++){
		
		addListener(nvimg[i], 'mouseover', bkchg);
		addListener(nvimg[i], 'mouseout', bkchg2);
		
		var divbk = doc.createElement('div');
		divbk.id= nvimg[i].id + '_fl';
	
		var ul = doc.createElement('ul');
		ul.className = 'linkStd';
		
		for(var j=0;j<globalnvList[i].length;j++){
			var li = doc.createElement('li');
			li.innerHTML = globalnvList[i][j];
			ul.appendChild(li);
		}
		
		divbk.appendChild(ul);
		innerdiv.appendChild(divbk);
		basediv.appendChild(innerdiv);
		
		//addListener(ul, 'mouseover', bkchgb_ul);
		addListener(divbk, 'mouseover', bkchgb);
		addListener(divbk, 'mouseout', bkchgb2);
	}
	module.$('globalNavigation').appendChild(basediv);
	
	var domElm = innerdiv.getElementsByTagName('div')
		gnvpa = domElm;

	var ddlng = domElm.length;
	var zz = 0;
	for(var k=0;k<ddlng;++k){
		if(zz <= domElm[k].offsetHeight){
		zz = domElm[k].offsetHeight;
		}
	}
	
	for(var k=0;k<ddlng;++k){
			var ddst = domElm[k].style;
			if(ie && !IE_newer) ddst.height = zz-10 + 'px';
			else ddst.minHeight = (zz +1)-10 + 'px';
	}
	
	if(module.Browser('IE',6)) DD_belatedPNG.fix('#flbase div');

	//basediv.style.display = 'none';
	//innerdiv.style.visibility = "visible";
	innerdiv.style.visibility = 'hidden';
	innerdiv.style.overflow = 'hidden';
	innerdiv.style.height = '1px';
}

function bkchg(e){
	module.Event.stopbubble(e);
	//module.$('flbase').style.display = 'block';
	module.$('flinnerdiv').style.visibility = 'visible';
	module.$('flinnerdiv').style.overflow = 'auto';
	module.$('flinnerdiv').style.height = 'auto';
	
	setTimeout(function(){
	var ddlng = gnvpa.length;
	var zz = 0;
	for(var k=0;k<ddlng;++k){
		if(zz <= gnvpa[k].offsetHeight){
		zz = gnvpa[k].offsetHeight;
		}
	}
		
	for(var k=0;k<ddlng;++k){
			var ddst = gnvpa[k].style;
			if(ie && !IE_newer) ddst.height = zz-10 + 'px';
			else ddst.minHeight = (zz)-10 + 'px';
	}
						},30);

	if(previd != ''){
		if(previd.indexOf('_fl') != -1) module.$(previd).style.backgroundPosition = '-203px 0';
		else module.$(previd+'_fl').style.backgroundPosition = '-203px 0';
	}
	timestop = true;
	clearInterval(timerb);
	if(this.id != 'gnv_home') module.$(this.id+'_fl').style.backgroundPosition = '0 0';
	previd=this.id;
}
function bkchg2(e){
	module.Event.stopbubble(e);
	settime();
	//module.$(this.id+'_fl').style.backgroundPosition = '0 0';
}
function bkchgb(e){
	if(previd != '') module.$(previd).style.backgroundPosition = '-203px 0';
	module.Event.stopbubble(e);
	timestop = true;
	clearInterval(timerb);
	if(this.id != 'gnv_home_fl') this.style.backgroundPosition = '0 0';
	previd=this.id;
}
function bkchgb2(e){
	module.Event.stopbubble(e);
	settime();
	//this.style.backgroundPosition = '0 0';
}

function bkchgb_ul(e){
	module.Event.stopbubble(e);
	timestop = true;
	clearInterval(timerb);
	this.parentNode.style.backgroundPosition = '-203px 0';
}


function outcheck(){
	time_sec --;
	if(time_sec <=0){
		clearInterval(timerb);
		if(!timestop){
		lyclear();
		}
	}
}

function settime(){
	if(!timestop) return;
	time_sec = 1;
	timestop = false;
	timerb = setInterval(outcheck,500);	
}

function lyclear(){
	
	var div = module.$('flinnerdiv').getElementsByTagName('div');
	
	for(var i=0;i<div.length;i++){
		div[i].style.backgroundPosition = '-203px 0';
		if(ie && !IE_newer) div[i].style.height = 'auto';
		else div[i].style.minHeight = '0';
		
	}
	module.$('flinnerdiv').style.visibility = 'hidden';
	module.$('flinnerdiv').style.overflow = 'hidden';
	module.$('flinnerdiv').style.height = '1px';
	//module.$('flbase').style.display = 'none';
}
addListener(window, 'load', floatmenu);
//module.DOMready.tgFunc(floatmenu);


