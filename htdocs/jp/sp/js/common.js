// =========================================================
//
//	common.js
//	全ページ共通で利用するjavascriptを記載
//
// =========================================================

// ===============================================
//
//	グローバル変数
// -----------------------------------------------
var mmc = {},
    sp_path = '/jp/sp/';

// ===============================================
//
//	初期化
// -----------------------------------------------
mmc.window = {
	elm : $(window),
	width : 0,
	height : 0,
	size_get : {}
};
mmc.head = {
	title : '',
	pathname : location.pathname,
	alternate : $('link[rel="canonical"]').attr('href')
};
mmc.body = {
	elm : {},
	page : {},
	contents : {}
};
mmc.overlay = {
	opened : false,
	opened_name : '',
	moving : false,
	close : null
};
mmc.lightbox = {
	opened : false
};
mmc.ua = {
	base : navigator.userAgent,
	os : ''
};
if( mmc.ua.base.indexOf('dcap') > -1 ){
	mmc.ua.os = 'inside_search';
} else if(mmc.ua.base.indexOf("iPhone") > -1 || mmc.ua.base.indexOf("iPod") > -1){
	mmc.ua.os = 'ios';
	$.cookie('passed_ua_check_sp', true, { path : '/' });
} else if(mmc.ua.base.indexOf("Android") > -1) {
	if(mmc.ua.base.indexOf("Android 2") > -1){
		mmc.ua.os = 'old_android';
	} else {
		mmc.ua.os = 'android';
	}
	$.cookie('passed_ua_check_sp', true, { path : '/' });
} else {
	mmc.ua.os = 'others';
}

//	ユーザーエージェント判別とリダイレクト
// -------------------------------------
if( mmc.ua.os == 'others' && $.cookie('passed_ua_check_pc') == undefined ){
	if(window.confirm('PC用のページに移動しますか？')){
		location.href = mmc.head.alternate;
	} else {
		$.cookie('passed_ua_check_pc', true, { path : '/' });
	}
}

// ===============================================
//
//	ウインドウのサイズを取得
// -----------------------------------------------
mmc.window.size_get = function(){
	mmc.window.width = Math.floor(mmc.window.elm.width());
	mmc.window.height = Math.floor(mmc.window.elm.height());
	//console.log(mmc.window.width, mmc.window.height);
};

// ===============================================
//
//	ブラウザ判別用スクリプト
// -----------------------------------------------
;(function($){
	
	if(!$.browser)$.browser = {};
	$.browser.ua = navigator.userAgent.toLowerCase();
	$.browser.android = /android/.test($.browser.ua);
	$.browser.iphone = /iphone/.test($.browser.ua);
	$.browser.ipod = /ipod/.test($.browser.ua);
	$.browser.ipad = /ipad/.test($.browser.ua);
	$.browser.ios = /iphone|ipod|ipad/.test($.browser.ua);
	if($.browser.android){
		$.browser.tablet = !/mobile/.test($.browser.ua);
	}else{
		$.browser.tablet = /ipad/.test($.browser.ua);
	}
	if(!$.browser.version)$.browser.version = {};
	if($.browser.android){
		$.browser.version = parseFloat($.browser.ua.slice($.browser.ua.indexOf("android")+8))
	}else if($.browser.ios){
		$.browser.version = parseFloat($.browser.ua.slice($.browser.ua.indexOf("os ")+3,$.browser.ua.indexOf("os ")+6).replace("_","."))
	}
	
})(jQuery);

// ===============================================
//
//	CSS3アニメーション用プラグイン
// -----------------------------------------------
$.fn.animate3 = function(prop,speed,easing,callback){
	var self = this;
	if (jQuery.isEmptyObject(prop)) {
		return self;
	}
	if(speed && !isNaN(speed)){
		speed = speed+"ms";
	}
	var def = $.Deferred();

	callback = callback? callback:function(){};
	self
		.css("-webkit-transition","all "+speed+" "+easing)
		.unbind("webkitTransitionEnd")
		.one("webkitTransitionEnd",function(){
			self.css("-webkit-transition","");
			setTimeout(function(){
				def.resolveWith(self); 
			},1);
			callback.apply(self);
		}).css(prop);

	return def.promise(self);
}

// ===============================================
//
//	スライドショー用プラグイン 横スライド
// -----------------------------------------------
$.fn.flipSlide = function(op){
	op = $.extend({
		viewport : ".image",
		inner : ".inner",
		slide : ".item",
		paging : ".paging ul",
		paging_class:"current",
		transitionFlag:true,
		next:false,
		prev:false
	},op);

	return this.each(function(){
		var $this = $(this);
		({
			//initialize
			init : function(){
				//get  slide length
				this.slideLength = $this.find(op.slide).length;
				if(this.slideLength==1){
					$this.find(op.next).remove();
					$this.find(op.prev).remove();
					$this.addClass('only')
					return false;
				}else if(this.slideLength==2){
					$this.find(op.slide).each(function(){
						$this.find(op.inner).append($(this).clone());
					});
				}
				setTimeout(function(){
					$this.find(op.slide).addClass("loaded");
				},100);

				//set slide width
				this.setWidth();

				this.setPrevImage();
				$this.find(op.slide).find("a").attr({
					"data-href" : function(){
						return $(this).attr("href");
					},
					"draggable":"false",
					"href" : "javascript:void(0)"
				})
				$this.find(op.slide).find("img").attr("draggable","false");
				this.setPaging();
				this.setEvent();
			},
			setPrevImage : function(){
				$this.find(op.slide).last().prependTo($this.find(op.inner));

			},
			setWidth :function(){
				var width = $this.find(op.viewport).innerWidth();
				//set slides width
				$this.find(op.inner).width(this.slideLength==2?width*this.slideLength*2:width*this.slideLength);
				//set slide width
				$this.find(op.slide).width(width);
				//default position
				this.defaultLeft = width;
				$this.find(op.inner).css("transform","translate3d(-"+this.defaultLeft+"px,0,0)")

			},
			setPaging : function(){
				var paging = $this.find(op.paging),html="";
				for(var i = 0 ; i<this.slideLength ; i++){
					html += "<li>"+(i+1)+"</li>";
				}
				paging.html(html);
				this.setPagingNumber();
			},
			page : 1,
			setPagingNumber : function(){
				$this.find(op.paging).children().removeClass(op.paging_class).eq(this.page-1).addClass(op.paging_class);	
			},
			setEvent : function(){
				//bind resize event
				$(window).resize($.proxy(this.resize,this));
				$this.find(op.inner)
					.on("touchstart",$.proxy(this.touchstart,this))
					.on("touchmove",$.proxy(this.touchmove,this))
					.on("touchend",$.proxy(this.touchend,this));

				var self = this;

				if(op.next){
					$this.find(op.next).click(function(){
						self.startX = 0;
						self.endX = 0;
						self.next();
					});
				}

				if(op.prev){
					$this.find(op.prev).click(function(){
						self.startX = 0;
						self.endX = 0;
						self.prev();
					});
				}
			},
			touchStartFlag : false,
			touchEndFlag : false,
			touchstart : function(e){
				if(this.touchStartFlag)return false;
				if(!e.pageX)e = event.touches[0];
				this.startX = e.pageX;
				this.startY = e.pageY;
				this.endX = this.startX;
				this.endY = this.startY;
				this.swipeFlag = false;
				this.scrollFlag = false;
				this.touchStartFlag = true;
				this.touchEndFlag = false;
				this.startScrollTop = $(window).scrollTop()-(window.outerHeight-window.innerHeight);

			},
			touchmove : function(e){
				if(!this.touchStartFlag || this.touchEndFlag)return false;
				if(!e.pageX)e = event.touches[0];
				this.endX = e.pageX;
				this.endY = e.pageY;
				if(!this.swipeFlag && !this.scrollFlag){
					if(Math.abs(this.startX-this.endX)<Math.abs(this.startY-this.endY)){
						this.scrollFlag = true;
					}else{
						event.preventDefault();
						this.swipeFlag = true;
					}
				}
				if(this.swipeFlag){
					$this.find(op.inner).css("transform","translate3d(-"+(this.defaultLeft+(this.startX-this.endX))+"px,0,0)");
				}
			},
			touchend : function(e){
				if(!this.touchStartFlag || this.touchEndFlag)return false;
				this.touchEndFlag = true
				var self = this;
				if(this.scrollFlag){
					self.flagRefresh();
				}else if(this.swipeFlag && this.startX-this.endX>20){
					this.next();
				}else if(this.swipeFlag && this.startX-this.endX<-20){
					this.prev();
				}else{
					setTimeout(function(){

						self.endScrollTop = $(window).scrollTop()-(window.outerHeight-window.innerHeight);
						if(Math.abs(self.startScrollTop-self.endScrollTop) < 20 &&
							 Math.abs(self.startY-self.endY) < 20 ){
							if($(e.target).closest("a").length>0){
								self.flagRefresh();
								location.href = $(e.target).closest("a").data("href");
							}else if($(e.target).filter("a").length>0){
								self.flagRefresh();
								location.href = $(e.target).filter("a").data("href");
							}else{
								console.log(1)
								self.defaults();
							}
						}else{
							self.defaults();
						}
					},10);
				}
			},
			next : function(){
				if(this.animateFlag)return false;
				this.animateFlag = true;
				var self = this;
				if(op.transitionFlag && !($.browser.android && $.browser.version<4)){
					var target = $this.find(op.slide).eq(1);
					target.animate3({
						"transform":"translate3d("+(this.defaultLeft-(this.startX-this.endX))+"px,0,0)"
					},400,"linear").find("a,span").animate3({
						"transform":"rotateY(90deg)",
						"opacity":0
					},550,"linear",function(){
						target.css({
							"transform":"translate3d(0,0,0)"
						}).find("a,span").css({
							"transform":"rotateY(0)",
							"opacity":1
						});
					});
				}
				self.page++;
				if(self.page>self.slideLength)self.page=1;
				self.setPagingNumber();

				$this.find(op.inner).animate3({
					"transform":"translate3d(-"+(this.defaultLeft*2)+"px,0,0)"
				},300,"ease-out",function(){
					setTimeout(function(){
						$this.find(op.slide).first().appendTo($this.find(op.inner));
						$this.find(op.inner).css("transform","translate3d(-"+self.defaultLeft+"px,0,0)");
						self.flagRefresh();
					},100);
				});
			},
			prev : function(){
				if(this.animateFlag)return false;
				this.animateFlag = true;
				var self = this;
				if(op.transitionFlag && !($.browser.android && $.browser.version<4)){
					var target = $this.find(op.slide).eq(1).addClass("prev");

					setTimeout(function(){
						target.animate3({
							"transform":"translate3d("+(-self.defaultLeft-(self.startX-self.endX))+"px,0,0)"
						},400,"linear").find("a,span").animate3({
							"transform":"rotateY(-90deg)",
							"opacity":0
						},550,"linear",function(){
							target.css({
								"transform":"translate3d(0,0,0)"
							}).removeClass("prev").find("a,span").css({
								"transform":"rotateY(0)",
								"opacity":1
							});
						});
					},10);
				}

				self.page--;
				if(self.page==0)self.page=self.slideLength;
				self.setPagingNumber();

				$this.find(op.inner).animate3({
					"transform":"translate3d(0,0,0)"
				},300,"ease-out",function(){
					setTimeout(function(){
						$this.find(op.slide).last().prependTo($this.find(op.inner));
						$this.find(op.inner).css("transform","translate3d(-"+self.defaultLeft+"px,0,0)");
						self.flagRefresh();
					},100)
				});
			},
			defaults : function(){
				var self = this;
				$this.find(op.inner).animate3({
					"transform":"translate3d(-"+self.defaultLeft+"px,0,1px)"
				},100,"ease",function(){
					$this.find(op.inner).css("transform","translate3d(-"+self.defaultLeft+"px,0,0)")
					self.flagRefresh();
				});
			},
			flagRefresh : function(){
				this.swipeFlag = false;
				this.scrollFlag = false;
				this.touchStartFlag = false;
				this.touchEndFlag = false;
				this.animateFlag = false;
			},
			resize :function(){
				if(!this.swipeFlag){
					this.setWidth();
				}
			}


		}).init();

	});
}
// ===============================================
//
//	スライドショー用プラグイン サムネイル
// -----------------------------------------------
$.fn.fadeSlide = function(op){
	var op = $.extend({
		img : ".image img",
		thumbnailWrap : ".thumbnail_list",
		thumbnails : ".thumbnail_list ul",
		thumbnail : ".thumbnail_list ul li"
	},op);

	return this.each(function(){
		var $this = $(this);
		({
			//initialize
			init : function(){
				this.setSize();
				$this.find(op.thumbnails).find("a").each(function(){
					$("<img>").attr("src",$(this).attr("href"));
				}).attr({
					"data-href" : function(){
						return $(this).attr("href");
					},
					"draggable":"false",
					"href" : "javascript:void(0)"
				})
				$this.find(op.thumbnails).find("img").attr("draggable","false");
				this.setEvent();
			},
			setSize : function(){
				this.thumbnailsLength = Math.ceil($this.find(op.thumbnail).length/2);
				this.thumbnailOuterWidth = $this.find(op.thumbnail).outerWidth(true);
				this.thumbnailsWidth = this.thumbnailsLength*this.thumbnailOuterWidth;
				$this.find(op.thumbnails).width(this.thumbnailsWidth);
				this.thumbnailsOuterWidth = $this.find(op.thumbnails).outerWidth();
			},
			setEvent : function(){
				$(window).resize($.proxy(this.resize,this));
				$this.find(op.thumbnails)
					.on("touchstart",$.proxy(this.touchstart,this))
					.on("touchmove",$.proxy(this.touchmove,this))
					.on("touchend",$.proxy(this.touchend,this));
			},
			touchStartFlag : false,
			touchEndFlag : false,
			touchstart : function(e){
				if(this.touchStartFlag)return false;
				if(!e.pageX)e = event.touches[0];
				this.startX = e.pageX;
				this.startY = e.pageY;
				this.endX = this.startX;
				this.endY = this.startY;
				this.swipeFlag = false;
				this.scrollFlag = false;
				this.touchStartFlag = true;
				this.touchEndFlag = false;
				this.startScrollTop = $(window).scrollTop()-(window.outerHeight-window.innerHeight);
				if(!this.defaultLeft)this.defaultLeft=0;
			},
			touchmove : function(e){
				if(!this.touchStartFlag || this.touchEndFlag)return false;
				if(!e.pageX)e = event.touches[0];
				this.endX = e.pageX;
				this.endY = e.pageY;
				if(!this.swipeFlag && !this.scrollFlag){
					if(Math.abs(this.startX-this.endX)<Math.abs(this.startY-this.endY)){
						this.scrollFlag = true;
					}else{
						event.preventDefault();
						this.swipeFlag = true;
					}
				}
				if(this.swipeFlag && this.thumbnailsOuterWidth>$this.find(op.thumbnailWrap).width()){
					$this.find(op.thumbnails).css("transform","translate3d("+(this.defaultLeft+(this.startX-this.endX)*-1)+"px,0,0)");
				}
			},
			touchend : function(e){
				if(!this.touchStartFlag || this.touchEndFlag)return false;
				this.touchEndFlag = true
				var self = this;
				if(this.swipeFlag){
					this.defaultLeft = this.defaultLeft+(this.startX-this.endX)*-1
				}

				this.endScrollTop = $(window).scrollTop()-(window.outerHeight-window.innerHeight);
				var rightFit = ((this.thumbnailsOuterWidth-$this.find(op.thumbnailWrap).outerWidth(true))*-1);

				//console.log(this.thumbnailsOuterWidth,$this.find(op.thumbnailWrap).width())
				if(Math.abs(this.startX-this.endX)<10 && Math.abs(this.startScrollTop-this.endScrollTop) < 10){
					if($(e.target).closest("a").length>0){
						var target = $(e.target).closest("a");
					}else if($(e.target).filter("a").length>0){
						var target = $(e.target).filter("a");
					}
					if(target && target.length==1){
						$this.find(op.thumbnails).find('a.current').removeClass('current');
						target.addClass('current');
						$this.find(op.img).before("<img src='"+target.data("href")+"' alt='"+target.attr("title")+"'>");
						$this.find(op.img).last().animate({
							"opacity":0
						},400,"linear",function(){
							$(this).remove();
							self.flagRefresh();
						});
					}else{
						self.flagRefresh();
					}

				// }else if(this.defaultLeft < rightFit && this.thumbnailsOuterWidth>$this.find(op.thumbnailWrap).width()){
					
				// 	$this.find(op.thumbnails).animate3({
				// 		"transform":"translate3d("+rightFit+"px,0,0)"
				// 	},200,"linear",function(){
				// 		self.defaultLeft = rightFit;
				// 	});
				}else if(this.swipeFlag && this.defaultLeft > 0 && this.thumbnailsOuterWidth>$this.find(op.thumbnailWrap).width()){
					$this.find(op.thumbnails).animate3({
						"transform":"translate3d(0,0,0)"
					},200,"linear",function(){
						self.defaultLeft = 0;
						self.flagRefresh();
					});
				}else if(this.swipeFlag && this.thumbnailsOuterWidth>$this.find(op.thumbnailWrap).width()){

					var maxLength = Math.floor(($this.find(op.thumbnailWrap).width()-20)/this.thumbnailOuterWidth);
					console.log(maxLength,$this.find(op.thumbnailWrap).width(),this.thumbnailOuterWidth)
					if(this.startX-this.endX>0){
						var targetLength = Math.ceil(-1*this.defaultLeft/this.thumbnailOuterWidth);
					}else{
						var targetLength = Math.floor(-1*this.defaultLeft/this.thumbnailOuterWidth);
					}

					if(this.thumbnailsLength-targetLength<maxLength)targetLength = this.thumbnailsLength-maxLength;
					var targetFit = targetLength*-1*this.thumbnailOuterWidth;

					//if(targetFit<rightFit)targetFit = rightFit;
					$this.find(op.thumbnails).animate3({
						"transform":"translate3d("+targetFit +"px,0,0)"
					},200,"linear",function(){
						self.defaultLeft = targetFit ;
						self.flagRefresh();
					});
				}else{
					self.flagRefresh();
				}
				
			},
			flagRefresh : function(){
				this.swipeFlag = false;
				this.scrollFlag = false;
				this.touchStartFlag = false;
				this.touchEndFlag = false;
			},
			resize : function(){
				$this.find(op.thumbnails).css("transform","translate3d(0,0,0)");
				this.defaultLeft = 0;
				this.setSize();
			}
		}).init();
	});
}
// ===============================================
//
//	JQueryでCSVを配列に変換　http://goo.gl/Lj3S7a
// -----------------------------------------------
/* Usage:
 *  jQuery.csv()(csvtext)		returns an array of arrays representing the CSV text.
 *  jQuery.csv("\t")(tsvtext)		uses Tab as a delimiter (comma is the default)
 *  jQuery.csv("\t", "'")(tsvtext)	uses a single quote as the quote character instead of double quotes
 *  jQuery.csv("\t", "'\"")(tsvtext)	uses single & double quotes as the quote character
 *  jQuery.csv(",", "", "\n")(tsvtext)	カンマ区切りで改行コード「\n」
 */
jQuery.extend({
	csv: function(delim, quote, lined) {
		delim = typeof delim == "string" ? new RegExp( "[" + (delim || ","   ) + "]" ) : typeof delim == "undefined" ? ","    : delim;
		quote = typeof quote == "string" ? new RegExp("^[" + (quote || '"'   ) + "]" ) : typeof quote == "undefined" ? '"'    : quote;
		lined = typeof lined == "string" ? new RegExp( "[" + (lined || "\r\n") + "]+") : typeof lined == "undefined" ? "\r\n" : lined;

		function splitline (v) {
			// Split the line using the delimitor
			var arr  = v.split(delim), out = [], q;
			for (var i=0, l=arr.length; i<l; i++) {
				if (q = arr[i].match(quote)) {
					for (j=i; j<l; j++) {
						if (arr[j].charAt(arr[j].length-1) == q[0]) {
							break;
						}
					}
					var s = arr.slice(i,j+1).join(delim);
					out.push(s.substr(1,s.length-2));
					i = j;
				}
				else {
					out.push(arr[i]);
				}
			}

			return out;
		}

		return function(text) {
			var lines = text.split(lined);
			for (var i=0, l=lines.length; i<l; i++) {
				lines[i] = splitline(lines[i]);
			}
			
			// 最後の行を削除
			var last = lines.length - 1;
			if (lines[last].length == 1 && lines[last][0] == "") {
				lines.splice(last, 1);
			}
			
			return lines;
		};
	}
});

// ===============================================
//
//	ヘッダメニュー関連
// -----------------------------------------------
var header_navi = function(){
	var elm = {
		menu : $('#GlobalMenu'),
		btn : {
			search : $('#GlobalMenu #GlobalMenuSearch'),
			clip : $('#GlobalMenu #GlobalMenuClip'),
			menu : $('#GlobalMenu #GlobalMenuSitemap')
		},
		inside_seach : $('#InsideSearch'),
		clip : $('#Clip'),
		sitemap : $('#Sitemap')
	};
	
	// 要素を外部ファイルから追加
	elm.inside_seach.load(sp_path + 'inc/inside_search.inc');
	elm.clip.load(sp_path + 'inc/clip.inc', function(){
		my_clip();
	});
	elm.sitemap.load(sp_path + 'inc/sitemap.inc');

	// 展開動作を実装
	elm.btn.search.overlay_sct({
		elm : elm.inside_seach,
		name : 'GlobalMenuSearch'
	});
	elm.btn.clip.overlay_sct({
		elm : elm.clip,
		name : 'GlobalMenuClip'
	});
	elm.btn.menu.overlay_sct({
		elm : elm.sitemap,
		name : 'GlobalMenuSitemap'
	});

};

// ===============================================
//
//	オーバーレイ・セクション
// -----------------------------------------------
$.fn.overlay_sct = function(option){
	var opt = $.extend({
			elm : {},
			btn : $(this),
			name : '',
			height : 0
		}, option),
		head = $('#GlobalHeader');
	
	var open_move = function(option){
		var op = $.extend({
			state : true
		}, option);

		mmc.window.elm.scrollTop(0);
		mmc.overlay.close = opt.elm;

		if( $('.inner', opt.elm).innerHeight() > mmc.window.height ){
			opt.height = $('.inner', opt.elm).innerHeight();
		} else {
			opt.height = mmc.window.height;
		}
		opt.btn.addClass('opened');

		opt.elm.stop().animate({
			height : '100%'
		}, 400, function(){
			mmc.body.contents.hide();
			mmc.overlay.opened = true;
			mmc.overlay.opened_name = opt.name;
			mmc.overlay.moving = false;
		}).addClass('opened');

		if( mmc.ua.os != 'old_android' && op.state === true ){
			history.replaceState('overlay_' + opt.name + '_close', null);
			history.pushState('overlay_' + opt.name + '_open', null);
		}
	};
	
	var close_move = function(element, callback){
		mmc.body.contents.show();
		element.stop().animate({
			height : 0
		}, 400, function(){
			$('li.opened', head).removeClass('opened');
			mmc.overlay.opened = false;
			mmc.overlay.moving = false;
			if( callback ){
				callback();
			}
		}).removeClass('opened');
	};
	
	$(this).click(function(){
		if( mmc.overlay.moving === true || mmc.lightbox.opened === true ){
			return;
		}
		mmc.overlay.moving = true;
		if( mmc.overlay.opened === false ){
			head.addClass('overlay_open');
			open_move();
		} else {
			if( $(this).attr('id') == mmc.overlay.opened_name ){
				if( mmc.ua.os != 'old_android' ){
					history.back();
				} else {
					close_move(opt.elm, function(){
						head.removeClass('overlay_open');
					});
				}
			} else {
				close_move($('.overlay.opened'), function(){
					open_move({
						state : false
					});
				});
			}
		}
	});
	
	$(opt.elm).on('click', '.close', function(){
		if( mmc.overlay.moving === true ){
			return;
		}
		mmc.overlay.moving = true;
		if( mmc.ua.os != 'old_android' ){
			history.back();
		} else {
			close_move(opt.elm, function(){
				head.removeClass('overlay_open');
			});
		}
	});

	window.addEventListener('popstate', function(e){
		if( e.state == 'overlay_' + opt.name + '_open' ){
			head.addClass('overlay_open');
			open_move({
				state : false
			});
		}
		if( e.state == 'overlay_' + opt.name + '_close' && mmc.overlay.close ){
			close_move(mmc.overlay.close, function(){
				head.removeClass('overlay_open');
			});
		}
	}, false);
};

// ===============================================
//
//	lightbox
// -----------------------------------------------
$.fn.lightbox = function(option){
	var opt = $.extend({
			elm : null,
			btn : $(this)
		}, option);

	opt.btn.click(function(){
		mmc.lightbox.opened = true;
		opt.elm.css({
			top : mmc.body.elm.scrollTop()
		}).show();
		mmc.body.page.css({
			opacity : 0.2
		});
	});
	
	$(opt.elm).on('click', '.close', function(){
		mmc.lightbox.opened = false;
		opt.elm.hide();
		mmc.body.page.css({
			opacity : 1
		});
	});
};

// ===============================================
//
//	SNSシェアボタン
// -----------------------------------------------
var share_btn = function(){
	var btn = {
		facebook : $('#Share .facebook span'),
		twitter : $('#Share .twitter span')
	};

	btn.facebook.on('click', function(){
		var href = 'http://www.facebook.com/share.php?u=';
		href += 'http://www.mitsubishi-motors.co.jp' + location.pathname;
		window.open(href);
		return false;
	});

	btn.twitter.on('click', function() {
		var href = 'http://twitter.com/share?count=horizontal';
		href += '&text=' + encodeURI(document.title);
		href += '&url=http://www.mitsubishi-motors.co.jp' + location.pathname;
		window.open(href);
		return false;
	});
};

// ===============================================
//
//	下層戻るボタン
// -----------------------------------------------
var back_btn = function(){
	var btn = $('#ArticleHeader .back');

	btn.on('click', function() {
		history.back();
	});
};

// ===============================================
//
//	開閉リスト
// -----------------------------------------------
var open_and_shut = function() {
	var elm = $('.open_and_shut .item'),
		arr = [],
		all_open = $('.all_open'),
		all_close = $('.all_close');

	var open = function(i){
		arr[i].text.show();
		arr[i].opened = true;
		arr[i].item.addClass('opened');
	};

	var close = function(i){
		arr[i].text.hide();
		arr[i].opened = false;
		arr[i].item.removeClass('opened')
	};

	elm.each(function(i) {
		arr[i] = {
			item : $(this),
			header : $('h1', this),
			text : $('.text', this),
			opened : false
		}

		if( arr[i].item.hasClass('first_view') ){
			open(i);
		}

		arr[i].header.click(function() {
			if( arr[i].opened === false ){
				open(i);
			} else {
				close(i);
			}
		});
	});

	all_open.click(function() {
		elm.each(function(i) {
			if( arr[i].opened === false ){
				open(i);
			}
		});
	});

	all_close.click(function() {
		elm.each(function(i) {
			if( arr[i].opened === true ){
				close(i);
			}
		});
	});
};

// ===============================================
//
//	選択リスト
// -----------------------------------------------
var select_list = function(){
	var elm  = $('.select_list .select_item');

	elm.each(function() {
		var item = $(this),
			btn = $('.btn', this),
			selected = false;

		item.click(function() {
			if( selected === false ){
				btn.text('解除');
				selected = true;
				item.addClass('selected');
			} else {
				btn.text('選択');
				selected = false;
				item.removeClass('selected');
			}
		});
	});
};

// ===============================================
//
//	新着情報
// -----------------------------------------------
var information = function(option){
	var elm = $.extend({
			parent : $('#Information > .inner'),
			recent : '',
			recent_width : 0,
			recent_txt : '',
			recent_txt_width : 0,
			max : 5,
			archive : '',
			archive_height : 0,
			open : '',
			close : '',
			csv : '/jp/sp/csv/information_index.csv'
		}, option),
		item_html = '',
		recent_item_html = '',
		position = 0,
		timer;

	if( elm.parent.length < 1 ){ return; };

	var timer_func = function(){
		if( position < elm.recent_txt_width * -1 ){
			position = elm.recent_width;
		}
		position -= 1;
		elm.recent_txt.css({ marginLeft : position });
		timer = setTimeout(function(){
			timer_func();
		}, 30);
	};

	elm.recent = $('.recent', elm.parent);
	elm.archive = $('.archive', elm.parent);
	elm.open = $('.open', elm.recent);
	elm.close = $('.close', elm.archive);

	$.ajax({
		url: elm.csv,
		type: 'GET',
		success: function(data) {
			var csv = $.csv()(data);

			if( csv.length < elm.max ){
				elm.max = csv.length;
			}

			for( var i = 0; i < csv.length + 1; i++){
				var indx = i;
				if( indx == elm.max ){
					$('p', elm.recent).prepend(recent_item_html);
					$('.archive_list', elm.archive).prepend(item_html);
					elm.recent_width = $('p a', elm.recent).width();
					elm.recent_txt = $('p a span', elm.recent);
					elm.recent_txt_width = elm.recent_txt.width();
					elm.archive_height = $('.archive_inner', elm.archive).height();

					if( elm.recent_txt_width > elm.recent_width ){
						setTimeout(function(){
							timer_func();
						}, 1000);

						mmc.window.elm.on('resize.info_txt_sizeget', function(){
							elm.recent_width = $('p a', elm.recent).width();
							clearTimeout(timer);
							position = 0;
							if( elm.recent_txt_width > elm.recent_width ){
								timer_func();
							} else {
								elm.recent_txt.css({ marginLeft : position });
							}
						});
					}
					break;
				}
				if( indx == 0 ){
					recent_item_html = '<a href="' + csv[indx][2] + '"';
					if( csv[indx][3] == 'pc' ){
						recent_item_html += ' class="pc"';
					}
					if( csv[indx][3] == 'external' ){
						recent_item_html += ' class="external"';
						recent_item_html += ' target="_blank"';
					}
					recent_item_html += '><span>' + csv[indx][0] + ' ' + csv[indx][1] + '</span></a>';
				}
				item_html += '<section class="item"><a href="' + csv[indx][2] + '"';
				if( csv[indx][3] == 'pc' ){
					item_html += ' class="pc"';
				}
				if( csv[indx][3] == 'external' ){
					item_html += ' class="external"';
					item_html += ' target="_blank"';
				}
				item_html += '>';
				item_html += '<h1>' + csv[indx][0] + '</h1>';
				item_html += '<p>' + csv[indx][1] + '</p>';
				item_html += '</a></section>';
			}
		},
		error: function() {
			return;
		}
	});

	elm.open.on('click', function(event) {
		event.preventDefault();
		elm.recent.hide();
		clearTimeout(timer);
		elm.archive.animate({
			height : elm.archive_height
		}, 250);
	});

	elm.close.on('click', function(event) {
		elm.archive.animate({
			height : 0
		}, 250, function(){
			timer = setTimeout(function(){
				timer_func();
			}, 10);
			elm.recent.show();
		});
	});
};

// ===============================================
//
//	マイクリップ
// -----------------------------------------------
var my_clip = function(){
	var cookie_name = 'sp_myclip',
		elm = {
			clip_add : $('.clip_add'),
			clip_list_in : $('.clip_list_inner')
		},
		data = [],
		item_html = '',
		already_cliped = false;

	var get_cookie = function(){
		if( $.cookie(cookie_name) == undefined ){
			data = [];
		} else {
			data = $.cookie(cookie_name);
			reload_clip_list();
		}
	};

	var set_cookie = function(){
		data.unshift({ title : mmc.head.title, pathname : mmc.head.pathname });
		$.removeCookie(cookie_name);
		$.cookie(cookie_name, data, { expires: 30, path: '/jp/sp/' });
		reload_clip_list();
	};

	var reload_clip_list = function(){
		// 変数・リストをリセット
		item_html = '';
		already_cliped = false;
		$('.item', elm.clip_list_in).remove();

		// クリップ済アイテムの数によって表示を変更
		if( data.length > 0 ){
			$('.no_clip', elm.clip_list_in).hide();
		} else {
			$('.no_clip', elm.clip_list_in).show();
		}

		// 配列からhtmlを生成
		for( var i = 0; i < data.length; i++ ){
			var indx = i;
			item_html += '<div class="item">';
			item_html += '<p class="title"><a href="' + data[indx].pathname + '">' + data[indx].title + '</a></p>';
			item_html += '<p class="delete" data-title="' + data[indx].title + '"><span>削除</span></p>';
			item_html += '</div>';
			if( data[indx].pathname == mmc.head.pathname ){
				already_cliped = true;
			}
			if( indx == data.length - 1 ){
				elm.clip_list_in.prepend(item_html);
				delete_btn();
				if( already_cliped === true ){
					elm.clip_add.addClass('already');
				}
			}
		}
	}

	var delete_btn = function(){
		$('.item .delete', elm.clip_list_in).click(function() {
			var title = $(this).attr('data-title');
			for( var i = 0; i < data.length; i++ ){
				var indx = i;
				if( data[indx].title == title ){
					data.splice(indx, 1);
					$.removeCookie(cookie_name);
					$.cookie(cookie_name, data, { expires: 30, path: '/jp/sp/' });
					elm.clip_add.removeClass('already');
					reload_clip_list();
					break;
				}
			}
		});
	};
	
	$.cookie.json = true;
	get_cookie();
	$('.btn', elm.clip_add).click(function(){
		set_cookie();
	});
};
// ===============================================
//
//	カラーチップ選択
// -----------------------------------------------
$.fn.color_chip_select = function(){
	var elm = {
			detail_img : $('.color_detail .image img', this),
			detail_txt : $('.color_detail .text p', this),
			chip_list : $('.color_chip_list', this)
		},
		color_arr = [];

	$('li', elm.chip_list).each(function(indx){
		var btn = $(this);

		color_arr[indx] = {
			img : $('img', this).attr('src'), 
			txt : $('img', this).attr('alt')
		};

		btn.on('click', function(){
			if( $(this).hasClass('current') ){
				return;
			}
			$('li.current', elm.chip_list).removeClass('current');
			btn.addClass('current');
			elm.detail_img.attr('src', color_arr[indx].img);
			elm.detail_txt.text(color_arr[indx].txt);
		});
	});
};
// ===============================================
//
//	カラーバリエーション
// -----------------------------------------------
$.fn.color_variation = function(){
	var elm = {
			parent : $(this),
			img : $('.image', this),
			item : $('.image .item img', this),
			bar_wrap : $('.image .bar_wrap', this),
			bar : $('.image .bar', this),
			color_select : $('.color_select', this),
			color_txt : $('.color_select p span', this),
			color_tip : $('.color_select ul', this),
			grade_select : $('.grade_select', this),
			different_grade : false
		},
		start = {
			x : 0,
			y : 0
		},
		end = {
			x : 0,
			y : 0
		},
		swipe_first = false,
		swipe_started = false,
		swipe_recet = false,
		you_can_touch = false,
		current_grade_num = '01',
		current_num = 1,
		current_num_zeropd = '01',
		current_color_num = '01',
		img_height = 0,
		bar_width = 0,
		img_arr = [],
		img_arr_lngth = 0,
		img_load_arr = [],
		color_arr = [];

// 関数定義
	// 横幅を取得して、css成形
	var img_layout = function(){
		bar_width = mmc.window.width / 18;
		elm.bar.width( bar_width );
		elm.bar.css({
			marginLeft : bar_width * (current_num - 1)
		});
	};

	// touchmove時に実行する関数
	var touch_move_fnc = function(x){
		if( Math.abs(x) >= 10 ){
			swipe_recet = true;

			if( x > 0 ) {
				current_num -= 1;
				if( current_num == 0 ){
					current_num = 18;
				}
			}
			if( x < 0 ) {
				current_num += 1;
				if( current_num == 19 ){
					current_num = 1;
				}
			}
			if( current_num < 10 ){
				current_num_zeropd = '0' + current_num;
			} else {
				current_num_zeropd = current_num;
			}
			img_change();
			elm.bar.css({
				marginLeft : bar_width * (current_num - 1)
			});
		}
	};

	// カラーチップ選択
	var color_tip_select = function(btn, indx){
		btn.on('click', function(){
			if( $(this).hasClass('current') ){
				return;
			}
			$('li.current', elm.color_tip).removeClass('current');
			$(this).addClass('current');
			current_color_num = indx + 1;
			if( current_color_num < 10 ){
				current_color_num = '0' + current_color_num;
			}
			img_change();
			elm.color_txt.text(color_arr[indx]);
		});
	};

	// グレード選択
	$.fn.grade_select = function(){
		var selected_value = $(this).val();

		if(selected_value != '00'){
			current_grade_num = selected_value;
			img_change();
		}
	};

	// 画像生成
	$.fn.image_add = function(g_indx, indx){
		var btn = $(this);
		color_arr[indx] = $('img', this).attr('alt');
		color_tip_select(btn, indx);
		for( var i = 1; i < 19; i++ ){
			var img_indx = i,
				num1 = 0;
				num2 = 0,
				html = '';

			if( indx < 9 ){
				num1 = '0' + (indx + 1);
			} else {
				num1 = indx + 1;
			}

			if( img_indx < 10 ){
				num2 = '0' + img_indx;
			} else {
				num2 = img_indx;
			}

			img_arr.push( elm.item.attr('src').replace(/^(.+).....(\..+)$/,'$1' + num1 + '_' + num2 + '$2'));
			html += '<div class="item item';
			if( g_indx > 0 ){
				html += g_indx + '_';
			}
			html += num1 + '_' + num2 + '"><img src="';
			if( g_indx > 0 ){
				html += elm.item.attr('src').replace(/^(.+)........(\..+)$/,'$1' + g_indx + '_' + num1 + '_' + num2 + '$2');
			} else {
				html += elm.item.attr('src').replace(/^(.+).....(\..+)$/,'$1' + num1 + '_' + num2 + '$2');
			}
			html += '" alt="" /></div>';
			
			$('.inner', elm.img).append(html);
		}
		if( indx == 0 ){
			$(this).addClass('current');
			elm.color_txt.text(color_arr[indx]);
		}
	}

	// 画像変更
	var img_change =  function(){
		$('.item.show', elm.img).removeClass('show').hide();
		if( elm.grade_select.length > 0 ){
			// console.log(current_grade_num);
			$('.item' + current_grade_num + '_' + current_color_num + '_' + current_num_zeropd, elm.img).addClass('show').show();
		} else {
			$('.item' + current_color_num + '_' + current_num_zeropd, elm.img).addClass('show').show();
		}
	}

// 初期化
	// 車体画像 + カラーチップの配列化
	if( elm.grade_select.length > 0 ){
		var grade_num = elm.grade_select.find('option').size();

		for (var i = 1; i < grade_num + 1; i++) {
			var g_indx = i;
			
			if( g_indx < 10 ){
				g_indx = '0' + g_indx;
			}

			$('li', elm.color_tip).each(function(indx){
				$(this).image_add(g_indx, indx);
			});
		};
	} else {
		$('li', elm.color_tip).each(function(indx){
			$(this).image_add(0, indx);
		});
	}

	$('.item:first-of-type', elm.img).remove();
	$('.item', elm.img).each(function(indx){
		if( indx == 0 ){
			$(this).addClass('show').show();
		}
	});

	img_arr_lngth = img_arr.length;

	// 画像のプリロード
	for( var i = 0; i < img_arr.length; i++ ){
		var indx = i;
		img_load_arr[indx] = new Image();
		img_load_arr[indx].onload = function(){
			img_arr_lngth = img_arr_lngth - 1;
			if( 0 >= img_arr_lngth ){
				// 最後の画像を読み込み終わった際の動作を指定
				you_can_touch = true;
			}
		};
		img_load_arr[indx].src = img_arr[indx];
	}

	//タッチイベント
	elm.img.on('touchstart', function(e){
		if( swipe_started === false ){
			start.x = e.originalEvent.changedTouches[0].pageX;
			start.y = e.originalEvent.changedTouches[0].pageY;
			swipe_started = true;
			if( swipe_first === false ){
				$('.turn', elm.img).fadeOut(250);
				swipe_first = true;
			}
		}
	});

	elm.img.on('touchmove', function(e){
		//if( swipe_started === true && you_can_touch == true ){
		if( swipe_started === true ){
			if( swipe_recet === true ){
				start.x = e.originalEvent.changedTouches[0].pageX;
				start.y = e.originalEvent.changedTouches[0].pageY;
				swipe_recet = false;
			}
			end.x = e.originalEvent.changedTouches[0].pageX;
			end.y = e.originalEvent.changedTouches[0].pageY;
			if( Math.abs(start.y - end.y) < 5 ){
				e.preventDefault();
			}
			touch_move_fnc(Math.floor(start.x - end.x));
		}
	});

	elm.img.on('touchend', function(){
		if( swipe_started === true ){
			swipe_started = false;
		}
	});

	//グレードセレクト
	elm.grade_select.find('select').on('change.grade_select', function(){
		$(this).grade_select();
	});

// 実行
	img_layout();
	mmc.window.elm.on('resize.color_variation', function(){
		img_layout();
	});
};
// ===============================================
//
//	イベント設定・実行
// -----------------------------------------------
$(document).ready(function(){
	mmc.body.elm = $('body');
	mmc.body.page = $('#Page');
	mmc.body.contents = $('#Contents');
	mmc.head.title = $('.title_clip').html();
	if( mmc.head.title == undefined && mmc.head.pathname == '/jp/sp/' ){
		mmc.head.title = 'トップページ';
	}

	mmc.window.elm.on('resize.win_size_get', function(){
		mmc.window.size_get();
	});
	
	mmc.window.elm.on('orientationchange.win_size_get', function(){
		mmc.window.size_get();
	});
	
	mmc.window.size_get();

	mmc.body.elm.on('click.link_in_overlay', '.overlay a:not([target="_blank"])', function() {
		if( mmc.ua.os == 'old_android' ){
			return;
		}
		var href = $(this).attr('href');
		if( mmc.head.pathname == href ){
			history.go(-1);
		} else {
			location.replace(href);
		}
		return false;
	});

	share_btn();
	back_btn();
	open_and_shut();
	select_list();

	//console.log(mmc);
});