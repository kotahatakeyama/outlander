<div class="inner">

<script>
$(function(){
	$("a.opw_m").click(function(){
		window.open(this.href);
		return false;
	});
});
</script>


<div id="menu">
<ul id="topmenu">
<li class="pc"><a href="http://www.mitsubishi-motors.com/jp/" class="mn1">トップページ</a></li>
<li class="pc"><a href="http://www.mitsubishi-motors.com/jp/products/index.html" class="mn2">商品情報</a></li>
<li class="pc"><a href="http://www.mitsubishi-motors.com/jp/spirit/index.html" class="mn3">三菱自動車のクルマづくり</a></li>
<li class="pc"><a href="http://www.mitsubishi-motors.com/jp/social/index.html" class="mn4">CSR・環境・社会</a></li>
<li class="pc"><a href="http://www.mitsubishi-motors.com/jp/corporate/index.html" class="mn5">企業情報・投資家情報</a></li>
<li class="pc"><a href="http://www.mitsubishi-motors.com/jp/events/index.html" class="mn6">ニュース・イベント</a></li>
</ul>
<div class="secmenu">
<p class="ml st"><a href="http://www.mitsubishi-motors.co.jp/sp/" class="opw_m aw">三菱自動車 Japan<br>オフィシャルサイト</a></p>
<p class="mr"><a href="http://www.mitsubishi-motors.com/publish/pressrelease_jp/index.html" class="pc">プレスリリース</a></p>
</div>
<div class="secmenu">
<p class="ml"><a href="http://www.mitsubishi-motors.com/en/global_network/" class="opw_m pc">Global Network</a></p>
<p class="mr"><a href="http://www.mitsubishi-motors.com/en/showroom/" class="opw_m aw">Global Showroom</a></p>
</div>
<div class="secmenu">
<p class="ml"><a href="http://www.mitsubishi-motors.com/jp/sitemap/index.html" class="pc">サイトマップ</a></p>
<p class="mr"><a href="http://www.mitsubishi-motors.co.jp/reference/index.html" class="opw_m pc">お問い合わせ</a></p>
</div>
<div class="secmenu">
<p class="ml"><a href="http://www.mitsubishi-motors.com/jp/corporate/recruit/index.html" class="pc">採用情報</a></p>
<p class="mr"><a href="http://www.mitsubishi-motors.com/jp/socialmedia/index.html" class="pc">ソーシャルメディア</a></p>
</div>
<div class="secmenu">
<p class="ml"><a href="http://www.mitsubishi-motors.com/jp/privacy/index.html" class="pc">個人情報保護方針</a></p>
<p class="mr"><a href="http://www.mitsubishi-motors.com/jp/usage/index.html" class="pc">サイトのご利用について</a></p>
</div>

<p class="close">閉じる</p>
</div>
	
<!-- / .inner --></div>