var release_tag = "<dl>";
var lastYear;
var cnt = 0;

$(function(){
	$.ajax({
		url: '/jp/share/xml/global_jp_release.xml',
		type:'get', 
		dataType:'xml',
		async:'true',
		cache :'false',
		success:global_jp_top_xml
	});
});

function global_jp_top_xml(xml,status){
	if(status!='success')return;
	$(xml).find('item').each(function(){
		var $day = $(this).find("day").text();
		lastYear = $day.substring(0,4);
		return false;
	}); 
	read_jp_xml();
}

function read_jp_xml(){
	$.ajax({
		url: '/publish/share/xml/' + lastYear + '_jp_release_data.xml',
		type:'get', 
		dataType:'xml',
		async:'true',
		cache :'false',
		success:global_jp_release_xml
	});
}

function global_jp_release_xml(xml,status){
	if(status!='success')return;
	$(xml).find('item').each(release_top); 
	
	if(cnt<3){
		$.ajaxSetup({ async: false });
		$.ajax({
			url: '/publish/share/xml/' + (lastYear-1) + '_jp_release_data.xml',
			type:'get', 
			dataType:'xml',
			async:'true',
			cache :'false',
			success:global_jp_release_xml2
		});
	} else {
		write_press();
	}
}

function global_jp_release_xml2(xml,status){
	if(status!='success')return;
	$(xml).find('item').each(release_top); 
	write_press();
}

function release_top(){
	var $day = $(this).find("day").text();
	var $url = $(this).find("url").text();
	var $title = $(this).find("title").text();
	var $icon = $(this).find("category").text();
	
	
	if ( $icon == 'corporate') {
		release_tag = release_tag + "<dt><span>" + $day + "</span><img src='/jp/investors/images/icon_business.png' width='49' alt='���' /></dt>";
	} else if ( $icon == 'products') {
		release_tag = release_tag + "<dt><span>" + $day + "</span><img src='/jp/investors/images/icon_products.png' width='49' alt='�V��' /></dt>";
	} else if ( $icon == 'motorshow') {
		release_tag = release_tag + "<dt><span>" + $day + "</span><img src='/jp/investors/images/icon_motors.png' width='75' alt='���[�^�[�V���[' /></dt>";
	} else if ( $icon == 'corporate_ethics') {
		release_tag = release_tag + "<dt><span>" + $day + "</span><img src='/jp/investors/images/icon_ethics.png' width='75' alt='��Ɨϗ��ψ���' /></dt>";
	} else if ( $icon == 'ir') {
		release_tag = release_tag + "<dt><span>" + $day + "</span><img src='/jp/investors/images/icon_ir.png' width='75' alt='���Y�E�̔��E�A�o����' /></dt>";
	}


	release_tag = release_tag + "<dd class='link'><a href='" + $url + "'>" + $title + "</a></dd>";
	cnt =cnt + 1;
	if(cnt==3)return false;
}

function write_press(){
	release_tag = release_tag + "</dl>";
	$('#press_area').html(release_tag); 
	$('.image_loading').css('display','none');
	$('#press_area').css('display','block');
}