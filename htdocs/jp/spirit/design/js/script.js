/*!
 * MMC DESIGN SITE
 * script.js
 */
!function (a, b, c, d) {
    function e() {
        for (var a, c = 3, d = b.createElement("div"); d.innerHTML = "<!--[if gt IE " + ++c + "]><i></i><![endif]-->", d.getElementsByTagName("i")[0];);
        return c > 4 ? c : a
    }
    c.easing.easeOutExpo = function (a, b, c, d, e) {
        return b == e ? c + d : d * (-Math.pow(2, -10 * b / e) + 1) + c
    },
        c.fn.hoverImageReplace = function (b) {
        var e = {
                matchStr         : "_ov",
                touchDeviceDetach: !0
            },
            f = c.extend(!1, e, b, this.data()),
            g = a.ontouchstart !== d,
            h = new RegExp("^(.+)" + f.matchStr + "(..+)$");
        return this.each(function () {
            var a,
                b;
            a = c(this),
                b = "IMG" === a[0].nodeName ? a : a.find("img"),
                a.on({
                "mouseenter.hoverImageReplace": function () {
                    if (!g || !f.touchDeviceDetach) {
                        var a = b.attr("src").replace(/^(.+)(\..+)$/, "$1" + f.matchStr + "$2");
                        b.attr("src", a);
                        b.attr("style","opacity:0.3");
                        b.animate({"opacity":"1"}, 500, "linear");
                    }
                },
                "mouseleave.hoverImageReplace": function () {
                    if (!g || !f.touchDeviceDetach) {
                        var a = b.attr("src").replace(h, "$1$2");
                        b.attr("src", a);
                    }
                }
            })
        })
    };
    var f,
        g = function (b, e, g) {
            var h = c(a);
            g && (g = c.easing[g] ? g : d),
                b.on("click", function (a) {
                var b = c(this);
                a.preventDefault(),
                    "#" !== b.attr("href") && (c("#pageNav").find("a").removeClass("active"), h.off("scroll.scrollNavEvent"), b.addClass("active"), c("html,body").stop(!0, !0).animate({
                    scrollTop: c(b.attr("href")).offset().top
                }, e, g, function () {
                    h.on("scroll.scrollNavEvent", function () {
                        var a = h.scrollTop();
                        f(a)
                    })
                }))
            })
        },
        h = function (b) {
            function d(a) {
                b.each(a >= i ? function () {
                    var a = c(this);
                    "false" !== a.attr("data-reposi") ? (f = a.attr("data-originH") / i, e = -(g.width() * f - a.attr("data-originH")) / 2) : e = 0,
                        a.css({
                        left      : "auto",
                        marginLeft: 0,
                        marginTop : e,
                        position  : "relative",
                        top       : "auto"
                    })
                } : function () {
                    var a = c(this);
                    e = -(a.attr("data-originH") / 2),
                        a.css({
                        left      : "50%",
                        marginLeft: -(i / 2),
                        marginTop : e,
                        position  : "absolute",
                        top       : "50%"
                    })
                })
            }
            var e,
                f,
                g = c(a),
                h = g.width(),
                i = 1400;
            d(h),
                g.on("resize", function () {
                var a = g.width();
                d(a)
            })
        },
        i = function () {
            function a(a, b) {
                var c = new String(b),
                    d = a - c.length;
                if (0 >= d) 
                    return c;
                for (; d-- > 0;) 
                    c = "0" + c;
                return c
            }
            function b() {
                var c,
                    i = new Date,
                    j = d - i,
                    k = 864e5,
                    l = Math.floor(j / k),
                    m = Math.floor(j % k / 36e5),
                    n = Math.floor(j % k / 6e4) % 60,
                    o = Math.floor(j % k / 1e3) % 60 % 60;
                j > 0 ? (e.text(a(2, l)), f.text(a(2, m)), g.text(a(2, n)), h.text(a(2, o)), c = setTimeout(function () {
                    b()
                }, 1e3)) : clearTimeout(c)
            }
            var d = new Date("June 29,2014 00:00:00"),
                e = c(".js-days"),
                f = c(".js-hour"),
                g = c(".js-min"),
                h = c(".js-sec");
            b()
        },
        j = function () {
            function a(a) {
                a.preventDefault(),
                    b.fadeOut("fast", function () {
                    e.empty(),
                        g.css({
                        overflow: "visible"
                    })
                })
            }
            var b,
                d,
                e,
                f,
                g = c("body");
            f = '<div class="modal" id="modal"><div class="modal_inner" id="modalInner"><div class="modal_conte" id="modalConte"></div></div><div class="close_wrap"><a href="#" class="js-modal-close"><img src="img/main/close_btn.png" alt="close"></a></div></div>',
                g.append(f),
                b = c("#modal"),
                d = c("#modalInner"),
                e = c("#modalConte"),
                c(".js-modal-open").on("click", function (a) {
                var f = c(this).next().children().clone();
                a.preventDefault(),
                    e.append(f),
                    g.css({
                    overflow: "hidden"
                }),
                    b.fadeIn("fast"),
                    d.scrollTop(0),
                    m()
            }),
                b.on("click", function (b) {
                "modal" === b.target.id && a(b)
            }),
                b.on("click", ".js-modal-close", function (b) {
                a(b)
            }),
                b.on("click", ".js-modal-close", function (b) {
                a(b)
            })
        },
        k = function (a) {
            function b(a) {
                a.preventDefault(),
                    d.fadeOut("fast", function () {
                    c(this).remove(),
                        h.css({
                        overflow: "visible"
                    })
                })
            }
            var d,
                e,
                f,
                g,
                h = c("body"),
                i = c(a),
                j = function (a) {
                    var b = '<iframe src="//www.youtube.com/embed/' + a + '?rel=0&autoplay=1&hd=1&autohide=2" width="800" height="450" frameborder="0" allowfullscreen></iframe>';
                    return b
                };
            f = '<div class="modal" id="movieModal"><div class="movie_modal_inner" id="movieModalInner"><div class="m_close_wrap"><a href="#" class="js_m_modal_close"><img src="img/main/close_btn_modal.png" alt="close"></a></div></div></div>',
                h.append(f),
                d = c("#movieModal"),
                e = c("#movieModalInner"),
                g = c(".js_m_modal_close"),
                e.prepend(j(i.attr("data-movie"))),
                h.css({
                overflow: "hidden"
            }),
                d.fadeIn("fast"),
                d.on("click", function (a) {
                "movieModal" === a.target.id && b(a)
            }),
                g.on("click", function (a) {
                b(a)
            })
        },
        l = function () {
            function a(a) {
                var b = 0;
                a.each(function () {
                    b < c(this).height() && (b = c(this).height())
                }),
                    a.height(b)
            }
            a(c(".wrapper > li"))
        },
        m = function () {
            function a(a) {
                {
                    var b = f.filter(":visible"),
                        c = f.eq(a);
                    c.attr("alt")
                }
                h.removeClass("active"),
                    h.eq(a).addClass("active"),
                    b.hide(),
                    c.show()
            }
            var b = c("#modal #slide_show"),
                d = b.find(".nextBtn"),
                e = b.find(".previewBtn"),
                f = (b.find(".title"),
                b.find(".photoList img")),
                g = b.find(".thumb li"),
                h = b.find(".thumb li span"),
                i = 0,
                j = f.length;
            d.click(function () {
                i++,
                    i === j && (i = 0),
                    a(i)
            }),
                e.click(function () {
                i--,
                    0 > i && (i = j - 1),
                    a(i)
            }),
                g.click(function () {
                return i === g.index(this) ? !1 : (i = g.index(this), void a(i))
            }),
                g.hover(function () {
                return c(this).find("span").hasClass("active") === !0 ? !1 : void c(this).stop().animate({
                    opacity: "0.5"
                }, 200)
            }, function () {
                c(this).stop().animate({
                    opacity: "1"
                }, 200)
            }),
                a(i)
        },
        n = function () {
            function a(a) {
                a.preventDefault(),
                    b.fadeOut("fast", function () {
                    e.empty(),
                        j.css("overflow", "visible")
                })
            }
            var b,
                d,
                e,
                f,
                g,
                h,
                i,
                j = c("body"),
                k = [];
            f = '<div class="modalG" id="modalG"><div class="modalG_inner" id="modalGInner"><div class="gallery" id="galleryConte"></div><div class="close_wrap"><a href="#" class="js-modal-close"><img src="img/main/close_btn.png" alt="close"></a></div></div></div>',
                g,
                h,
                j.append(f),
                b = c("#modalG"),
                d = c("#modalGInner"),
                e = c("#galleryConte"),
                c(".js-modal-g-open").click(function (a) {
                var d = new Image,
                    f = c(this).closest(".js-gallery-wrap").find("a"),
                    l = 0,
                    m = f.length;
                for (a.preventDefault(); m > l; l++) 
                    k[l] = f.eq(l).attr("data-image-url");
                i = c(this).closest(".js-gallery-wrap").find("a").index(this),
                    d.src = c(this).attr("data-image-url"),
                    e.append(g),
                    e.append(d),
                    e.append(h),
                    j.css("overflow", "hidden"),
                    b.fadeIn("fast")
            }),
                b.on("click", "#nextBtn", function () {
                var a;
                i >= k.length - 1 || (i++, a = k[i], e.children("img").attr("src", a))
            }),
                b.on("click", "#prevBtn", function () {
                var a;
                0 >= i || (i--, a = k[i], e.children("img").attr("src", a))
            }),
                b.on("click", function (b) {
                "modalGInner" === b.target.id && a(b)
            }),
                b.on("click", ".js-modal-close", function (b) {
                a(b)
            })
        },
        o = function (c, d, e) {
            for (var f, g, h, i, j, k, l = b.getElementsByTagName("meta"), m = d, n = e, o = (a.screen.width - m) / 2, p = (a.screen.height - n) / 2, q = 0, r = l.length; r > q; q++) 
                l[q].getAttribute("property") && (h = l[q].getAttribute("property"), "og:url" === h ? g = l[q].getAttribute("content") : "og:description" === h && (f = l[q].getAttribute("content")));
            i = 140 - (g.length + 1),
                f.length > i && (f = f.substr(0, i - 3) + "..."),
                j = "https://twitter.com/share?url=" + encodeURI(g) + "&text=" + encodeURI(f),
                k = "screenX=" + o + ",screenY=" + p + ",left=" + o + ",top=" + p + ",width=" + m + ",height=" + n + ",toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=yes",
                c.on("click", function (b) {
                b.preventDefault(),
                    a.open(j, "popup", k)
            })
        },
        p = function (c, d, e) {
            for (var f, g, h, i, j = d, k = e, l = b.getElementsByTagName("meta"), m = (a.screen.width - j) / 2, n = (a.screen.height - k) / 2, o = 0, p = l.length; p > o; o++) 
                l[o].getAttribute("property") && (g = l[o].getAttribute("property"), "og:url" === g && (f = l[o].getAttribute("content")));
            h = "https://www.facebook.com/sharer.php?u=" + f,
                i = "screenX=" + m + ",screenY=" + n + ",left=" + m + ",top=" + n + ",width=" + j + ",height=" + k + ",toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=yes",
                c.on("click", function (b) {
                b.preventDefault(),
                    a.open(h, "popup", i)
            })
        },
        q = function () {
            function b(a) {
                7 === l && (d.addClass("ie7"), c("body").append(d)),
                    a > i.top ? 7 === l ? d.addClass(k) : (h.addClass(k), j.addClass(k)) : 7 === l ? d.removeClass(k) : (h.removeClass(k), j.removeClass(k))
            }
            var d,
                f = c(a),
                g = f.scrollTop(),
                h = c("#pageNav"),
                i = h.offset(),
                j = c("#jsTopConte"),
                k = "fixed",
                l = e();
            7 === l && (d = h.clone()),
                b(g),
                f.on("scroll", function () {
                var a = f.scrollTop();
                b(a)
            })
        },
        r = function () {
            function b(a) {
                a >= 0 && a < k[0] ? d.removeClass(m) : a >= k[0] && a < k[1] ? (d.removeClass(m), d.eq(0).addClass(m)) : a >= k[1] && a < k[2] ? (d.removeClass(m), d.eq(1).addClass(m)) : a >= k[2] ? (d.removeClass(m), d.eq(2).addClass(m)) : d.removeClass(m)
            }
            var d,
                g,
                h = c(a),
                i = h.scrollTop(),
                j = [],
                k = [],
                l = c("#location"),
                m = "active",
                n = 0;
            for (d = 7 === e() ? c(".page_nav.ie7").find("a") : c("#pageNav").find("a"), g = d.length; g > n; n++) 
                j[n] = c(d.eq(n).attr("href")),
                    k[n] = Math.floor(j[n].offset().top);
            j.push(l),
                k.push(Math.floor(l.offset().top)),
                b(i),
                h.on("scroll.scrollNavEvent", function () {
                var a = h.scrollTop();
                b(a)
            }),
                f = b
        },
        s = function (b) {
            function d(a) {
                a >= g ? b.removeClass(h) : b.addClass(h)
            }
            var e = c(a),
                f = e.width(),
                g = 1400,
                h = "minwidth";
            d(f),
                e.on("resize", function () {
                var a = e.width();
                d(a)
            })
        },
        t = function (c, d, e) {
            for (var f, g, h, i, j = d, k = e, l = b.getElementsByTagName("meta"), m = (a.screen.width - j) / 2, n = (a.screen.height - k) / 2, o = 0, p = l.length; p > o; o++) 
                l[o].getAttribute("property") && (g = l[o].getAttribute("property"), "og:url" === g && (f = l[o].getAttribute("content")));
            h = "https://plus.google.com/share?url=" + f,
                i = "screenX=" + m + ",screenY=" + n + ",left=" + m + ",top=" + n + ",width=" + j + ",height=" + k + ",toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=yes",
                c.on("click", function (b) {
                b.preventDefault(),
                    a.open(h, "popup", i)
            })
        };
    s(c("#flexslider")),
        i(),
        j(),
        l(),
        n(),
        q(),
        o(c(".js_tw_btn"), 640, 480),
        p(c(".js_fb_btn"), 640, 480),
        t(c(".js_gp_btn"), 640, 580),
        g(c('a[href^="#"]'), 500, "easeOutExpo"),
        c(".js_movie_modal").on("click", function (a) {
        k(this, a)
    }),
        c(".js-img-hover").hoverImageReplace(),
        c(function () {
        c("#flexslider").flexslider({
            animation        : "fade",
            controlsContainer: ".pager",
            directionNav     : 1,
            keyboard         : 1,
            touch            : 1
        })
    }),
        c(a).on({
        load: function () {
            r(),
                h(c(".js-bgImgrePosi"))
        }
    })
jQuery.event.add(window,"load",function() {
    $(".load_img").css("display", "none");
	$("html").css("height", "auto");
	$("body").css("height", "auto");
	$(".main_conte").fadeIn("slow");
	r();
});

}(window, document, jQuery);