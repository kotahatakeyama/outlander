        (function($) {
            $(function() {
                $("#slider").simplyScroll({
                    orientation: 'vertical',
                    auto: true,
                    manualMode: 'loop',
                    frameRate: 10,
                    speed: 3,
                    pauseOnHover:false
                });
            });
        })(jQuery);

        $('.area:first').show();
        $('#tabs li:first').addClass('active');

        $('#tabs li').click(function() {
            $('#tabs li').removeClass('active');
            $(this).addClass('active');
            $('.area').hide();

            $($(this).find('a').attr('href')).fadeIn();
            return false;
        });

        $("#tabs ul li").click(function() {
            var num = $('#tabs ul li').index(this);
            var imgs = $("#tabs ul li img");
            imgs.each(function() {
                $(this).attr("src", "images/tab_text0" + imgs.index(this) + "_off.png");
            });
            imgs.eq(num).attr("src", "images/tab_text0" + num + "_on.png");
        });