//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$(function(){
	
	//スライドの初期化
	slideInit();
	
	//------------------------------------------------------
	$("#contents-loader img").imagesLoaded(function(){
			ie_set_png();
			//------------------------------------------------------
			$("#link-bt a").each(function(){
				$(this).css({"opacity": "0", "display": "none"});
				$(this).children(".over").css({ "opacity": "0" });
				$(this).children(".message").css({ "opacity": "0" });
				$(this).bind("mouseover", function(){ linkOver2($(this).children(".over")); });
				$(this).bind("mouseout", function(){ linkOver3($(this).children(".over")); });
			});
			$("#background-im").css({opacity: "0"});
			$("#log").css({opacity: 0, "z-index": 10});
			$("#corner-left-top").css({opacity: 0, "z-index": 11});
			$("#corner-left-bottom").css({opacity: 0, "z-index": 12});
			$("#corner-right-top").css({opacity: 0, "z-index": 13});
			$("#corner-right-bottom").css({opacity: 0, "z-index": 14});
			$("#social-link").css({opacity: 0, "z-index": 15});
			$("#link-bt").css({opacity: 0, "z-index": 16});
			$("#background-light").css({opacity: 0});
			$("#report-special-bt").css({opacity: 0});
				
			$("#popcontents-footer").css({"visibility": "visible"});
			
			linkDefaultCss.push( { "left": $("#report .footer-left-over-bg").css("left"), "width": $("#report .footer-left-over-bg").css("width") } );
			linkDefaultCss.push( { "left": $("#artist .footer-left-over-bg").css("left"), "width": $("#artist .footer-left-over-bg").css("width") } );
			linkDefaultCss.push( { "left": $("#system .footer-left-over-bg").css("left"), "width": $("#system .footer-left-over-bg").css("width") } );
			linkDefaultCss.push( { "left": $("#extramovie .footer-left-over-bg").css("left"), "width": $("#extramovie .footer-left-over-bg").css("width") } );
			linkDefaultCss.push( { "left": $("#about .footer-left-over-bg").css("left"), "width": $("#about .footer-left-over-bg").css("width") } );
			linkDefaultCss.push( { "left": $("#plan .footer-left-over-bg").css("left"), "width": $("#plan .footer-left-over-bg").css("width") } );		

			linkBtMessageCss.push({"left": $("#link-bt-report .message").css("left")});
			linkBtMessageCss.push({"left": $("#link-bt-artist .message").css("left")});
			linkBtMessageCss.push({"left": $("#link-bt-system .message").css("left")});
			linkBtMessageCss.push({"left": $("#link-bt-extramovie .message").css("left")});
			linkBtMessageCss.push({"left": $("#link-bt-about .message").css("left")});
			linkBtMessageCss.push({"left": $("#link-bt-plan .message").css("left")});
			
			//------------------------------------------------------
			start();
			onResize();
	});
	
	
	//------------------------------------------------------
	onResize();
	$("#footerpop").css({"visibility": "visible"});
	//リサイズしたら実行
	$(window).resize(function(){
		onResize();
	});
})


function linkOver2(__target)
{
	__target.css({ "display": "block" });
	__target.stop().animate({"opacity": 1}, 500, "easeInQuad", function()
	{
	});
}
function linkOver3(__target)
{
	__target.stop().animate({ "opacity": "0" }, 750, "easeOutQuad");
}

///////////////////////////////////////////////
function start()
{
	$("#nebuta-contents").css({"visibility": "visible"});
	slide_start();
}

//background
///////////////////////////////////////////////
function onResize()
{
	
	//画像サイズ指定
	var imgW = 1009;
	var imgH = 560;
	
	var win_minW = 1024;
	var win_minH = imgH + 129;
	//ウィンドウサイズ取得
	var winW = $(window).width();
	var winH = $(window).height();
	
	var offsetH = 131;
	if( jQuery.browser.msie ) offsetH = 129;
	else if( navigator.userAgent.indexOf("Win") != -1 && jQuery.browser.webkit ) offsetH = 129;
	else if( navigator.userAgent.indexOf("Mac") != -1 && navigator.userAgent.indexOf("Chrome") != -1 ) offsetH = 136;
	else if( navigator.userAgent.indexOf("Mac") != -1 && navigator.userAgent.indexOf("Safari") != -1 ) offsetH = 132;
	
	

	
	//コンテンツエリアをリサイズ
	$("#bg").css({"overflow": "hidden"});
	if( winH > win_minH )
	{
		$("#background-im").css({'height': winH - offsetH});
		$("#nebuta-contents").css({'height': winH - offsetH});
	}else
	{
		$("#background-im").css({'height': win_minH  - offsetH});
		$("#nebuta-contents").css({'height': win_minH - offsetH});
	}
	
	//ブラウザの横幅を調整
	if( winW < win_minW ){
		$("html").css({'width': win_minW});
	}else
	{
		$("html").css({'width': "100%"});
	}
	
	//ブラウザの高さを調整
	if( winH < win_minH ){
		$("html").css({'height': win_minH });
	}else{
		$("html").css({'height': "100%" });
	}

	if( winW < win_minW ) winW = win_minW;
	if( winH < win_minH ) winH = win_minH;
	var scaleW = winW / imgW;
	var scaleH = (winH - offsetH) / imgH;//125はヘッダー/フッターの総値
	var fixScale = Math.max(scaleW, scaleH);
	
	var setW = imgW * fixScale;
	var setH = imgH * fixScale;
	
	bakcgroundResize(setW, setH);
}

function bakcgroundResize(w, h)
{
	$('#background-im-image img').each(function()
	{
		$(this).css({
			'width': w,
			'height': h
		});
		
	}
	);
	
	$("#background-shadow img").css({"width": w, "height": h});
}


//background
///////////////////////////////////////////////
var footerLinkID = 0;
var imSrc = [
	{ id:"index_im_06", src: 'images/index_im_06.jpg' },
	{ id:"index_im_03", src: 'images/index_im_03.jpg' },
	{ id:"index_im_04", src: 'images/index_im_04.jpg' },
	{ id:"index_im_05", src: 'images/index_im_07.jpg' },
	{ id:"index_im_01", src: 'images/index_im_01.jpg' },
	{ id:"index_im_02", src: 'images/index_im_02.jpg' }
];

var slideImages = new Array(imSrc.length);

var flag = true;
function slide_start()
{

	var srcArray = [
		{ src: 'images/index_pt_11.gif' },
		{ src: 'images/index_pt_11.gif' },
		{ src: 'images/index_pt_11.gif' },
		{ src: 'images/index_pt_11.gif' },
		{ src: 'images/index_pt_11.gif' },
		{ src: 'images/index_pt_11.gif' }
	];
	$("#background-im-image-timer").crossSlide({
	  sleep: 8,
	  fade: 1
	},srcArray ,function(idx, img, idxOut, imgOut){
		onResize();
		if( idxOut != undefined ){
			fadeOut(footerLinkID);
		}else{
			footerLinkID = idx;
			if( flag ) fadeIn(idx);
		}
	}
	);
}
//フェードアウト
///////////////////////////////////////////////
var slide_first = true;
function fadeIn(target_idx)
{
	changeFooterLink(target_idx, "in");
	slideImages[target_idx].css({ "visibility": "visible" });

	$("#corner-left-top").css({opacity: 1});
	$("#corner-left-bottom").css({opacity: 1});
	$("#corner-right-top").css({opacity: 1});
	$("#corner-right-bottom").css({opacity: 1});
	
	$("#log").animate({opacity: 1}, 1000);
	
		$("#background-im").delay(500).animate({opacity: 1}, 500);
		
		if( target_idx == 0 ){
			$("#report-special-bt").css({"display": "block"});
			$("#report-special-bt").delay(1500).animate({opacity: 1, backgroundPosition: "0px 0px"}, 1000);
		}else {
			$("#link-bt").animate({"backgroundPosition": "0px 15px"}, 100);
			$("#link-bt").delay(1500).animate({opacity: 1, backgroundPosition: "0px 0px"}, 1000);
		}
		
			$("#background-shadow").delay(500).animate({opacity: 0}, 500, "easeInQuart");
			$("#background-light").delay(500).animate({opacity: 0.4}, 500, function(){
				$("#background-light").animate({"opacity": 0}, 500);
			});
				
				$("#corner-left-top").delay(2500).animate({opacity: 0}, 1000, "easeInSine");
					$("#corner-left-bottom img").delay(3200).animate({opacity: 0}, 1000);
						$("#corner-right-top img").delay(3900).animate({opacity: 0}, 1000);
							$("#corner-right-bottom img").delay(4600).animate({opacity: 0}, 1000);
							
								$("#social-link").delay(5000).animate({opacity: 1}, 1000);
}

//フェードイン
///////////////////////////////////////////////
function fadeOut(target_idx)
{
	changeFooterLink(target_idx, "out");
	$("#background-im").delay(400).animate({"opacity": "0"}, 500, function(){
		$("#corner-left-top").css({opacity: 0});
		$("#corner-left-bottom").css({opacity: 0});
		$("#corner-right-top").css({opacity: 0});
		$("#corner-right-bottom").css({opacity: 0});
		$("#background-shadow").css({opacity: 1});
		
		$("#corner-left-top").css({opacity: 1});
		$("#corner-left-bottom img").css({opacity: 1});
		$("#corner-right-top img").css({opacity: 1});
		$("#corner-right-bottom img").css({opacity: 1});
		
		$("#report-special-bt").css({"display": "none", "opacity": "0"});

		
		slideImages[target_idx].css({ "visibility": "hidden" });
	});
	$("#link-bt").delay(400).animate({"opacity": "0"});
}


//イニシャライズ
///////////////////////////////////////////////
//position: absolute; visibility: hidden; top: 0px; left: 0px; border: 0px none;
function slideInit()
{
	for( var i=0; i < imSrc.length; i++ )
	{
		$("#background-im-image").append("<img src=\""+ imSrc[i].src + "\" id=\"" + imSrc[i].id + "\" width=\"1009\" height=\"567\">");
		$("#" + imSrc[i].id).css({ "position": "absolute", "visibility": "hidden", "top": "0", "left": "0", "boder": "none" });
		
		slideImages[i] = $("#" + imSrc[i].id);
	}
	
}


var linkCurrent;
var linkDefaultCss = [];
var linkBtMessageCss = [];
function changeFooterLink(index, fade)
{
	var id = slideElement[index]["id"];
	var alpha;
	if( fade == "out" ) alpha = 0;
	else if( fade = "in" ){
		
		alpha = 1;
		$("#link-bt-" + id).css({"display": "block"});
		
		for(var i = 0; i < slideElement.length; i++){
			var _id = slideElement[i]["id"];
			var _href = "";
			if( i == index ){
				_href = $("#" + _id + " a").attr("href");
				_href = _href + "#skip";
				$("#" + _id + " a").attr("href", _href);
			}else{
				_href = $("#" + _id + " a").attr("href");
				_href = _href.split("#")[0];
				$("#" + _id + " a").attr("href", _href);
			}
		}
		
	}
	
	//フッターのリンク
	if( alpha == 0 ){
		$("#" + id + " .footer-left-over-bg").animate({"backgroundPosition": linkDefaultCss[index].width + " 0"}, 2000);
		
		$("#link-bt-" + id + " .message").animate({"left": "90px"}, 500);
		try {
			$("#link-bt-" + id + " .background").delay(200).animate({"left": "20px"}, 500);
		}catch(e)
		{
		}
	}
	else if( alpha == 1 ){
		$("#" + id + " .footer-left-over-bg").css({"backgroundPosition": "-" + linkDefaultCss[index].width + " 0"});
		$("#" + id + " .footer-left-over-bg").animate({"backgroundPosition": "0 0"}, 3000);
		
		$("#link-bt-" + id + " .message").css({"left": "35px", "opacity": "0"});
		$("#link-bt-" + id + " .message").delay(2500).animate({"left": linkBtMessageCss[index].left, "opacity": "1"}, 500);
		
		$("#link-bt-" + id + " .background").css({"left": "-20px", "opacity": "0"});
		$("#link-bt-" + id + " .background").delay(2800).animate({"left": "0", "opacity": "1"}, 500);
	}
	
	$("#link-bt-" + id).stop().animate({"opacity": String(alpha), "left": "0px"}, 1000, function(){
		if( alpha == 0 ) $(this).css({"display": "none"});
		else if( alpha == 1 ) $(this).delay(750, function(){  });
	});
}

var slideElement = [
	{
		defaltSrc: "images/index_bt_03.png",
		overSrc: "images/index_bt_07.png",
		link: "./report.html",
		id: "report"
	},
	{
		defaltSrc: "images/index_bt_03.png",
		overSrc: "images/index_bt_07.png",
		link: "./artist.html",
		id: "artist"
	},
	{
		defaltSrc: "images/index_bt_04.png",
		overSrc: "images/index_bt_08.png",
		link: "./system.html",
		id: "system"
	},
	{
		defaltSrc: "images/index_bt_04.png",
		overSrc: "images/index_bt_08.png",
		link: "./extramovie.html",
		id: "extramovie"
	},
	{
		defaltSrc: "images/index_bt_01.png",
		overSrc: "images/index_bt_05.png",
		id: "about",
		link: "./abount.html"
	},
	{
		defaltSrc: "images/index_bt_02.png",
		overSrc: "images/index_bt_06.png",
		link: "./plan.html",
		id: "plan"
	}
]


//IE png
///////////////////////////////////////////////
function ie_set_png()
{
	if( !jQuery.browser.msie ) return;
	
	if(parseInt(jQuery.browser.version) == 6 ){
	
		DD_belatedPNG.fix('#imiev-car img');
		DD_belatedPNG.fix('#log');
		DD_belatedPNG.fix('#corner-left-top');
		DD_belatedPNG.fix('#corner-left-top img');
		DD_belatedPNG.fix('#corner-left-bottom');
		DD_belatedPNG.fix('#corner-left-bottom img');
		DD_belatedPNG.fix('#corner-right-top');
		DD_belatedPNG.fix('#corner-right-top img');
		DD_belatedPNG.fix('#corner-right-bottom');
		DD_belatedPNG.fix('#corner-right-bottom img');
		DD_belatedPNG.fix('#link-bt-container img');
		DD_belatedPNG.fix('#link-bt-container a');
		DD_belatedPNG.fix('#mixi a');
		DD_belatedPNG.fix('#background-light');
		DD_belatedPNG.fix('#background-cover');
		DD_belatedPNG.fix('#link-bt');
		
		
		DD_belatedPNG.fix('#link-bt-about');
		DD_belatedPNG.fix('#link-bt-about img');
		DD_belatedPNG.fix('#link-bt-plan');
		DD_belatedPNG.fix('#link-bt-plan img');
		DD_belatedPNG.fix('#link-bt-artist');
		DD_belatedPNG.fix('#link-bt-artist img');
		DD_belatedPNG.fix('#link-bt-system');
		DD_belatedPNG.fix('#link-bt-system img');
		
		DD_belatedPNG.fix('.footer-left-over-bg');
		DD_belatedPNG.fix('.footer-left-over');
		DD_belatedPNG.fix('.background');
		DD_belatedPNG.fix('.message');
		DD_belatedPNG.fix('.over');
		
		DD_belatedPNG.fix('#report-special-bt');
		$("#link-bt-title-img").fixPng();
	}else if( parseInt(jQuery.browser.version) == 8 || parseInt(jQuery.browser.version) == 7 )
	{
		$("#link-bt-title-img").fixPng();
	}
}