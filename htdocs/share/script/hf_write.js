
document.write('<script src="/share/script/mars/suggest_ext.ja_global.js" type="text/javascript" charset="UTF-8"></script>');

if(module.Browser('IE',6)) document.write('<scr'+'ipt type="text/javascript" src="/share/script/DD_belatedPNG_0.0.8a-min.js"></scr'+'ipt>');
document.write('<scr'+'ipt type="text/javascript" src="/jp/share/script/globalmenu.js"></scr'+'ipt>');

var nv_root_path="";

function global_header_write(){
var global_header = [];
var global_navibox = [];
global_header.push('<div id="header">');
global_header.push('<p id="logo"><a href="http://www.mitsubishi-motors.com/jp/"><img src="/share/images/header_logo.gif" alt="MITSUBISHI MOTORS" height="68" width="93" /></a></p>');
global_header.push('<div id="headerLinkBlock">');
global_header.push('<div class="serachBlock">');
global_header.push('<form method="get" action="http://search.mitsubishi-motors.com/ja_global/search.x" name="search_form"><input type="text" name="q" value="" id="MF_form_phrase" autocomplete="off" class="search"><input type="image" value="検索" class="button1" src="/jp/share/images/search.gif"><input type="hidden" value="UTF-8" name="ie"><input type="hidden" name="page" value="1"></form>');
global_header.push('<p id="fontSize">文字サイズの変更：<span id="fontNormal" class="fontNormal">小</span>&nbsp;<span id="fontLarge" class="fontLargeout">中</span>&nbsp;<span id="fontxLarge" class="fontxLargeout">大</span></p></div>');


global_header.push('<div id="linkWrapper">');
global_header.push('<ul class="linkHome">');
global_header.push('<li><a href="/jp/">ホーム</a></li>');
global_header.push('</ul>');
global_header.push('<ul class="linkLeft">');
global_header.push('<li><a href="http://www.mitsubishi-motors.co.jp/" target="_blank">Mitsubishi Motors Japan オフィシャルサイト</a></li>');
global_header.push('<li class="first"><a href="http://www.mitsubishi-motors.com/en/global_network/index.html">Global Network</a></li>');
global_header.push('</ul>');
global_header.push('<ul class="linkRight">');
global_header.push('<li><a href="http://www.mitsubishi-motors.com/en/">English</a></li>');
global_header.push('<li><a href="/jp/sitemap/index.html">サイトマップ</a></li>');
global_header.push('<li class="first"><a href="http://www.mitsubishi-motors.co.jp/reference/index.html" target="_blank">お問い合わせ</a></li>');
global_header.push('</ul>');
global_header.push('</div>');
global_header.push('</div>');
global_header.push('</div>');

global_navibox.push('<div id="globalNavigation">');

global_navibox.push('<ul id="mainnv"><li><a href="/jp/products/index.html"><img src="/jp/share/images/global_nv_03.gif" alt="商品情報" width="184" height="31" class="imageover" id="gnv_products" /></a></li>');
global_navibox.push('<li><a href="/jp/spirit/index.html"><img src="/jp/share/images/global_nv_02.gif" alt="三菱自動車のクルマづくり" width="185" height="31" class="imageover" id="gnv_spirit" /></a></li>');
global_navibox.push('<li><a href="/jp/social/index.html"><img src="/jp/share/images/global_nv_06.gif" alt="CSR・環境・社会" width="185" height="31" class="imageover" id="gnv_social" /></a></li>');
global_navibox.push('<li><a href="/jp/corporate/index.html"><img src="/jp/share/images/global_nv_04.gif" alt="企業情報・投資家情報" width="185" height="31" class="imageover" id="gnv_corporate" /></a></li>');
global_navibox.push('<li><a href="/jp/events/index.html"><img src="/jp/share/images/global_nv_05.gif" alt="ニュース・イベント" width="181" height="31" class="imageover" id="gnv_events" /></a></li>');
global_navibox.push('</ul>');
global_navibox.push('</div>');

document.writeln(global_header.join(''));
document.write(global_navibox.join(''));
}



function global_footer_write(chkflg){
var global_footer;

if(chkflg == 0){
global_footer = '<div class="linkArea"><p><a href="'+nv_root_path+'/jp/corporate/recruit/index.html">採用情報</a> | <a href="'+nv_root_path+'/jp/usage/index.html">サイトのご利用について</a> | <a href="'+nv_root_path+'/jp/privacy/index.html">個人情報保護方針</a> | <a href="'+nv_root_path+'/jp/socialmedia/index.html">ソーシャルメディア</a></p></div>';

}else{
global_footer = '<div id="footer"><div class="linkArea"><p><a href="'+nv_root_path+'/jp/corporate/recruit/index.html">採用情報</a> | <a href="'+nv_root_path+'/jp/usage/index.html">サイトのご利用について</a> | <a href="'+nv_root_path+'/jp/privacy/index.html">個人情報保護方針</a> | <a href="'+nv_root_path+'/jp/socialmedia/index.html">ソーシャルメディア</a></p></div><div class="copyrightArea"><p>(c) Mitsubishi Motors Corporation. All rights reserved.</p></div></div>';
}

document.write(global_footer);

}



function getAdobeReaderWrite(){
	var reader = '';
	
	reader +='<div class="GetAcrobat">';
	reader +='<p class="adobeLeft">Adobe Readerをお持ちでない方は<span class="outerLink"><a href="http://get.adobe.com/jp/reader/" onClick="openWin(\'/jp/share/popup_01.html?\'+this.href,\'popout\',\'\',\'\',\'5\')\;return false\;" onKeyPress="openWin(\'/jp/share/popup_01.html?\'+this.href,\'popwin\',\'\',\'\',\'5\')\;return false\;">Adobe社のサイト</a><img src="/share/images/blank_icon_01.gif" alt="別ウィンドウ表示" width="18" height="12" /></span>よりダウンロードのうえご利用ください。</p>';
	reader +='<p class="adobeRight">';
	reader +='<a href="http://get.adobe.com/jp/reader/" onClick="openWin(\'/jp/share/popup_01.html?\'+this.href,\'popout\',\'\',\'\',\'5\');return false;" onKeyPress="openWin(\'/jp/share/popup_01.html?\'+this.href,\'popwin\',\'\',\'\',\'5\');return false;"><img src="/share/images/get_adobe_reader.gif" alt="Get Adobe Reader" /></a></p></div>';

	document.write(reader);
}


(function(){
//cookie
var cookieobj = {};

cookieobj.setcookie  = function(setdata,setname,setdate){
	var xDay = new Date;
	xDay.setDate(xDay.getDate() + setdate);
	xDay = xDay.toGMTString();
	document.cookie = setname + "=" +escape(setdata) + ";expires=" + xDay + ";path=/";
}

cookieobj.getcookie  = function(getname){

  var myCookie = getname + "=";
  var myValue = null;
  var myStr = document.cookie + ";" ;
  var myOfst = myStr.indexOf(myCookie);
   if (myOfst != -1){
      var myStart = myOfst + myCookie.length;
      var myEnd   = myStr.indexOf(";" , myStart);
      var myValue = unescape(myStr.substring(myStart,myEnd));
   }
   return myValue;
   
}

if(navigator.cookieEnabled){
var fontstyle = cookieobj.getcookie("mmc_global_font"); 
}else{
var fontstyle = 'fontNormal';
}

var clflg = {'fontNormal' : false,'fontLarge' : false,'fontxLarge' : false};

	if(fontstyle == null){
		var currentSize = '100';
		fontstyle='fontNormal';
	}else{
		if(fontstyle == 'fontNormal'){
			var currentSize = '100';
		}else if(fontstyle == 'fontLarge'){
			var currentSize = '110';
		}else if(fontstyle == 'fontxLarge'){
			var currentSize = '120';
		}
	}
	
clflg[fontstyle] =  true;

document.writeln( '<style type="text/css"><!--' );
document.write( '#subBody {font-size:' + currentSize + '% }' );
document.writeln( '--></style>' );
		  	
var fontsizeobj = function(buttonID,targetArea){
	this.initialize.apply(this, arguments);
}

fontsizeobj.prototype = {
		
		initialize: function(buttonID,targetArea){
		
		this.targetElem = document.getElementById(buttonID);
		this.bt_normal = document.getElementById('fontNormal');
		this.bt_large = document.getElementById('fontLarge');
		this.bt_xlarge = document.getElementById('fontxLarge');
		this.allflg = true;
		this.fontarea = document.getElementById(targetArea);
		this.btflg = fontstyle;
		var getmaincl = document.getElementById(fontstyle);
		getmaincl.className = fontstyle + 'ov';
		
		},
		
		setevent:function(){
			
		var setsize = (function (fontsizeobj){
			return function(){
								
			clflg[fontsizeobj.btflg] = false;
			if(navigator.cookieEnabled) cookieobj.setcookie(this.id,"mmc_global_font",365);
			this.className = this.id + 'ov';
			if(this.id != fontsizeobj.btflg) document.getElementById(fontsizeobj.btflg).className = fontsizeobj.btflg + 'out';
			
			if(this.id == 'fontNormal'){
			fontsizeobj.fontarea.style.fontSize = '100%';
			}else if(this.id == 'fontLarge'){
			fontsizeobj.fontarea.style.fontSize = '110%';
			}else if(this.id == 'fontxLarge'){
			fontsizeobj.fontarea.style.fontSize = '120%';
			}
			
			fontsizeobj.btflg = this.id;
			
			clflg[this.id] = true;
		//	location.reload();
		
			}
		})(this);
		
		var setbtColor = (function (fontsizeobj){
			return function(){
			this.className = this.id + 'ov';
			
				if(winIE55){
					this.style.cursor = "hand";
				}
		
			}
		})(this);
		
		var setbtColor2 = (function (fontsizeobj){
			return function(){
			if(!clflg[this.id]) this.className = this.id + 'out';
			}
		})(this);
		
		addListener(this.bt_normal, 'click', setsize);
		addListener(this.bt_large, 'click', setsize);
		addListener(this.bt_xlarge, 'click', setsize);
		addListener(this.bt_normal, 'mouseover', setbtColor);
		addListener(this.bt_large, 'mouseover', setbtColor);
		addListener(this.bt_xlarge, 'mouseover', setbtColor);
		addListener(this.bt_normal, 'mouseout', setbtColor2);
		addListener(this.bt_large, 'mouseout', setbtColor2);
		addListener(this.bt_xlarge, 'mouseout', setbtColor2);
		
		var evdel = (function (fontsizeobj){
			return function(){
			remListener(fontsizeobj.bt_normal, 'click', setsize);
			remListener(fontsizeobj.bt_large, 'click', setsize);
			remListener(fontsizeobj.bt_xlarge, 'click', setsize);
			remListener(fontsizeobj.bt_normal, 'mouseover', setbtColor);
			remListener(fontsizeobj.bt_large, 'mouseover', setbtColor);
			remListener(fontsizeobj.bt_xlarge, 'mouseover', setbtColor);
			remListener(fontsizeobj.bt_normal, 'mouseout', setbtColor2);
			remListener(fontsizeobj.bt_large, 'mouseout', setbtColor2);
			remListener(fontsizeobj.bt_xlarge, 'mouseout', setbtColor2);
			}
		})(this);
		
		addListener(window, 'unload', evdel);
		
	
		//addListener(this.allcl, 'click', setcl);
		
		var setcl2 = (function (fontsizeobj){
			return function(e){
				if(e.type == 'mouseover'){
					this.style.color = "#ef0041"
					if(winIE55){
						this.style.cursor = "hand";
					}
				}else{
					this.style.color = "#151515"
				}
			}
		})(this);
		
	
		}
}

function fontsizefunc(){

	if(!document.getElementById('fontSize')) return;
	document.getElementById('fontSize').style.visibility = 'visible';
	
	var ev2 = new fontsizeobj('fontSize','subBody');
	ev2.setevent();
}


addListener(window, 'load', fontsizefunc);


})();

