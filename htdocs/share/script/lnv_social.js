
var _localload = (function(){
						   
var navi;
var langageCode = 'jp'
var categoryCode = 'social';

var path = langageCode +'/'+ categoryCode;

var navi = ['<div id="localNavigation"><div class="inner"><ul>'];
navi[navi.length] = '<li><a href="/'+path+'/index.html"><span>CSR・環境・社会トップ</span></a></li>';
navi[navi.length] = '<li id="social_message"><a href="/'+path+'/pdf/2-1_top_message_1208.pdf" target="_blank"><span>トップメッセージ</span></a></li>';

	navi[navi.length] = '<li id="csr"><a href="/'+path+'/csr/index.html"><span>CSRへの取り組み</span></a>';
	
		navi[navi.length] = '<ul>';
		navi[navi.length] = '<li id="csr_csr_promotion"><a href="http://as.xa.ad-traffic.jp/ADCLICK/CID=000005736E8679DB75B63EFE/HASH=1FQI0" target="_blank"><span>CSR推進</span></a></li>';
		navi[navi.length] = '<li id="csr_governance"><a href="/'+path+'/csr/governance.html"><span>コーポレート・ガバナンス</span></a></li>';
		navi[navi.length] = '<li id="csr_risk"><a href="http://as.xa.ad-traffic.jp/ADCLICK/CID=0000057379D26422873637A7/HASH=1FQI1" target="_blank"><span>内部統制システムとリスク管理</span></a></li>';
		navi[navi.length] = '<li id="csr_compliance"><a href="/'+path+'/csr/compliance.html"><span>コンプライアンス</span></a></li>';
		navi[navi.length] = '</ul></li>';
	

navi[navi.length] = '<li id="environment"><a href="/'+path+'/environment/index.html"><span>環境への取り組み</span></a>';

	navi[navi.length] = '<ul>';
	navi[navi.length] = '<li id="environment_policy"><a href="/'+path+'/environment/policy/index.html"><span>基本姿勢</span></a>';
	
		navi[navi.length] = '<ul>';
		navi[navi.length] = '<li id="environment_policy_index"><a href="/'+path+'/environment/policy/index.html"><span>三菱自動車　環境指針</span></a></li>';
		navi[navi.length] = '<li id="environment_policy_system"><a href="/'+path+'/environment/policy/system.html"><span>環境取り組み体制</span></a></li>';
		navi[navi.length] = '</ul></li>';
		
	navi[navi.length] = '<li id="environment_vision2020"><a href="/'+path+'/environment/vision2020.html"><span>環境ビジョン2020</span></a></li>';
	navi[navi.length] = '<li id="environmental_actionplan2015"><a href="/'+path+'/environment/pdf/1-1-2_environmental_action_plan2015_1208.pdf" target="_blank"><span>環境行動計画2015</span></a></li>';
	navi[navi.length] = '<li id="environment_manage"><a href="/'+path+'/environment/pdf/1-3_environmental_management_1208.pdf" target="_blank"><span>環境マネジメント</span></a>';
	navi[navi.length] = '<li id="environment_warming"><a href="/'+path+'/environment/pdf/1-4_against_global_warming_1208.pdf" target="_blank"><span>地球温暖化防止</span></a>';
	navi[navi.length] = '<li id="environment_pollution"><a href="/'+path+'/environment/pdf/1-5_against_environmental_pollution_1208.pdf" target="_blank"><span>環境汚染防止</span></a>';
	navi[navi.length] = '<li id="environment_recycle_production"><a href="http://as.xa.ad-traffic.jp/ADCLICK/CID=00000573466E6AA41E8499C6/HASH=1FQF4" target="_blank"><span>リサイクル・省資源</span></a>';
	navi[navi.length] = '<li id="environment_actionbiodiversity"><a href="/'+path+'/environment/pdf/1-1_action_for_biodiversity_1208.pdf" target="_blank"><span>生物多様性への取り組み</span></a>';
	navi[navi.length] = '<li id="area_activities"><a href="/'+path+'/environment/pdf/1-7_area_activities_1208.pdf" target="_blank"><span>各地区の取り組み</span></a>';
	navi[navi.length] = '<li id="environment_database"><a href="/'+path+'/environment/database/recycle/index.html"><span>環境データベース</span></a>';

		navi[navi.length] = '<ul>';
		navi[navi.length] = '<li id="environment_database_recycle"><a href="/'+path+'/environment/database/recycle/index.html"><span>リサイクル料金</span></a></li>';
		navi[navi.length] = '<li id="environment_database_carmodels"><a href="/'+path+'/environment/database/carmodels/index.html"><span>車種別環境情報</span></a></li>';
		navi[navi.length] = '<li id="environment_database_factorydata"><a href="/'+path+'/environment/database/factorydata/index.html"><span>製作所環境データ</span></a></li>';
		navi[navi.length] = '</ul></li>';
	navi[navi.length] = '</ul></li>';

	navi[navi.length] = '<li id="approach"><a href="/'+path+'/approach/index.html"><span>社会への取り組み</span></a>';
	
		navi[navi.length] = '<ul>';
		navi[navi.length] = '<li id="approach_customer"><a href="/'+path+'/pdf/2-3-1_with_customers_1208.pdf" target="_blank"><span>お客様とともに</span></a></li>';
		navi[navi.length] = '<li id="approach_sale_comp"><a href="/'+path+'/pdf/2-3-2_with_dealership_1208.pdf" target="_blank"><span>販売会社とともに</span></a></li>';
		navi[navi.length] = '<li id="approach_partner"><a href="/'+path+'/pdf/2-3-3_with_suppliers_1208.pdf" target="_blank"><span>調達パートナーとともに</span></a></li>';
		navi[navi.length] = '<li id="approach_stock"><a href="/'+path+'/pdf/2-3-4_with_investors_1208.pdf" target="_blank"><span>株主・投資家とともに</span></a></li>';
		navi[navi.length] = '<li id="approach_staff"><a href="/'+path+'/pdf/2-3-5_witih_local_community_1208.pdf" target="_blank"><span>地域の皆様とともに</span></a></li>';
		navi[navi.length] = '<li id="approach_region"><a href="/'+path+'/pdf/2-3-6_with_employee_1208.pdf" target="_blank"><span>社員とともに</span></a></li>';
		navi[navi.length] = '</ul></li>';
	
	navi[navi.length] = '<li id="ethics_com"><a href="/'+path+'/ethics_com/index.html"><span>企業倫理委員会</span></a></li>';
	navi[navi.length] = '<li id="contribution"><a href="/'+path+'/contribution/index.html"><span>社会貢献活動</span></a>';
	
		navi[navi.length] = '<ul>';
		navi[navi.length] = '<li id="contribution_consult"><a href="/'+path+'/contribution/consult.html"><span>小学生自動車相談室</span></a></li>';
		navi[navi.length] = '<li id="contribution_program"><a href="/'+path+'/contribution/program.html"><span>三菱自動車体験授業プログラム</span></a></li>';
		navi[navi.length] = '<li id="contribution_visit"><a href="/'+path+'/contribution/visit.html"><span>企業訪問学習</span></a></li>';
		navi[navi.length] = '<li id="contribution_kidzania"><a href="/'+path+'/contribution/kidzania.html"><span>キッザニア</span></a></li>';
		navi[navi.length] = '<li id="contribution_kids"><a href="/jp/discoveries/kids/index.html"><span>こどもクルマミュージアム</span></a></li>';
		navi[navi.length] = '<li id="contribution_kurumano_gakko"><a href="/'+path+'/contribution/kurumano_gakko.html"><span>クルマの学校</span></a></li>';
		navi[navi.length] = '<li id="contribution_pajero_forest"><a href="/'+path+'/contribution/pajero_forest.html"><span>パジェロの森</span></a></li>';
		navi[navi.length] = '<li id="contribution_factory"><a href="/'+path+'/contribution/factory.html"><span>工場見学</span></a></li>';
		navi[navi.length] = '<li id="contribution_others"><a href="/'+path+'/contribution/others.html"><span>その他の取り組み</span></a></li>';
		navi[navi.length] = '</ul></li>';
	
	navi[navi.length] = '</ul></li>';


navi[navi.length] = '</ul>';
navi[navi.length] = '</div></div>';		

return navi.join('');
})();


function localNavi_write(){

document.write(_localload);

//DOMready.tgFunc(localnaviStay);
}


