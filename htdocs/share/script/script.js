(function(){
  "use strict";
  $(window).on('load resize', function(){
    var _w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    if( _w < 960){
      $('.switchImg').each(function(i){
        $(this).attr('src',$(this).attr('src').replace('_pc','_sp'));
      });
    }else{
      $('.switchImg').each(function(i){
        var img = $(this).data('img');
        $(this).attr('src',$(this).data('img') );
      });
    }
  });

   var ua = navigator.userAgent;
    if(ua.indexOf('iPhone') > 0 && ua.indexOf('iPod') == -1 || ua.indexOf('Android') > 0 && ua.indexOf('Mobile') > 0 && ua.indexOf('SC-01C') == -1 && ua.indexOf('A1_07') == -1 ){
        $('.tel-link img').each(function(){
            var alt = $(this).attr('alt');
            $(this).wrap($('<a>').attr('href', 'tel:' + alt.replace(/-/g, '')));
        });
    }

    //scroll
    $('a[href^=#]').click(function() {
      var speed = 600;
      var href= $(this).attr("href");
      var target = $(href == "#" || href == "" ? 'html' : href);
      var position = target.offset().top;
      $('body,html').animate({scrollTop:position}, speed, 'swing');
      return false;
   });

})();
