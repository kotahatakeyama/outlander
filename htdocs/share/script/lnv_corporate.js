
var _localload = (function(){
						   
var navi;
var langageCode = 'jp'
var categoryCode = 'corporate';

var path = langageCode +'/'+ categoryCode;

var navi = ['<div id="localNavigation"><div class="inner"><ul>'];
navi[navi.length] = '<li><a href="/'+path+'/index.html"><span>企業情報・投資家情報トップ</span></a></li>';
navi[navi.length] = '<li id="philosophy"><a href="/'+path+'/philosophy/index.html"><span>経営の考え方</span></a>';

	navi[navi.length] = '<ul>';
	navi[navi.length] = '<li id="philosophy_principle"><a href="/'+path+'/philosophy/principle.html"><span>三菱グループ三綱領</span></a></li>';
	navi[navi.length] = '<li id="philosophy_philosophy"><a href="/'+path+'/philosophy/philosophy.html"><span>企業理念</span></a></li>';
	navi[navi.length] = '<li><a href="/'+langageCode+'/spirit/communication_word/index.html"><span>Drive＠earth</span></a></li>';
	navi[navi.length] = '</ul></li>';

navi[navi.length] = '<li id="aboutus"><a href="/'+path+'/aboutus/index.html"><span>三菱自動車の概要</span></a>';

	navi[navi.length] = '<ul>';
	navi[navi.length] = '<li id="aboutus_profile"><a href="/'+path+'/aboutus/profile/index.html"><span>企業プロファイル</span></a>';
	
		navi[navi.length] = '<ul>';
		navi[navi.length] = '<li id="aboutus_profile_baselist"><a href="/'+path+'/aboutus/profile/world.html"><span>拠点一覧</span></a></li>';
		navi[navi.length] = '</ul></li>';
	
	navi[navi.length] = '<li id="aboutus_director"><a href="/'+path+'/aboutus/director.html"><span>取締役紹介</span></a></li>';
	navi[navi.length] = '<li id="aboutus_facilities"><a href="/'+path+'/aboutus/facilities/index.html"><span>事業所・関連施設</span></a>';
	
		navi[navi.length] = '<ul>';
		navi[navi.length] = '<li id="aboutus_facilities_honsya"><a href="/'+path+'/aboutus/facilities/guide_map/honsya.html"><span>本社</span></a></li>';
		navi[navi.length] = '<li id="aboutus_facilities_tokyodesign"><a href="/'+path+'/aboutus/facilities/guide_map/tokyodesign.html"><span>東京デザインスタジオ</span></a></li>';
		navi[navi.length] = '<li id="aboutus_facilities_okazaki"><a href="/'+path+'/aboutus/facilities/guide_map/okazaki.html"><span>技術センター</span></a></li>';
		navi[navi.length] = '<li id="aboutus_facilities_okazaki_2"><a href="/'+path+'/aboutus/facilities/guide_map/okazaki_2.html"><span>EV技術センター</span></a></li>';
		navi[navi.length] = '<li id="aboutus_facilities_kyoto"><a href="/'+path+'/aboutus/facilities/guide_map/kyoto.html"><span>京都研究所</span></a></li>';
		navi[navi.length] = '<li id="aboutus_facilities_tokachi"><a href="/'+path+'/aboutus/facilities/guide_map/tokachi.html"><span>十勝研究所</span></a></li>';
		navi[navi.length] = '<li id="aboutus_facilities_nagoya"><a href="/'+path+'/aboutus/facilities/guide_map/nagoya.html"><span>名古屋製作所</span></a></li>';
		navi[navi.length] = '<li id="aboutus_facilities_mizushima"><a href="/'+path+'/aboutus/facilities/guide_map/mizushima.html"><span>水島製作所</span></a></li>';
		navi[navi.length] = '<li id="aboutus_facilities_gifu"><a href="/'+path+'/aboutus/facilities/guide_map/gifu.html"><span>パジェロ製造（株）</span></a></li>';
		navi[navi.length] = '<li id="aboutus_facilities_kyoto_factory"><a href="/'+path+'/aboutus/facilities/guide_map/kyoto_factory.html"><span>パワートレイン製作所 京都工場</span></a></li>';
		navi[navi.length] = '<li id="aboutus_facilities_shiga"><a href="/'+path+'/aboutus/facilities/guide_map/shiga.html"><span>パワートレイン製作所 滋賀工場</span></a></li>';
		navi[navi.length] = '</ul></li>';
		
	navi[navi.length] = '<li id="aboutus_subsidiary"><a href="/'+path+'/aboutus/subsidiary.html"><span>主要関連会社</span></a></li>';
	navi[navi.length] = '<li id="aboutus_history"><a href="/'+path+'/aboutus/history/1870/index.html"><span>沿革</span></a></li>';
	
	navi[navi.length] = '</ul></li>';


navi[navi.length] = '<li id="ir"><a href="/'+path+'/ir/index.html"><span>株主・投資家の皆様へ</span></a>';

	navi[navi.length] = '<ul>';
	navi[navi.length] = '<li id="ir_irnews"><a href="/'+path+'/ir/irnews/index.html"><span>適時開示情報</span></a></li>';
	navi[navi.length] = '<li id="ir_corpmanage"><a href="/'+path+'/ir/corpmanage/index.html"><span>企業・経営情報</span></a>';
	
		navi[navi.length] = '<ul>';
		navi[navi.length] = '<li id="ir_corpmanage_topmessage"><a href="/'+path+'/ir/corpmanage/topmessage.html"><span>トップメッセージ</span></a></li>';
		navi[navi.length] = '<li id="ir_corpmanage_mp"><a href="/'+path+'/ir/corpmanage/mp.html"><span>中期経営計画 </span></a></li>';
		navi[navi.length] = '<li><a href="/'+path+'/philosophy/index.html"><span>経営の考え方</span></a></li>';
		navi[navi.length] = '<li><a href="/'+path+'/aboutus/profile/index.html"><span>企業プロファイル</span></a></li>';
		navi[navi.length] = '<li><a href="/'+path+'/social/csr/governance.html"><span>コーポレート・ガバナンス</span></a></li>';
		navi[navi.length] = '<li id="ir_corpmanage_guideline"><a href="/'+path+'/ir/corpmanage/guideline.html"><span>重要情報の適時開示に関する指針</span></a></li>';
		navi[navi.length] = '</ul></li>';	
	
	navi[navi.length] = '<li id="ir_finance_result"><a href="/'+path+'/ir/finance_result/index.html"><span>業績・財務情報</span></a>';
	
		navi[navi.length] = '<ul>';
		navi[navi.length] = '<li id="ir_finance_result_financial"><a href="/'+path+'/ir/finance_result/financial/index.html"><span>決算情報</span></a></li>';
		navi[navi.length] = '<li><a href="/'+path+'/ir/library/pdf/actual_annual_summary.pdf" target="_blank"><span>業績・財務サマリー</span></a></li>';
		navi[navi.length] = '<li id="ir_finance_result_result"><a href="/'+path+'/ir/finance_result/result.html"><span>生産・販売・輸出実績</span></a></li>';
		navi[navi.length] = '<li id="ir_finance_result_grading"><a href="/'+path+'/ir/finance_result/grading.html"><span>格付情報</span></a></li>';
		navi[navi.length] = '</ul></li>';
	
	navi[navi.length] = '<li id="ir_stockinfo"><a href="/'+path+'/ir/stockinfo/index.html"><span>株式・株価情報</span></a>';
	
		navi[navi.length] = '<ul>';
		navi[navi.length] = '<li id="ir_stockinfo_stock_status"><a href="/'+path+'/ir/stockinfo/stock_status.html"><span>株式の状況</span></a></li>';
		navi[navi.length] = '<li><a href="http://www.bloomberg.co.jp/apps/quote?T=jp09/quote.wm&ticker=7211%3AJP" target="_blank"><span>株価情報 </span></a></li>';
		navi[navi.length] = '<li id="ir_stockinfo_meeting"><a href="/'+path+'/ir/stockinfo/meeting.html"><span>株主総会</span></a></li>';
		navi[navi.length] = '<li id="ir_stockinfo_koukoku"><a href="/'+path+'/ir/stockinfo/koukoku.html"><span>電子公告</span></a></li>';
		navi[navi.length] = '<li id="ir_stockinfo_procedure"><a href="/'+path+'/ir/stockinfo/procedure.html"><span>株式手続きのご案内</span></a></li>';
		navi[navi.length] = '</ul></li>';
	
	navi[navi.length] = '<li id="ir_library"><a href="/'+path+'/ir/library/index.html"><span>IRライブラリー</span></a>';
	
		navi[navi.length] = '<ul>';
		navi[navi.length] = '<li id="ir_library_anual"><a href="/'+path+'/ir/library/anual.html"><span>アニュアルレポート</span></a></li>';
		navi[navi.length] = '<li id="ir_library_fact"><a href="/'+path+'/ir/library/fact.html"><span>ファクトブック</span></a></li>';
		navi[navi.length] = '<li><a href="/'+path+'/social/report/index.html"><span>社会・環境報告書</span></a></li>';
		navi[navi.length] = '<li><a href="/'+path+'/ir/finance_result/financial/index.html"><span>決算情報</span></a></li>';
		navi[navi.length] = '<li id="ir_library_yuka"><a href="/'+path+'/ir/library/yuka/index.html"><span>有価証券報告書</span></a></li>';
		navi[navi.length] = '<li id="ir_library_tsushin"><a href="/'+path+'/ir/library/tsushin.html"><span>株主通信</span></a></li>';
		navi[navi.length] = '</ul></li>';

	navi[navi.length] = '<li id="ir_mail"><a href="/'+path+'/ir/mail/index.html" target="_blank"><span>IRニュースメール</span></a></li>';
	navi[navi.length] = '<li id="ir_event"><a href="/'+path+'/ir/event/index.html"><span>IRイベント</span></a></li>';	
	navi[navi.length] = '<li id="ir_calendar"><a href="/'+path+'/ir/calendar/index.html"><span>IRカレンダー</span></a></li>';
	navi[navi.length] = '</ul></li>';

navi[navi.length] = '<li id="recruit"><a href="/'+path+'/recruit/index.html"><span>採用情報</span></a></li>';
navi[navi.length] = '<li id="pressrelease"><a href="/publish/pressrelease_jp/index.html"><span>プレスリリース</span></a></li>';


navi[navi.length] = '</ul>';
navi[navi.length] = '</div></div>';		

return navi.join('');
})();


function localNavi_write(){

document.write(_localload);

//DOMready.tgFunc(localnaviStay);
}


