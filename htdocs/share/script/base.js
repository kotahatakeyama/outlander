﻿
/* browser & os check */

var ua = navigator.userAgent.toUpperCase();
var apver = navigator.appVersion.toUpperCase();
var apnm = navigator.appName.toUpperCase();

var mac = apver.indexOf("MAC",0) >= 0;
var windows = apver.indexOf("WIN",0) >= 0;
//var xp = ua.match(/NT 5\.1|XP/);

if (typeof document.documentElement.style.msInterpolationMode != "undefined") {
  // IE 7 or newer 
  var IE_newer = true;
}
var ie  = apnm.indexOf("MICROSOFT INTERNET EXPLORER",0) >= 0;
var nn  = apnm.indexOf("NETSCAPE",0) >= 0;
var gecko = ua.indexOf("GECKO",0) >= 0;
var geckoNN = (gecko && ua.indexOf("NETSCAPE",0) >= 0);
var firefox = (gecko && ua.indexOf("FIREFOX",0) >= 0);
var safari = ua.indexOf("SAFARI",0) >= 0;
var opera = window.opera;
var apvernum = parseInt(apver);
//var ver = parseInt(navigator.appVersion); // ex. 3

var nn4  = ((nn && apvernum <= 4));
var winIE = ((windows && ua.indexOf('MSIE',0) >= 0));
var winIE55 = ((windows && ua.indexOf('MSIE 5.5',0) >= 0));
var macIE5 = ((mac && ua.indexOf('MSIE 5.',0) >= 0));

if(safari){
var ver = navigator.userAgent.split("/");
var ver = ua.split("/");
var vernum = ver[ver.length-1];
vernum = vernum.replace(/\./g,'');
vernum = vernum.slice(0,3);
var n = parseInt(vernum,10)
if (n > 412 && n < 522){
	var sv = "2";
}else if (n >= 522) {
	var sv = "3";
}
}


if (!Array.prototype.forEach) {
  Array.prototype.forEach = function(fun /*, thisp*/) {
    var len = this.length;
    if (typeof fun != "function") throw new TypeError();

    var thisp = arguments[1];
    for (var i = 0; i < len; i++){
      if (i in this) fun.call(thisp, this[i], i, this);
    }
  };
}


try{
document.execCommand('BackgroundImageCache', false, true);
}catch(e){}


var impath="http://www.mitsubishi-motors.com/share/images/";

document.write('<meta http-equiv="Imagetoolbar" content="no">');

/* window open */
function openWin(url,title,wdh,hgt,opt) {
	var win;
	if (url) {
		if (!title) title = "_blank";
		if (!opt) {
			var opti = "toolbar=yes,location=yes,directories=yes,status=yes,menubar=yes,scrollbars=yes,resizable=yes,favorites=yes";
		} else if (opt=="1") {
			var opti = "toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=no,resizable=yes,favorites=no";
		} else if (opt=="2") {
			var opti = "toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,favorites=no";
		} else if (opt=="3") {
			var opti = "toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=no,favorites=no";
		} else if (opt=="4") {
			<!-- MMTV -->
			var opti = "toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,favorites=no";
			wdh = "950";
			hgt = "635";
		} else if (opt=="5") {
			<!-- outerlink popup -->
			var opti = "toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=no,resizable=yes,favorites=no";
			wdh = "595";
			hgt = "476";
		} 
		
		if(!!wdh&&!!hgt) opti+=",width="+wdh+",height="+hgt;
		win = window.open(url,title,opti);
		win.focus();
	}
}


//page print
function pgprint(){
window.print();
}

//pulldown
function pulldownLink(){
	module.$$('pullDownNavi').forEach(function(obj2){
		
		var selecttag = obj2.getElementsByTagName('select');
		
		for(var i=0;i<selecttag.length;i++){
		addListener(selecttag[i],'change',function(){
		
			if(this.selectedIndex != 0) location.href = this.options[this.selectedIndex].value;
		
		});
		}								   
	});
}
addListener(window, 'load', pulldownLink);

//event Listener
function addListener(target, type, func) {
  if(target.attachEvent) {
    target.attachEvent("on" + type, function() {func.call(target, window.event);});
  } else if(target.addEventListener) {
    target.addEventListener(type, func, false);
  } else {
    target["on" + type] = func;
  }
}

function remListener(target, type, func) {
  if(target.attachEvent) {
    target.detachEvent("on" + type, func);
  } else if(target.addEventListener) {
    target.removeEventListener(type, func, false);
  }
}



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

var module = {};

module.UA = (function(){
	var doc = document;
	var ua = navigator.userAgent.toUpperCase();
	var apnm = navigator.appName.toUpperCase();
	var apver = navigator.appVersion;

	var barray = [];
	
	var mac = ua.indexOf("MAC",0) >= 0;
	var windows = ua.indexOf("WIN",0) >= 0;
	
	if(windows){
		barray['os'] = 'windows';
	}else if(mac){
		barray['os'] = 'mac';
	}else{
		barray['os'] = 'other'
	}
	
	if(window.attachEvent && !window.opera){
		barray['name'] = 'IE';
		
		if (typeof doc.documentElement.style.msInterpolationMode != 'undefined') {
			if(typeof document.documentMode != 'undefined') {
			barray['ver'] = '8';
			}else{
			barray['ver'] = '7';
			}
		}else{
			if(windows && ua.indexOf('MSIE 6',0) >= 0){
				barray['ver'] = '6';
			}else{
				barray['ver'] = '5';	
			}
		}
	}else{	

	if(ua.indexOf("SAFARI",0) >= 0 && ua.indexOf('APPLEWEBKIT/',0) >= 0){
		
			barray['name'] = 'Safari';
		
			barray['ver'] = (function(){
				var ver = ua.split("/");
				var vernum = ver[ver.length-1];
				vernum = vernum.replace(/\./g,'');
				vernum = vernum.slice(0,3);
				var n = parseInt(vernum,10)
				if (n >= 412 && n < 522){
				return '2';
				}else if (n >= 522) {
				return '3';
				}else{
				return '1';
				}
				})();
	}else if(ua.indexOf('GECKO',0) >= 0 && ua.indexOf('FIREFOX',0) >= 0){
		barray['name'] = 'Firefox';
		var ffua = ua.split('/');
		var ffver = parseInt(ffua[ffua.length-1].slice(0,1),10);	
		if (typeof window.postMessage != 'undefined' && ffver >= 3) {
			barray['ver'] = '3';		
		}else if(ffver == '2'){
			barray['ver'] = '2';
		}else{
			barray['ver'] = '1';
		}
		
	}else if(window.opera){
		barray['name'] = 'Opera';
	}else if(ua.indexOf('NETSCAPE',0) >= 0){
		barray['name'] = 'Netscape';
		
		if(apver == '5'){
			barray['ver'] = '6';
		}
	}else{
		barray['name'] = 'unknown';
	}
	
	}
	
	return barray;
})();


module.Browser = function(target,version){
	
	if(typeof target == 'undefined') return null;
	if(!module.stringChk(target)) return null;
	
	if(typeof version == 'undefined') var ver = false;
	else var ver = true; 
	
	var nm = module.UA.name.toUpperCase();
	var target = target.toUpperCase();
	
	if(nm.search(target) != -1) {
		
		if(ver){
			
			if(module.UA.ver.search(version,'i') != -1){
			return true;
			}else{
			return false;
			}		
		}
		return true;	
	}else{
		return false;
	}
	
} 



module.DOMready = {
	
	tgBox : [],
	flagbox : false,
	loadbox : false,
	
	tgFunc : function(tgfun){
		if(module.DOMready.loadbox){
			tgfun.call();
		}else{	
			module.DOMready.tgBox.push(tgfun);
			if(!module.DOMready.flagbox) module.DOMready.domFunc();
		}
	},

	domFunc : function() {
	var doc=document;
	module.DOMready.flagbox = true;
	
	if(module.Browser('Firefox',3)){
		module.Event.add(window,"DOMContentLoaded",module.DOMready.comp);
	}else if(module.UA.name == 'IE' && window == top){
		try {
			doc.documentElement.doScroll("left");
		} catch(error){
			setTimeout(arguments.callee,0);
			return;
		}
		module.DOMready.comp();
	}else if(module.Browser('Safari')){	
		if(doc.readyState != "loaded" && doc.readyState != "complete" ) {
			setTimeout(arguments.callee,0);
			return;
		}
		var stnum = doc.getElementsByTagName('style').length;
		var linkstyle = doc.getElementsByTagName('link');
		for(var d=0;d<linkstyle.length;d++){
			if(linkstyle[d].rel == 'stylesheet'){
				stnum++;
			}
		}
		if (doc.styleSheets.length != stnum) {
			setTimeout(arguments.callee,0);
			return;
		}
		module.DOMready.comp();
	}else{
		module.Event.add(window, "load", module.DOMready.comp);
	}
	},
	
	comp : function(){
	module.DOMready.tgBox.forEach(function(obj){
		obj.call();
	});
	module.DOMready.tgBox = null;
	module.DOMready.domFunc = null;
	},
	
	flag : function(){
	module.DOMready.loadbox = true;
	}
}



/* Event */
module.Event= {
	add : (function() {
    if (window.addEventListener) {
        return function(element, event, func) {
            element.addEventListener(event,func,false);
        };
    } else if (window.attachEvent) {
        return function(element, event, func) {
			if(!func._argument) {
                func._argument = [3];
            }
            var i = func._argument.length;
            func._argument[i] = new Array(3);
            func._argument[i][0] = element;
            func._argument[i][1] = event;
            func._argument[i][2] = function() {
            func.apply(element, arguments);
           };
  			element.attachEvent('on' + event, func._argument[i][2]);
        };
    } else {
        return function(element, event, func) {
            element['on'+event] = func;
        }
    }
	})(),

	remove : (function() {
		if (window.removeEventListener) {
			return function(element, event, func) {
				element.removeEventListener(event, func, false);
			}
		} else {
			return function(element, event, func) {
				var i = 0; var f = null;
           		while( i < func._argument.length ) {
                if(func._argument[i][0] == element && func._argument[i][1] == event ) {
                    f = func._argument[i][2];
                    break;
                }
                i++;
            	}
  			  	element.detachEvent("on"+event, f);
   				func._argument.splice(i,1);
			}
		}
	})(),
	
	stopbubble : function (e){	
	if (e.target) { 
     e.stopPropagation(); 
	}else if (window.event.srcElement) { 
     window.event.cancelBubble = true; 
   	}  	
	},
	
	stopevent : function (e){	
	if (e.target) { 
	 e.preventDefault(); 
	}else if (window.event.srcElement) { 
	window.event.returnValue=false;
   	}
	}
}


/* !!!!!!!!!! getID start !!!!!!!!!!! */
module.$ = function(IDS){
	var doc = document;
	if(module.stringChk(IDS)) return doc.getElementById(IDS);
	else return null;
}
/* !!!!!!!!!! getId end !!!!!!!!!!! */


/* !!!!!!!!!! getClass start !!!!!!!!!!! */
module.$$2 = function(CLS,element){
	
	if(!module.stringChk(CLS)) return;
	
	//if(!document.getElementsByClassName){
	var cl5 =[];
	var sp = /\s+/;
	
	module.each(module.getElementsByTagNameArray('*',element),function(obj){
		var classes = obj.getAttribute('class') || obj.getAttribute('className');
		
		if(classes){
	
			var classnm = classes.split(sp);
			var clsArray = CLS.split(sp);
			var clsstay;
			var clearflg = 0;
			
			module.each(classnm,function(clobj){
				if(CLS.indexOf(' ') != -1 && (CLS.lastIndexOf(' ') != CLS.length-1)){			
				for(var k=0;k<clsArray.length;++k){
					if(clsArray[k] == clobj){
						clearflg++;
						break;
					}
				}
				if(clearflg == clsArray.length){
						cl5[cl5.length] = obj;
				}
				}else{
				
				if(CLS.lastIndexOf(' ') == CLS.length-1){
					CLS = CLS.substring(0,CLS.length-1);
				}
				if (CLS == clobj){
					cl5[cl5.length] = obj;
				}
				}
			});
		}
	})
	//}else{
		
	/*if(typeof element == 'undefined') var doc = document;
	else var doc = element;
	
	var cl5 = doc.getElementsByClassName(CLS);
	
	}*/
	return cl5
}
module.$$ = function(CLS,element){
	
	if(!module.stringChk(CLS)) return;
	
	//if(!document.getElementsByClassName){
	var cl5 =[];
	var sp = /\s+/;
	
	module.each(document.getElementsByTagName('*'),function(obj){
		var classes = obj.getAttribute('class') || obj.getAttribute('className');
		
		if(classes){
	
			var classnm = classes.split(sp);
			var clsArray = CLS.split(sp);
			var clsstay;
			var clearflg = 0;
			
			module.each(classnm,function(clobj){
				if(CLS.indexOf(' ') != -1 && (CLS.lastIndexOf(' ') != CLS.length-1)){			
				for(var k=0;k<clsArray.length;++k){
					if(clsArray[k] == clobj){
						clearflg++;
						break;
					}
				}
				if(clearflg == clsArray.length){
						cl5[cl5.length] = obj;
				}
				}else{
				
				if(CLS.lastIndexOf(' ') == CLS.length-1){
					CLS = CLS.substring(0,CLS.length-1);
				}
				if (CLS == clobj){
					cl5[cl5.length] = obj;
				}
				}
			});
		}
	})
	//}else{
		
	/*if(typeof element == 'undefined') var doc = document;
	else var doc = element;
	
	var cl5 = doc.getElementsByClassName(CLS);
	
	}*/
	return cl5
}

/* !!!!!!!!!! getClass end !!!!!!!!!!! */
module.each = function(array,func){
	var i=0,lng = array.length;
	for(;i<lng;++i) func(array[i],i);
}

module.stringChk = function(object){
	if(typeof object == 'string' || object instanceof String) return object;
	return null;
}


module.getElementsByTagNameArray = function(tagname,element){
	if(typeof element == 'undefined') var doc = document;
	else var doc = element;

	if(doc == null)  var doc = document;
	
	var tagAll = [];
	if(!module.stringChk(tagname)) return;
	var getname = doc.getElementsByTagName(tagname);
	var len = getname.length;
	
	while(len--) tagAll[tagAll.length] = getname[tagAll.length];
	
	return tagAll;
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//   roll over   //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
module.ImgOverSetting = {
	imgfileNamePlus : '_a',
	imgfileNameStay : '_a'
}

module.Imgsrc = {

	add : function (objsrc,plusnm){
		if(objsrc.indexOf(plusnm) != -1) return objsrc;
		var ftype = objsrc.substring(objsrc.lastIndexOf('.'), objsrc.length);
		var presrc = objsrc.replace(ftype, plusnm + ftype);
		return presrc;
	},
	
	del : function (objsrc,plusnm){
		if(objsrc.indexOf(plusnm) == -1) return objsrc;
		var ftype = objsrc.substring(objsrc.lastIndexOf('.'), objsrc.length);
		var presrc = objsrc.replace(plusnm + ftype, ftype);
		return presrc;
	}
}

var setimgover = function (){
	
	
	//var imgtag = document.getElementsByTagName('img');
	var imgtag = module.getElementsByTagNameArray('img');
//imgtag = Array.prototype.slice.call(imgtag,arguments);

	setimgover.prefimg = [];
	if(imgtag.length == 0) return;
	
	var getURL = location.pathname;
	var path = getURL.split('/');
	//
	
	var bodyClass = document.getElementsByTagName('body')[0].className;
	
	imgtag.forEach(function(elm, index, array){
//		if('gnv_' + path[2] == elm.id || (bodyClass == 'jp_home' && elm.id=='gnv_home')) {
		if(('gnv_' + path[2] == elm.id && path[3] != 'pressrelease') || (bodyClass == 'jp_home' && elm.id=='gnv_home') ||	
			(path[3] == 'pressrelease' && elm.id == 'gnv_events') ||
			(path[2] == 'pressrelease_jp' && elm.id == 'gnv_events') ||
			(path[2] == 'ir_jp' && elm.id == 'gnv_corporate') ||
			(path[2] == 'pressrelease_en' && elm.id == 'gnv_events') ||
			(path[2] == 'ir_en' && elm.id == 'gnv_corporate')) {
					elm.className = 'gnv_stay';
					elm.src= module.Imgsrc.add(elm.src, module.ImgOverSetting.imgfileNameStay);
		}
		
		
		var classnm  = elm.className.split(' ');
		
		classnm.forEach(function(elm2, index2, array2){
			if(elm2 == 'imageover'){	
				var fimg = elm.src; 
				setimgover.prefimg[index] = new Image();
    			setimgover.prefimg[index].src =  module.Imgsrc.add(fimg, module.ImgOverSetting.imgfileNameStay);
				var imgoverObj = new imgoverfunc(elm,setimgover.prefimg[index]);
				imgoverObj.evset();
			}
			
			if(elm2 == 'printBtn'){
				elm.style.cursor = 'pointer';
				addListener(elm, 'click', pgprint);
			}
		});
	});
}

var imgoverfunc = function(imgelm,oversrc){
	this.initialize.apply(this, arguments);
}
imgoverfunc.prototype = {
	
	initialize : function(imgelm,oversrc){
		this.nowelm = imgelm;
		this.oversrc = oversrc;
		this.basesrc = this.nowelm.src;
	},
	
	evset : function(){
		var funcoverout = (function(imgoverfunc){
		return function(e){
			if(e.type == 'mouseover'){
				this.src = imgoverfunc.oversrc.src;
			}else if(e.type == 'mouseout' || e.type == 'click'){
				this.src = imgoverfunc.basesrc;
			}
		}
		})(this);

		addListener(this.nowelm, 'mouseover', funcoverout);
		addListener(this.nowelm, 'mouseout', funcoverout);
	
	}
	
}
//----------------------------------------------------------//
//　imgover
//----------------------------------------------------------//

module.DOMready.tgFunc(setimgover);

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

var objLine = function(domElm){
	var ddlng = domElm.length;
	if(ddlng <= 1) return;
	var zz = 0;
	for(var k=0;k<ddlng;++k){
			//if(ie && !IE7) domElm[k].style.height = 0;
			//else domElm[k].style.minHeight = 0;
			if(zz <= domElm[k].offsetHeight){
			zz = domElm[k].offsetHeight;
			}
	}
	
	for(var k=0;k<ddlng;++k){
			var ddst = domElm[k].style;
			if(ie && !IE_newer) ddst.height = zz + 'px';
			else ddst.minHeight = (zz +1) + 'px';
	}
}

var divLine2nd = function(){
	
	
	var arealine = module.$$2('areaLine',module.$('mainContents'));
	
	arealine.forEach(function(obj){
		
		var sublink = module.$$2('subLinks',obj);
		if(sublink.length > 1) objLine(sublink);
		

		if(document.getElementById('hierarchical_2nd')){
		var secinner = module.$$2('areaBox_txt',obj);
		if(secinner.length > 1) objLine(secinner);
		}
		
		if(document.getElementById('hierarchical_3rd')){
		var secinner = module.$$2('textBlock',obj);
		var p=[];
		
		secinner.forEach(function(obj2){				  
			p.push(obj2.getElementsByTagName('p')[0]);			  
		});		
		if(p[0])objLine(p);
		}
		
		//var sectxt = [];
		
		//for(var i=0;i<secinner.length;++i){
		//	sectxt.push(secinner[i]);	
		//}
		

		
							  
	});
	
	}	


//addListener(window, 'load', divLine2nd);
module.DOMready.tgFunc(divLine2nd);


///  //////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

var pulldownSetting = function(){
	var pulldownover = function(){
		module.$$2('pullDownInner',this.parentNode)[0].style.display = 'block';			
	}

	var pulldownout = function(){
		module.$$2('pullDownInner',this.parentNode)[0].style.display = 'none';			
	}
	
	var pulldownElm = module.$$2('pullDownBlock');
	
	if(pulldownElm.length <= 0) return;
				
		pulldownElm.forEach(function(obj){
		
		var obelm = module.$$2('pullDownInner',obj)[0];
		
		module.$$2('pullDownInner',obj)[0].style.display = 'none';	
		obj.style.visibility = 'visible';	
			
		var hobj = module.$$2('h',obj)[0];
		
		
		module.Event.add(hobj, "click", pulldownover);
		//module.Event.add(hobj, "click", pulldownover);
		
		module.Event.add(hobj, "mouseover", function(){
															this.style.color = '#ef0041';		   
															});
		module.Event.add(hobj, "mouseout", function(){
															this.style.color = '#151515';		   
															});
		module.Event.add(obelm, "mouseover", pulldownover);	
		module.Event.add(obelm, "mouseout", pulldownout);		
		});
		

}
//addListener(window, 'load', pulldownSetting);
module.DOMready.tgFunc(pulldownSetting);



/// localnavi stay //////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

var localnaviStay = function(){

	if(document.getElementById('localNavigation')){
		
		var bodyClass = module.$('subBody').className;
		
		
		var bodycl  = bodyClass.split(' ');
		
		if(bodycl[0]) {
			
			module.$(bodycl[0]).className="stay";		

			if(bodycl[1]) {
				
				if(bodycl[0] == 'environment' && (bodycl[1] == 'recycle' || bodycl[1] == 'carmodels' || bodycl[1] == 'factorydata')){
							module.$(bodycl[0]+'_database').className="stay";
				}else{
					if(module.$(bodycl[0]+'_'+bodycl[1])) module.$(bodycl[0]+'_'+bodycl[1]).className="stay";
				}
				
				if(bodycl[2]) {
					
					if(bodycl[0] == 'environment' && (bodycl[1] == 'recycle' || bodycl[1] == 'carmodels' || bodycl[1] == 'factorydata')){
						module.$(bodycl[0]+'_database_'+bodycl[1]).className="stay";
					}else{
						if(module.$(bodycl[0]+'_'+bodycl[1]+'_'+bodycl[2])) module.$(bodycl[0]+'_'+bodycl[1]+'_'+bodycl[2]).className="stay";
					}
					
				}
				
			}
		}
	}
}
addListener(window, 'load', localnaviStay);
//module.DOMready.tgFunc(localnaviStay);



var adtrafficTrack;
var adtrafficStartUp = function(){
  adtrafficTrack = adtrafficUserTrack._getTracker("2s");
  adtrafficTrack._setPageId("");
  adtrafficTrack._customParam = [
  ];
}
function xadimg(){
	
if(document.getElementById('pop') || document.getElementById('headerpop')){
  adtrafficTrack._trackFlashRequest();
}

}

