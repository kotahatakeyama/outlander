var _isSp = false;
var _isIe9 = false;
var _isIe8 = false;
var _isIe = false;
var _isLoad = false;
var _isSpSize = true;
var loadPercent = 0;
var nowPercent = 0;
var _touchEvent = 'click';
var _iosHeight;
var _isOpen = false;
var _openNum = 0;
var _fileArr = [];
var _imgArr = [];
var _length = -1;
var _thumbLength = -1;
var _listLength = 0;
var _isAnimate = false;
var _currentOtherCarNum = 0;
var _otherLength;
var _isAd = false;

var Project = function()
{
}

Project.prototype = {
    imagesArr : [
        "/en/spirit/design/share/images/lg.png",
    ],
    init:function(argument)
    {
        //UA
        var _ua = window.navigator.userAgent.toLowerCase();
        //バージョン
        var _appVer = window.navigator.appVersion.toLowerCase();

        if((_ua.indexOf("windows") != -1 && _ua.indexOf("touch") != -1)
            || _ua.indexOf("ipad") != -1
            || (_ua.indexOf("android") != -1 && _ua.indexOf("mobile") == -1)
            || (_ua.indexOf("firefox") != -1 && _ua.indexOf("tablet") != -1)
            || _ua.indexOf("kindle") != -1
            || _ua.indexOf("silk") != -1
            || _ua.indexOf("playbook") != -1
            || (_ua.indexOf("windows") != -1 && _ua.indexOf("phone") != -1)
            || _ua.indexOf("iphone") != -1
            || _ua.indexOf("ipod") != -1
            || (_ua.indexOf("android") != -1 && _ua.indexOf("mobile") != -1)
            || (_ua.indexOf("firefox") != -1 && _ua.indexOf("mobile") != -1)
            || _ua.indexOf("blackberry") != -1)  _isSp = true;
        
         if(_ua.indexOf('msie') != -1){
            if (_appVer.indexOf("msie 6.") != -1 || _appVer.indexOf("msie 7.") != -1 || _appVer.indexOf("msie 8.") != -1) _isIe8 = true;
            if (_isIe8 || _appVer.indexOf("msie 9.") != -1) _isIe9 = true;
         };

         if(_appVer.indexOf("msie 10.") != -1 || _isIe9) _isIe = true;

         if((_ua.indexOf("android") != -1 && _ua.indexOf("mobile") == -1)|| (_ua.indexOf("android") != -1 && _ua.indexOf("mobile") != -1)) _isAd = true;


        //スクロール無効化
        $("body").css({overflow:'hidden'})
        $(window).on('touchmove.noScroll', function(e) {
            e.preventDefault();
        });

        //先読み画像
        if(!_isIe9)
        {
            var images = [].concat(TOP.imagesArr);
            TOP.preloadImage(images);

            _iosHeight = TOP.getBrowserHeight();
            TOP.layout();

            //ローダー表示
            $("#loader").show();
            $("#loader p.indi").html(parseInt(0));
        }
        else
        {
            $("#loader").hide();
            TOP.loadComplete();
        }

        //resize
        $(window).on("resize", function(){ TOP.layout(); });
    },
    //画像の読み込み
    preloadImage:function(imageArray)
    {
        function preload(images, progress){
            var total = images.length;
            $(images).each(function()
            {
                var src = this;
                var img = $('<img/>');
                img.attr({'src' : src}).load(function()
                {
                    for(var i = 0; i < images.length; i++){
                        if (images[i] == src) images.splice(i, 1);
                    };
                    progress(total, total - images.length);
                });
            });
        }
        
        preload(imageArray, function(total, loaded){
            nowPercent = Math.ceil(100 * loaded / total);
        });
        _loadTime = setInterval(TOP.progress, 10);
    },
    progress:function(per)
    {
        if(loadPercent < nowPercent) loadPercent = nowPercent;
        $("#loader p.indi").html(parseInt(loadPercent));

        if(nowPercent >= 100 && loadPercent >= nowPercent) TOP.loadComplete();
    },
    loadComplete:function()
    {
        _isLoad = true;
        if(!_isIe9) clearInterval(_loadTime);

        var sw = TOP.getBrowserWidth();
        
        //ロードバー非表示
        $("body").css({overflow:'visible'});
        $(window).off('.noScroll'); 
        $("#loader").fadeOut(200);

        //nav
        $("nav").fadeIn(500)
        $("nav > p.head").on(_touchEvent, function() {
            TOP.slideNav();
        });

        //modal
        TOP.initPicture();
        $("section.gallery > div.list > div.pics > div.inner > a").on(_touchEvent, function(e){
            e.preventDefault();
            if(_isAnimate) return;
            _isAnimate = true;
            var href = $(this).attr("href");
            _openNum = parseInt(href.replace("p", ""))-1;
            TOP.showPicture(_openNum, 0);
            _isOpen = true;
            $("section.modal").fadeIn(200, function() {
                
                if(TOP.getBrowserWidth() < 769)
                {
                    //スクロール無効化
                    $("body").css({overflow:'hidden'})
                    $(window).on('touchmove.noScroll', function(e) {
                        e.preventDefault();
                    });
                };
                _isAnimate = false;
            });
            $("section.modal > div.inner").removeClass("scaleOut").addClass("scaleIn");
        });
        $("section.gallery > div.list > a.prev").on(_touchEvent, function(e){
            e.preventDefault();
            if(_isAnimate) return;
            _isAnimate = true;

            _listLength --;
            // TOP.showPicture(_openNum, 300);
            TOP.listChange(_listLength);
        });
        $("section.gallery > div.list > a.next").on(_touchEvent, function(e){
            e.preventDefault();
            if(_isAnimate) return;
            _isAnimate = true;

            _listLength ++;
            // TOP.showPicture(_openNum, 300);
            TOP.listChange(_listLength);
        });

        //モーダル
        if(TOP.getBrowserWidth() < 769)
        {
            $("section.modal > div.inner").addClass("scaleOut");
        }
        $("section.modal > div.inner > a.prev").on(_touchEvent, function(e){
            e.preventDefault();
            if(_isAnimate) return;
            _isAnimate = true;

            _openNum --;
            TOP.showPicture(_openNum, 300);
        });
        $("section.modal > div.inner > a.next").on(_touchEvent, function(e){
            e.preventDefault();
            if(_isAnimate) return;
            _isAnimate = true;

            _openNum ++;
            TOP.showPicture(_openNum, 300);
        });
        $("section.modal > div.inner > a.close").on(_touchEvent, function(e){
            e.preventDefault();
            if(_isAnimate) return;
            _isAnimate = true;

            _isOpen = false;
            $("section.modal").fadeOut(200, function() {
                $("body").css({overflow:'visible'})
                $(window).off('.noScroll');
                _isAnimate = false;
            });
             $("section.modal > div.inner").removeClass("scaleIn").addClass("scaleOut");
        });

        //キー操作
        $('html').keyup(function(e){
            if(_isAnimate) return;
            if($("section.modal").css("display") != "none")
            {
                switch(e.which){
                    case 39: // Key[→]
                        if(_openNum >= _length-1) return;
                        _isAnimate = true;
                        _openNum ++;
                        TOP.showPicture(_openNum, 300);
                    break;
                    case 37: // Key[←]
                        if(_openNum <= 0) return;
                        _isAnimate = true;
                        _openNum --;
                        TOP.showPicture(_openNum, 300);
                    break;
                };
            };
        });


        //other Car
        var _otherCarLength = $("section.gallery > div.otherBox > div.cars > div.inner > a").length;
        var _isOtherCarAnime = false;
        _otherLength = (sw < 769)? 2 : 5;
        if(_isIe8) _otherLength = 2;
        var _movX = $("section.gallery > div.otherBox > div.cars > div.inner > a").eq(0).width();
        $("section.gallery > div.otherBox > a.prev").hide();
        if(_otherCarLength <= _otherLength) $("section.gallery > div.otherBox > a.next").hide();
        $("section.gallery > div.otherBox > a.prev").on(_touchEvent, function(e){
            e.preventDefault();

            _currentOtherCarNum --;
            if(_currentOtherCarNum <= 0){
                _currentOtherCarNum = 0;
                $("section.gallery > div.otherBox > a.prev").hide();
            }
            $("section.gallery > div.otherBox > div.cars > div.inner").animate({left: -1 * (_movX*　_currentOtherCarNum)});
            if(_currentOtherCarNum < _otherCarLength - _otherLength) $("section.gallery > div.otherBox > a.next").show();
        });
        $("section.gallery > div.otherBox > a.next").on(_touchEvent, function(e){
            e.preventDefault();

            _currentOtherCarNum ++;
            if(_currentOtherCarNum >= _otherCarLength - _otherLength){
                _currentOtherCarNum = _otherCarLength - _otherLength;
                $("section.gallery > div.otherBox > a.next").hide();
            }

            $("section.gallery > div.otherBox > div.cars > div.inner").animate({left: -1 * (_movX*　_currentOtherCarNum)});
            if(_currentOtherCarNum > 0) $("section.gallery > div.otherBox > a.prev").show();
        });

        //スワイプ
        if(!_isIe9)
        {
            $("section.modal").hammer().on('swipeleft', function(e) {
                if(_openNum >= _length-1) return;

                _openNum ++;
                TOP.showPicture(_openNum, 300);
            });
            $("section.modal").hammer().on('swiperight', function(e) {
                if(_openNum <= 0) return;

                _openNum --;
                TOP.showPicture(_openNum, 300);
            });
        }

        //MOVIE
        $("#modal_movie > div.inner").addClass("scaleOut");

        $("a.mov").on(_touchEvent, function(e){
            e.preventDefault();

            var href = $(this).attr("href");

            if(_isAd) $("#modal_movie").css({position:"absolute", top:TOP.getScroll().y});
            //modal
            TOP.modalVideoInit(href);
            $("#modal_movie").addClass("scaleIn");

            $("#modal_movie").fadeIn(200);
            $("#modal_movie > div.inner").removeClass("scaleOut").addClass("scaleIn");
            
            $("body").css({overflow:'hidden'})
            $(window).on('touchmove.noScroll', function(e) {
                e.preventDefault();
            });
        });


        //scrollEvent
        $(window).on("scroll", function(){ TOP.scroll(); });

        TOP.scroll();
        TOP.layout();
    },
    initPicture:function()
    {
        _length = $("section.gallery > div.list > div.pics > div.inner > a").length;
        var _w = TOP.getBrowserWidth() - 60;
        $("section.modal > div.inner > div.pics").width(_w*_length);


        $("section.gallery > div.list > div.pics > div.inner > a").each(function(i){
           // var filename = $(this).children("img").attr('src').match(".+/(.+?)([\?#;].*)?$")[1];
           var filename = $(this).children("img").attr('src').replace(/\/s\//g, "\/l\/");
           _fileArr.push(filename);

           var $pic = $("<div></div>").addClass('p'+i).css({width: _w, height: TOP.getBrowserHeight});
           $("section.modal > div.inner > div.pics").append($pic);
        });

        if(TOP.getBrowserWidth() >= 769 && !_isIe8)
        {
            $("section.modal").hide();
            $("section.gallery > div.list > a.prev").hide();
        }
    },
    showPicture:function(n, speed)
    {
        var sw = Math.min(TOP.getBrowserWidth(), 960);
        if(_isIe8) sw = TOP.getBrowserWidth();
        var _mg = (sw < 769)? 30 : 0;
        if(_isIe8) _mg = 30;
        var _x = _mg - ((sw-(_mg*2)) * n);

        $("section.modal > div.inner > div.pics").animate({left:_x}, speed, 'swing',function(){
            if(_openNum == 0) $("section.modal > div.inner > a.prev").hide();
            else $("section.modal > div.inner > a.prev").show();
            if(_openNum == _length-1) $("section.modal > div.inner > a.next").hide();
            else $("section.modal > div.inner > a.next").show();
            _isAnimate = false;
        });

        TOP.loadPicture(n, true);//true:中央表示
        if(n > 0) TOP.loadPicture(n-1, false);
        if(n < _length-1) TOP.loadPicture(n+1, false);

        var $waku = $("section.gallery > div.list > div.pics > div.inner > div.waku");
        var _wakuX = n*78;
        $waku.animate({left:_wakuX}, speed, 'swing');

        var _ll = (sw < 769)? 0 : Math.floor(n / _thumbLength);
        if(_isIe8) _ll = 0;

        if(_listLength != _ll || speed ==0)
        {
            _listLength = _ll;
            TOP.listChange(_ll);
        }
    },
    listChange:function(n)
    {
        var $list = $("section.gallery > div.list > div.pics > div.inner");
        var sw = Math.min(TOP.getBrowserWidth(), 960);
        var _mg = (sw < 769)? 30 : 0;
        if(_isIe8) _mg = 30;

        if(n * _thumbLength + _thumbLength >= _length) {
            $("section.gallery > div.list > a.next").hide();
            var _l = ((n-1) * _thumbLength) + (_length - (n * _thumbLength));
        }
        else{
            $("section.gallery > div.list > a.next").show();
            _l = n * _thumbLength;
        };

        if(n == 0){
            $("section.gallery > div.list > a.prev").hide();
        }
        else{
            $("section.gallery > div.list > a.prev").show();
        }

        var _x = _l* -78;
        $list.animate({left:_x}, 300, 'swing', function(){ _isAnimate = false; });

        //ページネーション
        $('section.gallery > div.list > div.local_nav > ul > li').removeClass('v');
        $('section.gallery > div.list > div.local_nav > ul > li:nth-child('+ parseInt(n+1) +')').addClass('v');
    },
    loadPicture:function(n, bool)
    {
        if(bool) var _opacity = 1;
        else _opacity = 0.5;
        if( _imgArr[n] != undefined)
        {
            _imgArr[n].animate({'opacity': _opacity}, 300, 'swing');
            return;
        }

        var $target = $("section.modal > div.inner > div.pics > div").eq(n);
        var _src = _fileArr[n];
        var img = $('<img/>');
        img.attr({'src' : _src}).load(function(){
            if(TOP.getBrowserWidth() < 769 || _isIe8)
            {
                //sp
                var _mgTop = _imgArr[n].height()/2*-1;
                var _mgLeft = _imgArr[n].width()/2*-1;
                var _top = "50%";
            }
            else
            {
                //pc
                _mgTop = 0;
                _mgLeft = "-50%";
                _top = 0;
            }
            $(this).css({
                display:"block",
                marginLeft: _mgLeft,
                marginTop: _mgTop,
                top:_top,
                opacity:_opacity,
                position:"absolute",
                width:"100%"
            });
        });
        $target.append(img);
        _imgArr[n] = img;
    },
    modalVideoInit:function(href)
    {
        if(href)
        {
            $("#modal_movie > div.inner").html('<iframe src="https://www.youtube.com/embed/' + href + '" frameborder="0" allowfullscreen></iframe><a href="#">CLOSE</a>');
        };

        $("#modal_movie > div.inner > a").on(_touchEvent, function(e){
            e.preventDefault();
            $("#modal_movie > div.inner > a").off(_touchEvent);
            $("#modal_movie").removeClass("scaleIn");
            $("#modal_movie").fadeOut('200', function() {
                $("body").css({overflow:'visible'})
                $(window).off('.noScroll'); 
                $("#modal_movie > div.inner").html("");
            });
            $("#modal_movie > div.inner").removeClass("scaleIn").addClass("scaleOut");
        });
    },
    scroll:function()
    {
        var _scrollY = TOP.getScroll().y;

        if(_scrollY < 68)
            $("nav").css({position:"absolute", top:"68px"});
        else
            $("nav").css({position:"fixed", top:0});
    },
    layout:function()
    {
        var sw = TOP.getBrowserWidth();
        var sh = TOP.getBrowserHeight();

        //ブレークポイント
        if(sw < 769 || _isIe8)
        {
            if(!_isSpSize || _isOpen)
            {
                _isSpSize = true;
                _isOpen = false;
                $("section.modal").fadeOut(200, function() {
                    $("body").css({overflow:'visible'})
                    $(window).off('.noScroll');
                });
                $("section.modal > div.inner").removeClass("scaleIn").addClass("scaleOut");
            };

            $("section.gallery > div.list").width("100%").height("auto").css({marginLeft:0, marginTop:0});
            $("section.gallery > div.list > div.pics > div.inner").width("inherit").height("inherit").css({left:0});


            $("section.modal").css({marginLeft: -1* sw/2});
            var _picsMg = (sw < 769)? 30 : 0;
            var _picsX = _picsMg - ((sw-(_picsMg*2)) * _openNum);
            $("section.modal").height(sh);
            $("section.modal > div.inner > div.pics").height(sh).width((sw-60)*_length).css({left: _picsX});
            $("section.modal > div.inner > a.prev, section.modal > div.inner > a.next").height(sh/2).css({marginTop:sh/4*-1});
            $("section.modal > div.inner > div.pics > div").css({width: sw - 60, height: sh});
            $("section.modal > div.inner > div.pics > div").each(function(i){
                $(this).html("");
            });
            _imgArr = [];
            _thumbLength = -1;
        }
        else
        {
            sw = Math.min(TOP.getBrowserWidth(), 960);
            var _picsh = sw*618/960;


            if(_isLoad && _isSpSize)
            {
                _isSpSize = false;
                //スクロール無効化解除
                $("body").css({overflow:'visible'})
                $(window).off('.noScroll');


                _isOpen = true;
                $("section.modal").stop().show();
                TOP.showPicture(_openNum, 0);

                $("section.modal > div.inner").removeClass("scaleOut scaleIn").addClass("scaleIn");
            }
            
            $("section.gallery > div.list > div.pics > div.inner").width(78*_length).height(50);

            $("section.modal").css({marginLeft: -1* sw/2});

            var _picsMg = (sw < 769)? 30 : 0;
            var _picsX = _picsMg - ((sw-(_picsMg*2)) * _openNum);
            $("section.modal").height(_picsh);
            $("section.modal > div.inner > div.pics").height(_picsh).width((sw)*_length).css({left: _picsX});
            $("section.modal > div.inner > div.pics > div").css({width: sw, height: _picsh});
            $("section.modal > div.inner > div.pics > div").each(function(i){
                var _target = $(this).children("img");
                if(_target.height())
                {
                    _target.css({
                        "margin-left": "-50%",
                        "margin-top": 0,
                        top:0
                    });
                };
            });

            $("section.modal > div.inner > a.prev, section.modal > div.inner > a.next").height(_picsh).css({marginTop:(_picsh/2*-1)});
            var _btnW = Math.max(40 + (TOP.getBrowserWidth() - 960)/2, 40);
            var _btnX = (TOP.getBrowserWidth() - 960)/2*-1;
            _btnX = (_btnX > 0)? 0: _btnX;
            $("section.modal > div.inner > a.prev").css({width:_btnW, left:_btnX});
            $("section.modal > div.inner > a.next").css({width:_btnW, right:_btnX});



            var _tlength = Math.floor((sw - 100)/78);
            if(_isLoad && _tlength != _thumbLength)
            {
                var _listW = 78*_tlength;
                $("section.gallery > div.list").width(_listW).height(50).css({marginLeft: -1* _listW/2, marginTop:_picsh+32});
                //ページネーション
                var _page = Math.ceil((_length / _tlength));

                $("section.gallery > div.list > div.local_nav").html("");
                var _ul = $("<ul></ul>");
                $("section.gallery > div.list > div.local_nav").append(_ul);
                for(i=0; i < _page; i++)
                {
                    var _li = $("<li></li>");
                    if(i == 0) _li.addClass('v');
                    _ul.append(_li);
                };
                _ul.width(32*_page).css({marginLeft:_ul.width()/2*-1});
                $("section.gallery > div.list > div.local_nav > ul > li").on(_touchEvent, function(e){
                    e.preventDefault();
                    if(_isAnimate) return;
                    _isAnimate = true;

                    var _index = $('section.gallery > div.list > div.local_nav > ul > li').index(this);
                    _listLength = _index;

                    $('section.gallery > div.list > div.local_nav > ul > li').removeClass('v');
                    $('section.gallery > div.list > div.local_nav > ul > li:nth-child('+ parseInt(_index+1) +')').addClass('v');

                    TOP.listChange(_listLength);
                });

                _thumbLength = _tlength;
            };
        }
        var _otherCar = $("section.gallery > div.otherBox > div.cars > div.inner > a");
        var _otherCarLength = $("section.gallery > div.otherBox > div.cars > div.inner > a").length;
        var _wMg = (sw < 769)? 0 : 20;
        if(_isIe8) _wMg = 0;
        var _otherCarsInnerW = _otherCarLength * ($("section.gallery > div.otherBox > div.cars > div.inner > a").eq(0).width() + _wMg);
        _otherLength = (sw < 769)? 2 : 5;
        if(_isIe8) _otherLength = 2;
        var _otherCarsW = Math.min(_otherCarsInnerW, $("section.gallery > div.otherBox > div.cars > div.inner > a").eq(0).width()*_otherLength);
        var _carsW = $(window).width() * 0.8;
        var _carsInnerW = _carsW * 2;
        var winW = $(window).width();
        if(winW < 769) {
            // SP
            $("section.gallery > div.otherBox > div.cars").width(_carsW).css({marginLeft:-_carsW/2});
            $("section.gallery > div.otherBox > div.cars > div.inner").width(_carsInnerW);
            $("section.gallery > div.otherBox > div.cars > div.inner a").width(_carsInnerW / 4);
        }else{
            // PC
            $("section.gallery > div.otherBox > div.cars").width(_otherCarsW).css({marginLeft:_otherCarsW/2*-1});
            $("section.gallery > div.otherBox > div.cars > div.inner").width(_otherCarsInnerW);
        }

        $("section.gallery > div.otherBox > a.prev").css({marginLeft:_otherCarsW/2*-1 - 50});
        $("section.gallery > div.otherBox > a.next").css({marginLeft:_otherCarsW/2});

        _currentOtherCarNum = 0;
        $("section.gallery > div.otherBox > a.prev").hide();
        $("section.gallery > div.otherBox > div.cars > div.inner").css({left: 0});
        if(_currentOtherCarNum < _otherCarLength - _otherLength) $("section.gallery > div.otherBox > a.next").show();
    },
    shuffle:function(list)
    {
        var i = list.length;
        while (i) {
            var j = Math.floor(Math.random() * i);
            var t = list[--i];
            list[i] = list[j];
            list[j] = t;
        }
    },
    // スクロール位置を取得
    getScroll:function(){
        return {
            x:document.body.scrollLeft || document.documentElement.scrollLeft,
            y:document.body.scrollTop || document.documentElement.scrollTop
        }
    },
    getBrowserWidth:function()
	{
		if ( window.innerWidth ) { return window.innerWidth; }
		else if ( document.documentElement && document.documentElement.clientWidth != 0 ) { return document.documentElement.clientWidth; }
		else if ( document.body ) { return document.body.clientWidth; }
		return 0;
	},
	getBrowserHeight:function()
	{
		if ( window.innerHeight ) { return window.innerHeight; }
		else if ( document.documentElement && document.documentElement.clientHeight != 0 ) { return document.documentElement.clientHeight; }
		else if ( document.body ) { return document.body.clientHeight; }
		return 0;
	}
}


var TOP = new Project();

$(function(){
    TOP.init();
});
