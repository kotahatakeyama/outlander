(function(){
  'use strict';
  $(function() {

    var list = function toggleList() {
      var $btn = $('.parent');
      var flg = true;

      function load() {
        $btn.find('a').on('click',function() {
          flg = false;
        });

        $btn.on('click', function() {
          toggle($(this));

        });
      }

      function toggle($this){
        if(flg){
          $this.toggleClass('on').find('.child').slideToggle();
        } else{
          flg = true;
        }
      }
      return {
        start: load
      };
    };
    var module = list();
    module.start();
  });




})(window,document);