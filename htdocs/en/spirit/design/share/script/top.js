var _isSp = false;
var _isIe9 = false;
var _isIe8 = false;
var _isIe = false;
var _isAd = false;
var _skrollr;
var _isLoad = false;
var loadPercent = 0;
var nowPercent = 0;
var _isMovPlay = false;
var _isDSPlay = false;
var _isAutoScroll = false;
var _scrollpy = 0;
var _current = -1;
var _touchEvent = 'click'; //'ontouchend' in window ? 'touchend' : 'click';
var _iosHeight;
var _ccHitNum = 0;
var _movHitNum = 0;
// var _c11FileArr = [];
var _isc11Animate = false;
var _openNum = 0;

//パララックス設定                        日本語 : 英語
var _scrollY1 = ($("body").hasClass("jp"))? 3720: 2860;
var _scrollY2 = ($("body").hasClass("jp"))? 9000: 9600;
var _scrollY3 = ($("body").hasClass("jp"))? 20000: 20000;
var _scrollY4 = ($("body").hasClass("jp"))? 23000: 23000;
var _introHeight = _scrollY4;

//t01,t02のマスク処理のタイミング
var _msk1min = 800;
var _msk1max = _scrollY1;
var _msk2min = 6400;
var _msk2max = _scrollY2;
//背景ビデオのオンオフタイミング
var _videoStartandEnd = [10000, 21500];

var scrollStopEvent;

var _loadingImages_common_sp =[
    "/en/spirit/design/share/images/t01_1.png",
    "/en/spirit/design/share/images/t01_2_sp.png",
    "/en/spirit/design/share/images/StoryArea-chap01.png",
    "/en/spirit/design/share/images/StoryArea-chap02.png",
    "/en/spirit/design/share/images/StoryArea-chap03_msk_sp.png",
    "/en/spirit/design/share/images/StoryArea-chap03_sp.png",
    "/en/spirit/design/share/images/StoryArea-chap04_sp.png",
    "/en/spirit/design/share/images/t07_1.png",
    "/en/spirit/design/share/images/t08_1.png",
    "/en/spirit/design/share/images/concept_car0_mov.png",
    "/en/spirit/design/share/images/conceputCar-Slide00_sp.png",
    "/en/spirit/design/share/images/concept_car1_mov.png",
    "/en/spirit/design/share/images/conceputCar-Slide01_sp.png",
    "/en/spirit/design/share/images/concept_car2_mov.png",
    "/en/spirit/design/share/images/conceputCar-Slide02_sp.png",
    "/en/spirit/design/share/images/conceputCar-Slide03_sp.png",
    "/en/spirit/design/share/images/concept_car3_mov.png",
    "/en/spirit/design/share/images/t09_1_sp.png",
    "/en/spirit/design/share/images/designTheFuture-mov_btn.png",
    "/en/spirit/design/share/images/designTheFuture-recruit_btn.png",
    "/en/spirit/design/share/images/designTheFuture-Main_sp.png",
    "/en/spirit/design/share/images/t10_1.png",
    "/en/spirit/design/share/images/local-01.png",
    "/en/spirit/design/share/images/local-02.png",
    "/en/spirit/design/share/images/local-03.png",
    "/en/spirit/design/share/images/location-Map.jpg",
    "/en/spirit/design/share/images/t12_1.png"
];

var _loadingImages_common_pc =[
    "/en/spirit/design/share/images/t01_1.png",
    "/en/spirit/design/share/images/t01_2_pc.png",
    "/en/spirit/design/share/images/StoryArea-chap01-bg.jpg",
    "/en/spirit/design/share/images/StoryArea-chap02-bg.jpg",
    "/en/spirit/design/share/images/StoryArea-chap03_sp.png",
    "/en/spirit/design/share/images/StoryArea-chap04-bg.jpg",
    "/en/spirit/design/share/images/DynamicShield-left03.png",
    "/en/spirit/design/share/images/DynamicShield-left02.png",
    "/en/spirit/design/share/images/DynamicShield-left01.png",
    "/en/spirit/design/share/images/DynamicShield-right01.png",
    "/en/spirit/design/share/images/DynamicShield-right02.png",
    "/en/spirit/design/share/images/DynamicShield-right03.png",
    "/en/spirit/design/share/images/DynamicShield-center.png",
    "/en/spirit/design/share/images/dynamicshield_car_center_parts_1.png",
    "/en/spirit/design/share/images/dynamicshield_car_center_parts_2.png",
    "/en/spirit/design/share/images/dynamicshield_car_center_parts_3.png",
    "/en/spirit/design/share/images/dynamicshield_car_center_parts_4.png",
    "/en/spirit/design/share/images/t07_1.png",
    "/en/spirit/design/share/images/t08_1.png",
    "/en/spirit/design/share/images/concept_car0_mov.png",
    "/en/spirit/design/share/images/conceputCar-Slide00.png",
    "/en/spirit/design/share/images/concept_car1_mov.png",
    "/en/spirit/design/share/images/conceputCar-Slide01.png",
    "/en/spirit/design/share/images/concept_car2_mov.png",
    "/en/spirit/design/share/images/conceputCar-Slide02.png",
    "/en/spirit/design/share/images/concept_car3_mov.png",
    "/en/spirit/design/share/images/conceputCar-Slide03.png",
    "/en/spirit/design/share/images/t09_1_pc.png",
    "/en/spirit/design/share/images/designTheFuture-mov_btn.png",
    "/en/spirit/design/share/images/designTheFuture-recruit_btn.png",
    "/en/spirit/design/share/images/DesignTheFuture-Main.png",
    "/en/spirit/design/share/images/t10_1.png",
    "/en/spirit/design/share/images/local-01.png",
    "/en/spirit/design/share/images/local-02.png",
    "/en/spirit/design/share/images/local-03.png",
    "/en/spirit/design/share/images/location-Map.jpg",
    "/en/spirit/design/share/images/t12_1.png",
    "/en/spirit/design/share/images/roots_car_2.png",
    "/en/spirit/design/share/images/roots_car_2_parts_1.png",
    "/en/spirit/design/share/images/roots_car_2_parts_2.png",
    "/en/spirit/design/share/images/roots_car_2_parts_3.png",
    "/en/spirit/design/share/images/roots_txt_01.png",
    "/en/spirit/design/share/images/roots_car_3.png",
    "/en/spirit/design/share/images/roots_car_3_parts_1.png",
    "/en/spirit/design/share/images/roots_txt_02.png",
    "/en/spirit/design/share/images/roots_txt_03.png",
    "/en/spirit/design/share/images/roots_arrow.png"
];

var _loadingImages_jp_sp =[
    "/en/spirit/design/share/images/t02_1.png",
    "/en/spirit/design/share/images/t02_2.png",
    "/en/spirit/design/share/images/t02_3.png",
    "/en/spirit/design/share/images/t03_1_sp.png",
    "/en/spirit/design/share/images/t03_2_sp.png",
    "/en/spirit/design/share/images/t04_1.png",
    "/en/spirit/design/share/images/t04_2.png",
    "/en/spirit/design/share/images/t04_3.png",
    "/en/spirit/design/share/images/t04_4_sp.png",
    "/en/spirit/design/share/images/t05_1_sp.png"
];
var _loadingImages_en_sp =[
    "/en/spirit/design/share/images/t02_1_en_sp.png",
    "/en/spirit/design/share/images/t02_2_en_sp.png",
    "/en/spirit/design/share/images/t03_1_en_sp.png",
    "/en/spirit/design/share/images/t03_2_en_sp.png",
    "/en/spirit/design/share/images/t04_1_en_sp.png",
    "/en/spirit/design/share/images/t04_2_en_sp.png",
    "/en/spirit/design/share/images/t04_3_en_sp.png",
    "/en/spirit/design/share/images/t04_4_en_sp.png",
    "/en/spirit/design/share/images/t05_1_en_sp.png"
];

var _loadingImages_jp_pc =[
    "/en/spirit/design/share/images/t02_1.png",
    "/en/spirit/design/share/images/t02_2.png",
    "/en/spirit/design/share/images/t02_3.png",
    "/en/spirit/design/share/images/t03_1_pc.png",
    "/en/spirit/design/share/images/t03_2_pc.png",
    "/en/spirit/design/share/images/t04_1.png",
    "/en/spirit/design/share/images/t04_2.png",
    "/en/spirit/design/share/images/t04_3.png",
    "/en/spirit/design/share/images/t04_4_pc.png",
    "/en/spirit/design/share/images/t05_1_pc.png"
];
var _loadingImages_en_pc =[
    "/en/spirit/design/share/images/t02_1_en_pc.png",
    "/en/spirit/design/share/images/t02_2_en_pc.png",
    "/en/spirit/design/share/images/t03_1_en_pc.png",
    "/en/spirit/design/share/images/t03_2_en_pc.png",
    "/en/spirit/design/share/images/t04_1_en_pc.png",
    "/en/spirit/design/share/images/t04_2_en_pc.png",
    "/en/spirit/design/share/images/t04_3_en_pc.png",
    "/en/spirit/design/share/images/t04_4_en_pc.png",
    "/en/spirit/design/share/images/t05_1_en_pc.png"
];

function scrollStopEventTrigger(){
    var timer;
    if(timer){
      clearTimeout(timer);
    }
    timer = setTimeout(function(){$(window).trigger(scrollStopEvent)}, 200);
}

var Project = function()
{
}

Project.prototype = {
    init:function(argument)
    {
        //UA
        var _ua = window.navigator.userAgent.toLowerCase();
        //バージョン
        var _appVer = window.navigator.appVersion.toLowerCase();

        if((_ua.indexOf("windows") != -1 && _ua.indexOf("touch") != -1)
            || _ua.indexOf("ipad") != -1
            || (_ua.indexOf("android") != -1 && _ua.indexOf("mobile") == -1)
            || (_ua.indexOf("firefox") != -1 && _ua.indexOf("tablet") != -1)
            || _ua.indexOf("kindle") != -1
            || _ua.indexOf("silk") != -1
            || _ua.indexOf("playbook") != -1
            || (_ua.indexOf("windows") != -1 && _ua.indexOf("phone") != -1)
            || _ua.indexOf("iphone") != -1
            || _ua.indexOf("ipod") != -1
            || (_ua.indexOf("android") != -1 && _ua.indexOf("mobile") != -1)
            || (_ua.indexOf("firefox") != -1 && _ua.indexOf("mobile") != -1)
            || _ua.indexOf("blackberry") != -1)  _isSp = true;
        
         if(_ua.indexOf('msie') != -1){
            if (_appVer.indexOf("msie 6.") != -1 || _appVer.indexOf("msie 7.") != -1 || _appVer.indexOf("msie 8.") != -1) _isIe8 = true;
            if (_isIe8 || _appVer.indexOf("msie 9.") != -1) _isIe9 = true;
         };

        if(_appVer.indexOf("msie 10.") != -1 || _isIe9) _isIe = true;

        if((_ua.indexOf("android") != -1 && _ua.indexOf("mobile") == -1)|| (_ua.indexOf("android") != -1 && _ua.indexOf("mobile") != -1)) _isAd = true;

        //スクロール無効化
        $("body").css({overflow:'hidden'})
        $(window).on('touchmove.noScroll', function(e) {
            e.preventDefault();
        });

        if(!_isSp && !_isIe9)
        {
            //DYNAMIC SHIELD
            TOP.animateDynamicShield(false);
            //video
            $("#c04 > div.movie").html("<video id='video' loop><source src='/en/spirit/design/share/movie/background.mp4'></video>")
        }

        //先読み画像
        if(!_isIe9)
        {
            if(_isSp){
                var images = [].concat(_loadingImages_common_sp);
                if($("body").hasClass("jp")) images.concat(_loadingImages_jp_sp);
                else images.concat(_loadingImages_en_sp);

                //クエリ取得
                var _q = window.location.search.split("=")[1];
                if(_q != ("" || undefined)){
                    $("#loader > div.logo").hide();
                };
            }
            else{
                images = [].concat(_loadingImages_common_pc);
                if($("body").hasClass("jp")) images.concat(_loadingImages_jp_pc);
                else images.concat(_loadingImages_en_pc);
            };

            TOP.preloadImage(images);

            _iosHeight = TOP.getBrowserHeight();
            TOP.layout();
            TOP.scroll();

            //ローダー表示
            $(".throbber, .indi").css({opacity:1});
            $("#loader").show();
            $("#loader p.indi").html(parseInt(0));
        }
        else
        {
            $("#loader").hide();
            TOP.loadComplete();
        }

        //resize
        $(window).on("resize", function(){ TOP.layout(); });
    },
    //画像の読み込み
    preloadImage:function(imageArray)
    {
        function preload(images, progress){
            var total = images.length;
            $(images).each(function()
            {
                var src = this;
                var img = $('<img/>');
                img.attr({'src' : src}).load(function()
                {
                    for(var i = 0; i < images.length; i++){
                        if (images[i] == src) images.splice(i, 1);
                    };
                    progress(total, total - images.length);
                });
            });
        }
        
        preload(imageArray, function(total, loaded){
            nowPercent = Math.ceil(100 * loaded / total);
        });
        _loadTime = setInterval(TOP.progress, 10);
    },
    progress:function(per)
    {
        if(loadPercent < nowPercent) loadPercent = parseInt(nowPercent);
        $("#loader p.indi").html(parseInt(loadPercent));

        if(nowPercent >= 100 && loadPercent >= nowPercent) TOP.loadComplete();
    },
    loadComplete:function()
    {
        _isLoad = true;
        if(!_isIe9) clearInterval(_loadTime);

        var sw = TOP.getBrowserWidth();
        
        //スクロールを有効化
        $("body").css({overflow:'visible'});
        $(window).off('.noScroll'); 
        //ロードバー非表示
        $("#loader").fadeOut(1000);

        //nav
        $("nav").fadeIn(500)
        $("nav > p.head").on(_touchEvent, function(e){
            e.preventDefault();
            TOP.slideNav();
            $("nav > p.head > span.new").fadeOut(300);
        });
        //スムーススクロール
        $("nav li > a").on(_touchEvent, function(e) {
            e.preventDefault();
            var href = $(this).attr("href");
            var target = $(href == "#" || href == "" ? 'html' : href);
            if(TOP.getBrowserWidth() < 769) var _margin = 68;
            else{
                if(target.attr("id") == "c09" || target.attr("id") == "c10") _margin = 0;
                else _margin = 60;
            }
            var position = target.offset().top - _margin;
            var _speed;

            $("body").css({overflow:'visible'})
            $(window).off('.noScroll');

            $('#bout').fadeIn(300, function(){
                $('body,html').stop(true).animate({scrollTop:position}, 1);
                $('#bout').delay(100).fadeOut(300);
                _isAutoScroll = false;

                if($("nav > p.head").css('display') == "block")  TOP.slideNav();
            });

            // if(TOP.getScroll().y < $('#c06').offset().top && href != "#intro") _speed = 5000;
            // else if(TOP.getScroll().y > $('#c06').offset().top && href == "#intro") _speed = 1;
            // else _speed = 1000;

            // if(TOP.getBrowserWidth() < 769) _speed = 1000;

            // $('body,html').animate({scrollTop:position}, _speed, 'swing');

            // if($("nav > p.head").css('display') == "block")  TOP.slideNav();

            return false;
        });

        //スクロールArrow
        $("#scrollarrow > p").on(_touchEvent, function(e){
            e.preventDefault();

            if(_isAutoScroll) return;

            _isAutoScroll = true;

            var _scY = TOP.getScroll().y;
            var _duration;
            var _scrollY;
            // var _scrollY1 = 3720;
            // var _scrollY2 = 9000;
            // var _scrollY3 = 27000;
            // var _scrollY4 = 29500;
            var _scrollY5 = $('#c06').offset().top;
            var _ease;

            if(_scY < _scrollY1-100)
            {
                //00 > 01
                _scrollY = _scrollY1;
                _duration = 4500 * (_scrollY1-100 - _scY)/(_scrollY1-100);
                _ease = 'easeInSine';
            }
            else if(_scY < _scrollY2-100)
            {
                //01 > 02
                _scrollY = _scrollY2;
                _duration = 6000 * (_scrollY2-100-_scrollY1-100 - (_scY-_scrollY1-100)) / (_scrollY2-100-_scrollY1-100);
                _ease = 'linear';
            }
            else if(_scY < _scrollY3-100)
            {
                //02 > 03
                _scrollY = _scrollY3;
                _duration = 13000 * (_scrollY3-100-_scrollY2-100 - (_scY-_scrollY2-100)) / (_scrollY3-100-_scrollY2-100);
                _ease = 'linear';
            }
            else if(_scY < _scrollY4-100)
            {
                //03 > 04
                _scrollY = _scrollY4;
                _duration = 2000 * (_scrollY4-100-_scrollY3-100 - (_scY-_scrollY3-100)) / (_scrollY4-100-_scrollY3-100);
                _ease = 'linear';
            }
            else if(_scY < _scrollY5-100)
            {
                //04 > 05
                _scrollY = _scrollY5;
                _duration = 3000;
                _ease = 'easeOutSine';
            }

            $("#scrollarrow").stop(true).fadeOut(200);
            $('body,html').stop(true).animate({scrollTop:_scrollY}, _duration, _ease, function(){
                _isAutoScroll = false;
            });
        });


        //concept car
        //スワイプ処理
        if(!_isIe8)
        {
            var _ccContW = (sw < 769)? 260 : 960;
            var _ccMargin = (sw < 769)? 40 : 100;
            var _ccSpeed = 700;
            $('#c08 div.slideBox').velocity({left:(sw/2 - _ccContW/2)}, _ccSpeed, 'swing');
            $('#c08 div.local_nav li.prev').hide();
            
            if(!_isIe9)
            {
                $("#c08 div.slideBox").hammer().on('swipeleft', function(e) {
                    if(_ccHitNum == 4) return;

                    _ccHitNum ++;
                    TOP.setCC();
                });
                $("#c08 div.slideBox").hammer().on('swiperight', function(e) {
                    if(_ccHitNum == 0) return;

                    _ccHitNum --;
                    TOP.setCC();
                });

                $("#c08 div.movSlideBox .inner").hammer().on('swipeleft', function(e) {
                    if(_movHitNum === 1) return;

                    _movHitNum ++;
                    TOP.setDataMov();
                });
                $("#c08 div.movSlideBox .inner").hammer().on('swiperight', function(e) {
                    if(_movHitNum === 0) return;

                    _movHitNum --;
                    TOP.setDataMov();
                });

            }
            //Lnav
            $('#c08 div.local_nav li > a').on(_touchEvent, function(e){
                e.preventDefault();
                var href = $(this).attr("href");
                if(href == "prev") _ccHitNum --;
                else if(href == "next") _ccHitNum ++;
                else if(href == "p1") _ccHitNum = 0;
                else if(href == "p2") _ccHitNum = 1;
                else if(href == "p3") _ccHitNum = 2;
                else if(href == "p4") _ccHitNum = 3;
                else if(href == "p5") _ccHitNum = 4;
                TOP.setCC();
            });
            $('#c08 .mov_nav a').on(_touchEvent, function(e){
                e.preventDefault();
                var href = $(this).attr("href");
                if(href == "prev") _movHitNum --;
                else if(href == "next") _movHitNum ++;
                else if(href == "m1") _movHitNum = 0;
                else if(href == "m2") _movHitNum = 1;
                TOP.setDataMov();
            });
            TOP.setDataMov();


        };

        // $('#c08 div.slideBox > div.car1').on(_touchEvent, function(e){
        //     e.preventDefault();
        //     _ccHitNum = 0;
        //     TOP.setCC();
        // });
        // $('#c08 div.slideBox > div.car2').on(_touchEvent, function(e){
        //     e.preventDefault();
        //     _ccHitNum = 1;
        //     TOP.setCC();
        // });
        // $('#c08 div.slideBox > div.car3').on(_touchEvent, function(e){
        //     e.preventDefault();
        //     _ccHitNum = 2;
        //     TOP.setCC();
        // });



        //studio タブ
        var _studioTabisOpen = false;
        var _studioTabOpenNum = -1;
        var _studioTabOpenning = false;

        var _index;
        var _btmMargin;
        var _openMargin;
        var _detailHeight;

        function slide1()
        {
            $("#c10 > div.studioBox").removeClass("detail1 detail2 detail3");

            $("#c10 > div.studioBox > div.detailBox > div.detail1").show();
            _detailHeight = $("#c10 > div.studioBox > div.detailBox").height();
            $("#c10 > div.studioBox > div.detailBox > div.detail1").hide();

            if(_openMargin < _detailHeight)
            {
                    $('body,html').animate({scrollTop:TOP.getScroll().y + _detailHeight + _btmMargin - _openMargin}, 500, 'swing',function(){
                        $("#c10 > div.studioBox > div.detailBox > div.detail1").slideDown(500, 'swing',function(){ _studioTabOpenning = false; });
                        $("#c10 > div.studioBox").addClass("detail1");
                    });
            }
            else
            {
                $("#c10 > div.studioBox").addClass("detail1");
                $("#c10 > div.studioBox > div.detailBox > div.detail1").slideDown(500, 'swing',function(){ _studioTabOpenning = false; });
            }
        }
        function slide2()
        {
            $("#c10 > div.studioBox").removeClass("detail1 detail2 detail3");

            $("#c10 > div.studioBox > div.detailBox > div.detail2").show();
            _detailHeight = $("#c10 > div.studioBox > div.detailBox").height();
            $("#c10 > div.studioBox > div.detailBox > div.detail2").hide();

            if(_openMargin < _detailHeight)
            {
                    $('body,html').animate({scrollTop:TOP.getScroll().y + _detailHeight + _btmMargin - _openMargin}, 500, 'swing',function(){
                        $("#c10 > div.studioBox > div.detailBox > div.detail2").slideDown(500, 'swing',function(){ _studioTabOpenning = false; });
                        $("#c10 > div.studioBox").addClass("detail2");
                    });
            }
            else
            {
                $("#c10 > div.studioBox").addClass("detail2");
                $("#c10 > div.studioBox > div.detailBox > div.detail2").slideDown(500, 'swing',function(){ _studioTabOpenning = false; });
            }
        }
        function slide3()
        {
            $("#c10 > div.studioBox").removeClass("detail1 detail2 detail3");

            $("#c10 > div.studioBox > div.detailBox > div.detail3").show();
            _detailHeight = $("#c10 > div.studioBox > div.detailBox").height();
            $("#c10 > div.studioBox > div.detailBox > div.detail3").hide();

            if(_openMargin < _detailHeight)
            {
                    $('body,html').animate({scrollTop:TOP.getScroll().y + _detailHeight + _btmMargin - _openMargin}, 500, 'swing',function(){
                        $("#c10 > div.studioBox > div.detailBox > div.detail3").slideDown(500, 'swing',function(){ _studioTabOpenning = false; });
                        $("#c10 > div.studioBox").addClass("detail3");
                    });
            }
            else
            {
                $("#c10 > div.studioBox").addClass("detail3");
                $("#c10 > div.studioBox > div.detailBox > div.detail3").slideDown(500, 'swing',function(){ _studioTabOpenning = false; });
            }
        }

        $("#c10 > div.studioBox > ul.btns li").on(_touchEvent, function(e){
            e.preventDefault();


            if(_studioTabOpenning) return;

            _studioTabOpenning = true;
            _index = $("#c10 > div.studioBox > ul.btns li").index(this);
            _btmMargin = (sw < 769)? 0:55;
            _openMargin = TOP.getScroll().y + TOP.getBrowserHeight() - $("#c10 > div.studioBox > div.detailBox").offset().top;

            if(_index == 0)
            {
                if(_studioTabOpenNum == 0)
                {
                    //close
                    _studioTabOpenNum = -1;
                    $("#c10 > div.studioBox > div.detailBox > div.detail1").slideUp(500, 'swing',function(){
                        $("#c10 > div.studioBox").removeClass("detail1 detail2 detail3");
                        _studioTabOpenning = false;
                    });
                }
                else
                {
                    //open
                    if(_studioTabOpenNum == -1)
                    {
                        slide1();
                    }
                    else
                    {
                        //すでに開いている
                        if(_studioTabOpenNum == 1)$("#c10 > div.studioBox > div.detailBox > div.detail2").slideUp(500, 'swing',function(){ slide1(); });
                        else if(_studioTabOpenNum == 2)$("#c10 > div.studioBox > div.detailBox > div.detail3").slideUp(500, 'swing',function(){ slide1(); });
                    }

                    _studioTabOpenNum = 0;
                };
            }else if(_index == 1){
                if(_studioTabOpenNum == 1)
                {
                    //close
                    _studioTabOpenNum = -1;
                    $("#c10 > div.studioBox > div.detailBox > div.detail2").slideUp(500, 'swing',function(){
                        $("#c10 > div.studioBox").removeClass("detail1 detail2 detail3");
                        _studioTabOpenning = false;
                    });
                }
                else
                {
                    //open

                    if(_studioTabOpenNum == -1)
                    {
                        slide2();
                    }
                    else
                    {
                        //すでに開いている
                        if(_studioTabOpenNum == 0)$("#c10 > div.studioBox > div.detailBox > div.detail1").slideUp(500, 'swing',function(){ slide2(); });
                        else if(_studioTabOpenNum == 2)$("#c10 > div.studioBox > div.detailBox > div.detail3").slideUp(500, 'swing',function(){ slide2(); });
                    }

                    _studioTabOpenNum = 1;
                };
            }else if(_index == 2){
                if(_studioTabOpenNum == 2)
                {
                    //close
                    _studioTabOpenNum = -1;
                    $("#c10 > div.studioBox > div.detailBox > div.detail3").slideUp(500, 'swing',function(){
                        $("#c10 > div.studioBox").removeClass("detail1 detail2 detail3");
                        _studioTabOpenning = false;
                    });
                }
                else
                {
                    //open

                    if(_studioTabOpenNum == -1)
                    {
                        slide3();
                    }
                    else
                    {
                        //すでに開いている
                        if(_studioTabOpenNum == 0)$("#c10 > div.studioBox > div.detailBox > div.detail1").slideUp(500, 'swing',function(){ slide3(); });
                        else if(_studioTabOpenNum == 1)$("#c10 > div.studioBox > div.detailBox > div.detail2").slideUp(500, 'swing',function(){ slide3(); });
                    }

                    _studioTabOpenNum = 2;
                };
            };
        });

        //ルーツ
        TOP.animateRoots(false);
        $("#modal_dynamicshield > div.inner").addClass("scaleOut");
        $("#c07 > a").on(_touchEvent, function(e){
            var winW = $(window).outerWidth() + 30  ;
            $(window).on('resize', function() {
                var posy = (($(window).height() -700 ) /2)-50 >= 15 ? (($(window).height() -700 ) / 2) -50 : 15 ;
                $('.js-close').css({'top':posy});                
            });
            var posy = (($(window).height() -700 ) /2)-50 >= 15 ? (($(window).height() -700 ) / 2) -50 : 15 ;
            $('.js-close').css({'top':posy});
            e.preventDefault();

            $('#modal_dynamicshield .inner').width(winW*2);
            var href = $(this).attr("href");

            if(TOP.getBrowserWidth() < 769 || _isIe8)
            {
                window.location.href = href;
            }
            else
            {
                //modal
                $("#modal_dynamicshield").fadeIn(200, function() {
                    TOP.animateRoots(true);
                });
                $("#modal_dynamicshield  div.inner").removeClass("scaleOut").addClass("scaleIn");
                $("#modal_dynamicshield").addClass('active');

                $("body").css({overflow:'hidden'});
                $(window).on('touchmove.noScroll', function(e) {
                    e.preventDefault();
                });
            };
            return false;
        });
        $("#modal_dynamicshield a").on(_touchEvent, function(e){
            e.preventDefault();

            $("#modal_dynamicshield").fadeOut(200, function() {
                $("body").css({overflow:'visible'})
                $(window).off('.noScroll');
                TOP.animateRoots(false);
            });
            $("#modal_dynamicshield > div.inner").removeClass("scaleIn").addClass("scaleOut");
            $("#modal_dynamicshield").removeClass('active');
        });

        //MOVIE
        $("#modal_movie > div.inner").addClass("scaleOut");

        $("a.mov").on(_touchEvent, function(e){
            e.preventDefault();

            var href = $(this).attr("href");

            if(_isAd) $("#modal_movie").css({position:"absolute", top:TOP.getScroll().y});
            //modal
            TOP.modalVideoInit(href);
            $("#modal_movie").addClass("scaleIn");

            $("#modal_movie").fadeIn(200);
            $("#modal_movie > div.inner").removeClass("scaleOut").addClass("scaleIn");
            
            $("body").css({overflow:'hidden'})
            $(window).on('touchmove.noScroll', function(e) {
                e.preventDefault();
            });
        });

        //c1
        if(!_isIe8)
        {
            var _c11w = (TOP.getBrowserWidth()-80)*($("#c11 > div.pic").length);
            $("#modal_pic > div.inner > div.pics").height(TOP.getBrowserWidth()-80).width(_c11w).css({left: 40});
            $("#c11 > div.pic").each(function(i){
               var filename = $(this).children("img").attr('src');
               $("#modal_pic > div.inner > div.pics").append('<img src="' + filename + '">');
            });
            
            $("#c11 > div.pic").on(_touchEvent, function(e){
                e.preventDefault();
                if(TOP.getBrowserWidth() >= 769) return;

                if(_isAd) $("#modal_pic").css({position:"absolute", top:TOP.getScroll().y});

                //modal
                _openNum = $("#c11 > div.pic").index(this);
                TOP.modalchangePic(_openNum, 0);
                $("#modal_pic").addClass("scaleIn");
                $("#modal_pic").fadeIn(200);
                $("#modal_pic > div.inner").removeClass("scaleOut").addClass("scaleIn");
                
                $("body").css({overflow:'hidden'})
                $(window).on('touchmove.noScroll', function(e) {
                    e.preventDefault();
                });
            });
            $("#modal_pic > div.inner > a.prev").on(_touchEvent, function(e){
                e.preventDefault();
                if(_isc11Animate) return;
                _isc11Animate = true;

                _openNum --;
                TOP.modalchangePic(_openNum, 300);
            });
            $("#modal_pic > div.inner > a.next").on(_touchEvent, function(e){
                e.preventDefault();
                if(_isc11Animate) return;
                _isc11Animate = true;

                _openNum ++;
                TOP.modalchangePic(_openNum, 300);
            });
            $("#modal_pic > div.inner > a.close").on(_touchEvent, function(e){
                e.preventDefault();
                if(_isc11Animate) return;
                _isc11Animate = true;

                $("#modal_pic").fadeOut(200, function() {
                    $("body").css({overflow:'visible'})
                    $(window).off('.noScroll');
                    _isc11Animate = false;
                });
                 $("#modal_pic > div.inner").removeClass("scaleIn").addClass("scaleOut");
            });
        }
        //スワイプ
        if(!_isIe9)
        {
            $("#modal_pic").hammer().on('swipeleft', function(e) {
                if(_openNum >= $("#c11 > div.pic").length-1) return;

                _openNum ++;
                TOP.modalchangePic(_openNum, 300);
            });
            $("#modal_pic").hammer().on('swiperight', function(e) {
                if(_openNum <= 0) return;

                _openNum --;
                TOP.modalchangePic(_openNum, 300);
            });
        }

        //scrollEvent
        $(window).on("scroll", function(){ TOP.scroll(); });

        //scrollstopEvent
        $(window).on("scrollstop",function(){
            //scrollArrow

            var _end = _scrollY4 + 1;    //c05のテキストが出た位置
            if((!_isSp && !_isIe9) && TOP.getBrowserWidth() >= 769 && TOP.getScroll().y < _end && $("#scrollarrow").css('display') != "block")
            // if((!_isSp && !_isIe9) && TOP.getBrowserWidth() >= 769 && TOP.getScroll().y < $('#c06').offset().top-100 && $("#scrollarrow").css('display') != "block")
            {
               $("#scrollarrow").stop(true).fadeIn(200);
            }
        });

        TOP.scroll();
        TOP.layout();

        if((!_isSp && !_isIe9) && TOP.getBrowserWidth() >= 769 && TOP.getScroll().y < $('#c06').offset().top-100) 
        {
            $("#scrollarrow").stop(true).fadeIn(200);
        }

        //SNS
        $("#c12 > div.share > div.icons > a").on(_touchEvent, function(e){
            e.preventDefault();

            var href = $(this).attr("href");
            window.open(href, 'modalwindow', 'width=640, height=480, menubar=no, toolbar=no, scrollbars=yes');
        });

        //クエリ取得
        var _q = window.location.search.split("=")[1];
        if(TOP.getBrowserWidth() < 769) var _margin = 68;
        else _margin = 0;

        if(_q != ("" || undefined)){
            var position = $("#"+_q).offset().top - _margin;
            $('body,html').animate({scrollTop:position}, 1, 'swing');
            window.history.pushState(null, null, "index.html");

        };
    },
    animateRoots:function(bool)
    {
        if(bool)
        {
            //show
            $("#modal_dynamicshield > div.inner  div.car1 > div.car").velocity({opacity:1}, { delay: 0, duration: 500, easing: "swing"});
            $("#modal_dynamicshield > div.inner  div.car2 > div.car").velocity({opacity:1, left: 0}, { delay: 0, duration: 1000, easing: "swing"});
            $("#modal_dynamicshield > div.inner  div.car3 > div.car").velocity({opacity:1, left: 0}, { delay: 0, duration: 1000, easing: "swing"});

            $("#modal_dynamicshield > div.inner  div.car2 > div.p1").velocity({opacity:1}, { delay: 1000, duration: 300, easing: "swing"});
            $("#modal_dynamicshield > div.inner  div.car2 > div.p2").velocity({opacity:1}, { delay: 1050, duration: 300, easing: "swing"});
            $("#modal_dynamicshield > div.inner  div.car2 > div.p3").velocity({opacity:1, top: 0}, { delay: 1000, duration: 400, easing: "swing"});
            $("#modal_dynamicshield > div.inner  div.car3 > div.p1").velocity({opacity:1}, { delay: 1000, duration: 400, easing: "swing"});
            

            $("#modal_dynamicshield > div.inner  div.box1 > h2").velocity({opacity:1, marginLeft: -87}, { delay: 100, duration: 1000, easing: "swing"});
            $("#modal_dynamicshield > div.inner  div.box1 > h3").velocity({opacity:1, marginLeft: -140}, { delay: 200, duration: 1000, easing: "swing"});
            $(".jp #modal_dynamicshield > div.inner  div.box1 > p").velocity({opacity:1, marginLeft: -158}, { delay: 300, duration: 1000, easing: "swing"});
            $(".en #modal_dynamicshield > div.inner  div.box1 > p").velocity({opacity:1, marginLeft: -190}, { delay: 300, duration: 1000, easing: "swing"});

            $("#modal_dynamicshield > div.inner  div.box2 > h2").velocity({opacity:1, marginLeft: -103}, { delay: 100, duration: 1000, easing: "swing"});
            $("#modal_dynamicshield > div.inner  div.box2 > h3").velocity({opacity:1, marginLeft: -140}, { delay: 200, duration: 1000, easing: "swing"});
            $(".jp #modal_dynamicshield > div.inner  div.box2 > p").velocity({opacity:1, marginLeft: -140}, { delay: 300, duration: 1000, easing: "swing"});
            $(".en #modal_dynamicshield > div.inner  div.box2 > p").velocity({opacity:1, marginLeft: -135}, { delay: 300, duration: 1000, easing: "swing"});

            $("#modal_dynamicshield > div.inner  div.box3 > h2").velocity({opacity:1}, { delay: 1500, duration: 300, easing: "swing"});
            $("#modal_dynamicshield > div.inner  div.box3 > p").velocity({opacity:1}, { delay: 1500, duration: 300, easing: "swing"});

            $("#modal_dynamicshield > div.inner  div.arrow").velocity({opacity:1, top: 80}, { delay: 1600, duration: 500, easing: "swing"});

            $("#modal_dynamicshield > div.inner  div.car1 > div.p1").velocity({opacity:1}, { delay: 1800, duration: 300, easing: "swing"});
            $("#modal_dynamicshield > div.inner  div.car1 > div.p2").velocity({opacity:1}, { delay: 1850, duration: 500, easing: "swing"});
            $("#modal_dynamicshield > div.inner  div.car1 > div.p3").velocity({opacity:1, top: 0}, { delay: 1850, duration: 300, easing: "swing"});
            $("#modal_dynamicshield > div.inner  div.car1 > div.p4").velocity({opacity:1, top: 0}, { delay: 1950, duration: 350, easing: "swing"});
        }
        else
        {
            //hide
            $("#modal_dynamicshield > div.inner  div.car1 > div.car").css({opacity:0});
            $("#modal_dynamicshield > div.inner  div.car1 > div.p1").css({opacity:0});
            $("#modal_dynamicshield > div.inner  div.car1 > div.p2").css({opacity:0});
            $("#modal_dynamicshield > div.inner  div.car1 > div.p3").css({opacity:0, top: 30});
            $("#modal_dynamicshield > div.inner  div.car1 > div.p4").css({opacity:0, top: 10});

            $("#modal_dynamicshield > div.inner  div.car2 > div.car").css({opacity:0, left: -500});
            $("#modal_dynamicshield > div.inner  div.car2 > div.p1").css({opacity:0});
            $("#modal_dynamicshield > div.inner  div.car2 > div.p2").css({opacity:0});
            $("#modal_dynamicshield > div.inner  div.car2 > div.p3").css({opacity:0, top: 20});

            $("#modal_dynamicshield > div.inner  div.car3 > div.car").css({opacity:0, left: 500});
            $("#modal_dynamicshield > div.inner  div.car3 > div.p1").css({opacity:0});

            $("#modal_dynamicshield > div.inner  div.box1 > h2").css({opacity:0, marginLeft: -587});
            $("#modal_dynamicshield > div.inner  div.box1 > h3").css({opacity:0, marginLeft: -640});
            $("#modal_dynamicshield > div.inner  div.box1 > p").css({opacity:0, marginLeft: -640});

            $("#modal_dynamicshield > div.inner  div.box2 > h2").css({opacity:0, marginLeft: 403});
            $("#modal_dynamicshield > div.inner  div.box2 > h3").css({opacity:0, marginLeft: 440});
            $("#modal_dynamicshield > div.inner  div.box2 > p").css({opacity:0, marginLeft: 440});

            $("#modal_dynamicshield > div.inner  div.box3 > h2").css({opacity:0});
            $("#modal_dynamicshield > div.inner  div.box3 > p").css({opacity:0});

            $("#modal_dynamicshield > div.inner  div.arrow").css({opacity:0, top: 50});
        }
    },
    modalVideoInit:function(href)
    {
        if(href)
        {
            $("#modal_movie > div.inner").html('<iframe width="100%" height="100%" src="https://www.youtube.com/embed/' + href + '" frameborder="0" allowfullscreen></iframe>');
        }
        $("#modal_movie").append('<a class="js-close" href="#">CLOSE</a>');

        var posy = (($(window).height() -360 ) /2)-50 >= 15 ? (($(window).height() -360 ) / 2) -50 : 15 ;
        $('.js-close').css({'top':posy});

        $(window).on('resize', function() {
            var posy = (($(window).height() -360 ) /2)-50 >= 15 ? (($(window).height() -360 ) / 2) -50 : 15 ;
            $('.js-close').css({'top':posy});                
        });


        $("#modal_movie a.js-close").on(_touchEvent, function(e){
            e.preventDefault();
            $("#modal_movie a.js-close").off(_touchEvent);
            $("#modal_movie").removeClass("scaleIn");
            $("#modal_movie").fadeOut('200', function() {
                $("body").css({overflow:'visible'});
                $(window).off('.noScroll'); 
                $("#modal_movie > div.inner").html("");
            });
            $("#modal_movie > div.inner").removeClass("scaleIn").addClass("scaleOut");
        });
    },
    modalchangePic:function(n, d)
    {
        if(n <= 0)
            $("#modal_pic > div.inner > a.prev").hide();
        else
            $("#modal_pic > div.inner > a.prev").show();
        if(n >= $("#c11 > div.pic").length-1)
            $("#modal_pic > div.inner > a.next").hide();
        else
            $("#modal_pic > div.inner > a.next").show();

        var _picW = TOP.getBrowserWidth()-80;
        $("#modal_pic > div.inner > div.pics").velocity({left:(_picW*(n))*-1+40}, {duration: d, easing: "easeOutSine", complete:function(){
            _isc11Animate = false;
        }});
    },
    setDataMov:function()
    {
        var sw = TOP.getBrowserWidth();
        var _ccContW = (sw < 769)? 260 : 298;
        var _ccMargin = 0;
        var _ccSpeed = 300;
        var _posi = -_ccContW *_movHitNum;
        var $movBox = $('#c08 .movSlideBox .inner');
        var $movBoxChild = $movBox.find('a');
        var len =$movBoxChild.length;
        // $('#c08 div.slideBox > div.car0').off(_touchEvent);
        // $('#c08 div.slideBox > div.car1').off(_touchEvent);
        var winW = $(window).width();

            $(window).on('load resize', function() {
                winW = $(window).width();
                if(winW < 769){
                    $movBox.width('auto');
                }else{
                    $movBox.width($movBoxChild.width() * len +10);
                }
            });

        if(winW > 769){
            $movBox.width($movBoxChild.width() * len +10);
        }

        $movBox.velocity("stop").velocity({left:_posi}, {duration: _ccSpeed, easing: 'swing', complete: function(){  

            if(_movHitNum !== 0)
            {
                $('#c08 div.movSlideBox .slide-01').on(_touchEvent, function(e){
                    e.preventDefault();
                    _movHitNum = 0;
                    TOP.setDataMov();
                });
            }
            if(_movHitNum != 1)
            {
                $('#c08 div.movSlideBox .slide-02').on(_touchEvent, function(e){
                    e.preventDefault();
                    _movHitNum = 1;
                    TOP.setDataMov();
                });
            }
        }});

        $('#c08 div.mov_nav li').removeClass("v");
        if(_movHitNum === 0) {
            $('#c08 div.mov_nav li.m1').addClass("v");
            if(sw < 769)
            {
                $('#c08 div.mov_nav li.prev').hide();
                $('#c08 div.mov_nav li.next').hide();
            }
            else
            {
                $('#c08 div.mov_nav li.prev').css({'visibility':'hidden'});
                $('#c08 div.mov_nav li.next').css({'visibility':'inherit'});
            }
        }
        else if(_movHitNum === 1) {
            $('#c08 div.mov_nav li.m2').addClass("v");
            if(sw < 769)
            {
                $('#c08 div.mov_nav li.prev').hide();
                $('#c08 div.mov_nav li.next').hide();
            }
            else
            {
                $('#c08 div.mov_nav li.prev').css({'visibility':'inherit'});
                $('#c08 div.mov_nav li.next').css({'visibility':'hidden'});
            }
        }
    },
    setCC:function()
    {
        var sw = TOP.getBrowserWidth();
        var _ccContW = (sw < 769)? 260 : 960;
        var _ccMargin = (sw < 769)? 40 : 100;
        var _ccSpeed = 300;
        var _posi = (sw/2 - _ccContW/2) - (_ccContW + _ccMargin)*_ccHitNum;

        $('#c08 div.slideBox > div.car0').off(_touchEvent);
        $('#c08 div.slideBox > div.car1').off(_touchEvent);
        $('#c08 div.slideBox > div.car2').off(_touchEvent);
        $('#c08 div.slideBox > div.car3').off(_touchEvent);
        $('#c08 div.slideBox > div.car4').off(_touchEvent);

        $('#c08 div.slideBox').velocity("stop").velocity({left:_posi}, {duration: _ccSpeed, easing: 'swing', complete: function(){  

            if(_ccHitNum !== 0)
            {
                $('#c08 div.slideBox > div.car4').on(_touchEvent, function(e){
                    e.preventDefault();
                    _ccHitNum = 0;
                    TOP.setCC();
                });
            }
            if(_ccHitNum != 1)
            {
                $('#c08 div.slideBox > div.car3').on(_touchEvent, function(e){
                    e.preventDefault();
                    _ccHitNum = 1;
                    TOP.setCC();
                });
            }
            if(_ccHitNum != 2)
            {
                $('#c08 div.slideBox > div.car2').on(_touchEvent, function(e){
                    e.preventDefault();
                    _ccHitNum = 2;
                    TOP.setCC();
                });
            }
            if(_ccHitNum != 3)
            {
                $('#c08 div.slideBox > div.car1').on(_touchEvent, function(e){
                    e.preventDefault();
                    _ccHitNum = 3;
                    TOP.setCC();
                });
            }
            if(_ccHitNum != 4)
            {
                $('#c08 div.slideBox > div.car0').on(_touchEvent, function(e){
                    e.preventDefault();
                    _ccHitNum = 4;
                    TOP.setCC();
                });
            }
        }});

        $('#c08 div.local_nav li').removeClass("v");
        if(_ccHitNum === 0) {
            $('#c08 div.local_nav li.p1').addClass("v");
            if(sw < 769)
            {
                $('#c08 div.local_nav li.prev').hide();
                $('#c08 div.local_nav li.next').hide();
            }
            else
            {
                $('#c08 div.local_nav li.prev').hide();
                $('#c08 div.local_nav li.next').show();
            }
        }
        else if(_ccHitNum == 1) {
            $('#c08 div.local_nav li.p2').addClass("v");
            if(sw < 769)
            {
                $('#c08 div.local_nav li.prev').hide();
                $('#c08 div.local_nav li.next').hide();
            }
            else
            {
                $('#c08 div.local_nav li.prev').show();
                $('#c08 div.local_nav li.next').show();
            }
        }
         else if(_ccHitNum == 2) {
            $('#c08 div.local_nav li.p3').addClass("v");
            if(sw < 769)
            {
                $('#c08 div.local_nav li.prev').hide();
                $('#c08 div.local_nav li.next').hide();
            }
            else
            {
                $('#c08 div.local_nav li.prev').show();
                $('#c08 div.local_nav li.next').show();
            };
        }
         else if(_ccHitNum == 3) {
            $('#c08 div.local_nav li.p4').addClass("v");
            if(sw < 769)
            {
                $('#c08 div.local_nav li.prev').hide();
                $('#c08 div.local_nav li.next').hide();
            }
            else
            {
                $('#c08 div.local_nav li.prev').show();
                $('#c08 div.local_nav li.next').show();
            };
        }
         else if(_ccHitNum == 4) {
            $('#c08 div.local_nav li.p5').addClass("v");
            if(sw < 769)
            {
                $('#c08 div.local_nav li.prev').hide();
                $('#c08 div.local_nav li.next').hide();
            }
            else
            {
                $('#c08 div.local_nav li.prev').show();
                $('#c08 div.local_nav li.next').hide();
            };
        }

    },
    slideNav:function()
    {
        if($("nav > p.head > span.icon").hasClass("open")) $("nav > p.head > span.icon").removeClass("open")
        else $("nav > p.head > span.icon").addClass("open")

        $("nav > ul").slideToggle('normal', function(){
            if($(this).css('display') == "none")
            {
                //close
                $("body").css({overflow:'visible'})
                $(window).off('.noScroll');

                $("#skrollr-body > div.bg").off(_touchEvent);
            }
            else
            {
                //open
                $("body").css({overflow:'hidden'})
                $(window).on('touchmove.noScroll', function(e) {
                    e.preventDefault();
                });

                $("#skrollr-body > div.bg").on(_touchEvent, function() {
                    TOP.slideNav();
                });
            }

            if(TOP.getBrowserWidth() >= 769 && $("nav > ul").css('display') == "none") $("nav > ul").show();
        });
        $("#skrollr-body > div.bg").fadeToggle('normal');
    },
    animateDynamicShield:function(bool)
    {
        if(bool)
        {
            //show
            $("#c07 div.car_left1").velocity({opacity:1, left: '50%'}, 800, 'swing');
            $("#c07 div.car_left2").velocity({opacity:1, left: '50%'}, 1200, 'swing');
            $("#c07 div.car_left3").velocity({opacity:1, left: '50%'}, 1600, 'swing');
            $("#c07 div.car_right1").velocity({opacity:1, left: '50%'}, 1600, 'swing');
            $("#c07 div.car_right2").velocity({opacity:1, left: '50%'}, 1200, 'swing');
            $("#c07 div.car_right3").velocity({opacity:1, left: '50%'}, 800, 'swing');
            $("#c07 div.parts1").velocity({opacity:1}, { delay: 1000, duration: 300, easing: "swing"});
            $("#c07 div.parts2").velocity({opacity:1}, { delay: 1050, duration: 500, easing: "swing" });
            $("#c07 div.parts3").velocity({opacity:1, top:13}, { delay: 1050, duration: 300, easing: "swing" });
            $("#c07 div.parts4").velocity({opacity:1, top:13}, { delay: 1150, duration: 300, easing: "swing" });
        }
        else
        {
            //hide
            $("#c07 div.car_left1").velocity("stop").css({opacity:1, left: $("#c07 div.car_left1").width()*-1});
            $("#c07 div.car_left2").velocity("stop").css({opacity:1, left: $("#c07 div.car_left1").width()*-1});
            $("#c07 div.car_left3").velocity("stop").css({opacity:1, left: $("#c07 div.car_left1").width()*-1});
            $("#c07 div.car_right1").velocity("stop").css({opacity:1, left: '100%'});
            $("#c07 div.car_right2").velocity("stop").css({opacity:1, left: '100%'});
            $("#c07 div.car_right3").velocity("stop").css({opacity:1, left: '100%'});
            $("#c07 div.parts1").velocity("stop").css({opacity:0});
            $("#c07 div.parts2").velocity("stop").css({opacity:0});
            $("#c07 div.parts3").velocity("stop").css({opacity:0, top:43});
            $("#c07 div.parts4").velocity("stop").css({opacity:0, top:23});
        }
    },
    scroll:function()
    {
        var _scrollY = TOP.getScroll().y;

        if(_scrollY < 68)
        {
            $("nav").css({position:"absolute", top:"68px"});
            $("span.new").stop(true).fadeIn(300);
        }
        else
        {
            $("nav").css({position:"fixed", top:0});
            $("span.new").stop(true).fadeOut(300);
        }

        if((!_isSp && !_isIe9) && TOP.getBrowserWidth() >= 769)
        {
            //マスク1
            var _startP1 = ($("#c02 > div.bg").width() - $("#c02 > div.bg > img").width())/2 + ($("#c02 > div.bg").width()/2) + 500;
            var _per1 = Math.round((_scrollY - _msk1min)/((_msk1max - _msk1min)/100)*10)/10;
            var _moveX1;

            if(_scrollY < _msk1min) _moveX1 = _startP1 - (($("#c02 > div.bg").width() + 1000)/100 * 0);
            else if(_scrollY > _msk1max) _moveX1 = _startP1 - (($("#c02 > div.bg").width() + 1000)/100 * 100);
            
            if(_scrollY >= _msk1min-100 && _scrollY <= _msk1max+100)
            {
                _moveX1 = _startP1 - (($("#c02 > div.bg").width() + 1000)/100 * _per1);
            };
            $("#c02 > div.bg > img").css({left:_moveX1});

            //マスク2
            var _startP2 = ($("#c03 > div.bg").width() - $("#c03 > div.bg > img").width())/2 - ($("#c03 > div.bg").width()/2) - 500;
            var _per2 = Math.round((_scrollY - _msk2min)/((_msk2max - _msk2min)/100)*10)/10;
            var _moveX2;
            
            if(_scrollY < _msk2min) _moveX2 = _startP2 + (($("#c03 > div.bg").width() + 1000)/100 * 0);
            else if(_scrollY > _msk2max) _moveX2 = _startP2 + (($("#c03 > div.bg").width() + 1000)/100 * 100);
            if(_scrollY > _msk2min-100 && _scrollY < _msk2max+100)
            {
                _moveX2 = _startP2 + (($("#c03 > div.bg").width() + 1000)/100 * _per2);
            };
            $("#c03 > div.bg > img").css({left:_moveX2});

            //video再生
            if(_scrollY > _videoStartandEnd[0] && _scrollY < _videoStartandEnd[1])
            {
                if(!_isMovPlay)
                {
                    //moviePlay
                    _isMovPlay = true;
                    var v = document.getElementById("video");
                    v.play();

                    if((TOP.getBrowserWidth()/960-TOP.getBrowserHeight()/540)<=0)
                    {
                        $("#c04 > div.movie > video").css({
                            height:'100%',
                            left: '50%',
                            'margin-left': $("#c04 > div.movie > video").width()/2*-1,
                            'margin-top':0,
                            top:0,
                            width:'auto'
                        });
                    }
                    else
                    {
                        $("#c04 > div.movie > video").css({
                            height:'auto',
                            left:0,
                            'margin-left': 0,
                            'margin-top':$("#c04 > div.movie > video").height()/2*-1,
                            top: '50%',
                            width:'100%'
                        });
                    };
                }
            }
            else
            {
                if(_isMovPlay)
                {
                    //moviePlay
                    _isMovPlay = false;
                    var v = document.getElementById("video");
                    v.pause();
                }
            }

            // if(TOP.getBrowserWidth() >= 769)
            // {
                var scrollHeight = $(document).height();
                var scrollPosition = $(window).height() + _scrollY;

                if(_scrollY < $("#c06").offset().top-100){
                    
                    {
                        $("nav li > a").removeClass('v');
                        $("nav li").eq(0).children('a').addClass('v');
                    }
                    _current = 0;
                }
                else if(_scrollY < $("#c08").offset().top-100){
                    if(_current != 1)
                    {
                        $("nav li > a").removeClass('v');
                        $("nav li").eq(1).children('a').addClass('v');
                    }
                    _current = 1;
                }
                else if(_scrollY < $("#c09").offset().top-100){
                    if(_current != 2)
                    {
                        $("nav li > a").removeClass('v');
                        $("nav li").eq(2).children('a').addClass('v');
                    }
                    _current = 2;
                }
                else if(_scrollY < $("#c10").offset().top-100){
                    if(_current != 3)
                    {
                        $("nav li > a").removeClass('v');
                        $("nav li").eq(3).children('a').addClass('v');
                    }
                    _current = 3;
                }
                else if(_scrollY < $("#c13").offset().top-100){
                    if(_current != 4)
                    {
                        $("nav li > a").removeClass('v');
                        $("nav li").eq(4).children('a').addClass('v');
                    }
                    _current = 4;
                }
                // else if(_scrollY < $("#c12").offset().top){
                //     if((scrollHeight - scrollPosition) < 200)
                //     {
                //         if(_current != 5)
                //         {
                //             $("nav li > a").removeClass('v');
                //             $("nav li").eq(5).children('a').addClass('v');
                //         }
                //         _current = 5;
                //     }
                //     else
                //     {
                //         if(_current != 4)
                //         {
                //             $("nav li > a").removeClass('v');
                //             $("nav li").eq(4).children('a').addClass('v');
                //         }
                //         _current = 4;
                //     }
                // }
                else
                {
                    if(_current != 5)
                    {
                        $("nav li > a").removeClass('v');
                        $("nav li").eq(5).children('a').addClass('v');
                    }
                    _current = 5;
                }

                //scrollArrow
                if($("#scrollarrow").css('display') == "block")
                {
                   $("#scrollarrow").stop(true).fadeOut(100);
                }
                // if(!_isAutoScroll && _scrollY < $('#c06').offset().top-100 && $("#scrollarrow") != "block")
                // {
                //    $("#scrollarrow").fadeIn(200);
                // }
                // else if(!_isAutoScroll && _scrollY >= $('#c06').offset().top-100 && $("#scrollarrow") == "block")
                // {
                //    $("#scrollarrow").fadeOut(200);
                // }

                //gynamic shield animation
                var _ds_middleP = $("#c07").offset().top - TOP.getBrowserHeight()/2 + $("#c07 > div.head").height()/2;

                //if(_ds_middleP -40 < _scrollY && _scrollY < _ds_middleP +40 && !_isDSPlay)
                if(_scrollY < $("#c07").offset().top + 100 && $("#c07").offset().top - TOP.getBrowserHeight() + $("#c07 > div.head").height() - 100 < _scrollY && !_isDSPlay)
                {
                    _isDSPlay = true;
                    TOP.animateDynamicShield(true);
                }

                if(_isDSPlay && (_scrollY > _ds_middleP + TOP.getBrowserHeight()/2 + $("#c07 > div.head").height()/2 || _scrollY < _ds_middleP - TOP.getBrowserHeight()/2 - $("#c07 > div.head").height()/2))
                if(_isDSPlay && (_scrollY > $("#c07").offset().top + $("#c07 > div.head").height() || $("#c07").offset().top - TOP.getBrowserHeight() > _scrollY))
                {
                    _isDSPlay = false;
                    TOP.animateDynamicShield(false);
                }
            // }
        }
        _scrollpy = _scrollY;
    },
    layout:function()
    {
        //fontsize
        // var _fontSize = 62.5 + (Math.min(TOP.getBrowserWidth(), 600)-320)*0.15+1;
        // if(_fontSize < 62.5) _fontSize = 62.5;
        // else if(_fontSize > 125) _fontSize = 125;
        // $("html").css({"font-size":_fontSize+"%"});


        var sw = TOP.getBrowserWidth();
        var sh = TOP.getBrowserHeight();
        if(_isSp){
            if(_iosHeight < sh) _iosHeight = sh;
        }

        $("#c01").height(sh - $("nav").height());
        // $("#c02, #c03, #c04, #c05, #c06").height(sh);

        if($("nav > p.head > span").hasClass("open")) TOP.slideNav();
        else if($("nav > ul").css('display') != "none") $("nav > ul").hide();


        //conceptcar
        if(!_isIe8) TOP.setCC();


        //ブレークポイント
        if(sw < 769)
        {
            //SP
            $("#c02").height(250);
            $("#c03").height(350);
            $("#c04").height(600);
            $("#c05").height(500);
            // $("#c06").height(250);

            $("#intro").height("auto");

            if(_isLoad && _skrollr != undefined)
            {
                _skrollr.destroy();
                _skrollr = undefined;
            }

            //future
            $("#c09 > div.btns > a.mov").width(160).height(90);
            $("#c09 > div.btns > a.recruit").width(90).height(90);

            //studio
            $("#c10 > div.studioBox").width('100%');
            $("#c10 > div.studioBox").css({"margin-left": 0});

            //nav
            // $("nav li > a").removeClass('v');

            //scrollArrow
            if($("#scrollarrow").css('display') != "none")
            {
                $("#scrollarrow").hide();
            }

            //c11
            $("#modal_pic > div.inner > div.pics > img").width(sw-80).height(sw-80);


            //c13
            var _c13W = Math.min(560, sw);
            var _c13H = (_c13W*520)/560;
            $("#c13 > div.inner").width(_c13W).height(_c13H);
            

            var _picsX = 40 - ((sw-80) * _openNum);
            $("#modal_pic, #modal_pic > div.inner").height(sh);
            $("#modal_pic > div.inner > div.pics").height(sw-80).width((sw-80)*$("#c11 > div.pic").length).css({left: _picsX, marginTop:(sw-80)/2*-1});
            $("#modal_pic > div.inner > a.prev, #modal_pic > div.inner > a.next").height(sw-80).css({marginTop:(sw-80)/2*-1});
        }
        else
        {
            //PC
            $("#c02, #c03, #c04, #c05").height(sh);

            $("#intro").height(_introHeight + sh);

            if(_isSp || _isIe9)
            {
                $("#c04 > p.t04_1").css({"margin-top": "-200px"});
                $("#c04 > p.t04_2").css({"margin-top": "-122px"});
                $("#c04 > p.t04_3").css({"margin-top": "-43px"});
                $("#c04 > p.t04_4").css({"margin-top": "150px"});
            }

            if((!_isSp && !_isIe9) && _isLoad && _skrollr == undefined)
            {
                _skrollr = skrollr.init({forceHeight:false});
            };

            if($("nav > ul").css('display') == "none")
            {
                $("nav > ul").css({display:"block"});
            }

            //movie
            if((sw/960-sh/540)<=0)
            {
                $("#c04 > div.movie > video").css({
                    height:'100%',
                    left: '50%',
                    'margin-left': $("#c04 > div.movie > video").width()/2*-1,
                    'margin-top':0,
                    top:0,
                    width:'auto'
                });
            }
            else
            {
                $("#c04 > div.movie > video").css({
                    height:'auto',
                    left:0,
                    'margin-left': 0,
                    'margin-top':$("#c04 > div.movie > video").height()/2*-1,
                    top: '50%',
                    width:'100%'
                });
            };

            //future
            var _futureMovW = ($("#c09 > div.btns").width() / 520) * 296;
            var _futureRecW = ($("#c09 > div.btns").width() / 520) * 166;
            $("#c09 > div.btns > a.mov").width(_futureMovW).height(_futureRecW);
            var _futureRecW = ($("#c09 > div.btns").width() / 520) * 166;
            $("#c09 > div.btns > a.recruit").width(_futureRecW).height(_futureRecW);

            //studio
            $("#c10 > div.studioBox").width(Math.min(sw-2, 962));
            $("#c10 > div.studioBox").css({"margin-left": -1* $("#c10 > div.studioBox > ul.btns").width()/2 -1 + "px"});

            //c13
            var _c13W = Math.min(960, sw);
            var _c13H = (_c13W*540)/960;
            $("#c13 > div.inner").width(_c13W).height(_c13H);

            //modal_dynamicshield
            $("#modal_dynamicshield").height(sh);

            //スクロール矢印
            if((!_isSp && !_isIe9) && !_isAutoScroll && TOP.getScroll().y < $('#c06').offset().top-100 && $("#scrollarrow").css('display') != "block")
            {
                $("#scrollarrow").show();//fadeIn(100);
            }

            //ローダーと矢印の位置
            $("#scrollarrow, #loader .throbber").css({marginTop : sh/2 - (sh/100)*15 + "px"});

            if($("#modal_pic").css('display') != "none")
            {
                $("#modal_pic").hide();
            }
        }

        $("#c11 > div.pic").height($("#c11 > div.pic").width());


        if((_isSp || _isIe9) && sw >= 769)
        {
            if(_skrollr != undefined)
            {
                _skrollr.destroy();
                _skrollr = undefined;
            }

            $("#intro").css({height: "auto"});
            $("#c02, #c03, #c04, #c05").css({position: "relative"});

            if($("#scrollarrow").css('display') == "block" ) $("#scrollarrow").hide();

            $("#c02 > div.bg").css(
            {
                "background-image": "url('/en/spirit/design/share/images/StoryArea-chap01.png')",
                "background-position": "center center",
                "background-size": "auto 100%",
                position: "absolute",
            });
            $("#c02 > div.bg > img").css(
            {
                display: "none"
            });

            $("#c03 > div.bg").css(
            {
                "background-image": "url('/en/spirit/design/share/images/StoryArea-chap02.png')",
                "background-size": "auto 100%",
                position: "absolute",
            });
            $("#c03 > div.bg > img").css(
            {
                display: "none"
            });

            $("#c04 > div.msk").css(
            {
                display: "block"
            });

            $("#c04 > div.movie").css(
            {
                position: "absolute",
            });
            $("#c04 > div.movie #video").css(
            {
                display: "none"
            });

            $("#c05 > div.bg").css(
            {
                "background-position": "center center",
                position: "absolute"
            });



        }
    },
    shuffle:function(list)
    {
        var i = list.length;
        while (i) {
            var j = Math.floor(Math.random() * i);
            var t = list[--i];
            list[i] = list[j];
            list[j] = t;
        }
    },
    // スクロール位置を取得
    getScroll:function(){
        return {
            x:document.body.scrollLeft || document.documentElement.scrollLeft,
            y:document.body.scrollTop || document.documentElement.scrollTop
        }
    },
    getBrowserWidth:function()
	{
		if ( window.innerWidth ) { return window.innerWidth; }
		else if ( document.documentElement && document.documentElement.clientWidth != 0 ) { return document.documentElement.clientWidth; }
		else if ( document.body ) { return document.body.clientWidth; }
		return 0;
	},
	getBrowserHeight:function()
	{
		if ( window.innerHeight ) { return window.innerHeight; }
		else if ( document.documentElement && document.documentElement.clientHeight != 0 ) { return document.documentElement.clientHeight; }
		else if ( document.body ) { return document.body.clientHeight; }
		return 0;
	}
}


var TOP = new Project();

$(function(){
    //scrollstopイベント設定
    var scrollStopEvent = new $.Event("scrollstop");
    var delay = 200;
    var timer;
     
    function scrollStopEventTrigger(){
        if (timer) {
            clearTimeout(timer);
        }
        timer = setTimeout(function(){$(window).trigger(scrollStopEvent)}, delay);
    }
    // $('html,body').animate({ scrollTop: 0 }, '1');

    $(window).on("scroll", scrollStopEventTrigger);
    $("body").on("touchmove", scrollStopEventTrigger);
    //////

    TOP.init();



    // モーダル
    var modal = function($el) {
      var $arg = $el.find('a.modal-btn'),
          frameH,h1,posx,posy,h1pos,closePos,modalPos,winW,current_scrollY,winH,setH,closeH,
          frameW = 768,
          lang = $arg.data('lang');

     if(lang === 'jp'){
        h1 = '<h1 id="recruit-tit">募集職種</h1>';
     } else{
        h1 = '<h1 id="recruit-tit">Job opening type</h1>';
     }


      $arg.on('click', function() {
        $('body').append('<div id="overlay"></div>');
        $('#overlay').fadeIn();

        var modalView = '<div class="modal-iframe"><iframe id="frame" class="modal" src="'+$(this).attr('href')+ '"></iframe></div>',
            closeBtn = '<div id="close-btn"></div>';

        current_scrollY = $( window ).scrollTop(); 
        winW =$(window).width();


        $('body').prepend(modalView,closeBtn,h1);

        // ifarme 高さ設定
        if(null!=$('iframe.modal')){
            $('iframe.modal').load(function(){
              if (typeof $(this).attr('height') == 'undefined') {
                try{
                  frameH = this.contentWindow.document.documentElement.scrollHeight+10;
                }catch(e){}
                if(!isNaN(frameH) && !frameH == 0){
        
                }else{
                  frameH = $(window).height() * 0.8;
                }
                frameW = winW * 0.8 -15;
                posx  = ($(window).width() - frameW)  / 2;
                posy  =   frameH >  $(window).height() *0.8 ? $(window).height() *0.8 : frameH;
                closePos = ($(window).height() - posy) / 2 - 100 < 0 ? 10 : ($(window).height() - posy) / 2 -100;
                h1pos = closePos === 10 ? 10 : ($(window).height() - posy) / 2 - 70;
                modalPos = closePos === 10 ? 80 : ($(window).height() - posy) / 2 +20;

                modalSetPosition();
                // $('#close-btn').css({'right': posx+'px','top':($(window).height() - posy) / 2 - 60 +'px'}).animate({'opacity':1},600,"swing");
              }
            });
        }
        $('iframe.modal').triggerHandler('load');


        $('html').addClass('modal-html');
        $('.modal').stop(true,true).fadeIn('400', function() {
            $('#close-btn,h1#recruit-tit').stop().animate({'opacity':1},200,"swing");
        });

        $(window).on('resize', function() {
            winW = $(window).width();
            posx  = (winW- frameW)  / 2;
            posy  =   frameH >  $(window).height() *0.8 ? $(window).height() *0.8 : frameH;
            frameW = winW * 0.8 -15;

            closePos = ($(window).height() - posy) / 2 - 100 < 0 ? 10 : ($(window).height() - posy) / 2 -100;
            h1pos = closePos === 10 ? 10 : ($(window).height() - posy) / 2 - 70;
            modalPos = closePos === 10 ? 80 : ($(window).height() - posy) / 2 +20;
            modalSetPosition();
            // $('#close-btn').css({'right': posx+'px','top':($(window).height() - posy) / 2 - 60 +'px'});
            return false;
        });

        $(window).on('orientationchange', function() {
          modalClose();
        });
        $('#close-btn, #overlay').on('click', function() {
          modalClose();
        });

        return false;
      });

      function modalSetPosition() {
        winH = $(window).height();
        h1H = $('#recruit-tit').height();
        closeH = 12;

        if(winW < 769) {
            // sp
            frameW = '100%';
            h1H = $('#recruit-tit').height();
            h1margin = 30;
            // setH = winH;
            h1pos = '8%';
            modalPos =  $(window).height() * 0.2;
            // modalPos = frameH /2  ;
            $('h1#recruit-tit').css({'top': h1pos,'margin-top':0});
            $('#close-btn').css({'top': closePos,'margin-top':0});
            $('.modal-iframe').css({
              'top': modalPos,
              'margin-top':0,
              'max-width': '100%',
              'width':'100%',
              'height':$(window).height()- modalPos,
              'margin-top':0
            });
        } else {
          // pc
          h1margin =  100;
          // ブラウザ縦値とiframeの縦値を比較
          if(winH - 100 < (frameH) ){
            setH = winH - 160;
          }else{
            setH = frameH;
          }
            $('.modal-iframe').css({
              'max-width':frameW,
              'width': '70%',
              'height':setH,
              'margin-top': -setH/2 +h1H,
              'top':'50%'
            });
            $('#close-btn').css({'margin-top': -setH/2 +h1H -h1margin + closeH,'top':'50%'});
            $('h1#recruit-tit').css({'margin-top': -setH/2 +h1H -h1margin,'top':'50%' });
            $('iframe.modal').css({'max-height': $(window).height() *0.8 - 3});
        }


      }

      function modalClose() {
          $('#close-btn,h1#recruit-tit').remove();
          $('.modal').fadeOut();
          $('#overlay').fadeOut('slow',function(){
            $('.modal-iframe').remove();
            $('#overlay').remove();
            $('html').removeClass('modal-html');
          });
          // $('html, body').stop().animate({scrollTop: current_scrollY },1000);
           TOP.scroll();
      }

    };
    modal($('.modal-wrap'));

});



$(function(){
    "use strict";
    var slider = function(el) {
        var $el         = el,
            $slider     = $el.find('.slider'),
            $btn        = $el.closest('#modal_dynamicshield').find('.js-btn'),
            $clsBtn     = $el.closest('#modal_dynamicshield').find('.js-close'),
            index       = 0,
            len = $slider.length,
            posL,winW,winH;

        function init() {
            $slider[0].classList.add('active');
            onload();
        } 

        function onload() {
            $(window).on('load resize', function() {
                winW = $(window).outerWidth() + 15;
                winH = $(window).height();
                $el.css({'width':winW*len});
                $slider.css({'width': winW});
                onclick();
                setData();
            });
        }

        function onclick() {
            $btn.on('click', function() {
                var num = $btn.index($(this));
                index += num > 0 ? 1 : -1;
                setData();
            });

            $clsBtn.on('click', function() {
                $el.css({'width':winW});
                $slider[1].classList.remove('animate');
                $slider.removeClass('active');
                $slider.find('h2').removeClass('active')
                index = 0;
                endAnim();
                setData()
            });
        }

        function setData() {
            posL = winW ;
            var clsPos = (winH - 780) / 2;
            if(index <= 0) {
                $slider[1].classList.remove('active');
                $slider[0].classList.add('active');
                $btn[1].style.display="block";
                $btn[0].style.display="none";
                index=0;
            }
            if(index >= len-1){
                $slider[0].classList.remove('active');
                $slider[1].classList.add('active');
                $slider[1].classList.add('animate');
                $btn[1].style.display="none";
                $btn[0].style.display="block";
                index=len-1;
                console.log($el.find('.box4').css('display'));
                if($el.find('.box4').css('display') !== 'block'){
                    animate();
                }
            } 
            render(clsPos);
        }

        function render(clsPos) {

            $el.css({'right': posL * (index)});
            // var closeBtn = $('.js-close').css({'top':clsPos+70});
        }

        function animate() {
            var $box4 = $('#modal_dynamicshield').find('.box4'),
                $box5 = $('#modal_dynamicshield').find('.box5'),
                $box6 = $('#modal_dynamicshield').find('.box6'),
                speed = 800;
                endAnim();

            $box4.stop().fadeIn(speed, function(){
                $box4.stop().animate({'opacity':1},speed, function() {
                    $box4.find('.img-02').animate({'opacity':1},
                        speed,
                        'swing',
                        function(){
                            box5();
                        });
                })
            })


            function box5() {
                $box5.stop(true,true).fadeIn(speed, function() {
                    draw1()
                    draw2()
                    setTimeout(function() {
                        box6();
                    },3000);

                    function draw1(){
                        var cs       = document.getElementById('cv-01'),
                            ctx      = cs.getContext('2d'),
                            csWidth  = cs.width,
                            csHeight = cs.height,
                            center   = {
                              x: csWidth / 2,
                              y: csHeight / 2
                            };

                        // 線の共通スタイル
                        ctx.strokeStyle = '#00b7ee';
                        ctx.lineWidth   = 1;
                        ctx.lineJoin    = 'round';

                        var Graph = function(arg) {
                          this.initialize(arg);
                          /* 共通の静的プロパティ */
                          this.moveLength = 0;
                          this.addLength  = 5;
                          this.isAnim     = function() {
                            return (this.moveLength < this.hypotenuse);
                          };
                        };

                        (function (p) {
                          /**
                           * インスタンスごとの初期設定
                           * @param  {array.obj} arg [beginPos.x, beginPos.y, endPos.x, endPos.y]
                           */
                          p.initialize = function(arg) {
                            this.dfd = $.Deferred();
                            this.beginPos = {
                              x: arg.beginPos.x,
                              y: arg.beginPos.y
                            };
                            this.movePos = {
                              x: arg.beginPos.x,
                              y: arg.beginPos.y
                            };
                            this.endPos = {
                              x: arg.endPos.x,
                              y: arg.endPos.y
                            };
                            this.side = {
                              x: this.endPos.x - this.beginPos.x,
                              y: this.endPos.y - this.beginPos.y
                            };
                            this.hypotenuse = Math.sqrt(Math.pow(this.side.x, 2) + Math.pow(this.side.y, 2));
                            this.radian = Math.atan2(this.side.y, this.side.x);
                          };
                          p.draw = function() {
                            ctx.beginPath();
                            ctx.moveTo(this.beginPos.x, this.beginPos.y);
                            ctx.lineTo(this.movePos.x, this.movePos.y);
                            ctx.closePath();
                            ctx.stroke();
                          };
                          p.update = function() {
                            this.moveLength += this.addLength;
                            this.movePos.x += Math.cos(this.radian) * this.addLength;
                            this.movePos.y += Math.sin(this.radian) * this.addLength;
                          };
                          p.render = function(num) {
                            this.draw();
                            if (this.isAnim() === true) {
                              this.update();
                              this.movePos.x = (this.isAnim() === false) ? this.endPos.x : this.movePos.x;
                              this.movePos.y = (this.isAnim() === false) ? this.endPos.y : this.movePos.y;
                              requestAnimationFrame(this.render.bind(this));
                            } else {
                              // アニメーションが完了したら通知する
                              this.dfd.resolve();
                            }
                            if(!$('.box5 .text-01').hasClass('active')) $('.box5 .text-01').addClass('active');
                            return this.dfd.promise();
                          };
                        })(Graph.prototype);

                        var graphData = [
                          {
                            beginPos: { x: 0, y: csHeight},
                            endPos: { x: 120, y: 0}
                          },
                          {
                            beginPos: { x: 120, y: 0},
                            endPos: { x: csWidth, y: 0}
                          }

                        ];

                        // 以降、グラフインスタンスの生成とレンダリング
                        var graphObj = {},
                            i = 0,
                            j = 0,
                            l = graphData.length;
                        for (; i < l; i++) {
                          graphObj[i] = new Graph(graphData[i]);
                        }

                        $(function() {
                          var d = (new $.Deferred()).resolve();
                          $.each(graphObj, function(i, obj){
                            d = d.then(function() {

                              return obj.render();
                            });
                          });
                        })
                    }
                    function draw2(){
                        var cs       = document.getElementById('cv-02'),
                            ctx      = cs.getContext('2d'),
                            csWidth  = cs.width,
                            csHeight = cs.height,
                            center   = {
                              x: csWidth / 2,
                              y: csHeight / 2
                            };

                        // 線の共通スタイル
                        ctx.strokeStyle = '#00b7ee';
                        ctx.lineWidth   = 1;
                        ctx.lineJoin    = 'round';

                        var Graph = function(arg) {
                          this.initialize(arg);
                          /* 共通の静的プロパティ */
                          this.moveLength = 0;
                          this.addLength  = 5;
                          this.isAnim     = function() {
                            return (this.moveLength < this.hypotenuse);
                          };
                        };

                        (function (p) {
                          /**
                           * インスタンスごとの初期設定
                           * @param  {array.obj} arg [beginPos.x, beginPos.y, endPos.x, endPos.y]
                           */
                          p.initialize = function(arg) {
                            this.dfd = $.Deferred();
                            this.beginPos = {
                              x: arg.beginPos.x,
                              y: arg.beginPos.y
                            };
                            this.movePos = {
                              x: arg.beginPos.x,
                              y: arg.beginPos.y
                            };
                            this.endPos = {
                              x: arg.endPos.x,
                              y: arg.endPos.y
                            };
                            this.side = {
                              x: this.endPos.x - this.beginPos.x,
                              y: this.endPos.y - this.beginPos.y
                            };
                            this.hypotenuse = Math.sqrt(Math.pow(this.side.x, 2) + Math.pow(this.side.y, 2));
                            this.radian = Math.atan2(this.side.y, this.side.x);
                          };
                          p.draw = function() {
                            ctx.beginPath();
                            ctx.moveTo(this.beginPos.x, this.beginPos.y);
                            ctx.lineTo(this.movePos.x, this.movePos.y);
                            ctx.closePath();
                            ctx.stroke();
                          };
                          p.update = function() {
                            this.moveLength += this.addLength;
                            this.movePos.x += Math.cos(this.radian) * this.addLength;
                            this.movePos.y += Math.sin(this.radian) * this.addLength;
                          };
                          p.render = function(num) {
                            this.draw();
                            if (this.isAnim() === true) {
                              this.update();
                              this.movePos.x = (this.isAnim() === false) ? this.endPos.x : this.movePos.x;
                              this.movePos.y = (this.isAnim() === false) ? this.endPos.y : this.movePos.y;
                              requestAnimationFrame(this.render.bind(this));
                            } else {
                              // アニメーションが完了したら通知する
                              this.dfd.resolve();
                            }
                            if(!$('.box5 .text-02').hasClass('active')) $('.box5 .text-02').addClass('active');
                            return this.dfd.promise();
                          };
                        })(Graph.prototype);

                        var graphData = [
                          {
                            beginPos: { x: 0, y: csHeight},
                            endPos: { x: 80, y: 0}
                          },
                          {
                            beginPos: { x: 80, y: 0},
                            endPos: { x: csWidth, y: 0}
                          }

                        ];

                        // 以降、グラフインスタンスの生成とレンダリング
                        var graphObj = {},
                            i = 0,
                            j = 0,
                            l = graphData.length;
                        for (; i < l; i++) {
                          graphObj[i] = new Graph(graphData[i]);
                        }

                        $(function() {
                          var d = (new $.Deferred()).resolve();
                          $.each(graphObj, function(i, obj){
                            d = d.then(function() {

                              return obj.render();
                            });
                          });
                        })
                    } 
                })
            }

            function box6() {
                    $box6.stop(true,false).fadeIn(speed);
            }
        }
        // animation

        function endAnim() {
            $el.find('.box6,.box5,.box4').stop(true,true).hide();
            $el.find('.box4').css({'opacity':0});
            $el.find('.box4 .img-02').css({'opacity':0});
            var cv = document.getElementById('cv-01'),
                cv2 = document.getElementById('cv-02'),
                ctx = cv.getContext('2d'),
                ctx2 = cv2.getContext('2d');

            ctx.clearRect(0,0,cv.width,cv.height);
            ctx2.clearRect(0,0,cv2.width,cv2.height);
        }

        return {
            start: init
        };
    };

    var module  = slider( $('.js-slider') );
        module.start();
})













