/*!
 * MMC DESIGN SITE
 * script.js
 */
!function (a, b, c, d) {
    function e() {
        for (var a, c = 3, d = b.createElement("div"); d.innerHTML = "<!--[if gt IE " + ++c + "]><i></i><![endif]-->", d.getElementsByTagName("i")[0];);
        return c > 4 ? c : a
    }
    c.easing.easeOutExpo = function (a, b, c, d, e) {
        return b == e ? c + d : d * (-Math.pow(2, -10 * b / e) + 1) + c
    },
        c.fn.hoverImageReplace = function (b) {
        var e = {
                matchStr         : "_ov",
                touchDeviceDetach: !0
            },
            f = c.extend(!1, e, b, this.data()),
            g = a.ontouchstart !== d,
            h = new RegExp("^(.+)" + f.matchStr + "(..+)$");
        return this.each(function () {
            var a,
                b;
            a = c(this),
                b = "IMG" === a[0].nodeName ? a : a.find("img"),
                a.on({
                "mouseenter.hoverImageReplace": function () {
                    if (!g || !f.touchDeviceDetach) {
                        var a = b.attr("src").replace(/^(.+)(\..+)$/, "$1" + f.matchStr + "$2");
                        b.attr("src", a);
                        b.attr("style","opacity:0.3");
                        b.animate({"opacity":"1"}, 500, "linear");
                    }
                },
                "mouseleave.hoverImageReplace": function () {
                    if (!g || !f.touchDeviceDetach) {
                        var a = b.attr("src").replace(h, "$1$2");
                        b.attr("src", a);
                    }
                }
            })
        })
    };
    var f,
        g = function (b, e, g) {
            var h = c(a);
            g && (g = c.easing[g] ? g : d),
                b.on("click", function (a) {
                var b = c(this);
                a.preventDefault(),
                    "#" !== b.attr("href") && (c("#pageNav").find("a").removeClass("active"), h.off("scroll.scrollNavEvent"), b.addClass("active"), c("html,body").stop(!0, !0).animate({
                    scrollTop: c(b.attr("href")).offset().top
                }, e, g, function () {
                    h.on("scroll.scrollNavEvent", function () {
                        var a = h.scrollTop();
                    })
                }))
            })
        },
        h = function (b) {
            function d(a) {
                b.each(a >= i ? function () {
                    var a = c(this);
                    "false" !== a.attr("data-reposi") ? (f = a.attr("data-originH") / i, e = -(g.width() * f - a.attr("data-originH")) / 2) : e = 0,
                        a.css({
                        left      : "auto",
                        marginLeft: 0,
                        marginTop : e,
                        position  : "relative",
                        top       : "auto"
                    })
                } : function () {
                    var a = c(this);
                    e = -(a.attr("data-originH") / 2),
                        a.css({
                        left      : "50%",
                        marginLeft: -(i / 2),
                        marginTop : e,
                        position  : "absolute",
                        top       : "50%"
                    })
                })
            }
            var e,
                f,
                g = c(a),
                h = g.width(),
                i = 1400;
            d(h),
                g.on("resize", function () {
                var a = g.width();
                d(a)
            })
        },
        i = function () {
            function a(a, b) {
                var c = new String(b),
                    d = a - c.length;
                if (0 >= d) 
                    return c;
                for (; d-- > 0;) 
                    c = "0" + c;
                return c
            }
            function b() {
                var c,
                    i = new Date,
                    j = d - i,
                    k = 864e5,
                    l = Math.floor(j / k),
                    m = Math.floor(j % k / 36e5),
                    n = Math.floor(j % k / 6e4) % 60,
                    o = Math.floor(j % k / 1e3) % 60 % 60;
                j > 0 ? (e.text(a(2, l)), f.text(a(2, m)), g.text(a(2, n)), h.text(a(2, o)), c = setTimeout(function () {
                    b()
                }, 1e3)) : clearTimeout(c)
            }
            var d = new Date("June 29,2014 00:00:00"),
                e = c(".js-days"),
                f = c(".js-hour"),
                g = c(".js-min"),
                h = c(".js-sec");
            b()
        },
        j = function () {
            function a(a) {
                a.preventDefault(),
                    b.fadeOut("fast", function () {
                    e.empty(),
                        g.css({
                        overflow: "visible"
                    })
                })
            }
            var b,
                d,
                e,
                f,
                g = c("body");
            f = '<div class="modal" id="modal"><div class="modal_inner" id="modalInner"><div class="modal_conte" id="modalConte"></div></div><div class="close_wrap"><a href="#" class="js-modal-close"><img src="img/main/close_btn.png" alt="close"></a></div></div>',
                g.append(f),
                b = c("#modal"),
                d = c("#modalInner"),
                e = c("#modalConte"),
                c(".js-modal-open").on("click", function (a) {
                var f = c(this).next().children().clone();
                a.preventDefault(),
                    e.append(f),
                    g.css({
                    overflow: "hidden"
                }),
                    b.fadeIn("fast"),
                    d.scrollTop(0),
                    m()
            }),
                b.on("click", function (b) {
                "modal" === b.target.id && a(b)
            }),
                b.on("click", ".js-modal-close", function (b) {
                a(b)
            }),
                b.on("click", ".js-modal-close", function (b) {
                a(b)
            })
        },
        k = function (a) {
            function b(a) {
                a.preventDefault(),
                    d.fadeOut("fast", function () {
                    c(this).remove(),
                        h.css({
                        overflow: "visible"
                    })
                })
            }
            var d,
                e,
                f,
                g,
                h = c("body"),
                i = c(a),
                j = function (a) {
                    var b = '<iframe src="//www.youtube.com/embed/' + a + '?rel=0&autoplay=1&hd=1&autohide=2" width="800" height="450" frameborder="0" allowfullscreen></iframe>';
                    return b
                };
            f = '<div class="modal" id="movieModal"><div class="movie_modal_inner" id="movieModalInner"><div class="m_close_wrap"><a href="#" class="js_m_modal_close"><img src="img/main/close_btn.png" alt="close"></a></div></div></div>',
                h.append(f),
                d = c("#movieModal"),
                e = c("#movieModalInner"),
                g = c(".js_m_modal_close"),
                e.prepend(j(i.attr("data-movie"))),
                h.css({
                overflow: "hidden"
            }),
                d.fadeIn("fast"),
                d.on("click", function (a) {
                "movieModal" === a.target.id && b(a)
            }),
                g.on("click", function (a) {
                b(a)
            })
        },
        l = function () {
            function a(a) {
                var b = 0;
                a.each(function () {
                    b < c(this).height() && (b = c(this).height())
                }),
                    a.height(b)
            }
            a(c(".wrapper > li"))
        },
        m = function () {
            function a(a) {
                {
                    var b = f.filter(":visible"),
                        c = f.eq(a);
                    c.attr("alt")
                }
                h.removeClass("active"),
                    h.eq(a).addClass("active"),
                    b.hide(),
                    c.show()
            }
            var b = c("#modal #slide_show"),
                d = b.find(".nextBtn"),
                e = b.find(".previewBtn"),
                f = (b.find(".title"),
                b.find(".photoList img")),
                g = b.find(".thumb li"),
                h = b.find(".thumb li span"),
                i = 0,
                j = f.length;
            d.click(function () {
                i++,
                    i === j && (i = 0),
                    a(i)
            }),
                e.click(function () {
                i--,
                    0 > i && (i = j - 1),
                    a(i)
            }),
                g.click(function () {
                return i === g.index(this) ? !1 : (i = g.index(this), void a(i))
            }),
                g.hover(function () {
                return c(this).find("span").hasClass("active") === !0 ? !1 : void c(this).stop().animate({
                    opacity: "0.5"
                }, 200)
            }, function () {
                c(this).stop().animate({
                    opacity: "1"
                }, 200)
            }),
                a(i)
        },
        n = function () {
            function a(a) {
                a.preventDefault(),
                    b.fadeOut("fast", function () {
                    e.empty(),
                        j.css("overflow", "visible")
                })
            }
            var b,
                d,
                e,
                f,
                g,
                h,
                i,
                j = c("body"),
                k = [];
            f = '<div class="modalG" id="modalG"><div class="modalG_inner" id="modalGInner"><div class="gallery" id="galleryConte"></div><div class="close_wrap"><a href="#" class="js-modal-close"><img src="img/main/close_btn.png" alt="close"></a></div></div></div>',
                g = '<div class="gallery_nav_btn prev" id=""><img src="img/main/prev_btn.png" alt=""></div>',
                h = '<div class="gallery_nav_btn next" id="nextBtn"><img src="img/main/next_btn.png" alt=""></div>',
                j.append(f),
                b = c("#modalG"),
                d = c("#modalGInner"),
                e = c("#galleryConte"),
                c(".js-modal-g-open").click(function (a) {
                var d = new Image,
                    f = c(this).closest(".js-gallery-wrap").find("a"),
                    l = 0,
                    m = f.length;
                for (a.preventDefault(); m > l; l++) 
                    k[l] = f.eq(l).attr("data-image-url");
                i = c(this).closest(".js-gallery-wrap").find("a").index(this),
                    d.src = c(this).attr("data-image-url"),
                    e.append(g),
                    e.append(d),
                    e.append(h),
                    j.css("overflow", "hidden"),
                    b.fadeIn("fast")
            }),
                b.on("click", "#nextBtn", function () {
                var a;
                i >= k.length - 1 || (i++, a = k[i], e.children("img").attr("src", a))
            }),
                b.on("click", "#prevBtn", function () {
                var a;
                0 >= i || (i--, a = k[i], e.children("img").attr("src", a))
            }),
                b.on("click", function (b) {
                "modalGInner" === b.target.id && a(b)
            }),
                b.on("click", ".js-modal-close", function (b) {
                a(b)
            })
        },
        s = function (b) {
            function d(a) {
                a >= g ? b.removeClass(h) : b.addClass(h)
            }
            var e = c(a),
                f = e.width(),
                g = 1400,
                h = "minwidth";
            d(f),
                e.on("resize", function () {
                var a = e.width();
                d(a)
            })
        };
    s(c("#flexslider")),
        i(),
        j(),
        l(),
        n(),
        g(c('a[href^="#"]'), 500, "easeOutExpo"),
        c(".js_movie_modal").on("click", function (a) {
        k(this, a)
    }),
        c(".js-img-hover").hoverImageReplace(),
        c(function () {
        c("#flexslider").flexslider({
            animation        : "fade",
            controlsContainer: ".pager",
            directionNav     : 1,
            keyboard         : 1,
            touch            : 1
        })
    }),
        c(a).on({
        load: function () {
                h(c(".js-bgImgrePosi"))
        }
    })
}(window, document, jQuery);


$(function(){ 
    $('.js-img-hover').click(function(){
    	var srcstr = $(this).attr('src');
    	srcstr = srcstr.replace("thumb_","");
    	srcstr = srcstr.replace("_ov","");
    	$('.img_area img').attr('src',srcstr).fadeIn();
    });
}); 
