/* ----------------------------------------------------------------------------------
　スムーズスクロール
---------------------------------------------------------------------------------- */
var scroll_func = function () {
	$('html,body').animate({ scrollTop: $($(this).attr("href")).offset().top }, 'slow','swing');
	return false;
}

$(function(){
	$('[href^=#]').not('[href$=#], .sort_btn li a, a.app_cbox').click(scroll_func);
});


/* ----------------------------------------------------------------------------------
　change UA
---------------------------------------------------------------------------------- */
if (!(navigator.userAgent.indexOf('iPhone') > 0 || navigator.userAgent.indexOf('iPod') > 0 || (navigator.userAgent.indexOf('Android') > 0 && navigator.userAgent.indexOf('Mobile') > 0))) {
	$('meta[name="viewport"]').attr('content','width=980px');
}

/* ----------------------------------------------------------------------------------
　ロールオーバー
---------------------------------------------------------------------------------- */
var ua = navigator.userAgent;
var isIE8 = ua.match(/msie [8.]/i);

if (!isIE8) {
	$.fn.rollover2 = function() {
		return this.each(function() {
			var target = this;
			$.each($(this).find('img[src*=_off]'), function(){
				var img = this;
				$(img).addClass('rollover2');
				if($(img).prev().is('[src*=_on]')){}else{
					
					var overimg = $(document.createElement('img')).attr('src', img.src.replace(/_off/, '_on'));
					$(overimg).css({ opacity:0, position:'absolute' }).addClass('over');
					$(img).before(overimg);
					
					if($(overimg).parents('button').length){
						$(overimg).parents('button').hover(function(){
							var hoverimg = $(this).find('img')[0];
							$(hoverimg).stop().animate({ opacity:1 }, 200);
						},function(){
							var hoverimg = $(this).find('img')[0];
							$(hoverimg).stop().animate({ opacity:0 }, 200);
						});
					}else if($(overimg).parents('.entry_item>a').length){
						$(overimg).parents('.entry_item>a').hover(function(){
							var hoverimg = $(this).find('img')[0];
							$(hoverimg).stop().animate({ opacity:1 }, 200);
						},function(){
							var hoverimg = $(this).find('img')[0];
							$(hoverimg).stop().animate({ opacity:0 }, 200);
						});
					}else{
						$(overimg).hover(function(){
							var hoverimg = this;
							$(hoverimg).stop().animate({ opacity:1 }, 200);
						},function(){
							var hoverimg = this;
							$(hoverimg).stop().animate({ opacity:0 }, 200);
						});
					}
					
				}
			});
		});
	};
	$(function(){
		$('img[src*=_on]').hover(function(){
			$(this).css({ opacity:1 });
		});
	});
	$(function(){
		$('a img').hover( function(){
			$(this).not('img[src*=_on],img[src*=_off], .logo img, #Head_nav img, .item_cover img').stop().animate({ opacity:0.7 }, 200);
		}, function(){
			$(this).not('img[src*=_on],img[src*=_off], .logo img, #Head_nav img, .item_cover img').stop().animate({ opacity:1 }, 200);
		});
	});

	$(function(){ $('body').rollover2(); });

}

/* ----------------------------------------------------------------------------------
　リストの最後の要素に .last を付加
---------------------------------------------------------------------------------- */
$(function(){
	$('li:last-child').addClass('last');
});

/* ----------------------------------------------------------------------------------
　UAを判別し、bodyにクラスをつける。
　縦横を判別し、bodyにクラスをつける。
---------------------------------------------------------------------------------- */
$(function(){
	setOperate();
});
function setOperate(){
	setView();
	var agent = navigator.userAgent;
	if(agent.search(/iPhone/) != -1){
		$("body").addClass("iphone"); //iPhoneには「body class="iphone"」追加
		window.onorientationchange = setView;
	}else if(agent.search(/iPad/) != -1){
		$("body").addClass("ipad"); //iPadには「body class="ipad"」追加
		window.onorientationchange = setView;
	}else if(agent.search(/Android/) != -1){
		$("body").addClass("android"); //Androidには「body class="android"」追加
		window.onresize = setView;
	}else if(agent.search(/Mac OS/) != -1){
		$("body").addClass("mac"); //macには「body class="mac"」追加
		window.onorientationchange = setView;
	}else{
		$("body").addClass("other"); //上記以外には「body class="other"」追加
		window.onorientationchange = setView;
	}
}
function setView(){
	var orientation = window.orientation;
	if(orientation === 0){
		$("body").addClass("portrait"); //画面が縦向きの場合は「body class="portrait"」追加
		$("body").removeClass("landscape"); //画面が縦向きの場合は「body class="landscape"」削除
	}else{
		$("body").addClass("landscape"); //画面が横向きの場合は「body class="landscape"」追加
		$("body").removeClass("portrait"); //画面が横向きの場合は「body class="portrait"」削除
	}
}

/* ----------------------------------------------------------------------------------
　aタグタッチ中にクラスをつける
---------------------------------------------------------------------------------- */
$('a').bind({
	'touchstart': function(e) {
		$(this).removeClass("notouchstyle").addClass("touchstyle");
	},
	'touchend': function(e) {
		$(this).removeClass("touchstyle").addClass("notouchstyle");
	}
});


/* ----------------------------------------------------------------------------------
　PC Models
---------------------------------------------------------------------------------- */
$(function(){
	$(window).load(function(){
		model_li();
	});

	function model_li(){
		$('.entry_models li:nth-child(3n)').addClass('mr0')
	}	
});
/* ----------------------------------------------------------------------------------
　SP Gnav acc btn
---------------------------------------------------------------------------------- */
$(function(){
	var flag = 0;
	$('#Gnav_switch').click(function(){
		if(flag == 0){
			$(this).find('img').attr('src', $(this).find('img').attr('src').replace('_static', '_active'));
			flag = 1;
		}else if(flag == 1){
			$(this).find('img').attr('src', $(this).find('img').attr('src').replace('_active', '_static'));
			flag = 0;
		}
		$('#Gnav_over').stop().fadeToggle('fast');
		$('#Gnav').stop().slideToggle('fast');
		$('#Head').toggleClass('active');
		return false;
	});
});

// IE7/IE8で透過png画像フェード //////////////////////////////////////////////////////
$(function() {
    if(navigator.userAgent.indexOf("MSIE") != -1) {
        $('img').not('.movie_thumb img').each(function() {
            if($(this).attr('src').indexOf('.png') != -1) {
                $(this).css({
                    'filter': 'progid:DXImageTransform.Microsoft.AlphaImageLoader(src="' +
                    $(this).attr('src') +
                    '", sizingMethod="scale");'
                }).wrap('<span class="img_png"></span>');
            }
        });
    }
});

// Android get ver  //////////////////////////////////////////////////////
$(function(){
	var _UA = navigator.userAgent;
	if( _UA.search(/Android 2.[123]/) != -1 ) {
		$("body").addClass("old_android");
	}
});
