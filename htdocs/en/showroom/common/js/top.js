/* ----------------------------------------------------------------------------------
　movie thumb
---------------------------------------------------------------------------------- */
$(function(){

	$("a.app_cbox").colorbox({inline:true});

	var video = [];
	$('.movie_youtube iframe').each(function(){
		video.push($(this).attr('src'));
	})

	var flag = 0;

	$(document).on('click', '.movie_thumb', function(){

		ga('send', 'event', 'SR_TOP', 'movie', 'movie', 1);

		if ( navigator.userAgent.indexOf('iPhone') > 0 || (navigator.userAgent.indexOf('Android') > 0 && navigator.userAgent.indexOf('Mobile') > 0) ) {
			var mv_url = $(this).find('a').attr('href');
			window.open(mv_url,'_blank');
			return false;
		}


		flag = 1;
		$('.movie_thumb').css({ visibility:'hidden' });
		var $iframe = $(this).parent().find('.movie_youtube iframe');
		var index = $('.movie_youtube iframe').index($iframe);
		var src = video[index]+'&autoplay=1';
		$iframe.css({ visibility:'visible' }).attr('src', src);

		var ww = $(window).width();

		if(ww<=768){
			return false;
		}

	});

	$('.sort_name:not(.models_tab .sort_name)').click(function(){
		$(this).next().slideToggle();
		$(this).find('span').toggleClass('active');
	});

	$(window).on('load resize', function() {
		var ww = $(window).width();
		if(ww<=768){
			var mt_w = $('.movie_thumb').width();
			var mt_h = $('.movie_thumb').height();
			$('.movie_youtube iframe').width(mt_w);
			$('.movie_main, .movie_youtube iframe').height(mt_h);
			$('.sort_btn').css('display','none');
			if(flag == 1){
				$('.movie_title').hide();
			}
		}else{
			$('.movie_youtube iframe').width('630px');
			$('.movie_youtube iframe').height('354px');
			$('.movie_main').height('354px');
			$('.movie_title').show();
			$('.sort_btn').css('display','inline');
			$('.sort_name span').removeClass('active');
		}
	});


	/**
	 * モデル絞込み
	 */
	(function(){

		var isLteIE8         = navigator.appVersion.match(/MSIE [678]/);

		var $wrapper         = $('#Models');
		var $categories      = $wrapper.find('.sort_btn > ul > li');
		var $itemBox         = $wrapper.find('.entry_models');
		var $items           = $itemBox.children('ul').children().clone(true);
		var $currentCategory = $('.sort_name > span');

		$categories.find('a').on('click', function(){

			$(this).parent().addClass('active').siblings().removeClass('active');
			var currentCategoryText = $currentCategory.text();
			var newCategoryText = $(this).text();
			if(currentCategoryText!=='FILTER VEHICLES' || newCategoryText!=='ALL') $currentCategory.text(newCategoryText);

			$itemBox.height($itemBox.height());

			if(isLteIE8){
				$itemBox.children().remove();
			}else{
				$itemBox.children().fadeOut(null, function(){ $(this).remove(); });
			}

			var $categoryItems = (this.hash==='#tag-all'? $items: $items.filter('.'+this.hash.substr(1))).clone(true);
			var $newItemBoxUl = $('<ul></ul>').addClass('cFix').css({ position:'absolute', left:0, top:0 }).hide().appendTo($itemBox);
			$newItemBoxUl.append($categoryItems);
			$categoryItems.filter(':nth-child(3n)').addClass('mr0');

			if(isLteIE8){
				$newItemBoxUl.css({ position:'static' }).show();
				$itemBox.height($newItemBoxUl.height());
			}else{
				$newItemBoxUl.fadeIn();
				$itemBox.stop(true, true).animate({ height:$newItemBoxUl.height() });
			}

			return false;

		});

		if(!isLteIE8){
			var nowWidth = window.innerWidth;
			$(window).on('resize', function(){
				var re_w = window.innerWidth;
				if(re_w != nowWidth){
					nowWidth = re_w;
					$itemBox.height('');
					$categories.filter('.active').find('a').click();
				}
			});
		}

	})();
	/**
	 * galleryエリア
	 */
	(function(){

		var mode = 'pc';
		if(window.matchMedia &&  window.matchMedia('(max-width:767px)').matches){
			mode = 'sp';
		}
		var sp_youtube_setup = function(){
			$('#Gallery .movie_list .thumbnail_area').each(function(index, el) {
				var video_id = $(this).data('youtubeid');
				var iframe = '<iframe src="//www.youtube.com/embed/' + video_id + '?rel=0" frameborder="0" allowfullscreen>';
				$(this).find('img').hide();
				$(this).append(iframe);
			});
		}
		var sp_youtube_reset = function(){
			$('#Gallery .movie_list .thumbnail_area').each(function(index, el) {
				$(this).find('img').show();
				$(this).find('iframe').remove();
			});
		}
		var photo_list_show = function(show_c){
			if (show_c === undefined) {
				if(mode == 'sp'){
					show_c = 5;
				}else{
					show_c = 12;
				}
			}
			$('.photo_list ul li').not('.show').each(function(index, el) {
				if(index > show_c-1){return false;}
				var $this = $(this);
				var $this_a = $(this).find('a');
				if(mode == 'sp'){
					var imgsrc = $this_a.attr('href');
					var $image =$('<img class="SP" src="" alt="">');
					$image.attr('src',imgsrc);
					$this.append($image).addClass('show');
				}else{
					var imgsrc = $this_a.data('photosrc');
					var $image =$('<img class="PC" src="" alt="">');
					$image.attr('src',imgsrc);
					$this_a.append($image).parent('li').addClass('show');
				}
			});
			if($('.photo_list ul li').not('.show').length <= 0){
				$('.photo_list_btn').remove();
			}
		}
		var photo_list_show_resize = function(){
			$('.photo_list ul li.show').each(function(index, el) {
				var $this = $(this);
				var $this_a = $(this).find('a');
				if(mode == 'sp'){
					if(!$this.find('img.SP')[0]){
						var imgsrc = $this_a.attr('href');
						var $image =$('<img class="SP" src="" alt="">');
						$image.attr('src',imgsrc);
						$this.append($image).addClass('show');
					}
				}else{
					if(!$this.find('img.PC')[0]){
						var imgsrc = $this_a.data('photosrc');
						var $image =$('<img class="PC" src="" alt="">');
						$image.attr('src',imgsrc);
						$this_a.append($image).parent('li').addClass('show');
					}
				}
			});

			if(mode != 'sp'){
				var show_c = 12;
				var count = $('.photo_list ul li.show').length;
				var def = $('.photo_list ul li.show').length % show_c;
				if(def > 0){
					var shows = show_c - def;
					photo_list_show(shows);
				}
			}
		}
		$(function(){
			$('#Gallery .movie_list .thumbnail_area').click(function(event) {
				if(mode == 'pc'){
					var video_id = $(this).data('youtubeid');
					$.colorbox({iframe:true, innerWidth:711, innerHeight:399,href:'http://www.youtube.com/embed/'+video_id+'?rel=0&wmode=transparent&autoplay=1'});
				}
			});
			$(".photo_list .photo_g_modal").colorbox({
				rel:'photo_g_modal',
				current:"{current}/{total}"
			});
			photo_list_show();

			$('.photo_list').addClass('show');
			$('#Show_More').addClass('show');
			$('#Show_More').click(function(event) {
				photo_list_show();
				return false;
			});
			if(mode == 'sp'){
				sp_youtube_setup();
			}
			/* リサイズ処理 */
			var timer = false;
			$(window).resize(function() {
				if (timer !== false) {
					if(window.matchMedia && window.matchMedia('(max-width:767px)').matches && mode == 'pc'){
						mode = 'sp';
						$.colorbox.close();
						sp_youtube_setup();
						photo_list_show_resize();
						$('.photo_list').removeClass('mode_pc');
					}else if((!window.matchMedia || !window.matchMedia('(max-width:767px)').matches) && mode == 'sp'){
						mode = 'pc';
						$.colorbox.close();
						sp_youtube_reset();
						photo_list_show_resize();
						$('.photo_list').addClass('mode_pc');
					}
				}
				timer = setTimeout(function() {
				}, 200);
			});
		});

	})();

	/**
	 * スライド
	 */
	(function(){

		var $slide     = $('#slide');
		var $slideMain = $slide.find('.slide_main');
		var $slidePrev = $slide.find('.slide_backbtn');
		var $slideNext = $slide.find('.slide_nextbtn');
		var $slideNavi = $slide.find('.slide_pagenavi');

		var onChange = function(slider, duration){
			var d = (duration==null)? 1200: d;
			var $mainvis = slider.slides.eq(slider.animatingTo);
			$mainvis.find('.mainvis_text').stop(true, true).hide().css({ left:0 }).delay(d? 500: 0).fadeIn(d);
			$mainvis.find('.pc_btn').stop(true, true).hide().css({ left:0 }).delay(d? 700: 0).fadeIn(d);
			$mainvis.find('.app_btn').stop(true, true).hide().css({ left:0 }).delay(d? 900: 0).fadeIn(d);
		};

		if(window.innerWidth<=640) $slideMain.children('ul').children().width($(window).width());

		$slideMain.flexslider({
			animation        : 'slide',
			slideshowSpeed   : 6000,
			animationSpeed   : 400,
			useCSS           : false,
			directionNav     : false,
			smoothHeight     : true,
			controlsContainer: $slideNavi,
			start            : onChange,
			before           : onChange
		});

		$slidePrev.click(function(){
			$slideMain.flexslider('prev');
			return false;
		});

		$slideNext.click(function(){
			$slideMain.flexslider('next');
			return false;
		});

		var slideResizeTimer = null;
		$(window).on('resize', function(){
			clearTimeout(slideResizeTimer);
			slideResizeTimer = setTimeout(function(){
				var slider = $slideMain.data('flexslider');
				slider.setup();
				var $mainvis = slider.slides.eq(slider.animatingTo);
				$mainvis.find('.mainvis_text').stop(true).show();
				$mainvis.find('.pc_btn').stop(true).show();
				$mainvis.find('.app_btn').stop(true).show();
			}, 100);
		});


	})();



	/* 20150917 追加 */
	// SP SortButton //////////////////////////////////////////////////////

	$('.ga_sort_name, .ga_sort_btn li').on('click.pulldown', function(){
		$('.ga_sort_name').find('span').toggleClass('active');
		$('.ga_sort_name').next().slideToggle('normal');
	});

});
