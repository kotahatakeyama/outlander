
var resizeHandler = function(){

	var w = $(window).width();
	var h = $(window).height();

	if(!('innerWidth' in window)) window.innerWidth = w;

	if(window.innerWidth > 767){

		var minH=490;
		var maxH=750;
		var headerHeight = 120;

		if(w<1280){
			$("#Mainvis_bg").css({width: "100%"});
			}else{
			$("#Mainvis_bg").css({width: "100%"});
		 }

		if(h<minH){
			$('#Mainvis_bg').css({height: minH-headerHeight});
		}else if(h>maxH){
			$('#Mainvis_bg').css({height: maxH-headerHeight});
		}else{
			$('#Mainvis_bg').css({height: h-headerHeight});
		}

	}else{

		$('#Mainvis_bg').css({ width:'', height:'' });

	}

	//$(".imgstyle_01").css("display","none");

};

$(document).ready(resizeHandler);
$(window).resize(resizeHandler);

//リサイズ時関数
function resize_window() {

	w = $(window).width();
	if (!('innerWidth' in window)) window.innerWidth = w;
	if (w < 768) {
		if (!f_flag) {
			sp_mode = true;
			sp_format();
			f_flag = true;
		}
		if (!sp_mode) {
			sp_mode = true;
			sp_format();
		}
	} else {
		if (!f_flag) {
			sp_mode = false;
			pc_format();
			f_flag = true;
		}
		if (sp_mode) {
			sp_mode = false;
			pc_format();
		}
	}
	if (sp_mode) {
		sp_resize();
	} else {
		pc_resize();
	}

}

function sp_format() {
	$('#Mainvis_bg,#Mainvis .player_wrap').attr('style', '');
	$('.thumb_movie_wrap').css({
		opacity: 0
	});
	if (p_flag) {
		try {
			mainvis_pla.destroy();
			t_movie_01.destroy();
			t_movie_02.destroy();
			t_movie_03.destroy();
			t_movie_04.destroy();
		} catch (e) {}

		mainvis_pla_flg = false;
		t_movie_01_flg = false;
		t_movie_02_flg = false;
		t_movie_03_flg = false;
		t_movie_04_flg = false;
	}

}

function pc_format() {
	if (!movie_none) {
		if (p_flag) {
			onYouTubeIframeAPIReady();
		} else {
			var tag = document.createElement('script');
			tag.src = "https://www.youtube.com/iframe_api";
			var firstScriptTag = document.getElementsByTagName('script')[0];
			firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
			p_flag = true;
		}
	}

}

function sp_resize() {}

function pc_resize() {

	var mainvis_w = $('#Mainvis').width();
	var h = $(window).height();
	var minH = 490;
	var maxH = 840;
	var headerHeight = 120;

	if (h < minH) {
		h = minH - headerHeight;
	} else if (h > maxH) {
		h = maxH - headerHeight;
	} else {
		h = h - headerHeight;
	}
	$('#Mainvis_bg').css({
		'height': h + 'px'
	});
	if (!movie_none) {
		var t_h = (mainvis_w / 16) * 9;
		var t_w = (h / 9) * 16;

		if (h > t_h) {
			$('#Mainvis .player_wrap').css({
				'height': h + 'px',
				'width': t_w + 'px'
			});
			$('#mainvis_pla').css({
				'left': '-' + (t_w - mainvis_w) / 2 + 'px',
				'top': '0'
			});
		} else {
			$('#Mainvis .player_wrap').css({
				'height': t_h + 'px',
				'width': mainvis_w + 'px'
			});
			$('#mainvis_pla').css({
				'left': '0',
				'top': '-' + (t_h - h) /2 + 'px'
			});
		}
	}
}
(function($) {
	$(function() {

		resize_window();
		var timer = false;
		$(window).resize(function() {
			if (timer !== false) {
				clearTimeout(timer);
			}
			timer = setTimeout(function() {
				resize_window();
			}, 200);
		});
		$('#Mainvis_ov').click(function() {
			return false;
		});
		$('#Mainvis_ov').dblclick(function() {
			return false;
		});
		if (istablet) {
			$('#sec_design_01 .pc_bg,#sec_design_02 .pc_bg,#sec_performance_01 .pc_bg,#sec_performance_02 .pc_bg').addClass('istablet');
			$('#sec_design_01 .imgstyle_02,#sec_design_02 .imgstyle_02,#sec_performance_01 .imgstyle_02,#sec_performance_02 .imgstyle_02').addClass('istablet');
			$('#sec_design_01 .pc_bg img').attr('src', './images/bg_02_tb.jpg');
		}

	});

	$(window).on('unload', function() {});

})(jQuery);
