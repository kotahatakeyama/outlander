/* ----------------------------------------------------------------------------------
　スムーズスクロール
---------------------------------------------------------------------------------- */
var scroll_func = function () {
  $('html,body').animate({ scrollTop: $($(this).attr("href")).offset().top }, 'slow','swing');
  return false;
}

$(function(){
  $('[href^=#]').not('[href$=#], a.pd_switch, .tab_area a, .btn_order a, #Foot a, #Morebtn').click(scroll_func);
  $('#Morebtn').on('click', function() {
    var offset = $($(this).attr("href")).offset().top - 120;
    $('html,body').animate({ scrollTop: offset }, 'slow','swing');
    return false;
  });
});


/* ----------------------------------------------------------------------------------
　change UA
---------------------------------------------------------------------------------- */
if (!(navigator.userAgent.indexOf('iPhone') > 0 || navigator.userAgent.indexOf('iPod') > 0 || (navigator.userAgent.indexOf('Android') > 0 && navigator.userAgent.indexOf('Mobile') > 0))) {
  $('meta[name="viewport"]').attr('content','width=980px');
}

/* ----------------------------------------------------------------------------------
　ロールオーバー
---------------------------------------------------------------------------------- */
var ua = navigator.userAgent;
var isIE8 = ua.match(/msie [8.]/i);

if (!isIE8) {
  $.fn.rollover2 = function() {
    return this.each(function() {
      var target = this;
      $.each($(this).find('img[src*=_off]'), function(){
        var img = this;
        $(img).addClass('rollover2');
        if($(img).prev().is('[src*=_on]')){}else{

          var overimg = $(document.createElement('img')).attr('src', img.src.replace(/_off/, '_on'));
          $(overimg).css({ opacity:0, position:'absolute' }).addClass('over');
          $(img).before(overimg);

          if($(overimg).parents('button').length){
            $(overimg).parents('button').hover(function(){
              var hoverimg = $(this).find('img')[0];
              $(hoverimg).stop().animate({ opacity:1 }, 200);
            },function(){
              var hoverimg = $(this).find('img')[0];
              $(hoverimg).stop().animate({ opacity:0 }, 200);
            });
          }else{
            $(overimg).hover(function(){
              var hoverimg = this;
              $(hoverimg).stop().animate({ opacity:1 }, 200);
            },function(){
              var hoverimg = this;
              $(hoverimg).stop().animate({ opacity:0 }, 200);
            });
          }

        }
      });
    });
  };
  $(function(){
    $('img[src*=_on]').hover(function(){
      $(this).css({ opacity:1 });
    });
  });
  $(function(){
    $('a img').hover( function(){
      $(this).not('img[src*=_on],img[src*=_off], .logo img, #Head_nav img, .item_cover img, .btnstyle_01 img, .btnstyle_02 img').stop().animate({ opacity:0.7 }, 200);
    }, function(){
      $(this).not('img[src*=_on],img[src*=_off], .logo img, #Head_nav img, .item_cover img, .btnstyle_01 img, .btnstyle_02 img').stop().animate({ opacity:1 }, 200);
    });
  });

  $(function(){ $('body').rollover2(); });

}

/* ----------------------------------------------------------------------------------
　リストの最後の要素に .last を付加
---------------------------------------------------------------------------------- */
$(function(){
  $('li:last-child').addClass('last');
});

/* ----------------------------------------------------------------------------------
　UAを判別し、bodyにクラスをつける。
　縦横を判別し、bodyにクラスをつける。
---------------------------------------------------------------------------------- */
$(function(){
  setOperate();
});
function setOperate(){
  setView();
  var agent = navigator.userAgent;
  if(agent.search(/iPhone/) != -1){
    $("body").addClass("iphone"); //iPhoneには「body class="iphone"」追加
    window.onorientationchange = setView;
  }else if(agent.search(/iPad/) != -1){
    $("body").addClass("ipad"); //iPadには「body class="ipad"」追加
    window.onorientationchange = setView;
  }else if(agent.search(/Android/) != -1){
    $("body").addClass("android"); //Androidには「body class="android"」追加
    window.onresize = setView;
  }else if(agent.search(/Mac OS/) != -1){
    $("body").addClass("mac"); //macには「body class="mac"」追加
    window.onorientationchange = setView;
  }else{
    $("body").addClass("other"); //上記以外には「body class="other"」追加
    window.onorientationchange = setView;
  }
}
function setView(){
  var orientation = window.orientation;
  if(orientation === 0){
    $("body").addClass("portrait"); //画面が縦向きの場合は「body class="portrait"」追加
    $("body").removeClass("landscape"); //画面が縦向きの場合は「body class="landscape"」削除
  }else{
    $("body").addClass("landscape"); //画面が横向きの場合は「body class="landscape"」追加
    $("body").removeClass("portrait"); //画面が横向きの場合は「body class="portrait"」削除
  }
}

/* add tablet class */
$(function() {
  var userAgent = window.navigator.userAgent.toLowerCase();
  var tabletFlag = false;
  if((navigator.userAgent.indexOf('Android') > 0 && navigator.userAgent.indexOf('Mobile') == -1) || navigator.userAgent.indexOf('A1_07') > 0 || navigator.userAgent.indexOf('SC-01C') > 0 || navigator.userAgent.indexOf('iPad') > 0){
        tabletFlag = true;
  }
  if (tabletFlag) {
    $('body').addClass('tablet');
  }
});

/* ----------------------------------------------------------------------------------
　aタグタッチ中にクラスをつける
---------------------------------------------------------------------------------- */
$('a').bind({
  'touchstart': function(e) {
    $(this).removeClass("notouchstyle").addClass("touchstyle");
  },
  'touchend': function(e) {
    $(this).removeClass("touchstyle").addClass("notouchstyle");
  }
});


/* ----------------------------------------------------------------------------------
　SP Gnav acc btn
---------------------------------------------------------------------------------- */
$(function(){
  var flag = 0;
  $('#Gnav_inner').wrap('<div id="Gnav_wrap"></div>');
  $('#Gnav_switch').click(function(){
    var $Gnav_over = $('#Gnav_over').stop(true);
    if(flag == 0){
      $Gnav_over.fadeTo(0, 0).animate({ opacity:1, height:'201%' });
      $(this).find('img').attr('src', $(this).find('img').attr('src').replace('_static', '_active'));
      flag = 1;
    }else if(flag == 1){
      $Gnav_over.animate({ opacity:0, height:'200%' }, function(){
        $Gnav_over.hide();
      });
      $(this).find('img').attr('src', $(this).find('img').attr('src').replace('_active', '_static'));
      flag = 0;
    }
    $('#Gnav').stop().slideToggle('fast');
    $('#Head').toggleClass('active');
    return false;
  });
});

// IE7/IE8で透過png画像フェード //////////////////////////////////////////////////////
$(function() {
    if(navigator.userAgent.indexOf("MSIE") != -1) {
        $('img').not('.movie_thumb img').each(function() {
            if($(this).attr('src').indexOf('.png') != -1) {
                $(this).css({
                    'filter': 'progid:DXImageTransform.Microsoft.AlphaImageLoader(src="' +
                    $(this).attr('src') +
                    '", sizingMethod="scale");'
                }).wrap('<span class="img_png"></span>');
            }
        });
    }
});

// Android get ver  //////////////////////////////////////////////////////
$(function(){
  var _UA = navigator.userAgent;
  if( _UA.search(/Android 2.[123]/) != -1 ) {
    $("body").addClass("old_android");
  }
});

// Add Bookmark  //////////////////////////////////////////////////////
function toFavorite(toURL,toStr){

  if(navigator.userAgent.indexOf("MSIE") > -1){
    //Internet Explorer
    window.external.AddFavorite(toURL,toStr);

  /*}else if(navigator.userAgent.indexOf("Opera") > -1){
    //Opera
    window.open(toURL,'sidebar','title='+toStr);*/

  }else if(navigator.userAgent.indexOf("Trident") > -1){
    //Internet Explorer11
    //alert(toURL+"\nPlease press Ctrl+D (Cmd+D) to bookmark");
    alert("Please press Ctrl+D (Cmd+D) to bookmark");

  }else if(navigator.userAgent.indexOf("Chrome") > -1 || navigator.userAgent.indexOf("Firefox") > -1){
    //Chrome,Firefox
    //alert(toURL+"\nPlease press Ctrl+D (Cmd+D) to bookmark");
    alert("Please press Ctrl+D (Cmd+D) to bookmark");

  }else if(navigator.userAgent.indexOf("iPad") > -1){
    //iPad
    //alert(toURL+"\nPlease press Ctrl+D (Cmd+D) to bookmark");
    alert("Please press iPad bookmark icon to bookmark");

  }else if(navigator.userAgent.indexOf("Android") > -1){
    //Android
    //alert(toURL+"\nPlease press Ctrl+D (Cmd+D) to bookmark");
    alert("Please press tablet bookmark icon to bookmark");

  }else if(navigator.userAgent.indexOf("Safari") > -1){
    //Safari
    //alert(toURL+"\nPlease press Ctrl+D (Cmd+D) to bookmark");
    alert("Please press Ctrl+D (Cmd+D) to bookmark");

  }else{
    //その他
    //alert("Please press Ctrl+D (Cmd+D) to bookmark");
  }
}



//Tabへダイレクトリンク(外部リンク用)
$(function() {
  var hash = location.hash;

  if(hash === "#tab2-01" || hash === "#tab2-02" || hash === "#tab2-03") {
    var bkpoint = 767;
    var speed = 400;
    var pos = $('.tab-btns').offset().top - 130;

    function init() {
      window.addEventListener('resize', resize());
    }

    function resize() {
      var w = window.innerWidth;

      if(w < bkpoint){
        pos = $('.tab-btns').offset().top;
      } else {
        pos = $('.tab-btns').offset().top - 130;
      }
    }
    // 一旦現在されているタブをクリア
    $('.tab-contents').removeClass('active');
    // 一旦選択されているタブボタンをクリア
    $('.tab-btns li').removeClass('active');

    switch (hash) {
      case "#tab2-01":
      var index = 0;
        break;
      case "#tab2-02":
      var index = 1;
        break;
      default:
      var index = 2;
    }

    // 選択されたタブをアクティブにする
    $('.tab-contents')[index].classList.add('active');
    // 選択されたタブのボタンをアクティブにする
    $('html,body').stop().animate({'scrollTop':pos},speed);
    $('.type-02 li')[index].classList.add('active');
    $('.type-01 li')[index].classList.add('active');

    init();
  }
});


//Tabへダイレクトリンク(ローカル用)
$(function() {
  $('#tab_btn_click li a').click(function(){
    var index = $('#tab_btn_click li a').index(this);

    var bkpoint = 767;
    var speed = 400;
    var pos = $('.tab-btns').offset().top - 130;

    function init() {
      window.addEventListener('resize', resize());
    }

    function resize() {
      var w = window.innerWidth;

      if(w < bkpoint){
        pos = $('.tab-btns').offset().top;
      }else{
        pos = $('.tab-btns').offset().top - 130;
      }
    }
    // 一旦現在されているタブをクリア
    $('.tab-contents').removeClass('active');
    // 一旦選択されているタブボタンをクリア
    $('.tab-btns li').removeClass('active');
    // 選択されたタブをアクティブにする
    $('.tab-contents')[index].classList.add('active');
    // 選択されたタブのボタンをアクティブにする
    $('html,body').stop().animate({'scrollTop':pos},speed);
    $('.type-02 li')[index].classList.add('active');
    $('.type-01 li')[index].classList.add('active');

    init();
  });
});

// Youtube   //////////////////////////////////////////////////////
$(function() {
  var modal = function() {

    cboxSwitch();
    var timer = false;
    $(window).on('resize', function() {
      if (timer !== false) {
        clearTimeout(timer);
      }
      timer = setTimeout(function() {
        cboxSwitch();
      }, 200);
    });

    function cboxSwitch() {
      var w = (window.innerWidth) ? window.innerWidth : document.body.clientWidth;
      if (w < 768) {
        $('.modal-mov').removeClass('cboxElement');
        $('.modal-mov').on('click', function() {
          return false;
        });
      } else {
        $(".modal-mov").off('click');
        $('.modal-mov').colorbox({
          iframe:true,
          innerWidth:648,
          innerHeight:364
        });
      }
    }

  };
  if($('.cboxElement')[0] !== undefined) modal();
});

// アンカーリンクへの遷移ズレ調整
$(function() {
  wid = $(window).outerWidth();
  $("a[id]" && "a[name]").each(function() {
    if (wid >= 768) {
      $(this).css({
        "display" : "block",
        "position" : "relative",
        "top" : - 100
      });
    } else {
      $(this).css({
        "display" : "block",
        "position" : "relative",
        "top" : - 50
      });
    }
  });
});
