// ブラウザ名を取得
var getBrowser = function() {
	var ua = window.navigator.userAgent.toLowerCase();
	var ver = window.navigator.appVersion.toLowerCase();
	var name = 'unknown';

	if (ua.indexOf("msie") != -1) {
		if (ver.indexOf("msie 6.") != -1) {
			name = 'ie6';
		} else if (ver.indexOf("msie 7.") != -1) {
			name = 'ie7';
		} else if (ver.indexOf("msie 8.") != -1) {
			name = 'ie8';
		} else if (ver.indexOf("msie 9.") != -1) {
			name = 'ie9';
		} else if (ver.indexOf("msie 10.") != -1) {
			name = 'ie10';
		} else {
			name = 'ie';
		}
	} else if (ua.indexOf('trident/7') != -1) {
		name = 'ie11';
	} else if (ua.indexOf('chrome') != -1) {
		name = 'chrome';
	} else if (ua.indexOf('safari') != -1) {
		name = 'safari';
	} else if (ua.indexOf('opera') != -1) {
		name = 'opera';
	} else if (ua.indexOf('firefox') != -1) {
		name = 'firefox';
	} else if (ua.match(/Trident/)) {
		version = ua.match(/(MSIE\s|rv:)([\d\.]+)/)[2];
		name = 'ie' + version;
	}
	return name;
};
var istablet = false;
//tablet判定
var ua = navigator.userAgent;
if (ua.indexOf('iPhone') > 0 || ua.indexOf('iPod') > 0 || ua.indexOf('Android') > 0 && ua.indexOf('Mobile') > 0) {} else if (ua.indexOf('iPad') > 0 || ua.indexOf('Android') > 0) {
	istablet = true;
} else {}
var movie_none = false;
var browser = getBrowser();
if (browser == 'ie8' || istablet) {
	movie_none = true;
}
//一番最初かどうかの判定
var f_flag = false;
//pc版が初めてかどうか判定
var p_flag = false;
//スマホモード切替
var sp_mode = false;
var w;
/* メインビジュアルのプレイヤー */
var mainvis_pla_flg = false;
var mainvis_pla;
var mainvis_plaVars = {
	'fs': 0,
	'loop': 1,
	'autoplay': 1,
	'modestbranding': 1,
	'controls': 0,
	'showinfo': 0,
	'rel': 0,
	'enablejsapi': 1,
	'version': 3,
	'origin': '*',
	'allowfullscreen': true,
	'wmode': 'transparent',
	'iv_load_policy': 3,
	'disablekb': 1,
	'playlist': '2bcajOe2KXQ'
}

function mainvis_plaReady(event) {
	event.target.mute();
	event.target.playVideo();
	$("#Mainvis #Mainvis_inner .txtarea_01 .titstyle_01").fadeOut(1000);
	$("#Mainvis #Mainvis_inner .txtarea_01 .car_sub").fadeOut(1000);
}

function mainvis_plaStateChange(event) {
	event.target.mute();
	var bgHeight = $("#Mainvis").outerHeight();
	var wrapHeight = $(".playerWrap").outerHeight();
	var position = (wrapHeight - bgHeight) / 4;
	if (event.data == 1 && !mainvis_pla_flg) {
		mainvis_pla_flg = true;
		$('#Mainvis_bg').css({
			background: 'none'
		});
		$('#Mainvis .player_wrap').animate({
			opacity: 1
		}, {
			duration: 500,
			easing: 'swing'
		});
		$('#Mainvis .player_wrap').css({
			"position": "absolute",
			"top": "0",
			"left": "0",
			"right": "0",
			"bottom": "0",
			"margin": "auto"
		});
	}
}
	/* サムネイル01のプレイヤー */
var t_movie_01_flg = false;
var t_movie_01;
var t_movie_01Vars = {
	'disablekb': 1,
	'fs': 0,
	'loop': 1,
	'autoplay': 1,
	'modestbranding': 1,
	'controls': 0,
	'showinfo': 0,
	'rel': 0,
	'enablejsapi': 1,
	'version': 3,
	'origin': '*',
	'allowfullscreen': true,
	'wmode': 'transparent',
	'iv_load_policy': 3,
	'playlist': 'pA-bNcSkT2E'
}

function t_movie_01Ready(event) {
	event.target.mute();
	event.target.playVideo();
	event.target.setPlaybackQuality('small');
}

function t_movie_01StateChange(event) {
	event.target.mute();
	if (event.data == 1 && !t_movie_01_flg) {
		t_movie_01.pauseVideo();
		var $m_ov = $('#thumb_movie_01').parent('.thumb_movie_wrap').parent('.thumb_img').find('.thumb_movie_ov');
		$m_ov.hover(
			function() {
				if (!t_movie_01_flg) {
					t_movie_01_flg = true;
					$('#thumb_movie_01').parent('.thumb_movie_wrap').animate({
						opacity: 1
					}, {
						duration: 100,
						easing: 'swing'
					});
				}
				$(this).parent().parent().find('.thumb_img_ov').stop().hide();
				t_movie_01.playVideo();
			},
			function() {
				$(this).stop().animate({
					opacity: 1
				}, {
					duration: 300,
					easing: 'swing'
				});
				$(this).parent().parent().find('.thumb_img_ov').stop().show();
				t_movie_01.pauseVideo();
			}
		);
	}
}

/* サムネイル02のプレイヤー */
var t_movie_02_flg = false;
var t_movie_02;
var t_movie_02Vars = {
	'disablekb': 1,
	'fs': 0,
	'loop': 1,
	'autoplay': 1,
	'modestbranding': 1,
	'controls': 0,
	'showinfo': 0,
	'rel': 0,
	'enablejsapi': 1,
	'version': 3,
	'origin': '*',
	'allowfullscreen': true,
	'wmode': 'transparent',
	'iv_load_policy': 3,
	'playlist': '27qTncckU1w'
}

function t_movie_02Ready(event) {
	event.target.mute();
	event.target.playVideo();
	event.target.setPlaybackQuality('small');
}

function t_movie_02StateChange(event) {
	event.target.mute();
	if (event.data == 1 && !t_movie_02_flg) {
		t_movie_02.pauseVideo();
		var $this = $(this);
		var $m_ov = $('#thumb_movie_02').parent('.thumb_movie_wrap').parent('.thumb_img').find('.thumb_movie_ov');
		$m_ov.hover(
			function() {
				if (!t_movie_02_flg) {
					t_movie_02_flg = true;
					$('#thumb_movie_02').parent('.thumb_movie_wrap').animate({
						opacity: 1
					}, {
						duration: 100,
						easing: 'swing'
					});
				}
				$(this).parent().parent().find('.thumb_img_ov').stop().hide();
				t_movie_02.playVideo();
			},
			function() {
				$(this).parent().parent().find('.thumb_img_ov').stop().show();
				t_movie_02.pauseVideo();
			}
		);
	}
}

/* サムネイル03のプレイヤー */
var t_movie_03_flg = false;
var t_movie_03;
var t_movie_03Vars = {
	'disablekb': 1,
	'fs': 0,
	'loop': 1,
	'autoplay': 1,
	'modestbranding': 1,
	'controls': 0,
	'showinfo': 0,
	'rel': 0,
	'enablejsapi': 1,
	'version': 3,
	'origin': '*',
	'allowfullscreen': true,
	'wmode': 'transparent',
	'iv_load_policy': 3,
	'playlist': 'q2ScxQOY6FY'
}

function t_movie_03Ready(event) {
	event.target.mute();
	event.target.playVideo();
	event.target.setPlaybackQuality('small');
}

function t_movie_03StateChange(event) {
	event.target.mute();
	if (event.data == 1 && !t_movie_03_flg) {
		t_movie_03.pauseVideo();
		var $this = $(this);
		var $m_ov = $('#thumb_movie_03').parent('.thumb_movie_wrap').parent('.thumb_img').find('.thumb_movie_ov');
		$m_ov.hover(
			function() {
				if (!t_movie_03_flg) {
					t_movie_03_flg = true;
					$('#thumb_movie_03').parent('.thumb_movie_wrap').animate({
						opacity: 1
					}, {
						duration: 100,
						easing: 'swing'
					});
				}
				$(this).parent().parent().find('.thumb_img_ov').stop().hide();
				t_movie_03.playVideo();
			},
			function() {
				$(this).parent().parent().find('.thumb_img_ov').stop().show();
				t_movie_03.pauseVideo();
			}
		);
	}
}

/* サムネイル04のプレイヤー */
var t_movie_04_flg = false;
var t_movie_04;
var t_movie_04Vars = {
	'disablekb': 1,
	'fs': 0,
	'loop': 1,
	'autoplay': 1,
	'modestbranding': 1,
	'controls': 0,
	'showinfo': 0,
	'rel': 0,
	'enablejsapi': 1,
	'version': 3,
	'origin': '*',
	'allowfullscreen': true,
	'wmode': 'transparent',
	'iv_load_policy': 3,
	'playlist': 'KcZkkBLnwcw'
}

function t_movie_04Ready(event) {
	event.target.mute();
	event.target.playVideo();
	event.target.setPlaybackQuality('small');
}

function t_movie_04StateChange(event) {
	event.target.mute();
	if (event.data == 1 && !t_movie_04_flg) {
		t_movie_04.pauseVideo();
		var $this = $(this);
		var $m_ov = $('#thumb_movie_04').parent('.thumb_movie_wrap').parent('.thumb_img').find('.thumb_movie_ov');
		$m_ov.hover(
			function() {
				if (!t_movie_04_flg) {
					t_movie_04_flg = true;
					$('#thumb_movie_04').parent('.thumb_movie_wrap').animate({
						opacity: 1
					}, {
						duration: 100,
						easing: 'swing'
					});
				}
				$(this).parent().parent().find('.thumb_img_ov').stop().hide();
				t_movie_04.playVideo();
			},
			function() {
				$(this).parent().parent().find('.thumb_img_ov').stop().show();
				t_movie_04.pauseVideo();
			}
		);
	}
}
function onYouTubeIframeAPIReady() {
	if (browser == 'safari') {
		mainvis_plaVars.html5 = 1;
	}
	try {
		mainvis_pla = new YT.Player('mainvis_pla', {
			height: '100%',
			width: '100%',
			videoId: '2bcajOe2KXQ',
			playerVars: mainvis_plaVars,
			events: {
				'onReady': mainvis_plaReady,
				'onStateChange': mainvis_plaStateChange
			}
		});
		t_movie_01 = new YT.Player('thumb_movie_01', {
			height: '202px',
			width: '360px',
			videoId: 'pA-bNcSkT2E',
			wmode: 'transparent',
			playerVars: t_movie_01Vars,
			events: {
				'onReady': t_movie_01Ready,
				'onStateChange': t_movie_01StateChange
			}
		});

		t_movie_02 = new YT.Player('thumb_movie_02', {
			height: '202px',
			width: '360px',
			videoId: '27qTncckU1w',
			wmode: 'transparent',
			playerVars: t_movie_02Vars,
			events: {
				'onReady': t_movie_02Ready,
				'onStateChange': t_movie_02StateChange
			}
		});
		t_movie_03 = new YT.Player('thumb_movie_03', {
			height: '202px',
			width: '360px',
			videoId: 'q2ScxQOY6FY',
			wmode: 'transparent',
			playerVars: t_movie_03Vars,
			events: {
				'onReady': t_movie_03Ready,
				'onStateChange': t_movie_03StateChange
			}
		});

		t_movie_04 = new YT.Player('thumb_movie_04', {
			height: '202px',
			width: '360px',
			videoId: 'KcZkkBLnwcw',
			wmode: 'transparent',
			playerVars: t_movie_04Vars,
			events: {
				'onReady': t_movie_04Ready,
				'onStateChange': t_movie_04StateChange
			}
		});
	} catch (e) {}

}

//リサイズ時関数
function resize_window() {

	w = $(window).width();
	if (!('innerWidth' in window)) window.innerWidth = w;
	if (w < 768) {
		if (!f_flag) {
			sp_mode = true;
			sp_format();
			f_flag = true;
		}
		if (!sp_mode) {
			sp_mode = true;
			sp_format();
		}
	} else {
		if (!f_flag) {
			sp_mode = false;
			pc_format();
			f_flag = true;
		}
		if (sp_mode) {
			sp_mode = false;
			pc_format();
		}
	}
	if (sp_mode) {
		sp_resize();
	} else {
		pc_resize();
	}

}

function sp_format() {
	$('#Mainvis_bg,#Mainvis .player_wrap').attr('style', '');
	$('.thumb_movie_wrap').css({
		opacity: 0
	});
	if (p_flag) {
		try {
			mainvis_pla.destroy();
			t_movie_01.destroy();
			t_movie_02.destroy();
			t_movie_03.destroy();
			t_movie_04.destroy();
		} catch (e) {}

		mainvis_pla_flg = false;
		t_movie_01_flg = false;
		t_movie_02_flg = false;
		t_movie_03_flg = false;
		t_movie_04_flg = false;
	}

}

function pc_format() {
	if (!movie_none) {
		if (p_flag) {
			onYouTubeIframeAPIReady();
		} else {
			var tag = document.createElement('script');
			tag.src = "https://www.youtube.com/iframe_api";
			var firstScriptTag = document.getElementsByTagName('script')[0];
			firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
			p_flag = true;
		}
	}

}

function sp_resize() {}

function pc_resize() {

	var mainvis_w = $('#Mainvis').width();
	var h = $(window).height();
	var minH = 490;
	var maxH = 840;
	var headerHeight = 120;

	if (h < minH) {
		h = minH - headerHeight;
	} else if (h > maxH) {
		h = maxH - headerHeight;
	} else {
		h = h - headerHeight;
	}
	$('#Mainvis_bg').css({
		'height': h + 'px'
	});
	if (!movie_none) {
		var t_h = (mainvis_w / 16) * 9;
		var t_w = (h / 9) * 16;

		if (h > t_h) {
			$('#Mainvis .player_wrap').css({
				'height': h + 'px',
				'width': t_w + 'px'
			});
			$('#mainvis_pla').css({
				'left': '-' + (t_w - mainvis_w) / 2 + 'px',
				'top': '0'
			});
		} else {
			$('#Mainvis .player_wrap').css({
				'height': t_h + 'px',
				'width': mainvis_w + 'px'
			});
			$('#mainvis_pla').css({
				'left': '0',
				'top': '-' + (t_h - h) /2 + 'px'
			});
		}
	}
}
(function($) {
	$(function() {

		resize_window();
		var timer = false;
		$(window).resize(function() {
			if (timer !== false) {
				clearTimeout(timer);
			}
			timer = setTimeout(function() {
				resize_window();
			}, 200);
		});
		$('#Mainvis_ov').click(function() {
			console.log('click');
			return false;
		});
		$('#Mainvis_ov').dblclick(function() {
			return false;
		});
		if (istablet) {
			$('#sec_design_01 .pc_bg,#sec_design_02 .pc_bg,#sec_performance_01 .pc_bg,#sec_performance_02 .pc_bg').addClass('istablet');
			$('#sec_design_01 .imgstyle_02,#sec_design_02 .imgstyle_02,#sec_performance_01 .imgstyle_02,#sec_performance_02 .imgstyle_02').addClass('istablet');
			$('#sec_design_01 .pc_bg img').attr('src', './images/bg_02_tb.jpg');
		}

	});

	$(window).on('unload', function() {});

})(jQuery);
