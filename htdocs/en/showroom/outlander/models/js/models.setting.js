(function($) {
$(function() {
	$(".modal").colorbox();
	cboxSwitch();

	var timer = false;
	$(window).on('resize', function() {
		if (timer !== false) {
			clearTimeout(timer);
		}
		timer = setTimeout(function() {
			cboxSwitch();
		}, 200);
	});

	function cboxSwitch() {
		var w = (window.innerWidth) ? window.innerWidth : document.body.clientWidth;
		if (w < 768) {
			$('.modal').removeClass('cboxElement');
			$('.modal').on('click', function() {
				return false;
			});
		} else {
			$(".modal").off('click');
			$(".modal").colorbox();
		}
	}
});
})(jQuery);

