$(function(){
  var anim = function(el) {
    var $elm = el,
        bkPoint = 767,
        $anim01 = $elm.find('.top-anim-01'),
        w,pos;

    function init() {
      w = window.innerWidth;
      var spH = $elm.find('.sp_imgstyle_01').height();
      $elm.find('.anim-wrap-sp').height(spH);
      $(window).on('load resize', function(){
        var spH = $elm.find('.sp_imgstyle_01').height();
        $elm.find('.anim-wrap-sp').height(spH);
        w = window.innerWidth;

        // if(w <= bkPoint) return;
        if(w <= bkPoint) {
          loadSp();
        }else{
          load();          
        }
      });

      if(w <= bkPoint) {
        loadSp();
      }else{
        load();          
      }

    }

    function load() {
      $elm.find('#Mainvis_bg').addClass('active');
      setTimeout(function() {
        $elm.find('.top-anim-01.pc').addClass('active anim');
      },200);

      // setTimeout(function() {
      //   $anim01.addClass('anim');
      // },600);

      setTimeout(function() {
        $elm.find('.titstyle_02').addClass('active');
      },1200);

      setTimeout(function() {
        $elm.find('.titstyle_01').addClass('active');
      },1200);

      setTimeout(function() {
        $elm.find('.tx').addClass('active');
      },1200);

      setTimeout(function() {
        $elm.find('.btnstyle_02').addClass('active');
      },1200);

      setTimeout(function() {
        $elm.find('.top-anim-bg').addClass('active');
      },600);
    }

    function loadSp() {
      setTimeout(function() {
        $elm.find('.top-anim-01.sp').addClass('active anim');
      },200);

      // setTimeout(function() {
      //   $anim01.addClass('anim');
      // },600);

      setTimeout(function() {
        $elm.find('.sp_imgstyle_01').addClass('active');
      },800);

    }

    init();

  };

  anim($('.js-anim'));
});