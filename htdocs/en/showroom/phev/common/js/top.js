
var resizeHandler = function(){
	
	var w = $(window).width();
	var h = $(window).height();

	if(!('innerWidth' in window)) window.innerWidth = w;

	if(window.innerWidth > 767){

		var minH=490;
		var maxH=750;
		var headerHeight = 120;
	
		if(w<1280){
			$("#Mainvis_bg").css({width: "100%"});
			}else{
			$("#Mainvis_bg").css({width: "100%"});
		 }
	
		if(h<minH){
			// $('#Mainvis_bg').css({height: minH-headerHeight});	
		}else if(h>maxH){
			// $('#Mainvis_bg').css({height: maxH-headerHeight});	
		}else{
			// $('#Mainvis_bg').css({height: h-headerHeight});
		}

		$('#Mainvis_bg').css({height: '630px'});	

		
	}else{
		
		// animation active
		// $('#Mainvis_bg').css({ width: '100%', height: (w/16) * 7});	
		
		$('#Mainvis_bg').css({ width:'', height:'' });	
		
	}
	
	$(".imgstyle_01").css("display","none");

};

$(document).ready(resizeHandler);
$(window).resize(resizeHandler);

$(function(){

	$('#flexslider').flexslider({
		animation: "slide",
		controlNav: true,
		animationLoop: false,
		slideshow: false,
		smoothHeight: true,
		sync: "#carousel",
		directionNav: true,
		prevText: '<img src="./images/btn_prev_flex.jpg">',
		nextText: '<img src="./images/btn_next_flex.jpg">'
	});

	$('#carousel').flexslider({
		animation: "slide",
		controlNav: false,
		animationLoop: false,
		slideshow: false,
		itemWidth: 161,
		asNavFor: '#flexslider'
	});


	/* movie height */
	$(window).on('load resize', function() {
		var ww = $(window).width();
		if(ww<=768){
			var mt_w = $('.movie_thumb').width();
			var mt_h = $('.movie_thumb.SP').height();
			var tit_h = $('.titlearea01').height();
			$('.movie_youtube iframe').width(mt_w);
			$('.movie_main, .movie_youtube iframe').height(mt_h);	
			$('#flexslider .flex-viewport').height(mt_h+tit_h);
		}else{
			$('.movie_youtube iframe').width('712px');
			$('.movie_youtube iframe').height('400px');
			$('.movie_main').height('400px');
		}
	});

});




/* youtube api */

var tag = document.createElement('script');
tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

var player = [];
function onYouTubeIframeAPIReady() {

	player[0] = new YT.Player('Movie_01', {
		height: '400',
		width: '712',
		videoId: '9OQT2qNycWQ',
		playerVars: {
			'showinfo': 0, 'autohide': 1, 'autoplay':0, 'controls': 1, 'rel': 0, 'loop': 0, 'wmode': 'transparent'
		},
		events: {
			'onReady': onPlayerReady
		}
	});

	player[1] = new YT.Player('Movie_02', {
		height: '400',
		width: '712',
		videoId: 'New86isvtPs',
		playerVars: {
			'showinfo': 0, 'autohide': 1, 'autoplay':0, 'controls': 1, 'rel': 0, 'loop': 0, 'wmode': 'transparent'
		},
		events: {
			'onReady': onPlayerReady
		}
	});

	player[2] = new YT.Player('Movie_03', {
		height: '400',
		width: '712',
		videoId: 'YP28NWk6qLE',
		playerVars: {
			'showinfo': 0, 'autohide': 1, 'autoplay':0, 'controls': 1, 'rel': 0, 'loop': 0, 'wmode': 'transparent'
		},
		events: {
			'onReady': onPlayerReady
		}
	});

	player[3] = new YT.Player('Movie_04', {
		height: '400',
		width: '712',
		videoId: 'o8Mzri64jeU',
		playerVars: {
			'showinfo': 0, 'autohide': 1, 'autoplay':0, 'controls': 1, 'rel': 0, 'loop': 0, 'wmode': 'transparent'
		},
		events: {
			'onReady': onPlayerReady
		}
	});
}

function onPlayerReady(){

	$('.flex-next,.flex-prev').click(function(){
		player[0].pauseVideo();
		player[1].pauseVideo();
		player[2].pauseVideo();
		player[3].pauseVideo();
	});

	$('#carousel li').not('.flex-active-slide').click(function(){
		player[0].pauseVideo();
		player[1].pauseVideo();
		player[2].pauseVideo();
		player[3].pauseVideo();
	});

	var flag = 0;
	$(document).on('click', '.movie_thumb', function(){
		ga('send', 'event', 'SR_TOP', 'movie', 'movie', 1);
		if ( navigator.userAgent.indexOf('iPhone') > 0 || (navigator.userAgent.indexOf('Android') > 0 && navigator.userAgent.indexOf('Mobile') > 0) ) {
			//var mv_url = $(this).find('a').attr('href');
			//window.open(mv_url,'_blank');
			return true;
		}

		flag = 1;
		$(this).css({ visibility:'hidden' });
		var $iframe = $(this).parent().find('.movie_youtube iframe');
		var index = $('.movie_youtube iframe').index($iframe);
		// var src = video[index]+'&autoplay=1';
		// $iframe.css({ visibility:'visible' }).attr('src', src);
		player[index].playVideo();
		$iframe.css({ visibility:'visible' });
		var ww = $(window).width();
		if(ww<=768){
			return false;
		}

	});

}

$(function(){
	var agent = navigator.userAgent;
	if(agent.search(/iPad/) != -1){
		$('.flex-next,.flex-prev').on('touchend',function(){
			player[0].pauseVideo();
			player[1].pauseVideo();
			player[2].pauseVideo();
			player[3].pauseVideo();
		});
		$('#carousel li').not('.flex-active-slide').on('touchend',function(){
			player[0].pauseVideo();
			player[1].pauseVideo();
			player[2].pauseVideo();
			player[3].pauseVideo();
		});
		return false;
	}	
});