/* ----------------------------------------------------------------------------------
　スムーズスクロール
---------------------------------------------------------------------------------- */
var scroll_func = function () {
  $('html,body').animate({ scrollTop: $($(this).attr("href")).offset().top }, 'slow','swing');
  return false;
}

$(function(){
  $('[href^=#]').not('[href$=#], a.pd_switch, .tab_area a, .btn_order a, #Foot a, #Morebtn, .tab-btns a').click(scroll_func);
  $('#Morebtn').on('click', function() {
    var offset = $($(this).attr("href")).offset().top - 120;
    $('html,body').animate({ scrollTop: offset }, 'slow','swing');
    return false;
  });
});


/* ----------------------------------------------------------------------------------
　change UA
---------------------------------------------------------------------------------- */
if (!(navigator.userAgent.indexOf('iPhone') > 0 || navigator.userAgent.indexOf('iPod') > 0 || (navigator.userAgent.indexOf('Android') > 0 && navigator.userAgent.indexOf('Mobile') > 0))) {
  $('meta[name="viewport"]').attr('content','width=980px');
}

/* ----------------------------------------------------------------------------------
　ロールオーバー
---------------------------------------------------------------------------------- */
var ua = navigator.userAgent;
var isIE8 = ua.match(/msie [8.]/i);

if (!isIE8) {
  $.fn.rollover2 = function() {
    return this.each(function() {
      var target = this;
      $.each($(this).find('img[src*=_off]'), function(){
        var img = this;
        $(img).addClass('rollover2');
        if($(img).prev().is('[src*=_on]')){}else{
          
          var overimg = $(document.createElement('img')).attr('src', img.src.replace(/_off/, '_on'));
          $(overimg).css({ opacity:0, position:'absolute' }).addClass('over');
          $(img).before(overimg);
          
          if($(overimg).parents('button').length){
            $(overimg).parents('button').hover(function(){
              var hoverimg = $(this).find('img')[0];
              $(hoverimg).stop().animate({ opacity:1 }, 200);
            },function(){
              var hoverimg = $(this).find('img')[0];
              $(hoverimg).stop().animate({ opacity:0 }, 200);
            });
          }else{
            $(overimg).hover(function(){
              var hoverimg = this;
              $(hoverimg).stop().animate({ opacity:1 }, 200);
            },function(){
              var hoverimg = this;
              $(hoverimg).stop().animate({ opacity:0 }, 200);
            });
          }
          
        }
      });
    });
  };
  $(function(){
    $('img[src*=_on]').hover(function(){
      $(this).css({ opacity:1 });
    });
  });
  $(function(){
    $('a img').hover( function(){
      $(this).not('img[src*=_on],img[src*=_off], .logo img, #Head_nav img, .item_cover img, .btnstyle_01 img, .btnstyle_02 img').stop().animate({ opacity:0.7 }, 200);
    }, function(){
      $(this).not('img[src*=_on],img[src*=_off], .logo img, #Head_nav img, .item_cover img, .btnstyle_01 img, .btnstyle_02 img').stop().animate({ opacity:1 }, 200);
    });
  });

  $(function(){ $('body').rollover2(); });
  
}

/* ----------------------------------------------------------------------------------
　リストの最後の要素に .last を付加
---------------------------------------------------------------------------------- */
$(function(){
  $('li:last-child').addClass('last');
});

/* ----------------------------------------------------------------------------------
　UAを判別し、bodyにクラスをつける。
　縦横を判別し、bodyにクラスをつける。
---------------------------------------------------------------------------------- */
$(function(){
  setOperate();
});
function setOperate(){
  setView();
  var agent = navigator.userAgent;
  if(agent.search(/iPhone/) != -1){
    $("body").addClass("iphone"); //iPhoneには「body class="iphone"」追加
    window.onorientationchange = setView;
  }else if(agent.search(/iPad/) != -1){
    $("body").addClass("ipad"); //iPadには「body class="ipad"」追加
    window.onorientationchange = setView;
  }else if(agent.search(/Android/) != -1){
    $("body").addClass("android"); //Androidには「body class="android"」追加
    window.onresize = setView;
  }else if(agent.search(/Mac OS/) != -1){
    $("body").addClass("mac"); //macには「body class="mac"」追加
    window.onorientationchange = setView;
  }else{
    $("body").addClass("other"); //上記以外には「body class="other"」追加
    window.onorientationchange = setView;
  }
}
function setView(){
  var orientation = window.orientation;
  if(orientation === 0){
    $("body").addClass("portrait"); //画面が縦向きの場合は「body class="portrait"」追加
    $("body").removeClass("landscape"); //画面が縦向きの場合は「body class="landscape"」削除
  }else{
    $("body").addClass("landscape"); //画面が横向きの場合は「body class="landscape"」追加
    $("body").removeClass("portrait"); //画面が横向きの場合は「body class="portrait"」削除
  }
}

/* add tablet class */
$(function() {
  var userAgent = window.navigator.userAgent.toLowerCase();
  var tabletFlag = false;
  if((navigator.userAgent.indexOf('Android') > 0 && navigator.userAgent.indexOf('Mobile') == -1) || navigator.userAgent.indexOf('A1_07') > 0 || navigator.userAgent.indexOf('SC-01C') > 0 || navigator.userAgent.indexOf('iPad') > 0){
        tabletFlag = true;
  }
  if (tabletFlag) {
    $('body').addClass('tablet');
  }
});

/* ----------------------------------------------------------------------------------
　aタグタッチ中にクラスをつける
---------------------------------------------------------------------------------- */
$('a').bind({
  'touchstart': function(e) {
    $(this).removeClass("notouchstyle").addClass("touchstyle");
  },
  'touchend': function(e) {
    $(this).removeClass("touchstyle").addClass("notouchstyle");
  }
});


/* ----------------------------------------------------------------------------------
　SP Gnav acc btn
---------------------------------------------------------------------------------- */
$(function(){
  var flag = 0;
  $('#Gnav_inner').wrap('<div id="Gnav_wrap"></div>');
  $('#Gnav_switch').click(function(){
    var $Gnav_over = $('#Gnav_over').stop(true);
    if(flag == 0){
      $Gnav_over.fadeTo(0, 0).animate({ opacity:1, height:'201%' });
      $(this).find('img').attr('src', $(this).find('img').attr('src').replace('_static', '_active'));
      flag = 1;
    }else if(flag == 1){
      $Gnav_over.animate({ opacity:0, height:'200%' }, function(){
        $Gnav_over.hide();
      });
      $(this).find('img').attr('src', $(this).find('img').attr('src').replace('_active', '_static'));
      flag = 0;
    }
    $('#Gnav').stop().slideToggle('fast');
    $('#Head').toggleClass('active');
    return false;
  });
});

// IE7/IE8で透過png画像フェード //////////////////////////////////////////////////////
$(function() {
    if(navigator.userAgent.indexOf("MSIE") != -1) {
        $('img').not('.movie_thumb img').each(function() {
            if($(this).attr('src').indexOf('.png') != -1) {
                $(this).css({
                    'filter': 'progid:DXImageTransform.Microsoft.AlphaImageLoader(src="' +
                    $(this).attr('src') +
                    '", sizingMethod="scale");'
                }).wrap('<span class="img_png"></span>');
            }
        });
    }
});

// Android get ver  //////////////////////////////////////////////////////
$(function(){
  var _UA = navigator.userAgent;
  if( _UA.search(/Android 2.[123]/) != -1 ) {
    $("body").addClass("old_android");
  }
});

// Add Bookmark  //////////////////////////////////////////////////////
function toFavorite(toURL,toStr){
  
  if(navigator.userAgent.indexOf("MSIE") > -1){
    //Internet Explorer
    window.external.AddFavorite(toURL,toStr);
    
  /*}else if(navigator.userAgent.indexOf("Opera") > -1){
    //Opera
    window.open(toURL,'sidebar','title='+toStr);*/
    
  }else if(navigator.userAgent.indexOf("Trident") > -1){
    //Internet Explorer11
    //alert(toURL+"\nPlease press Ctrl+D (Cmd+D) to bookmark");
    alert("Please press Ctrl+D (Cmd+D) to bookmark");
  
  }else if(navigator.userAgent.indexOf("Chrome") > -1 || navigator.userAgent.indexOf("Firefox") > -1){
    //Chrome,Firefox
    //alert(toURL+"\nPlease press Ctrl+D (Cmd+D) to bookmark");
    alert("Please press Ctrl+D (Cmd+D) to bookmark");
  
  }else if(navigator.userAgent.indexOf("iPad") > -1){
    //iPad
    //alert(toURL+"\nPlease press Ctrl+D (Cmd+D) to bookmark");
    alert("Please press iPad bookmark icon to bookmark");
    
  }else if(navigator.userAgent.indexOf("Android") > -1){
    //Android
    //alert(toURL+"\nPlease press Ctrl+D (Cmd+D) to bookmark");
    alert("Please press tablet bookmark icon to bookmark");
  
  }else if(navigator.userAgent.indexOf("Safari") > -1){
    //Safari
    //alert(toURL+"\nPlease press Ctrl+D (Cmd+D) to bookmark");
    alert("Please press Ctrl+D (Cmd+D) to bookmark");
    
  }else{
    //その他
    //alert("Please press Ctrl+D (Cmd+D) to bookmark");
  }
}



/* 20150917 追加 */
// SP SortButton //////////////////////////////////////////////////////
$(function(){
  $('.ga_sort_name, .ga_sort_btn li').on('click.pulldown', function(){
    $('.ga_sort_name').find('span').toggleClass('active');
    $('.ga_sort_name').next().slideToggle('normal');
  });
});


// Youtube   //////////////////////////////////////////////////////
$(function() {
  var modal = function() {

    cboxSwitch();
    var timer = false;
    $(window).on('resize', function() {
      if (timer !== false) {
        clearTimeout(timer);
      }
      timer = setTimeout(function() {
        cboxSwitch();
      }, 200);
    });

    function cboxSwitch() {
      var w = (window.innerWidth) ? window.innerWidth : document.body.clientWidth;
      if (w < 768) {
        $('.modal-mov').removeClass('cboxElement');
        $('.modal-mov').on('click', function() {
          return false;
        });
      } else {
        $(".modal-mov").off('click');
        $('.modal-mov').colorbox({
           iframe:true,
              innerWidth:648,
              innerHeight:364
        });
      }
    }

  };
  if($('.cboxElement')[0] !== undefined) modal();
});


$(function() {
  var UA, iOS;
    if (navigator.userAgent.indexOf('iPhone') > 0 || navigator.userAgent.indexOf('iPad') > 0 || navigator.userAgent.indexOf('iPod') > 0 || navigator.userAgent.indexOf('Android') > 0) {UA = "sp"; }
    if (navigator.userAgent.indexOf('iPhone') > 0 || navigator.userAgent.indexOf('iPad') > 0 || navigator.userAgent.indexOf('iPod') > 0 ) {iOS = true; }

  function youtubeApi(){
    var script = document.createElement('script');
        script.src = "https://www.youtube.com/iframe_api";
    var firstScript = document.getElementsByTagName('script')[0];
        firstScript.parentNode.insertBefore( script, firstScript );
        
        window.onload = ready();
  }
  youtubeApi();

  var Movie = function(movUrl,$el,playId,player){
    this.movies = movUrl;
    this.$el = $el;
    this.player = player;
    this.playId = playId;
    this.init();
  };
  Movie.prototype = {
    init: function(){
      var _this = this;

      _this.nowWatch = 0;
      _this.$movPlayer = _this.$el.find('#'+_this.playId);
      _this.playerH = _this.$el.height() ;

      // Set Youtube Api
        _this.player = new YT.Player(_this.playId,{
          width: '100%',
          height: '100%',
          videoId: _this.movies[_this.nowWatch],
          playerVars: {
            showinfo: 1,
            rel: 0,
            autohide: 1,
            controls: 1
          },
          events:{

          }
        });
        _this.player.addEventListener('onReady', function(){
          _this.movie();
        });
        _this.player.addEventListener('onStateChange', function(e) {
          switch(e.data){
            case YT.PlayerState.PLAYING:
              _this.onPlayMovie();
              break;
            case YT.PlayerState.ENDED:
              _this.endMovie();
              break;
            case YT.PlayerState.BUFFERING:
            if(UA === "sp") _this.onBuffering();
          }
        });
    },
    movie: function(){
      var _this = this;
        if(UA !== "sp"){
          _this.$el.find('img').off('click');
          _this.$el.on({"mouseenter": function(){
            _this.$el.find('img').stop().animate({opacity:0.7},800,"swing");
          },"mouseleave": function(){
            _this.$el.find('img').stop().animate({opacity:1},800,"swing");
          }
        });
        }else{
          // _this.$el.find('img').on('click', function(){
          //   _this.startMovie();
          // });
        }
    },
    // 再生
    startMovie: function(){
      var _this = this;

      if(_this.player.loadVideoById === undefined ){
        setTimeout(_this.delayPlay, 1500);
      }
      if(UA === "sp") _this.$el.find('#'+_this.playId).height('100%');
      if(!iOS){
        _this.player.loadVideoById(_this.movies[_this.nowWatch]);
      } else{
        _this.player.cueVideoById(_this.movies[_this.nowWatch]);
        _this.$el.find('#'+_this.playId).height('100%');
        _this.$el.find("iframe").animate({"opacity":1},300);
        _this.$el.find('.movie-img').fadeTo(1,0);
      }
    },
    delayPlay: function(){
      var _this = this;
      _this.player.loadVideoById(_this.movies[_this.nowWatch]);
    },
    // 変更
    changeMovie: function(n){
      var _this = this;
      _this.nowWatch=n;
    },
    // 再生中
    onPlayMovie: function(){
      var _this = this;
      _this.$el.find('#'+_this.playId).height('100%');
      _this.$el.find("iframe").animate({"opacity":1},300);
      _this.$el.find('.movie-img').fadeTo(1,0);
      _this.$el.css('background','transparent');

      _this.player.playVideo();
    },
    // 停止
    stopMovie: function(){
      var _this = this;
        _this.player.pauseVideo();
        _this.$el.find('iframe').css("opacity",0);
    },
    endMovie: function(){
      var _this = this;
      // 再生終了時にサムネイルに戻す
    },
    stopVideo: function(){
      var _this = this;
      _this.player.stopVideo();
    },
    // バッファリング
    onBuffering: function(){
      var _this = this;
      _this.$el.find('#'+_this.playId).height('100%');
      _this.$el.find("iframe").animate({"opacity":1},300);
      _this.$el.find('.movie-img').fadeTo(1,0);
    }
  };
  function ready() {
    window.onYouTubeIframeAPIReady = function(){
      // new Movie([youtubeid],wrap要素, <id>,  )
      // ユーティリティ
      if($('.utility')[0] !== undefined){
        var utility = new Movie([ 'Ob9Up2c6-U0'],$('.js-mov-wrap-01'),'mov01','Mov01');
        var utility1 = new Movie([ 'rIxWdPB6WrM'],$('.js-mov-wrap-02'),'mov02','Mov02');
      }

      if($('.safety')[0] !== undefined){
        var safety = new Movie([ 'MDzjDrCKf3o'],$('.js-mov-wrap-01'),'mov01','Mov01');
        var safety1 = new Movie([ 'JrDAWGETSZ4'],$('.js-mov-wrap-02'),'mov02','Mov02');
        var safety2 = new Movie([ 'OsgYTMmnraE'],$('.js-mov-wrap-03'),'mov03','Mov03');
        var safety3 = new Movie([ 'mfRtMxhGGeE'],$('.js-mov-wrap-04'),'mov04','Mov04');
        var safety4 = new Movie([ '56SQQ4nPSuw'],$('.js-mov-wrap-05'),'mov05','Mov05');
        var safety5 = new Movie([ 'fZXmgLgFxvk'],$('.js-mov-wrap-06'),'mov06','Mov06');
        var safety6 = new Movie([ '3dzawYxqEic'],$('.js-mov-wrap-07'),'mov07','Mov07');
        var safety7 = new Movie([ '-CxuRoDx_L0'],$('.js-mov-wrap-08'),'mov08','Mov08');
      }

      if($('.system')[0] !== undefined){
        var system = new Movie([ '_SRoekEQIzI'],$('.js-mov-wrap-01'),'mov01','Mov01');
        var system1 = new Movie([ 'WXWnzrXdgyY'],$('.js-mov-wrap-02'),'mov02','Mov02');
        var system2 = new Movie([ 'wpNJTLdRSos'],$('.js-mov-wrap-03'),'mov03','Mov03');
        var system3 = new Movie([ 'iOzmJUi9hvo'],$('.js-mov-wrap-04'),'mov04','Mov04');
      }

      if($('.perfomance')[0] !== undefined){
        var perfomance = new Movie([ 'KRhzUl3GfOc'],$('.js-mov-wrap-01'),'mov01','Mov01');
      }

      if($('.gallery')[0] !== undefined){
        var gallery = new Movie([ 'bL7jEJvJDKU'],$('.js-mov-wrap-01'),'mov01','Mov01');
      }

    };
  }


});