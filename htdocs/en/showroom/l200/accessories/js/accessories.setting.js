$(function() {

	var userAgent = window.navigator.userAgent.toLowerCase();
	var appVersion = window.navigator.appVersion.toLowerCase();
	var isIE8 = false;
	if(userAgent.indexOf("msie") != -1){
		if(appVersion.indexOf("msie 8.") != -1) {
			isIE8 = true;
		}
	}
	
	$('#flexslider01').flexslider({
		animation: "slide",
		controlNav: true,
		animationLoop: false,
		slideshow: false,
		sync: "#carousel01",
		directionNav: true,
		prevText: '<img src="./images/btn_prev_flex_w.jpg">',
		nextText: '<img src="./images/btn_next_flex_w.jpg">'
	});

	$('#carousel01').flexslider({
		animation: "slide",
		controlNav: false,
		animationLoop: false,
		slideshow: false,
		itemWidth: 174,
		asNavFor: '#flexslider01',
		prevText: '<img src="./images/carousel_prev_w.png">',
		nextText: '<img src="./images/carousel_next_w.png">'
	});
	
	$('#flexslider02').flexslider({
		animation: "slide",
		controlNav: true,
		animationLoop: false,
		slideshow: false,
		sync: "#carousel02",
		directionNav: true,
		prevText: '<img src="./images/btn_prev_flex.jpg">',
		nextText: '<img src="./images/btn_next_flex.jpg">'
	});

	$('#carousel02').flexslider({
		animation: "slide",
		controlNav: false,
		animationLoop: false,
		slideshow: false,
		itemWidth: 174,
		asNavFor: '#flexslider02',
		directionNav: false,
		prevText: '<img src="./images/carousel_prev.png">',
		nextText: '<img src="./images/carousel_next.png">'
	});
	
	$('#flexslider03').flexslider({
		animation: "slide",
		controlNav: true,
		animationLoop: false,
		slideshow: false,
		sync: "#carousel03",
		directionNav: true,
		prevText: '<img src="./images/btn_prev_flex_w.jpg">',
		nextText: '<img src="./images/btn_next_flex_w.jpg">'
	});

	$('#carousel03').flexslider({
		animation: "slide",
		controlNav: false,
		animationLoop: false,
		slideshow: false,
		itemWidth: 174,
		directionNav: false,
		asNavFor: '#flexslider03'
	});
	
	$('#flexslider04').flexslider({
		animation: "slide",
		controlNav: true,
		animationLoop: false,
		slideshow: false,
		sync: "#carousel04",
		directionNav: true,
		prevText: '<img src="./images/btn_prev_flex_w.jpg">',
		nextText: '<img src="./images/btn_next_flex_w.jpg">'
	});

	$('#carousel04').flexslider({
		animation: "slide",
		controlNav: false,
		animationLoop: false,
		slideshow: false,
		itemWidth: 174,
		asNavFor: '#flexslider04',
		prevText: '<img src="./images/carousel_prev_w.png">',
		nextText: '<img src="./images/carousel_next_w.png">'
	});
	

});
