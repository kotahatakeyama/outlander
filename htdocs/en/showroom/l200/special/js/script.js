(function($) {
var mode = 'pc';
if(window.matchMedia &&  window.matchMedia('(max-width:767px)').matches){
	mode = 'sp';
}
$(function(){
	var video1_id = 'mi4YdWlAhjU';
	var video2_id = 'D_1-JOWak34';
	var video3_id = 'tWh624-qb9k';
	var video1,video2,video3;
	var sp_youtube_setup = function(){
		if(video1 || video2 || video3) return false;
		var default_vars ={
			'loop'            : 0,
			'autoplay'        : 0,
			'modestbranding'  : 1,
			'controls'        : 1,
			'showinfo'        : 0,
			'rel'             : 0,
			'enablejsapi'     : 1,
			'version'         : 3,
			'origin'          : '*',
			'allowfullscreen' : true,
			'wmode'           : 'transparent',
			'iv_load_policy'  : 3
		};
		var tag = document.createElement('script');
		tag.src = "https://www.youtube.com/iframe_api";
		var firstScriptTag = document.getElementsByTagName('script')[0];
		firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

		window.onYouTubeIframeAPIReady = function(){
			var video1_vars = default_vars;
			video1 = new YT.Player('Youtube_01', {
				height: '100%',
				width: '100%',
				videoId: video1_id,/* 動画ID */
				playerVars: video1_vars
			});
			var video2_vars = default_vars;
			video2 = new YT.Player('Youtube_02', {
				height: '100%',
				width: '100%',
				videoId: video2_id,/* 動画ID */
				playerVars: video2_vars
			});
			var video3_vars = default_vars;
			video3 = new YT.Player('Youtube_03', {
				height: '100%',
				width: '100%',
				videoId: video3_id,/* 動画ID */
				playerVars: video3_vars
			});
		}
	}
	var sp_youtube_stop = function(){
		if(!video1 && !video2 && !video3) return false;
		var video1_s = video1 ? video1.getPlayerState() : -1;
		var video2_s = video2 ? video2.getPlayerState() : -1;
		var video3_s = video3 ? video3.getPlayerState() : -1;
		if(video1 && video1_s != -1 && video1_s != 5){video1.pauseVideo();}
		if(video2 && video2_s != -1 && video2_s != 5){video2.pauseVideo();}
		if(video3 && video3_s != -1 && video3_s != 5){video3.pauseVideo();}
	}
	$('#Youtube_01_thumb').click(function(event) {
		if(mode == 'pc'){
			$.colorbox({iframe:true, innerWidth:711, innerHeight:399,href:'http://www.youtube.com/embed/'+video1_id+'?rel=0&wmode=transparent&autoplay=1'});
		}
	});
	$('#Youtube_02_thumb').click(function(event) {
		if(mode == 'pc'){
			$.colorbox({iframe:true, innerWidth:711, innerHeight:399,href:'http://www.youtube.com/embed/'+video2_id+'?rel=0&wmode=transparent&autoplay=1'});
		}
	});
	$('#Youtube_03_thumb').click(function(event) {
		if(mode == 'pc'){
			$.colorbox({iframe:true, innerWidth:711, innerHeight:399,href:'http://www.youtube.com/embed/'+video3_id+'?rel=0&wmode=transparent&autoplay=1'});
		}
	});

	/* globalnetwork */
	$('.area_list .list_wrap a').click(function(event) {
		if(mode == 'pc'){
			$.colorbox({
				inline:true,
				width:"760px",
				href: $(this).data('modal')
			});
			return false
		}
	});
	$('.local_tit a').click(function(event) {
		if(mode == 'pc'){
			var $this = $(this);
			var href = $this.parent('.local_tit').parent('li').find('.local_cont').attr("id");
			$.colorbox({
				inline:true,
				width:"760px",
				href: "#"+href
			});
			return false
		}
	});
	$('.local_list_name span').click(function(event) {
		if(mode == 'sp'){
			var $this = $(this);
			$this.toggleClass('active');
			$('.local_list').slideToggle();
		}
	});
	$('#Information .area_list_name span').click(function(event) {
		if(mode == 'sp'){
			var $this = $(this);
			$this.toggleClass('active');
			$('#Information .list_wrap').slideToggle();
		}
	});
	var Local_area_reset = function(){
		if(mode == 'sp'){
			$('.local_list_name span').removeClass('active');
			$('.local_list').attr('style','');
			$('#Information .list_wrap').attr('style','');
		}else{
			$('.local_tit a').removeClass('open');
			$('.local_cont').attr('style','');
		}
	}

	if(mode == 'sp'){
		sp_youtube_setup();
	}
	/* リサイズ処理 */
	var timer = false;
	$(window).resize(function() {
		if (timer !== false) {
			if(window.matchMedia && window.matchMedia('(max-width:767px)').matches && mode == 'pc'){
				mode = 'sp';
				$.colorbox.close();
				Local_area_reset();
				sp_youtube_setup();
			}else if((!window.matchMedia || !window.matchMedia('(max-width:767px)').matches) && mode == 'sp'){
				mode = 'pc';
				$.colorbox.close();
				Local_area_reset();
				sp_youtube_stop();
			}
		}
		timer = setTimeout(function() {
		}, 200);
	});
});
})(jQuery);