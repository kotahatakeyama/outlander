;(function(){

	//最初に表示したいページ番号（0～）
	var index  = 2;

	$(function(){

		//タブブロック全体を囲む要素
		var $tabs       = $('#Tabs');

		//タブのボタンにあたる要素
		var $tabButton   = $tabs.find('.tabList li a');

		//タブの中身にあたる要素
		var $tabContent = $tabs.find('.tabBox .tab_cont');

		var loadOpenTimer = null;

		var $currentCategory = $('.sort_name > span');


		//ロールオーバー画像を用意する
		// $tabButton.find('img').each(function(){
		// 	$(this).data('off', $(this).attr('src').replace(/_on\.(.+?)$/, '_off.$1'));
		// 	$(this).data('on',  $(this).attr('src').replace(/_off\.(.+?)$/, '_on.$1'));
		// });

		//最初のページをonにして、他のページは非表示にする
	//	$tabButton.eq(index).find('img').attr('src', $tabButton.eq(index).find('img').data('on'));
		$tabButton.eq(index).parent().addClass('active');
		$tabContent.not(':eq('+ index +'), .SPECIFICATIONS _list_inner ul li').hide();

		//クリックされたらタブを切り替える
		$tabButton.off('click').css({ cursor:'pointer' }).click(function(){
			clearTimeout(loadOpenTimer);
			var newIndex = $tabButton.index(this);
			if(index !== newIndex){
				// $(this).find('img').attr('src', $(this).find('img').data('on'));
				// $tabButton.eq(index).find('img').attr('src', $tabButton.eq(index).find('img').data('off'));
				$tabButton.parent().removeClass('active');
				$(this).parent().addClass('active');
				$tabContent.eq(index).hide();
				$tabContent.eq(newIndex).show();
				var $sort = $('.sort_btn > ul > li').eq(newIndex);
				$currentCategory.text($sort.text());
				$sort.addClass('active').siblings().removeClass('active');
				index = newIndex;
			}
			//return false;
		});

		$(window).on('load', function(){
			if(location.hash){
				var $target = $('a[href='+ location.hash +']');
				if($target.length) $target.click();
			}
		});

		var hashes = [];
		$tabButton.each(function(){
			hashes.push('a[href*='+this.hash+']');
		})
		$(hashes.join(',')).not($tabButton).off('click').on('click', function(){
			scroll_func.call($('<a href="#dirPath"></a>'));
			var $target = $('a[href='+ this.hash +']');
			if($target.length) $target.click();
		});

	});

	$(function(){

		$('.sort_name').click(function(){
			$(this).next().slideToggle();
			$(this).find('span').toggleClass('active');
		});

		//タブブロック全体を囲む要素
		var $tabs       = $('#Tabs');

		//タブのボタンにあたる要素
		var $tabButton   = $tabs.find('.sort_btn li');

		//タブの中身にあたる要素
		var $tabContent = $tabs.find('.tabBox .tab_cont');

		//現在のモデル名を表示
		var $wrapper         = $('#Tabs');
		var $categories      = $wrapper.find('.sort_btn > ul > li');
		var $currentCategory = $('.sort_name > span');

		$categories.find('a').on('click', function(){
			$(this).parent().addClass('active').siblings().removeClass('active');
			$currentCategory.text($(this).text());
		});

		//最初のページをonにして、他のページは非表示にする
		$tabButton.eq(index).find('a').attr('src', $tabButton.eq(index).find('a').data('on'));
		$tabContent.not(':eq('+ index +'), .SPECIFICATIONS _list_inner ul li').hide();

		//クリックされたらタブを切り替える
		$tabButton.css({ cursor:'pointer' }).click(function(){
			var newIndex = $tabButton.index(this);
			if(index !== newIndex){
				// console.log($tabs.find('.tabList li a').eq(newIndex).find('img'));
				$tabs.find('.tabList li').eq(newIndex).addClass('active');
				$tabs.find('.tabList li').eq(index).removeClass('active');
				$tabContent.eq(index).hide();
				$tabContent.eq(newIndex).show();
				index = newIndex;
			}
			return false;
		});

		$(window).on('load', function(){
			if(location.hash){
				var $target = $('a[href='+ location.hash +']');
				if($target.length) $target.click();
			}
		});

	});


}).call(this);
