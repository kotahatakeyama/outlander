// ブラウザ名を取得
var getBrowser = function() {
	var ua = window.navigator.userAgent.toLowerCase();
	var ver = window.navigator.appVersion.toLowerCase();
	var name = 'unknown';

	if (ua.indexOf("msie") != -1) {
		if (ver.indexOf("msie 6.") != -1) {
			name = 'ie6';
		} else if (ver.indexOf("msie 7.") != -1) {
			name = 'ie7';
		} else if (ver.indexOf("msie 8.") != -1) {
			name = 'ie8';
		} else if (ver.indexOf("msie 9.") != -1) {
			name = 'ie9';
		} else if (ver.indexOf("msie 10.") != -1) {
			name = 'ie10';
		} else {
			name = 'ie';
		}
	} else if (ua.indexOf('trident/7') != -1) {
		name = 'ie11';
	} else if (ua.indexOf('chrome') != -1) {
		name = 'chrome';
	} else if (ua.indexOf('safari') != -1) {
		name = 'safari';
	} else if (ua.indexOf('opera') != -1) {
		name = 'opera';
	} else if (ua.indexOf('firefox') != -1) {
		name = 'firefox';
	} else if (ua.match(/Trident/)) {
		version = ua.match(/(MSIE\s|rv:)([\d\.]+)/)[2];
		name = 'ie' + version;
	}
	return name;
};
var istablet = false;
//tablet判定
var ua = navigator.userAgent;
if (ua.indexOf('iPhone') > 0 || ua.indexOf('iPod') > 0 || ua.indexOf('Android') > 0 && ua.indexOf('Mobile') > 0) {} else if (ua.indexOf('iPad') > 0 || ua.indexOf('Android') > 0) {
	istablet = true;
} else {}
var movie_none = false;
var browser = getBrowser();
if (browser == 'ie8' || istablet) {
	movie_none = true;
}
//一番最初かどうかの判定
var f_flag = false;
//pc版が初めてかどうか判定
var p_flag = false;
//スマホモード切替
var sp_mode = false;
var w;
/* メインビジュアルのプレイヤー */
var mainvis_pla_flg = false;
var mainvis_pla;
var mainvis_plaVars = {
	'fs': 0,
	'loop': 1,
	'autoplay': 1,
	'modestbranding': 1,
	'controls': 0,
	'showinfo': 0,
	'rel': 0,
	'enablejsapi': 1,
	'version': 3,
	'origin': '*',
	'allowfullscreen': true,
	'wmode': 'transparent',
	'iv_load_policy': 3,
	'disablekb': 1,
	'playlist': 'vaApGgdssQk'
}

function mainvis_plaReady(event) {
	event.target.mute();
	event.target.playVideo();
	$("#Mainvis #Mainvis_inner .txtarea_01 .titstyle_01").fadeOut(1000);
	$("#Mainvis #Mainvis_inner .txtarea_01 .car_sub").fadeOut(1000);
}

function mainvis_plaStateChange(event) {
	event.target.mute();
	var bgHeight = $("#Mainvis").outerHeight();
	var wrapHeight = $(".playerWrap").outerHeight();
	var position = (wrapHeight - bgHeight) / 4;
	if (event.data == 1 && !mainvis_pla_flg) {
		mainvis_pla_flg = true;
		$('#Mainvis_bg').css({
			background: 'none'
		});
		$('#Mainvis .player_wrap').animate({
			opacity: 1
		}, {
			duration: 500,
			easing: 'swing'
		});
		$('#Mainvis .player_wrap').css({
			"position": "absolute",
			"top": "0",
			"left": "0",
			"right": "0",
			"bottom": "0",
			"margin": "auto"
		});
    if ($(window).outerHeight() < 845) {
		$('#Mainvis .player_wrap').css({
			"top": "-7%",
      "bottom": "inherit"
		});
    } if ($(window).outerHeight() < 780) {
		$('#Mainvis .player_wrap').css({
			"top": "-18%",
      "bottom": "inherit"
		});
  } if ($(window).outerWidth() / $(window).outerHeight() <= 1.78) {
		$('#Mainvis .player_wrap').css({
			"position": "absolute",
			"top": "0",
			"left": "0",
			"right": "0",
			"bottom": "0",
			"margin": "auto"
		});
    }
	}
}

function t_movie_04Ready(event) {
	event.target.mute();
	event.target.playVideo();
	event.target.setPlaybackQuality('small');
}
function onYouTubeIframeAPIReady() {
	if (browser == 'safari') {
		mainvis_plaVars.html5 = 1;
	}
	try {
		mainvis_pla = new YT.Player('mainvis_pla', {
			height: '100%',
			width: '100%',
			videoId: 'vaApGgdssQk',
			playerVars: mainvis_plaVars,
			events: {
				'onReady': mainvis_plaReady,
				'onStateChange': mainvis_plaStateChange
			}
		});
	} catch (e) {}

}

//リサイズ時関数
function resize_window() {

	w = $(window).width();
	if (!('innerWidth' in window)) window.innerWidth = w;
	if (w < 768) {
		if (!f_flag) {
			sp_mode = true;
			sp_format();
			f_flag = true;
		}
		if (!sp_mode) {
			sp_mode = true;
			sp_format();
		}
	} else {
		if (!f_flag) {
			sp_mode = false;
			pc_format();
			f_flag = true;
		}
		if (sp_mode) {
			sp_mode = false;
			pc_format();
		}
	}
	if (sp_mode) {
		sp_resize();
	} else {
		pc_resize();
	}

}

function sp_format() {
	$('#Mainvis_bg,#Mainvis .player_wrap').attr('style', '');
	$('.thumb_movie_wrap').css({
		opacity: 0
	});
	if (p_flag) {
		try {
			mainvis_pla.destroy();
			t_movie_01.destroy();
			t_movie_02.destroy();
			t_movie_03.destroy();
			t_movie_04.destroy();
		} catch (e) {}

		mainvis_pla_flg = false;
		t_movie_01_flg = false;
		t_movie_02_flg = false;
		t_movie_03_flg = false;
		t_movie_04_flg = false;
	}

}

function pc_format() {
	if (!movie_none) {
		if (p_flag) {
			onYouTubeIframeAPIReady();
		} else {
			var tag = document.createElement('script');
			tag.src = "https://www.youtube.com/iframe_api";
			var firstScriptTag = document.getElementsByTagName('script')[0];
			firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
			p_flag = true;
		}
	}

}

function sp_resize() {}

function pc_resize() {

	var mainvis_w = $('#Mainvis').width();
	var h = $(window).height();
	var minH = 490;
	var maxH = 930;
	var headerHeight = 120;

	if (h < minH) {
		h = minH - headerHeight;
	} else if (h > maxH) {
		h = maxH - headerHeight;
	} else {
		h = h - headerHeight;
	}
	$('#Mainvis_bg').css({
		'height': h + 'px'
	});
	if (!movie_none) {
		var t_h = (mainvis_w / 16) * 9;
		var t_w = (h / 9) * 16;

		if (h > t_h) {
			$('#Mainvis .player_wrap').css({
				'height': h + 'px',
				'width': t_w + 'px'
			});
			$('#mainvis_pla').css({
				'left': '-' + (t_w - mainvis_w) / 2 + 'px',
				'top': '0'
			});
		} else {
			$('#Mainvis .player_wrap').css({
				'height': t_h + 'px',
				'width': mainvis_w + 'px'
			});
			$('#mainvis_pla').css({
				'left': '0',
				'top': '-' + (t_h - h) /2 + 'px'
			});
		}
	}
}
(function($) {
	$(function() {

		resize_window();
		var timer = false;
		$(window).resize(function() {
			if (timer !== false) {
				clearTimeout(timer);
			}
			timer = setTimeout(function() {
				resize_window();
			}, 200);
		});
		$('#Mainvis_ov').click(function() {
			console.log('click');
			return false;
		});
		$('#Mainvis_ov').dblclick(function() {
			return false;
		});
		if (istablet) {
			$('#sec_design_01 .pc_bg,#sec_design_02 .pc_bg,#sec_performance_01 .pc_bg,#sec_performance_02 .pc_bg').addClass('istablet');
			$('#sec_design_01 .imgstyle_02,#sec_design_02 .imgstyle_02,#sec_performance_01 .imgstyle_02,#sec_performance_02 .imgstyle_02').addClass('istablet');
			$('#sec_design_01 .pc_bg img').attr('src', './images/bg_02_tb.jpg');
		}

	});

	$(window).on('unload', function() {});


	/* sideNav */

	var wH = $(window).height();
	var wW = $(window).width();

	$(function(){
	  var header2 = $('header');
	  var offset = header2.offset();
	  var $sideNav = $('#sideNav');
	  var $headerG = $('#headerGroup');
	  var hH = $headerG.height();
	  var wT,_y,isFirst = true;
	  var mouseoutSubNav = false;
	  var mouseoutMainNav = false;

	  //nav height
	  var $naviH, ary =[];
	  $(".sideNavList li").each(function (i){
	    ary.push(parseInt($(this).find("img").attr("height")));
	  });

	  var sum  = function(arr) {
	    var sum = 0;
	    for (var i=0,len=arr.length; i<len; ++i) {
	        sum += arr[i];
	    }
	    return sum;
	  };

	  $naviH = sum(ary);
	  $sideNav.css({right:'0', height : $naviH});
	  $sideNav.find(".sideNavList").css({height : $naviH});

	  function setNaviPositon(){
	    if($(window).scrollTop() > offset.top) {
	      header2.addClass('header2_position_fixed');
	    } else {
	      header2.removeClass('header2_position_fixed');
	    }

	    wH = $(window).height();
	    wW = $(window).width();
	    hH = $headerG.height();
	    wT = $(window).scrollTop();


	    if($('header').hasClass('header2_position_fixed')){
	      if(wH <= $naviH + 80) {
	        _y = 0;
	      } else{
	        _y = ($(window).height() - hH)/2 -$naviH/2;
	      }
	    } else {
	      if(wH <= $naviH + 160) {
	        _y = 0;
	      } else{
	        _y = ($(window).height()- hH )/2 -$naviH/2;
	        wT = wT/2;
	      }
	    }

	    if(isFirst) {
	      $sideNav.css({
	        top: wT,
	        marginTop : _y
	      });
	      isFirst = false;
	    } else {
	      $sideNav.stop().animate({
	        duration: 'fast',
	        top: wT,
	        marginTop : _y,
	        right:'-203'
	      });
	    }
	  }

	  function fireMoveNavi(){
	    setNaviPositon();
	  }

	  function init(){
	    if ($(window).scrollTop() < (offset.top)) {
	      $('html,body').stop().animate({scrollTop:(offset.top)},500, function() {
	        setTimeout(function(){
	          $('#sideNav').stop().animate({right:'-203px'},500);
	        },500);
	      });
	    } else {
	      isFirst = true;
	      setNaviPositon();
	      $('#sideNav').stop().animate({right:'-203px'},500, function (){
	      });
	    }
	  }


	  $(".sideNavList").hover(
	    function (e) {
	      if(mouseoutSubNav === true) {
	        //サブメニューから離れた時は隠す
	        mouseoutSubNav = false;
	        e.preventDefault();
	        return false;
	      } else {
	        if($('#sideNav').css('right') == '-203px'){
	          $(this).stop().animate({right:'203px'},200);
	          if($('.sub-sideNavList')[0]) {
	            if($(".sub-sideNavList").css('right') !== '494px') {
	              $('.sub-sideNavList').stop().animate({right:'203px'},200);
	            } else {
	              if( !$(e.target).hasClass('js-open-sub-nav') ) {
	                $('.sub-sideNavList').stop().animate({right:'203px'},200);
	              } else {
	                $('.sub-sideNavList').stop().animate({right:'494px'},200);
	              }
	            }
	          }
	        }else{
	        }
	      }
	    },
	    function (e) {
	      $(this).stop().animate({right:'0'},200);
	      if($('.sub-sideNavList')[0]) {
	        $('.sub-sideNavList').stop().animate({right:'-203px'},200);
	        if(!$(e.relatedTarget).hasClass('js-list-item-img')) {
	          mouseoutMainNav = true;
	          $(".js-open-sub-nav").stop().animate({right:'-30px'}, 200).css('z-index', '-1');

	          if(!$(e.relatedTarget).hasClass('js-sub-list-item-bottom')) {
	            $(".sub-sideNavList").stop().animate({right:'0'},200);
	          }
	        }
	      }
	    }
	  );

	  $('a[href*=#]:not([href=#])').click(function() {
	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
	      var target = $(this.hash);
	      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	      if (target.length) {
	        if($(window).scrollTop() > offset.top) {
	            $('html,body').stop().animate({
	              scrollTop: (target.offset().top)
	            }, 500);
	            return false;
	         } else {
	            $('html,body').stop().animate({
	              scrollTop: (target.offset().top - 80)
	            }, 500);
	            return false;
	        }
	     }
	    }
	  });

	  setNaviPositon();
	  $(window).scroll(fireMoveNavi);
	  $(window).resize(fireMoveNavi);
	  setTimeout(init,1000);

	});

})(jQuery);

$(function(){
	var agent = navigator.userAgent;
	if(agent.search(/iPad/) != -1){
		$('.flex-next,.flex-prev').on('touchend',function(){
			player[0].pauseVideo();
			player[1].pauseVideo();
			player[2].pauseVideo();
			player[3].pauseVideo();
		});
		$('#carousel li').not('.flex-active-slide').on('touchend',function(){
			player[0].pauseVideo();
			player[1].pauseVideo();
			player[2].pauseVideo();
			player[3].pauseVideo();
		});
		return false;
	}
});
