$(function() {
	var tab = function(el) {
		var $el = el;
		var $btnWrap = $el.find('.tab-btns');
		var $cont = $el.find('.tab-contents');
		var $btn = $btnWrap.find('li');
		var pos = $el.offset().top - 70;
		var speed = 400;
		var bkpoint = 767;
		var href,index;

		function init() {
			controller();
			window.addEventListener('resize', resize());
		}

		function resize() {
			var w = window.innerWidth;
			var ot = w * 0.2;

			if(w < bkpoint){
				pos = $el.offset().top + ot;
			}else{
				pos = $el.offset().top - 70;
			}

		}

		function controller() {
			$btn.on('click', function() {
				index = $(this).index();
				changeTab($(this));
				setNav();
			});

			$btn.on('mouseenter', function() {
				$(this).addClass('on');
			});

			$btn.on('mouseleave', function() {
				$(this).removeClass('on');
			});

		}

		function setNav() {
			$btn.removeClass('active');
			$btnWrap.each(function(i){
				var nav = $(this).find('li');
				nav[index].classList.add('active');
			});

		}

    function changeTab(t) {
			$cont.css({'opacity':0}).stop(true,true).hide( function() {
        $cont.animate({'display':'none!important'},speed);
				$cont.removeClass('active');
				$('html,body').stop().animate({'scrollTop':pos},speed);
				$($cont[index]).stop(true,true).fadeIn(speed, function(){
					$cont[index].classList.add('active');
  				$cont.css({'display':'block!important'});
				});
			});
		}
		init();
	};

	tab($('.nav-tab-01'));
});
