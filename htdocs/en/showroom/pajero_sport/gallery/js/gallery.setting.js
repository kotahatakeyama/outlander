$(function() {
	$('#flexslider').flexslider({
		animation: "slide",
		controlNav: true,
		animationLoop: false,
		slideshow: false,
		sync: "#carousel",
		directionNav: true,
		prevText: '<img src="./images/back.png" alt="previous">',
		nextText: '<img src="./images/next.png" alt="next">'
	});
	$('#carousel').flexslider({
		animation: "slide",
		controlNav: true,
		animationLoop: false,
		slideshow: false,
		itemWidth: 174,
		directionNav: true,
		asNavFor: '#flexslider',
		prevText: '<img src="./images/carousel_prev.png" alt="previous">',
		nextText: '<img src="./images/carousel_next.png" alt="next">'
	});
});

/* ----------------------------------------------------------------------------------
　movie thumb
---------------------------------------------------------------------------------- */
$(function() {
	var video = [];
	$('.movie_youtube iframe').each(function(){
		video.push($(this).attr('src'));
	})

	var flag = 0;

	$(document).on('click', '.movie_thumb', function(){

		ga('send', 'event', 'PAJEROSPORT_GALLERY', 'movie1', 'action', 1);

		if ( navigator.userAgent.indexOf('iPhone') > 0 || (navigator.userAgent.indexOf('Android') > 0 && navigator.userAgent.indexOf('Mobile') > 0) ) {
			var mv_url = $(this).find('a').attr('href');
			window.open(mv_url,'_blank');
			return false;
		}

		flag = 1;
		// $('.movie_thumb').css({ visibility:'hidden' });
		// var $iframe = $(this).parent().find('.movie_youtube iframe');
		// var index = $('.movie_youtube iframe').index($iframe);
		// var src = video[index]+'&autoplay=1';
		// $iframe.css({ visibility:'visible' }).attr('src', src);

		var ww = $(window).width();
		if(ww<=768){
			return false;
		}

	});

	$(window).on('load resize', function() {
		var ww = $(window).width();
		if(ww<=768){
			var mt_w = $('.movie_thumb').width();
			var mt_h = $('.movie_thumb').height();
			$('.movie_youtube iframe').width(mt_w);
			$('.movie_main, .movie_youtube iframe').height(mt_h);
			$('.sort_btn').css('display','none');
			if(flag == 1){
				$('.movie_title').hide();
			}
		}else{
			$('.movie_youtube iframe').width('712px');
			$('.movie_youtube iframe').height('400px');
			$('.movie_main').height('400px');
			$('.movie_title').show();
			$('.sort_btn').css('display','inline');
			$('.sort_name span').removeClass('active');
		}
	});
});


$(function() {
	var video = [];
	$('.movieB_youtube iframe').each(function(){
		video.push($(this).attr('src'));
	})

	var flag = 0;

	$(document).on('click', '.movieB_thumb', function(){

		ga('send', 'event', 'PAJEROSPORT_GALLERY', 'movie2', 'action', 1);

		if ( navigator.userAgent.indexOf('iPhone') > 0 || (navigator.userAgent.indexOf('Android') > 0 && navigator.userAgent.indexOf('Mobile') > 0) ) {
			var mv_url = $(this).find('a').attr('href');
			window.open(mv_url,'_blank');
			return false;
		}

		flag = 1;
		// $('.movieB_thumb').css({ visibility:'hidden' });
		// var $iframe = $(this).parent().find('.movieB_youtube iframe');
		// var index = $('.movieB_youtube iframe').index($iframe);
		// var src = video[index]+'&autoplay=1';
		// $iframe.css({ visibility:'visible' }).attr('src', src);

		var ww = $(window).width();
		if(ww<=768){
			return false;
		}

	});

	$(window).on('load resize', function() {
		var ww = $(window).width();
		if(ww<=768){
			var mt_w = $('.movieB_thumb').width();
			var mt_h = $('.movieB_thumb').height();
			$('.movieB_youtube iframe').width(mt_w);
			$('.movieB_main, .movieB_youtube iframe').height(mt_h);
			$('.sort_btn').css('display','none');
			if(flag == 1){
				$('.movieB_title').hide();
			}
		}else{
			$('.movieB_youtube iframe').width('712px');
			$('.movieB_youtube iframe').height('400px');
			$('.movieB_main').height('400px');
			$('.movieB_title').show();
			$('.sort_btn').css('display','inline');
			$('.sort_name span').removeClass('active');
		}
	});
});



/*360views------------------------------*/
$(function(){

	/*変数*/
	var targetArea=$('#Loading_list');//viewerの登録
	var imgList=new Array();//画像のリスト
	var currentId=4;//表示画像ID
	var prevId=0;//一個前のID
	var speed=40;//切り替え距離

	var startTime;//ドラッグ開始時間
	var stopTime;//ドラッグ終了時間
	var dragTime=200;//tweenが実行される時間
	var intarvalSpeed=0;//インターバルの時間
	var tween;//tween
	var easing=0;//イージング

	var firstPos;//ドラッグ開始位置
	var startPos;//ドラッグ起点
	var mode;//正か負か
	var direction;//進行方向

	var init=function(){
			imgs=targetArea.find('li img');
			imgList.length = 0;
			imgs.each(function(){
			imgList.push($(this));
			});

			for(var i=0; i<imgList.length;i++){
				imgList[i].addClass('previous-img');
			}
			imgList[currentId].removeClass('previous-img');
			imgList[currentId].addClass('current-img');

			targetArea.draggable({containment:targetArea});
			targetArea.draggable('option','axis','x');
			StartDragEvt();
			mouseEvt();
	};

	/*マウスイベント*/
	var mouseEvt=function(){
			targetArea.mousewheel(function(event, delta, x, y){
				if(x > 0){
					startRote("right");
					return false;
				}else if (x < 0){
					startRote("left");
					return false;
				}
			});
	};


	$('.rotation_right').click(function(){
		startRote("right");
		ga('send', 'event', 'PAJEROSPORT_GALLERY', '360', 'action', 1);
		return false;
	});
	$('.rotation_left').click(function(){
		startRote("left");
		ga('send', 'event', 'PAJEROSPORT_GALLERY', '360', 'action', 1);
		return false;
	});

	/*ドラッグイベント*/
	var StartDragEvt=function(){
		//ドラッグ開始
		targetArea.draggable({
			start: function(event, ui) {
			var startDate = new Date();
			startTime = startDate.getTime();

			firstPos=event.clientX;
			startPos=firstPos;
			intarvalSpeed=0;
			easing=0;
			clearInterval(tween);
			}
		});

		//ドラッグ中
		targetArea.draggable({
			drag: function(event, ui){
				var distance=startPos-event.clientX;
				//console.log(distance);
				if(distance>=0){
					mode="right";
				}else{
					mode="left";
				}
				distance=Math.abs(distance);
				if(distance>=speed){
				startRote(mode);
				startPos=event.clientX;
				}

			}
		});

		//ドラッグ終了
		targetArea.draggable({
			stop: function(event, ui) {
			var startDate = new Date();
     	stopTime = startDate.getTime();
			var time=stopTime-startTime;

			var distance=firstPos-event.clientX;
			var pos=Math.round((distance)/speed);
			easing=(70-Math.abs(pos))/9;

			//進行方向
			if(mode=="right"){
				direction=1;
			}else{
				direction=-1;
			}
			//イージング
			if((time<=dragTime)&&(Math.abs(pos)>=5)){
				tween = setInterval(function(){ startRote(direction); },intarvalSpeed);
				}

			}
		});
	};


	//表示切替
	var startRote=function(mode){
		prevId=currentId;

		if(mode=="right"){
			currentId++;
		}else if(mode=="left"){
			currentId--;
		}else{
			currentId+=mode;
			easing+=0.03;
			intarvalSpeed+=1*easing;

			clearInterval(tween);
			tween = setInterval(function(){ startRote(direction); },intarvalSpeed);
			if(intarvalSpeed>=60){
			clearInterval(tween);
			}
		}

		if(currentId>=imgList.length){
			currentId=0+(currentId-imgList.length);
		}else if(currentId<0){
			currentId=(imgList.length)+currentId;
		}

		imgList[prevId].removeClass('current-img');
		imgList[prevId].addClass('previous-img');
		imgList[currentId].removeClass('previous-img');
		imgList[currentId].addClass('current-img');
	};


	//プリロード
	var preload = function(){
			var	img = [],
			img_arr = $('#Loading_list img'), // ローディング対象の画像を指定
			img_arr_lngth = img_arr.size();
			img_arr.each(function(indx){
			$('.loading_img').show();
			$('#Loading_list ul').css({opacity: 0});
			img[indx] = new Image();
			img[indx].onload = function(){
				img_arr_lngth = img_arr_lngth - 1;
				if( 0 >= img_arr_lngth ){
					// 最後の画像を読み込み終わった際の動作を指定
					$('.loading_img').hide();
					$('#Loading_list ul').animate({opacity: 1}, {duration:150, queue: false});
					init();
				}
			};
			img[indx].src = $(this).attr('src');
		});
	};

	/*実行*/
	preload();

});
