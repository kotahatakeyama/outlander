$(function(){

	var $elem = $(".toolbox"),
		$content = $(".footer_ir"),
		$win = $(window);

	var contentTop = 0;

	$win
		.load(function(){
			updatePosition();
			update();
		})
		.resize(function(){
			updatePosition();
			update();
		})
		.scroll(function(){
			update();
		});

	function updatePosition(){
		contentTop = $content.offset().top + $elem.outerHeight();
	}

	function update(){
		if( $win.scrollTop() + $win.height() > contentTop ){
			$elem.addClass("static");
			$content.addClass("wrap_static");
		}else if( $elem.hasClass("static") ){
			$elem.removeClass("static");
			$content.removeClass("wrap_static");
		}
	}

});