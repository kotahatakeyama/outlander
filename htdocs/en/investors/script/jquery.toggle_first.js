$(function() {
	  $('.wrap_toggle:not(:first)').hide();
	  $('.title_toggle:first').addClass('active');
	  $('.title_toggle').click(function() {
        var $this = $(this);
        if ($this.hasClass('active')) {
          $this.removeClass('active');
        }
        else {
          $this.addClass('active');
        }
        $this.next('.wrap_toggle').slideToggle('fast');
      });
});