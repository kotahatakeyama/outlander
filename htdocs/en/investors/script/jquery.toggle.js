$(function() {
	  $('.wrap_toggle').hide();
	  $('.title_toggle').click(function() {
        var $this = $(this);
        if ($this.hasClass('active')) {
          $this.removeClass('active');
        }
        else {
          $this.addClass('active');
        }
        $this.next('.wrap_toggle').slideToggle('fast');
      });
});