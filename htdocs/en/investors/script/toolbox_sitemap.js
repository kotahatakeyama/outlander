var html = '<div class="box_sitemap clearfix">';
html += '<div class="box">';
html += '<h3 class="tit_map"><a href="/en/investors/">Information for our Shareholders and Investors</a></h3>';
html += '</div>';


html += '<div class="box">';
html += '<h3 class="tit_map"><a href="/en/investors/corpmanage/">Corporate and Management Information</a></h3>';
html += '<ul>';
html += '<li><a href="/en/investors/corpmanage/topmessage.html">Top Message</a></li>';
html += '<li><a href="/en/investors/individual/mp.html">Mid-Term Business Plan</a></li>';
html += '<li><a href="/en/investors/corpmanage/guideline.html">Policy on Timely Disclosure of Material Information</a></li>';
html += '<li><a href="/en/investors/corpmanage/risk.html">Business-Related Risks</a></li>';
html += '<li><a href="/en/corporate/philosophy/">Three Principles and MMC\'s Corporate Philosophy</a></li>';
html += '<li><a href="/en/corporate/aboutus/profile/">Corporate Profile</a></li>';
html += '</ul>';
html += '</div>';

html += '<div class="box">';
html += '<h3 class="tit_map"><a href="/en/investors/governance/">Corporate Governance</a></h3>';
html += '<ul>';
html += '<li><a href="/en/investors/governance/policy.html">Basic Policy and Framework</a></li>';
html += '<li><a href="/en/investors/governance/system.html">Internal Control Systems</a></li>';
html += '<li><a href="/en/investors/governance/risk.html">Risk Management</a></li>';
html += '<li><a href="/en/investors/governance/director.html">Members of the Board</a></li>';
html += '<li><a href="/en/investors/governance/compensation.html">Remuneration of Director and Auditor</a></li>';
html += '<li><a href="/en/investors/governance/antisocial.html">Basic Policy against Anti-Social Forces</a></li>';
html += '</ul>';
html += '</div>';

html += '<div class="box">';
html += '<h3 class="tit_map"><a href="/en/investors/finance_result/">Business Performance and Financial Information</a></h3>';
html += '<ul>';
html += '<li><a href="/en/investors/finance_result/highlight.html">Business Results Report for Current Term</a></li>';
html += '<li><a href="/en/investors/finance_result/indices.html">Business Performance and Financial Information</a></li>';
html += '<li><a href="/en/investors/finance_result/data.html">Financial Statements</a></li>';
html += '<li><a href="/en/investors/finance_result/segment.html">Information by Region</a></li>';
html += '<li><a href="/en/investors/finance_result/result.html">Production, Sales and Export Data</a></li>';
html += '</ul>';
html += '</div>';
html += '</div>';

html += '<div class="box_sitemap clearfix">';
html += '<div class="box">';
html += '<h3 class="tit_map"><a href="/en/investors/library/">IR Library</a></h3>';
html += '<ul>';
html += '<li><a href="/en/investors/library/earning.html">Financial Results Information</a></li>';
html += '<li><a href="/en/investors/library/anual.html">Annual Report</a></li>';
html += '<li><a href="/en/investors/library/fact.html">Facts and Figures</a></li>';
html += '<li><a href="/en/social/report/">CSR Report</a></li>';
html += '</ul>';
html += '</div>';

html += '<div class="box">';
html += '<h3 class="tit_map"><a href="/en/investors/event/">IR Events</a></h3>';
html += '<ul>';
html += '<li><a href="/en/investors/event/calendar.html">IR Calendar</a></li>';
html += '<li><a href="/en/investors/library/earning.html">Financial Results Meetings</a></li>';
html += '<li><a href="/en/investors/event/plan.html">Mid-Term Business Plan Meetings</a></li>';
html += '<li><a href="/en/investors/event/others.html">Information Meetings for Business Initiatives</a></li>';
html += '<li><a href="/en/investors/event/meeting.html">General Shareholders&rsquo; Meetings</a></li>';
html += '</ul>';
html += '</div>';

html += '<div class="box">';
html += '<h3 class="tit_map"><a href="/en/investors/stockinfo/">Stock and Corporate Bond Information</a></h3>';
html += '<ul>';
html += '<li><a href="/en/investors/stockinfo/stock_status.html">Stock Status</a></li>';
html += '<li><a href="/en/investors/stockinfo/overview.html">Shareholder Composition</a></li>';
html += '<li><a href="/en/investors/stockinfo/rating.html">Credit Ratings and Bonds Information</a></li>';
html += '<li><a href="/en/investors/stockinfo/analyst.html">Analyst Coverage</a></li>';
html += '<li><a href="/en/investors/stockinfo/procedure.html">Stock Procedure Information</a></li>';
html += '<li><a href="/en/investors/stockinfo/return.html">Shareholder Return</a></li>';
html += '<li><a href="/en/investors/stockinfo/pdf/articles_of_incorporation.pdf" target="_blank">Articles of Incorporation</a><img src="/en/investors/images/icon_pdf.png" width="12" height="12" alt="PDF" /></li>';
html += '</ul>';
html += '</div>';

html += '<div class="box">';
html += '<h3 class="tit_map"><a href="/en/investors/individual/">Mitsubishi Motors at a Glance</a></h3>';
html += '<ul>';
html += '<li><a href="/en/investors/individual/mp.html">Mid-Term Business Plan</a></li>';
html += '<li><a href="/en/investors/individual/about.html">Overview of Mitsubishi Motors</a></li>';
html += '<li><a href="/en/investors/stockinfo/return.html">Shareholder Return</a></li>';
html += '<li><a href="/en/investors/individual/relation.html">Related Contents</a></li>';
html += '</ul>';
html += '</div>';
html += '</div>';

html += '<div class="box_sitemap clearfix">';
html += '<div class="box">';
html += '<h3 class="tit_map"><a href="/en/investors/irnews/">IR News</a></h3>';
html += '</div>';

html += '<div class="box">';
html += '<h3 class="tit_map"><a href="/en/investors/inquiry.html">IR Inquiries</a></h3>';
html += '</div>';

html += '<div class="box">';
html += '<h3 class="tit_map"><a href="/en/investors/faq.html">FAQ</a></h3>';
html += '</div>';

html += '<div class="box">';
html += '<h3 class="tit_map"><a href="/en/investors/exemption.html">IR Site Disclaimer</a></h3>';
html += '</div>';
html += '</div>';

html += '<div class="box_sitemap clearfix">';
html += '<div class="box mt5">';
html += '<h3 class="tit_map"><a href="//www.bloomberg.com/quote/7211:JP" target="_blank">Stock Price Information<img src="/share/images/blank_icon_01.gif" alt="" width="18" height="12" /></a></h3>';
html += '</div>';

html += '<div class="box mt5">';
html += '<h3 class="tit_map"><a href="/jp/investors/">Japanese IR</a></h3>';
html += '</div>';
html += '</div>';

document.write(html);