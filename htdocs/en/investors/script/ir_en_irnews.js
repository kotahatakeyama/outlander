var release_tag_ir = "<dl>";
var lastYear_ir;
var cnt_ir = 0;

$(function(){
	$.ajax({
		url: '/en/investors/irnews/xml/data.xml',
		type:'get', 
		dataType:'xml',
		async:'true',
		cache :'false',
		success:global_en_topir_xml
	});
});

function global_en_topir_xml(xml,status){
	if(status!='success')return;
	$(xml).find('item').each(function(){
		var $day = $(this).find("day").text();
		lastYear_ir = $day.substring($day.length-4,$day.length);
		return false;
	}); 
	read_en_ir_xml();
}

function read_en_ir_xml(){
	$.ajax({
		url: '/en/investors/irnews/xml/data_' + lastYear_ir + '.xml',
		type:'get', 
		dataType:'xml',
		async:'true',
		cache :'false',
		success:global_en_irnews_xml
	});
}

function global_en_irnews_xml(xml,status){
	if(status!='success')return;
	$(xml).find('item').each(irnews_top); 
	
	if(cnt_ir<3){
		$.ajaxSetup({ async: false });
		$.ajax({
			url: '/en/investors/irnews/xml/data_' + (lastYear_ir-1) + '.xml',
			type:'get', 
			dataType:'xml',
			async:'true',
			cache :'false',
			success:global_en_irnews_xml2
		});
	} else {
		write_irnews();
	}
}

function global_en_irnews_xml2(xml,status){
	if(status!='success')return;
	$(xml).find('item').each(irnews_top); 
	write_irnews();
}

function irnews_top(){
	var $day = $(this).find("day").text();
	var $url = $(this).find("url").text();
	var $title = $(this).find("title").text();
	var $icon = $(this).find("category").text();
	var $link = $(this).find("destination").text();	
	
	if ( $icon == 'corporate') {
		release_tag_ir = release_tag_ir + "<dt><span>" + $day + "</span><img src='/en/investors/images/icon_n_corp.png' width='75' height='16' alt='Corporate' /></dt>";
	} else if ( $icon == 'financial') {
		release_tag_ir = release_tag_ir + "<dt><span>" + $day + "</span><img src='/en/investors/images/icon_n_result.png' width='106' height='16' alt='Financial Results' /></dt>";
	} else if ( $icon == 'disclosure') {
		release_tag_ir = release_tag_ir + "<dt><span>" + $day + "</span><img src='/en/investors/images/icon_n_disc.png' width='106' height='16' alt='Timely Disclosure' /></dt>";
	} else if ( $icon == 'information') {
		release_tag_ir = release_tag_ir + "<dt><span>" + $day + "</span><img src='/en/investors/images/icon_n_news.png' width='75' height='16' alt='News' /></dt>";
	}
	
	if ( $link == 'inside') {
		release_tag_ir = release_tag_ir + "<dd class='link'><a href='" + $url + "'>" + $title + "</a></dd>";
	} else if ( $link == 'outside') {
		release_tag_ir = release_tag_ir + "<dd class='link'><a href='" + $url + "' target='_blank'>" + $title + "</a><img src='/share/images/blank_icon_01.gif' alt='' width='18' height='12' /></dd>";
	} else if ( $link == 'pdf') {
		release_tag_ir = release_tag_ir + "<dd class='link'><a href='" + $url + "' target='_blank'>" + $title + "</a><img src='/en/investors/images/icon_pdf.png' alt='' width='12' height='12' /></dd>";
	}
	
	cnt_ir =cnt_ir + 1;
	if(cnt_ir==3)return false;
}

function write_irnews(){
	release_tag_ir = release_tag_ir + "</dl>";
	$('#irnews_area').html(release_tag_ir); 
	$('.image_loading_ir').css('display','none');
	$('#irnews_area').css('display','block');
}