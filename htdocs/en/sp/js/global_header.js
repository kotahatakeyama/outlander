// =========================================================
//
//	global_header.js
//
// =========================================================
var load_header = function(option){
	var opt = $.extend({
			index : false
		}, option),
		header_html = '';

	header_html += '<h1><a href="http://www.mitsubishi-motors.com/en/"><img src="/en/sp/investors/img/logo.png" alt="MITSUBISHI MOTORS"></a></h1>';
	header_html += '<div id="GlobalMenu">';
	header_html += '<ul>';
	header_html += '<li class="header_mn_eng"><a href="http://www.mitsubishi-motors.com/jp/" class="opw"><img src="/en/sp/investors/img/header_mn_eng.png" alt="Japanese"></a></li>';
	header_html += '<li id="GlobalMenuClip" class="header_mn_clip"><img src="/en/sp/investors/img/header_mn_clip.png" alt="clip"></li>';
	header_html += '<li id="GlobalMenuSitemap" class="header_mn_menu"><img src="/en/sp/investors/img/header_mn_menu.png" alt="MENU"></li>';
	header_html += '</ul>';
	header_html += '</div><!-- / #GlobalMenu -->';
	
	$(document).ready(function(){
		$('#GlobalHeader').html( header_html );
		header_navi();
	});
};