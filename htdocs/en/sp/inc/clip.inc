<div class="inner">
	<div class="clip_add">
		<p class="btn"><span>add this page to my clip</span></p>
		<p class="already_clip">This page is already clip.</p>
	</div><!-- /.clip_add -->

	<section class="clip_list">
		<h1>My clip List</h1>
		
		<div class="clip_list_inner">
			
			
			<p class="no_clip">There is no clipped file.</p>
		</div><!-- /.clip_list_inner -->
	</section><!-- /.clip_list -->

	<section class="about_myclip">
		<h1>My clip function</h1>
		<p>It is the ability to save the page you are currently viewing. Next time, when you visit this web site also, You can move immediately to the saved page.</p>
	</section><!-- /.about_myclip -->

	<p class="close">Close</p>
<!-- / .inner --></div>
