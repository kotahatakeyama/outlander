<div class="inner">

<script>
$(function(){
	$("a.opw_m").click(function(){
		window.open(this.href);
		return false;
	});
});
</script>

<div id="menu">
<ul id="topmenu">
<li class="pc"><a href="http://www.mitsubishi-motors.com/en/" class="mn1">Top Page</a></li>
<li class="pc"><a href="http://www.mitsubishi-motors.com/en/products/index.html" class="mn2">Product Information</a></li>
<li class="pc"><a href="http://www.mitsubishi-motors.com/en/spirit/index.html" class="mn3">Mitsubishi Motors Spirit</a></li>
<li class="pc"><a href="http://www.mitsubishi-motors.com/en/social/index.html" class="mn4">Sustainability</a></li>
<li class="pc"><a href="http://www.mitsubishi-motors.com/en/corporate/index.html" class="mn5">Corporate Info and IR</a></li>
<li class="pc"><a href="http://www.mitsubishi-motors.com/en/events/index.html" class="mn6">News･Events</a></li>
</ul>
<div class="secmenu">
<p class="ml st"><a href="http://www.mitsubishi-motors.co.jp/sp/index.html" class="opw_m aw">Mitsubishi Motors Japan<br>Official Site</a></p>
<p class="mr"><a href="http://www.mitsubishi-motors.com/publish/pressrelease_en/index.html" class="pc">Press Release</a></p>
</div>
<div class="secmenu">
<p class="ml"><a href="http://www.mitsubishi-motors.com/en/global_network/index.html" class="opw_m pc">Global Network</a></p>
<p class="mr"><a href="http://www.mitsubishi-motors.com/en/showroom/" class="opw_m aw">Global Showroom</a></p>
</div>
<div class="secmenu">
<p class="ml"><a href="http://www.mitsubishi-motors.com/en/sitemap/index.html" class="pc">Site Map</a></p>
<p class="mr"><a href="http://www.mitsubishi-motors.com/en/socialmedia/index.html" class="pc">Social Media</a></p>
</div>
<div class="secmenu">
<p class="ml"><a href="http://www.mitsubishi-motors.com/en/privacy/index.html" class="pc">Privacy Policy</a></p>
<p class="mr"><a href="http://www.mitsubishi-motors.com/en/usage/index.html" class="pc">Terms of Use</a></p>
</div>

<p class="close">Close</p>
</div>
	
<!-- / .inner --></div>