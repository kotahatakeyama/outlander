
function localNavi_write(){
var navi;
var langageCode = 'en'
var categoryCode = 'discoveries';

var path = langageCode +'/'+ categoryCode;

navi = '<div id="localNavigation"><div class="inner"><ul>';
navi += '<li><a href="/'+path+'/index.html"><span>Discoveries</span></a></li>';

navi += '<li id="motorshow"><a href="/'+path+'/motorshow/index.html"><span>Motor Shows</span></a></li>';
navi += '<li id="wallpaper"><a href="/wallpapermaker/index.html" target="_blank" onclick="openWin(this.href,\'wallpaper\',\'980\',\'555\',\'1\');return false;" onkeypress="openWin(this.href,\'wallpaper\',\'980\',\'555\',\'1\');return false;"><span>Mitsubishi Motors Wallpaper Maker</span></a>';
navi += '<li id="kids"><a href="/'+path+'/kids/index.html"><span>Kids&#039; Car Museum</span></a></li>';
navi += '<li id="motorsports"><a href="/special/motorsports/" target="_blank"><span>Motor Sports</span></a></li>';
navi += '<li><a href="http://www.youtube.com/user/MitsubishiMotorsTV" target="_blank"><span>Mitsubishi-Motors.TV</span></a></li>';
navi += '<li><a href="http://www.mitsubishi-motors.com/en/discoveries/msstream/index.html" target="_blank"><span>M\'s Stream</span></a></li>';
navi += '<li><a href="http://www.mitsubishi-motors.com/wpmakerclassic/index_en.html" target="_blank" onclick="openWin(this.href,\'wallpaper\',\'980\',\'555\',\'1\');return false;" onkeypress="openWin(this.href,\'wallpaper\',\'980\',\'555\',\'1\');return false;"><span>Wallpaper Maker Classic</span></a></li>';
navi += '<li id="designchallange"><a href="/'+path+'/designchallange/index.html"><span>Design Challenge</span></a></li>';
navi += '</ul>';
navi += '</div></div>';

document.write(navi);

//DOMready.tgFunc(localnaviStay);
}
