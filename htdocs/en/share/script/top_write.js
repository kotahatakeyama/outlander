var top_tag = "";
var update_tag = "";
var release_tag = "";
var cnt = 0;
var cnt1 = 0;
var cnt2 = 0;
var func_rm=0;

$(function(){
	$.ajax({
		url: '/en/share/xml/global_en_message.xml',
		type:'get', 
		dataType:'xml',
		async:'true',
		cache :'false',
		success:global_en_message_xml

	});
	$.ajax({
		url: '/en/share/xml/global_en_release.xml',
		type:'get', 
		dataType:'xml',
		async:'true',
		cache :'false',
		success:global_en_release_xml
	});
});


function release_top(){
	var $day = $(this).find("day").text();
	var $url = $(this).find("url").text();
	var $title = $(this).find("title").text();

	if(cnt==0){
	release_tag += '<dt class="first">' + $day + '</dt><dd><a href="' + $url + '">' + $title + '</a></dd>';
	cnt=cnt+1;
	} else {
	release_tag += '<dt>' + $day + '</dt><dd><a href="' + $url + '">' + $title + '</a></dd>';
	}
}


function message_top(){
	var $toppage = $(this).find("toppage").text();
	var $day = $(this).find("day").text();
	var $url = $(this).find("url").text();
	var $title = $(this).find("title").text();
	var $target = $(this).find("target").text();

	if($toppage == "ON") {
		if(cnt1==0){
			top_tag += '<dt class="first">' + $day + '</dt>';
		} else {
			top_tag += '<dt>' + $day + '</dt>';
		}
		cnt1=cnt1+1;

		if($url == "no_URL") {
			top_tag += '<dd>' + $title + '</dd>';
		} else {
			if($target=="blank"){
				top_tag += '<dd><a href="' + $url + '" target="_blank">' + $title + '</a></dd>';
			} else {
				top_tag += '<dd><a href="' + $url + '">' + $title + '</a></dd>';
			}
		}
	}
}

function message_update(){
	var $day = $(this).find("day").text();
	var $url = $(this).find("url").text();
	var $title = $(this).find("title").text();
	var $target = $(this).find("target").text();

	update_tag += '<dt>' + $day + '</dt>';
	if($url == "no_URL") {
		update_tag += '<dd>' + $title + '</dd>';
	} else {
		if($target=="blank"){
			update_tag += '<dd><a href="' + $url + '" target="_blank">' + $title + '</a></dd>';
		} else {
			update_tag += '<dd><a href="' + $url + '">' + $title + '</a></dd>';
		}
	}
}


function global_en_release_xml(xml,status){
	if(status!='success')return;
	$(xml).find('item').each(release_top); 
	$('#top_release').html(release_tag); 
	cnt2 = cnt2 + 1;
	loading_image();
	scroll_func();
}


function global_en_message_xml(xml,status){
	if(status!='success')return;
	$(xml).find('item').each(message_top); 
	$('#top_message').html(top_tag); 
	$(xml).find('item').each(message_update); 
	$('#update_message').html(update_tag); 
	cnt2 = cnt2 + 1;
	loading_image();
	scroll_func();
}


function loading_image(){
	if(cnt2==2){
		$('.top_frames').css('height','300px'); 
		$('#top_message').css('display','block'); 
		$('#top_release').css('display','block'); 
		$('.image_loading').css('display','none');
	}
}


function scroll_func() {
func_rm+=1;
	if(func_rm==2){

		$('#image_loading').css('display', 'none');
		$('#pressRelease').fadeIn(500);
		scroll_add();
	}
}

function scroll_add()
	{
	$('.top_frames').jScrollPane({
		verticalDragMinHeight:15
	});
}