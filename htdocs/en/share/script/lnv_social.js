
var _localload = (function(){
						   
var navi;
var langageCode = 'en'
var categoryCode = 'social';

var path = langageCode +'/'+ categoryCode;

var navi = ['<div id="localNavigation"><div class="inner"><ul>'];
navi[navi.length] = '<li><a href="/'+path+'/index.html"><span>Sustainability</span></a></li>';
navi[navi.length] = '<li id="social_message"><a href="/'+path+'/top_message/"><span>Top Message</span></a></li>';

navi[navi.length] = '<li id="csr"><a href="/'+path+'/csr/index.html"><span>Corporate Social Responsibility</span></a>';
	
	navi[navi.length] = '<ul>';
	navi[navi.length] = '<li id="csr_csr_promotion"><a href="/'+path+'/csr/csr_promotion.html"><span>Approach to CSR Activities</span></a></li>';
	navi[navi.length] = '<li id="csr_cg"><a href="/'+langageCode+'/investors/governance/index.html"><span>Corporate Governance</span></a></li>';
	navi[navi.length] = '<li id="csr_system"><a href="/en/investors/governance/system.html"><span>Internal Control System</span></a></li>';
	navi[navi.length] = '<li id="csr_risk"><a href="/en/investors/governance/risk.html"><span>Risk Management</span></a></li>';
	navi[navi.length] = '</ul></li>';
/**/
navi[navi.length] = '<li id="environment"><a href="/'+path+'/environment/index.html"><span>Environmental Initiatives</span></a>';

navi[navi.length] = '<ul>';

navi[navi.length] = '<li id="environment_policy"><a href="/'+path+'/environment/policy/basicpolicy.html"><span>Policies</span></a>';

	navi[navi.length] = '<ul>';
	navi[navi.length] = '<li id="environment_policy_basicpolicy"><a href="/'+path+'/environment/policy/basicpolicy.html"><span>Environmental Policy</span></a></li>';
	navi[navi.length] = '<li id="environment_policy_vision2020"><a href="/'+path+'/environment/policy/vision2020.html"><span>Environmental Vision 2020</span></a></li>';
	navi[navi.length] = '<li id="environment_policy_plan2015"><a href="/'+path+'/environment/policy/plan2015.html"><span>Environment Initiative Program 2015</span></a></li>';
	navi[navi.length] = '<li id="environment_policy_society"><a href="/'+path+'/environment/policy/society.html"><span>Initiative for Preserving Biodiversity</span></a></li>';/**/
	navi[navi.length] = '</ul></li>';

navi[navi.length] = '<li id="environment_management"><a href="/'+path+'/environment/management/index.html"><span>Environmental Management</span></a>';

	navi[navi.length] = '<ul>';
	navi[navi.length] = '<li id="environment_management_index"><a href="/'+path+'/environment/management/index.html"><span>Environmental Organization</span></a></li>';
	navi[navi.length] = '<li id="environment_management_lca"><a href="/'+path+'/environment/management/lca.html"><span>Life Cycle Assessment (LCA)</span></a></li>';
	navi[navi.length] = '</ul></li>';

navi[navi.length] = '<li id="environment_product"><a href="/'+path+'/environment/product/index.html"><span>Products and Technologies</span></a>';

	navi[navi.length] = '<ul>';
	navi[navi.length] = '<li id="environment_product_index"><a href="/'+path+'/environment/product/index.html"><span>Reduction of on-road CO2 emission rate</span></a></li>';
	navi[navi.length] = '<li id="environment_product_electric"><a href="/'+path+'/environment/product/electric.html"><span>Development of Electric Vehicle Technology</span></a>';
		navi[navi.length] = '<ul>';
		navi[navi.length] = '<li id="environment_product_electric_electric_ev"><a href="/'+path+'/environment/product/electric_ev.html"><span>Electric Vehicles</span></a>';
		navi[navi.length] = '<li id="environment_product_electric_electric_phev"><a href="/'+path+'/environment/product/electric_phev.html"><span>Plug-in Hybrid Electric Vehicles</span></a>';
		navi[navi.length] = '</ul></li>';
	navi[navi.length] = '<li id="environment_product_mileage"><a href="/'+path+'/environment/product/mileage.html"><span>Development of Fuel Economy Improving Technologies</span></a>';
		navi[navi.length] = '<ul>';
		navi[navi.length] = '<li id="environment_product_mileage_mileage_engine"><a href="/'+path+'/environment/product/mileage_engine.html"><span>Engine Improvement</span></a>';
		navi[navi.length] = '<li id="environment_product_mileage_mileage_frame"><a href="/'+path+'/environment/product/mileage_frame.html"><span>Vehicle Body Improvement</span></a>';
		navi[navi.length] = '</ul></li>';
	navi[navi.length] = '<li id="environment_product_emission"><a href="/'+path+'/environment/product/emission.html"><span>Purifying Exhaust Gas while Driving</span></a></li>';
	navi[navi.length] = '<li id="environment_product_voc"><a href="/'+path+'/environment/product/voc.html"><span>Reduction of In-cabin VOC</span></a></li>';
	navi[navi.length] = '<li id="environment_product_recycle"><a href="/'+path+'/environment/product/recycle.html"><span>Recycling Initiatives</span></a>';
		navi[navi.length] = '<ul>';
		navi[navi.length] = '<li id="environment_product_recycle_recycle_development"><a href="/'+path+'/environment/product/recycle_development.html"><span>Recycling-based Design and Development</span></a>';
		navi[navi.length] = '</ul></li>';
	navi[navi.length] = '<li id="environment_product_environmental_burden"><a href="/'+path+'/environment/product/environmental_burden.html"><span>Reduction of Hazardous Substances</span></a></li>';
	navi[navi.length] = '</ul></li>';

navi[navi.length] = '<li id="environment_activity"><a href="/'+path+'/environment/activity/index.html"><span>Business Activities</span></a>';

	navi[navi.length] = '<ul>';
	navi[navi.length] = '<li id="environment_activity_index"><a href="/'+path+'/environment/activity/index.html"><span>Efforts in Production</span></a>';
		navi[navi.length] = '<ul>';
		navi[navi.length] = '<li id="environment_activity_index_factory_co2"><a href="/'+path+'/environment/activity/factory_co2.html"><span>Reducing CO2 Emission</span></a>';
		navi[navi.length] = '<li id="environment_activity_index_factory_airpollution"><a href="/'+path+'/environment/activity/factory_airpollution.html"><span>Preventing Air Pollution</span></a>';
		navi[navi.length] = '<li id="environment_activity_index_factory_prtr"><a href="/'+path+'/environment/activity/factory_prtr.html"><span>Management of Chemical Substances</span></a>';
		navi[navi.length] = '<li id="environment_activity_index_factory_soilpollution"><a href="/'+path+'/environment/activity/factory_soilpollution.html"><span>Preventing Soil and Water Pollution</span></a>';
		navi[navi.length] = '<li id="environment_activity_index_factory_resource"><a href="/'+path+'/environment/activity/factory_resource.html"><span>Promoting Effective Use of Resources</span></a>';
		navi[navi.length] = '</ul></li>';

	navi[navi.length] = '<li id="environment_activity_supplier"><a href="/'+path+'/environment/activity/supplier.html"><span>Collaborative Efforts with Suppliers</span></a></li>';

	navi[navi.length] = '</ul></li>';
	
navi[navi.length] = '</ul></li>';

navi[navi.length] = '<li id="approach"><a href="/'+path+'/pdf/2015e_01.pdf" target="_blank"><span>Social Initiatives</span></a></li>';
navi[navi.length] = '<li id="contribution"><a href="/'+path+'/citizenship/index.html"><span>Corporate Citizenship</span></a></li>';
navi[navi.length] = '<li id="kids"><a href="/en/discoveries/kids/index.html"><span>Kids&#039; Car Museum</span></a></li>';
navi[navi.length] = '<li id="social_report"><a href="/'+path+'/report/index.html"><span>CSR Report</span></a></li>';

navi[navi.length] = '</ul>';
navi[navi.length] = '</div></div>';

return navi.join('');
})();


function localNavi_write(){

document.write(_localload);

//DOMready.tgFunc(localnaviStay);
}

