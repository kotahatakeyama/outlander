
//var nv_root_path="http://www.mitsubishi-motors.com";


var globalnvList = new Array();
var lang = '/en';

/* Product Information */
globalnvList[0] = [["<a href='"+lang+"/products/index.html'><span>Product Information</span></a>"]];

/* Mitsubishi Motors Spirit */
globalnvList[1] = [
				["<a href='"+lang+"/spirit/communication_word/index.html'><span>Corporate Tagline / Drive@earth</span></a>"],
				["<a href='"+lang+"/spirit/earth_technology/index.html'><span>Our Automobile Manufacturing Ideals / @earth TECHNOLOGY</span></a>"],
				["<a href='"+lang+"/products/index.html'><span>Product Information</span></a>"],
				["<a href='"+lang+"/spirit/technology/library/index.html'><span>Automobile Technology</span></a>"],
				["<a href='"+lang+"/spirit/design/index.html' target='_blank'><span>Design</span></a>"],
				["<a href='"+lang+"/spirit/history/index.html'><span>History of Product</span></a>"]
				];

/* Sustainability */
globalnvList[2] = [
				["<a href='"+lang+"/social/top_message/'><span>Top Message</span></a>"],
				["<a href='"+lang+"/social/csr/index.html'><span>Corporate Social Responsibility</span></a>"],
				["<a href='"+lang+"/social/environment/index.html'><span>Environmental Initiatives</span></a>"],
				["<a href='"+lang+"/social/pdf/2015e_01.pdf' target='_blank'><span>Social Initiatives</span></a>"],
				["<a href='"+lang+"/social/citizenship/index.html'><span>Corporate Citizenship</span></a>"]
				];


/* Company ･ Investors */
globalnvList[3] = [
				["<a href='"+lang+"/corporate/philosophy/index.html'><span>Approach to Management</span></a>"],
				["<a href='"+lang+"/corporate/aboutus/index.html'><span>Overview of Mitsubishi Motors</span></a>"],
				["<a href='/en/investors/index.html'><span>Information for our Shareholders and Investors</span></a>"]
];

/* Events */
globalnvList[4] = [
				["<a href='/publish/pressrelease_en/index.html'><span>Press Release</span></a>"],
				["<a href='"+lang+"/events/motorshow/index.html'><span>Motor Shows</span></a>"],
				["<a href='/special/motorsports/' target='_blank'><span>Motor Sports</span></a>"],
				["<a href='/en/events/baja/2015/index.html' target='_blank'><span>Baja Portalegre 500</span></a>"],
				["<a href='/en/events/ppihc/2014/index.html' target='_blank'><span>Pikes Peak Challenge</span></a>"],
				["<a href='/en/events/star/index.html' target='_blank'><span>Starry Sky Project</span></a>"],
				["<a href='"+lang+"/events/designchallange/index.html'><span>Internal Design Event</span></a>"],
				["<a href='/en/events/msstream/index.html' target='_blank'><span>M's Stream</span></a>"],
				["<a href='/wallpapermaker/index_en.html' target='_blank' onclick='openWin(this.href,\"wallpaper\",\"980\",\"555\",\"1\");return false;' onkeypress='openWin(this.href,\"wallpaper\",\"980\",\"555\",\"1\");return false;'><span>Wallpaper Maker</span></a>"],
				["<a href='/wpmakerclassic/index_en.html' target='_blank' onclick='openWin(this.href,\"wallpaper\",\"980\",\"555\",\"1\");return false;'><span>Wallpaper Maker Classic</span></a>"]
];

var firston = true;
var timerb;
var time_sec = 1;
var timestop = false;
var previd = '';
var gnvpa = '';

function floatmenu(){
	var doc = document;
	var basediv = doc.createElement('div');
	basediv.id = "flbase";
	
	var innerdiv = doc.createElement('div');
	innerdiv.id = "flinnerdiv";
	
	var nvimg = module.$('mainnv').getElementsByTagName('img');
	
	for(var i=0;i<nvimg.length;i++){
		
		addListener(nvimg[i], 'mouseover', bkchg);
		addListener(nvimg[i], 'mouseout', bkchg2);
		
		var divbk = doc.createElement('div');
		divbk.id= nvimg[i].id + '_fl';
	
		var ul = doc.createElement('ul');
		ul.className = 'linkStd';
		
		for(var j=0;j<globalnvList[i].length;j++){
			var li = doc.createElement('li');
			li.innerHTML = globalnvList[i][j];
			ul.appendChild(li);
		}
		
		divbk.appendChild(ul);
		innerdiv.appendChild(divbk);
		basediv.appendChild(innerdiv);
		
		//addListener(ul, 'mouseover', bkchgb_ul);
		addListener(divbk, 'mouseover', bkchgb);
		addListener(divbk, 'mouseout', bkchgb2);
	}
	module.$('globalNavigation').appendChild(basediv);
	
	var domElm = innerdiv.getElementsByTagName('div')
		gnvpa = domElm;

	var ddlng = domElm.length;
	var zz = 0;
	for(var k=0;k<ddlng;++k){
		if(zz <= domElm[k].offsetHeight){
		zz = domElm[k].offsetHeight;
		}
	}
	
	for(var k=0;k<ddlng;++k){
			var ddst = domElm[k].style;
			if(ie && !IE_newer) ddst.height = zz-10 + 'px';
			else ddst.minHeight = (zz +1)-10 + 'px';
	}
	
	if(module.Browser('IE',6)) DD_belatedPNG.fix('#flbase div');

	//basediv.style.display = 'none';
	//innerdiv.style.visibility = "visible";
	innerdiv.style.visibility = 'hidden';
	innerdiv.style.overflow = 'auto';
	innerdiv.style.height = '0';
}

function bkchg(e){
	module.Event.stopbubble(e);
	//module.$('flbase').style.display = 'block';
	module.$('flinnerdiv').style.visibility = 'visible';
	module.$('flinnerdiv').style.overflow = 'auto';
	module.$('flinnerdiv').style.height = 'auto';
	
	setTimeout(function(){
	var ddlng = gnvpa.length;
	var zz = 0;
	for(var k=0;k<ddlng;++k){
		if(zz <= gnvpa[k].offsetHeight){
		zz = gnvpa[k].offsetHeight;
		}
	}
		
	for(var k=0;k<ddlng;++k){
			var ddst = gnvpa[k].style;
			if(ie && !IE_newer) ddst.height = zz-10 + 'px';
			else ddst.minHeight = (zz)-10 + 'px';
	}
						},30);

	if(previd != ''){
		if(previd.indexOf('_fl') != -1) module.$(previd).style.backgroundPosition = '-203px 0';
		else module.$(previd+'_fl').style.backgroundPosition = '-203px 0';
	}
	timestop = true;
	clearInterval(timerb);
	if(this.id != 'gnv_home') module.$(this.id+'_fl').style.backgroundPosition = '0 0';
	previd=this.id;
}
function bkchg2(e){
	module.Event.stopbubble(e);
	settime();
	//module.$(this.id+'_fl').style.backgroundPosition = '0 0';
}
function bkchgb(e){
	if(previd != '') module.$(previd).style.backgroundPosition = '-203px 0';
	module.Event.stopbubble(e);
	timestop = true;
	clearInterval(timerb);
	if(this.id != 'gnv_home_fl') this.style.backgroundPosition = '0 0';
	previd=this.id;
}
function bkchgb2(e){
	module.Event.stopbubble(e);
	settime();
	//this.style.backgroundPosition = '0 0';
}

function bkchgb_ul(e){
	module.Event.stopbubble(e);
	timestop = true;
	clearInterval(timerb);
	this.parentNode.style.backgroundPosition = '-203px 0';
}


function outcheck(){
	time_sec --;
	if(time_sec <=0){
		clearInterval(timerb);
		if(!timestop){
		lyclear();
		}
	}
}

function settime(){
	if(!timestop) return;
	time_sec = 1;
	timestop = false;
	timerb = setInterval(outcheck,300);	
}

function lyclear(){
	
	var div = module.$('flinnerdiv').getElementsByTagName('div');
	
	for(var i=0;i<div.length;i++){
		div[i].style.backgroundPosition = '-203px 0';
		if(ie && !IE_newer) div[i].style.height = 'auto';
		else div[i].style.minHeight = '0';
		
	}
	module.$('flinnerdiv').style.visibility = 'hidden';
	module.$('flinnerdiv').style.overflow = 'auto';
	module.$('flinnerdiv').style.height = '0';
	//module.$('flbase').style.display = 'none';
}
addListener(window, 'load', floatmenu);
//module.DOMready.tgFunc(floatmenu);


