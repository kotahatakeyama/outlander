
var _localload = (function(){
						   
var navi;
var langageCode = 'en'
var categoryCode = 'corporate';

var path = langageCode +'/'+ categoryCode;

var navi = ['<div id="localNavigation"><div class="inner"><ul>'];
navi[navi.length] = '<li><a href="/'+path+'/index.html"><span>Company･Investors</span></a></li>';
navi[navi.length] = '<li id="philosophy"><a href="/'+path+'/philosophy/index.html"><span>Our Approach to Management </span></a>';

	navi[navi.length] = '<ul>';
	navi[navi.length] = '<li id="philosophy_principle"><a href="/'+path+'/philosophy/principle.html"><span>The Three Principles of Mitsubishi Group</span></a></li>';
	navi[navi.length] = '<li id="philosophy_philosophy"><a href="/'+path+'/philosophy/philosophy.html"><span>Corporate Philosophy</span></a></li>';
	navi[navi.length] = '<li><a href="/'+langageCode+'/spirit/communication_word/index.html"><span>Drive@earth</span></a></li>';
	navi[navi.length] = '</ul></li>';

navi[navi.length] = '<li id="aboutus"><a href="/'+path+'/aboutus/index.html"><span>Overview of Mitsubishi Motors</span></a>';

	navi[navi.length] = '<ul>';
	navi[navi.length] = '<li id="aboutus_profile"><a href="/'+path+'/aboutus/profile/index.html"><span>Corporate Profile</span></a>';
	
		navi[navi.length] = '<ul>';
		navi[navi.length] = '<li id="aboutus_profile_baselist"><a href="/'+path+'/aboutus/profile/world.html"><span>A List of Bases</span></a></li>';
		navi[navi.length] = '</ul></li>';
	
	navi[navi.length] = '<li id="aboutus_director"><a href="/'+langageCode+'/investors/governance/director.html"><span>Members of the Board</span></a></li>';
	navi[navi.length] = '<li id="aboutus_facilities"><a href="/'+path+'/aboutus/facilities/index.html"><span>Manufacturing Centers and Related Facilities</span></a>';
	
		navi[navi.length] = '<ul>';
		navi[navi.length] = '<li id="aboutus_facilities_honsya"><a href="/'+path+'/aboutus/facilities/guide_map/honsya.html"><span>Head Office</span></a></li>';
		navi[navi.length] = '</ul></li>';
		
	navi[navi.length] = '<li id="aboutus_subsidiary"><a href="/'+path+'/aboutus/subsidiary.html"><span>Major Affiliates</span></a></li>';
	navi[navi.length] = '<li id="aboutus_history"><a href="/'+path+'/aboutus/history/1870/index.html"><span>History of Mitsubishi Motors</span></a></li>';


	navi[navi.length] = '</ul></li>';




navi[navi.length] = '<li id="ir"><a href="/en/investors/index.html"><span>Information for our Shareholders and Investors</span></a>';

	navi[navi.length] = '<ul>';

	
	navi[navi.length] = '<li id="ir_corpmanage"><a href="/en/investors/corpmanage/index.html"><span>Corporate and Management Information</span></a>';
	
		navi[navi.length] = '<ul>';
		navi[navi.length] = '<li id="ir_corpmanage_topmessage"><a href="/en/investors/corpmanage/topmessage.html"><span>Top Message</span></a></li>';
		navi[navi.length] = '<li id="ir_corpmanage_mp"><a href="/en/investors/individual/mp.html"><span>Mid-Term Business Plan</span></a></li>';
		navi[navi.length] = '<li id="ir_corpmanage_guideline"><a href="/en/investors/corpmanage/guideline.html"><span>Policy on Timely Disclosure of Material Information</span></a></li>';
		navi[navi.length] = '<li id="ir_corpmanage_risk"><a href="/en/investors/corpmanage/risk.html"><span>Business-Related Risks</span></a></li>';
		navi[navi.length] = '<li><a href="/'+path+'/philosophy/index.html"><span>Three Principles and MMC\'s Corporate Philosophy</span></a></li>';
		navi[navi.length] = '<li><a href="/'+path+'/aboutus/profile/index.html"><span>Corporate Profile</span></a></li>';
		navi[navi.length] = '</ul></li>';
		

	navi[navi.length] = '<li id="ir_governance"><a href="/en/investors/governance/index.html"><span>Corporate Governance</span></a>';
	
		navi[navi.length] = '<ul>';
		navi[navi.length] = '<li id="ir_governance_policy"><a href="/en/investors/governance/policy.html"><span>Basic Policy and Framework</span></a></li>';
		navi[navi.length] = '<li id="ir_governance_system"><a href="/en/investors/governance/system.html"><span>Internal Control Systems</span></a></li>';
		navi[navi.length] = '<li id="ir_governance_risk"><a href="/en/investors/governance/risk.html"><span>Risk Management</span></a></li>';
		navi[navi.length] = '<li id="ir_governance_director"><a href="/en/investors/governance/director.html"><span>Members of the Board</span></a></li>';
		navi[navi.length] = '<li id="ir_governance_compensation"><a href="/en/investors/governance/compensation.html"><span>Remuneration of Director and Auditor</span></a></li>';
		navi[navi.length] = '<li id="ir_governance_antisocial"><a href="/en/investors/governance/antisocial.html"><span>Basic Policy against Anti-Social Forces</span></a></li>';
		navi[navi.length] = '<li id="ir_governance_corporate_governance_report"><a href="/en/social/csr/pdf/governance_2015.pdf" target="_blank"><span>Corporate Governance Report</span></a></li>';
		navi[navi.length] = '</ul></li>';

	
	navi[navi.length] = '<li id="ir_financial"><a href="/en/investors/finance_result/index.html"><span>Business Performance and Financial Information</span></a>';
	
		navi[navi.length] = '<ul>';
		navi[navi.length] = '<li id="ir_financial_highlight"><a href="/en/investors/finance_result/highlight.html"><span>Business Results Report for Current Term</span></a></li>';
		navi[navi.length] = '<li id="ir_financial_indices"><a href="/en/investors/finance_result/indices.html"><span>Business Performance and Financial Information</span></a></li>';
		navi[navi.length] = '<li id="ir_financial_data"><a href="/en/investors/finance_result/data.html"><span>Financial Statements</span></a></li>';
		navi[navi.length] = '<li id="ir_financial_segment"><a href="/en/investors/finance_result/segment.html"><span>Information by Region</span></a></li>';
		navi[navi.length] = '<li id="ir_financial_result"><a href="/en/investors/finance_result/result.html"><span>Production, Sales and Export Data</span></a></li>';
		navi[navi.length] = '</ul></li>';

	
	navi[navi.length] = '<li id="ir_library"><a href="/en/investors/library/index.html"><span>IR Library</span></a>';
	
		navi[navi.length] = '<ul>';
		navi[navi.length] = '<li id="ir_library_earning"><a href="/en/investors/library/earning.html"><span>Financial Results Information</span></a></li>';
		navi[navi.length] = '<li id="ir_library_anual"><a href="/en/investors/library/anual.html"><span>Annual Report</span></a></li>';
		navi[navi.length] = '<li id="ir_library_fact"><a href="/en/investors/library/fact.html"><span>Facts and Figures</span></a></li>';
		navi[navi.length] = '<li><a href="/en/social/report/index.html"><span>CSR Report</span></a></li>';
		navi[navi.length] = '</ul></li>';
		
		
	navi[navi.length] = '<li id="ir_event"><a href="/en/investors/event/index.html"><span>IR Events</span></a>';
	
		navi[navi.length] = '<ul>';
		navi[navi.length] = '<li id="ir_event_calendar"><a href="/en/investors/event/calendar.html"><span>IR Calendar</span></a></li>';
		navi[navi.length] = '<li id="ir_event_presentation"><a href="/en/investors/library/earning.html"><span>Financial Results Meetings</span></a></li>';
		navi[navi.length] = '<li id="ir_event_plan"><a href="/en/investors/event/plan.html"><span>Mid-Term Business Plan Meetings</span></a></li>';
		navi[navi.length] = '<li id="ir_event_others"><a href="/en/investors/event/others.html"><span>Information Meetings for Business Initiatives</span></a></li>';
		navi[navi.length] = '<li id="ir_event_meeting"><a href="/en/investors/event/meeting.html"><span>General Shareholders&rsquo; Meetings</span></a></li>';
		navi[navi.length] = '</ul></li>';


	navi[navi.length] = '<li id="ir_stockinfo"><a href="/en/investors/stockinfo/index.html"><span>Stock and Corporate Bond Information</span></a>';
	
		navi[navi.length] = '<ul>';
		navi[navi.length] = '<li id="ir_stockinfo_stock_status"><a href="/en/investors/stockinfo/stock_status.html"><span>Stock Status</span></a></li>';
		navi[navi.length] = '<li id="ir_stockinfo_overview"><a href="/en/investors/stockinfo/overview.html"><span>Shareholder Composition</span></a></li>';
		navi[navi.length] = '<li id="ir_stockinfo_rating"><a href="/en/investors/stockinfo/rating.html"><span>Credit Ratings and Bonds Information</span></a></li>';
		navi[navi.length] = '<li id="ir_stockinfo_analyst"><a href="/en/investors/stockinfo/analyst.html"><span>Analyst Coverage</span></a></li>';
		navi[navi.length] = '<li id="ir_stockinfo_procedure"><a href="/en/investors/stockinfo/procedure.html"><span>Stock Procedure Information</span></a></li>';
		navi[navi.length] = '<li id="ir_stockinfo_return"><a href="/en/investors/stockinfo/return.html"><span>Shareholder Return</span></a></li>';
		navi[navi.length] = '<li id="ir_stockinfo_articles_of_incorporation"><a href="/en/investors/stockinfo/pdf/articles_of_incorporation.pdf" target="_blank"><span>Articles of Incorporation</span></a></li>';
		navi[navi.length] = '</ul></li>';
		
		
	navi[navi.length] = '<li id="ir_individual"><a href="/en/investors/individual/index.html"><span>Mitsubishi Motors at a Glance</span></a>';
	
		navi[navi.length] = '<ul>';
		navi[navi.length] = '<li id="ir_individual_mp"><a href="/en/investors/individual/mp.html"><span>Mid-Term Business Plan</span></a></li>';
		navi[navi.length] = '<li id="ir_individual_about"><a href="/en/investors/individual/about.html"><span>Overview of Mitsubishi Motors</span></a></li>';
		navi[navi.length] = '<li id="ir_individual_return"><a href="/en/investors/stockinfo/return.html"><span>Shareholder Return</span></a></li>';
		navi[navi.length] = '<li id="ir_individual_relation"><a href="/en/investors/individual/relation.html"><span>Related Contents</span></a></li>';
		navi[navi.length] = '</ul></li>';
	

	navi[navi.length] = '</ul></li>';


	

navi[navi.length] = '</ul>';
navi[navi.length] = '</div></div>';

return navi.join('');
})();


function localNavi_write(){

document.write(_localload);

//DOMready.tgFunc(localnaviStay);
}

